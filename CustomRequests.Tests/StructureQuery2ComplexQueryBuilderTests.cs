using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Estat.Sdmxsource.Extension.Builder;
using NUnit.Framework;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference;

namespace CustomRequests.Tests
{
    [TestFixture]
    class StructureQuery2ComplexQueryBuilderTests
    {
        [TestCase("conceptscheme/ESTAT/CENSUSHUB_CONCEPTS/1.0")]
        public void TestWithoutFilter(string restQuery)
        {
            var restStructureQueryCore = new RESTStructureQueryCore(restQuery);
            var complexStructureQuery = new StructureQuery2ComplexQueryBuilder().Build(restStructureQueryCore);
            Assert.False(complexStructureQuery.StructureReference.ChildReference.Any());
        }


        [TestCase("conceptscheme/ESTAT/CENSUSHUB_CONCEPTS/1.0/TOPIC+HC+SOME")]
        public void TestWithFilter(string restQuery)
        {
            var restStructureQueryCore = new RESTStructureQueryCore(restQuery);
            var complexStructureQuery = new StructureQuery2ComplexQueryBuilder().Build(restStructureQueryCore);
            Assert.True(complexStructureQuery.StructureReference.ChildReference.ToList().Exists(item=>item.Id.SearchParameter == "TOPIC"));
            Assert.True(complexStructureQuery.StructureReference.ChildReference.ToList().Exists(item=>item.Id.SearchParameter == "HC"));
            Assert.True(complexStructureQuery.StructureReference.ChildReference.ToList().Exists(item=>item.Id.SearchParameter == "SOME"));
        }
    }
}
