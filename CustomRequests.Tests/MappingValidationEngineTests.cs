using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CustomRequests.Tests
{
    using System.IO;

    using Estat.Sdmxsource.Extension.Engine;
    using Estat.Sdmxsource.Extension.Model;

    using NUnit.Framework;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Engine;
    using Org.Sdmxsource.Sdmx.Api.Manager.Parse;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Header;
    using Org.Sdmxsource.Sdmx.Structureparser.Manager.Parsing;
    using Org.Sdmxsource.Sdmx.StructureRetrieval.Manager;
    using Org.Sdmxsource.Sdmx.Util.Objects.Container;
    using Org.Sdmxsource.Util.Io;

    class MappingValidationEngineTests
    {
        [Test]
        public void TestCodelistErrors()
        {
            IStructureParsingManager manager = new StructureParsingManager();
            IDataStructureObject dataStructure;
            ISdmxObjects objects = new SdmxObjectsImpl();
            using (var readable = new FileReadableDataLocation("tests/V21/Structure/test-sdmxv2.1-ESTAT+STS+2.0.xml"))
            {
                var structureWorkspace = manager.ParseStructures(readable);
                var structureObjects = structureWorkspace.GetStructureObjects(false);
                dataStructure = structureObjects.DataStructures.First();
                var codelists = structureObjects.Codelists;
                objects.AddIdentifiables(codelists);
            }

            
            // initialize the data writing engine. It can be for SDMX versions 2.0 or 2.1
            IDataWriterEngine dataWriterEngine = new MappingValidationWriterEngine(new MemoryStream(), new InMemoryRetrievalManager(objects),SdmxSchema.GetFromEnum(SdmxSchemaEnumType.VersionTwoPointOne));

            // write header

            // start dataset
            dataWriterEngine.StartDataset(
                null, dataStructure,
                new DatasetHeaderCore(
                    null, DatasetAction.GetFromEnum(DatasetActionEnumType.Information),
                    new DatasetStructureReferenceCore(
                        null,
                        dataStructure.AsReference,
                        null,
                        null,
                        null)));

            // write dataset attributes
            dataWriterEngine.WriteAttributeValue("FREQ", "A");
            dataWriterEngine.WriteAttributeValue("FREQ", "X");
            dataWriterEngine.StartGroup("SIBLING");
            dataWriterEngine.WriteGroupKeyValue("FREQ", "Y");
            dataWriterEngine.StartSeries();
            dataWriterEngine.WriteObservation("FREQ", "Z", "primaryMeasure", null);
            dataWriterEngine.WriteObservation("TEST", "X","primaryMeasure",null);
            dataWriterEngine.Close(null);
        }

        [Test]
        public void TestDuplicateObservations()
        {
            IStructureParsingManager manager = new StructureParsingManager();
            IDataStructureObject dataStructure;
            ISdmxObjects objects = new SdmxObjectsImpl();
            using (var readable = new FileReadableDataLocation("tests/V21/Structure/test-sdmxv2.1-ESTAT+STS+2.0.xml"))
            {
                var structureWorkspace = manager.ParseStructures(readable);
                var structureObjects = structureWorkspace.GetStructureObjects(false);
                dataStructure = structureObjects.DataStructures.First();
                var codelists = structureObjects.Codelists;
                objects.AddIdentifiables(codelists);
            }


            var writer = new MappingValidationWriterEngine(new MemoryStream(), new InMemoryRetrievalManager(objects), SdmxSchema.GetFromEnum(SdmxSchemaEnumType.VersionTwoPointOne));

            writer.StartDataset(
                null, dataStructure,
                new DatasetHeaderCore(
                    null, DatasetAction.GetFromEnum(DatasetActionEnumType.Information),
                    new DatasetStructureReferenceCore(
                        null,
                        dataStructure.AsReference,
                        null,
                        null,
                        null)));

            writer.StartSeries();
            writer.WriteSeriesKeyValue("D1", "Z");
            writer.WriteSeriesKeyValue("D2", "Y");
            writer.WriteSeriesKeyValue("D3", "X");

            writer.WriteObservation("TIME_PERIOD", "2001", "1");

            // duplicate Z:Y:X:2001 already writen
            writer.WriteObservation("TIME_PERIOD", "2001", "2");

            writer.StartSeries();
            writer.WriteSeriesKeyValue("D1", "Z");
            writer.WriteSeriesKeyValue("D2", "Y");
            writer.WriteSeriesKeyValue("D3", "X");
            // duplicate Z:Y:X:2001 already writen
            writer.WriteObservation("TIME_PERIOD", "2001", "1");

            // not duplicate Z:Y:Z:2001 not written
            writer.StartSeries();
            writer.WriteSeriesKeyValue("D1", "Z");
            writer.WriteSeriesKeyValue("D2", "Y");
            writer.WriteSeriesKeyValue("D3", "Z");

            writer.WriteObservation("TIME_PERIOD", "2001", "1");
        }


        [Test]
        public void TestValidation()
        {
            IStructureParsingManager manager = new StructureParsingManager();
            IDataStructureObject dataStructure;
            ISdmxObjects objects = new SdmxObjectsImpl();
            using (var readable = new FileReadableDataLocation("tests/V21/Structure/test-sdmxv2.1-ESTAT+STS+2.0.xml"))
            {
                var structureWorkspace = manager.ParseStructures(readable);
                var structureObjects = structureWorkspace.GetStructureObjects(false);
                dataStructure = structureObjects.DataStructures.First();
                var codelists = structureObjects.Codelists;
                objects.AddIdentifiables(codelists);
            }

            var memoryStream = new MemoryStream();
            var writer = new MappingValidationWriterEngine(memoryStream, new InMemoryRetrievalManager(objects), SdmxSchema.GetFromEnum(SdmxSchemaEnumType.VersionTwoPointOne));

            writer.StartDataset(
                null, dataStructure,
                new DatasetHeaderCore(
                    null, DatasetAction.GetFromEnum(DatasetActionEnumType.Information),
                    new DatasetStructureReferenceCore(
                        null,
                        dataStructure.AsReference,
                        null,
                        null,
                        null)));

            writer.StartSeries();
            writer.WriteSeriesKeyValue("FREQ", "M");
            writer.WriteSeriesKeyValue("ADJUSTMENT", "C");
            writer.WriteSeriesKeyValue("STS_ACTIVITY", "NS0040");
            writer.WriteSeriesKeyValue("STS_INSTITUTION", "1");
            writer.WriteSeriesKeyValue("STS_BASE_YEAR", "1");
            writer.WriteAttributeValue("TIME_FORMAT", "P1M");

            writer.WriteObservation("TIME_PERIOD", "2005-01", "1.1");
            writer.WriteAttributeValue("OBS_CONF", "F"); 
            writer.WriteAttributeValue("OBS_STATUS", "E");

            writer.WriteObservation("TIME_PERIOD", "2005-02", "1.1");
            writer.WriteAttributeValue("OBS_CONF", "F");
            writer.WriteAttributeValue("OBS_STATUS", "E");


            writer.WriteObservation("TIME_PERIOD", "2005-03", "1.1");
            writer.WriteAttributeValue("OBS_CONF", "F");
            writer.WriteAttributeValue("OBS_STATUS", "E");

            writer.WriteObservation("TIME_PERIOD", "2005-01", "1.1");
            writer.WriteAttributeValue("OBS_CONF", "F");
            writer.WriteAttributeValue("OBS_STATUS", "E");

            writer.StartSeries();
            writer.WriteSeriesKeyValue("FREQ", "M");
            writer.WriteSeriesKeyValue("ADJUSTMENT", "C");
            writer.WriteSeriesKeyValue("STS_ACTIVITY", "NS0040");
            writer.WriteSeriesKeyValue("STS_INSTITUTION", "1");
            writer.WriteSeriesKeyValue("STS_BASE_YEAR", "1");
            writer.WriteAttributeValue("TIME_FORMAT", "P1M");


            writer.WriteObservation("TIME_PERIOD", "2007-03", "1.1");
            writer.WriteAttributeValue("OBS_CONF", "F");
            writer.WriteAttributeValue("OBS_STATUS", "E");

            writer.WriteObservation("TIME_PERIOD", "2007-03", "1.1");
            writer.WriteAttributeValue("OBS_CONF", "F");
            writer.WriteAttributeValue("OBS_STATUS", "E");


            writer.StartSeries();
            writer.WriteSeriesKeyValue("FREQ", "X");
            writer.WriteSeriesKeyValue("ADJUSTMENT", "C");
            writer.WriteSeriesKeyValue("STS_ACTIVITY", "NS0040");
            writer.WriteSeriesKeyValue("STS_INSTITUTION", "1");
            writer.WriteSeriesKeyValue("STS_BASE_YEAR", "1");
            writer.WriteAttributeValue("TIME_FORMAT", "P1M");

            writer.WriteObservation("TIME_PERIOD", "2005-03", "1.1");
            writer.WriteAttributeValue("OBS_CONF", "F");
            writer.WriteAttributeValue("OBS_STATUS", "E");


            writer.WriteObservation("TIME_PERIOD", "2013-15", "1.1");
            writer.WriteObservation("TIME_PERIOD", "2013-02", "1.1");
            writer.WriteObservation("TIME_PERIOD", "2013-M02", "1.1");

            writer.Close();
            memoryStream.Seek(0, SeekOrigin.Begin);
            var actualMessage = new StreamReader(memoryStream).ReadToEnd();
            var expectedMessage = "{\"CodelistErrors\":{\"STS_BASE_YEAR\":[\"1\"],\"FREQ\":[\"X\"]},\"DuplicateObservations\":[\"M:C:NS0040:1:1:2005-01\",\"M:C:NS0040:1:1:2007-03\"],\"TimePeriodErrors\":[\"2013-15\"],\"TextFormatErrors\":[]}";
            Assert.That(actualMessage,Is.EqualTo(expectedMessage));
        }
    }
}
