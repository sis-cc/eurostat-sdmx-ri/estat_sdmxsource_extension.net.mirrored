// -----------------------------------------------------------------------
// <copyright file="CrossToCsvTransformTest.cs" company="EUROSTAT">
//   Date Created : 2018-1-10
//   Copyright (c) 2009, 2018 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using NUnit.Framework;

namespace CustomRequests.Tests
{
    using System.IO;
    using System.Linq;
    using System.Text;

    using Estat.Sdmxsource.Extension.Factory;
    using Estat.Sdmxsource.Extension.Model;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Manager.Parse;
    using Org.Sdmxsource.Sdmx.DataParser.Manager;
    using Org.Sdmxsource.Sdmx.DataParser.Transform;
    using Org.Sdmxsource.Sdmx.Structureparser.Manager.Parsing;
    using Org.Sdmxsource.Sdmx.Util.Extension;
    using Org.Sdmxsource.Util.Io;

    [TestFixture]
    public class CrossToCsvTransformTest
    {
        private readonly IStructureParsingManager _parsingManager = new StructureParsingManager();

        [TestCase("tests/V20/ESTAT+DUMMY_CENSUS_DATAFLOW1+2.0_2018_01_10_16_37_35.ESTAT+CENSUSHUB+1.1_partial_structure.xml", "tests/V20/ESTAT+DUMMY_CENSUS_DATAFLOW1+2.0_2018_01_10_16_27_00.dataset.xml", 9)]
        public void ShouldTransformExceptedRows(string structurePath, string dataPath, int expectedRows)
        {
            var structureFile = new FileInfo(structurePath);
            var dataFile = new FileInfo(dataPath);
            var outputFile = new FileInfo(dataFile.Name + ".csv");
            var sdmxObjects = structureFile.GetSdmxObjects(this._parsingManager);
            var dsd = sdmxObjects.DataStructures.First();
            
            var dataParseManager = new DataParseManager(new DataReaderManager(), new DataReaderWriterTransform(),
                new DataWriterManager(new CsvDataWriterFactory()), this._parsingManager, new WriteableDataLocationFactory());

            using (var sourceData = new FileReadableDataLocation(dataPath))
            using (var ouputStream = outputFile.Create())
            {
                var csvDataFormat = new CsvDataFormat(
                    DataType.GetFromEnum(DataEnumType.Csv),
                    new CsvWriterOptions()
                    {
                        FieldSeparator = ";"
                    },
                    new UTF8Encoding(false));
                dataParseManager.PerformTransform(
                    sourceData,
                    ouputStream,
                    csvDataFormat,
                    dsd,
                    null);
            }

            int rowCount = 0;
            using (var reader = outputFile.OpenText())
            {
                reader.ReadLine(); // the header
                while (reader.ReadLine() != null)
                {
                    rowCount++;
                }
            }

            Assert.That(rowCount, Is.EqualTo(expectedRows));
        }
    }
}