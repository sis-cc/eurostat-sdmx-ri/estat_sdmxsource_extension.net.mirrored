// -----------------------------------------------------------------------
// <copyright file="TestSdmxDateExtensions.cs" company="EUROSTAT">
//   Date Created : 2013-10-02
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace CustomRequests.Tests
{
    using System;
    using System.Collections;

    using Estat.Sdmxsource.Extension.Extension;
    using Estat.Sdmxsource.Extension.Model;

    using NUnit.Framework;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Complex;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Util.Date;

    using TimeRangeCore = Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference.Complex.TimeRangeCore;

    /// <summary>
    /// Test unit for <see cref="SdmxDateExtensions"/>
    /// </summary>
    [TestFixture]
    public class TestSdmxDateExtensions
    {
        /// <summary>
        /// Test unit for <see cref="SdmxDateExtensions.StartsBefore" />
        /// </summary>
        /// <param name="firstPeriod">The first period.</param>
        /// <param name="secondPeriod">The second period.</param>
        /// <param name="expectedResult">if set to <c>true</c> [expected result].</param>
        [TestCase("0001", "0002", true)]
        [TestCase("9999", "0002", false)]
        [TestCase("2001-01", "2000-12", false)]
        [TestCase("2001-01", "2001-12", true)]
        [TestCase("2001-Q1", "2000-Q2", false)]
        [TestCase("2001-Q1", "2001-Q2", true)]
        public void TestStartsBefore(string firstPeriod, string secondPeriod, bool expectedResult)
        {
            ISdmxDate firstSdmxDate = new SdmxDateCore(firstPeriod);
            ISdmxDate secondSdmxDate = new SdmxDateCore(secondPeriod);
            Assert.AreEqual(expectedResult, firstSdmxDate.StartsBefore(secondSdmxDate));
        }

        /// <summary>
        /// Test unit for <see cref="SdmxDateExtensions.EndsAfter" />
        /// </summary>
        /// <param name="firstPeriod">The first period.</param>
        /// <param name="secondPeriod">The second period.</param>
        /// <param name="expectedResult">if set to <c>true</c> [expected result].</param>
        [TestCase("0001", "0002", false)]
        [TestCase("9999", "0002", true)]
        [TestCase("2001-01", "2000-12", true)]
        [TestCase("2001-01", "2001-12", false)]
        [TestCase("2001-Q1", "2000-Q2", true)]
        [TestCase("2001-Q1", "2001-Q2", false)]
        public void TestEndsAfter(string firstPeriod, string secondPeriod, bool expectedResult)
        {
            ISdmxDate firstSdmxDate = new SdmxDateCore(firstPeriod);
            ISdmxDate secondSdmxDate = new SdmxDateCore(secondPeriod);
            Assert.AreEqual(expectedResult, firstSdmxDate.EndsAfter(secondSdmxDate));
        }

        /// <summary>
        /// Tests to query period.
        /// </summary>
        /// <param name="period">The period.</param>
        /// <param name="frequencyCode">The frequency code.</param>
        /// <param name="expectedYear">The expected year.</param>
        /// <param name="expectedPeriod">The expected period.</param>
        [TestCase("2001-01-04", TimeFormatEnumType.QuarterOfYear, 2001, 2)]
        [TestCase("1980-01-01", TimeFormatEnumType.QuarterOfYear, 1980, 1)]
        [TestCase("2011-06-29", TimeFormatEnumType.QuarterOfYear, 2011, 3)]
        [TestCase("2011-12-29", TimeFormatEnumType.QuarterOfYear, 2012, 1)]
        [TestCase("2011-08-29", TimeFormatEnumType.QuarterOfYear, 2011, 4)]
        [TestCase("2001-01-04", TimeFormatEnumType.Month, 2001, 2)]
        [TestCase("1980-01-01", TimeFormatEnumType.Month, 1980, 1)]
        [TestCase("2011-06-29", TimeFormatEnumType.Month, 2011, 7)]
        [TestCase("2011-12-29", TimeFormatEnumType.Month, 2012, 1)]
        [TestCase("2011-08-29", TimeFormatEnumType.Month, 2011, 9)]
        [TestCase("2001-01-04", TimeFormatEnumType.Year, 2002, 0)]
        [TestCase("1980-01-01", TimeFormatEnumType.Year, 1980, 0)]
        [TestCase("2011-06-29", TimeFormatEnumType.Year, 2012, 0)]
        [TestCase("2011-12-29", TimeFormatEnumType.Year, 2012, 0)]
        [TestCase("2011-08-29", TimeFormatEnumType.Year, 2012, 0)]
        [TestCase("2001-01-04", TimeFormatEnumType.HalfOfYear, 2001, 2)]
        [TestCase("1980-01-01", TimeFormatEnumType.HalfOfYear, 1980, 1)]
        [TestCase("2011-06-29", TimeFormatEnumType.HalfOfYear, 2011, 2)]
        [TestCase("2011-07-01", TimeFormatEnumType.HalfOfYear, 2011, 2)]
        [TestCase("2011-12-29", TimeFormatEnumType.HalfOfYear, 2012, 1)]
        [TestCase("2011-08-29", TimeFormatEnumType.HalfOfYear, 2012, 1)]
        public void TestToQueryPeriod(string period, TimeFormatEnumType frequencyCode, int expectedYear, int expectedPeriod)
        {
            var sdmxDate = new SdmxDateCore(period);
            IPeriodicity periodicity = PeriodicityFactory.Create(frequencyCode);
            SdmxQueryPeriod sdmxQueryPeriod = sdmxDate.ToQueryPeriod(periodicity);
            Assert.AreEqual(expectedYear, sdmxQueryPeriod.Year);
            bool hasPeriod = expectedPeriod > 0;
            Assert.AreEqual(hasPeriod, sdmxQueryPeriod.HasPeriod);
            if (hasPeriod)
            {
                Assert.AreEqual(expectedPeriod, sdmxQueryPeriod.Period);
            }
        }

        /// <summary>
        /// Tests the <see cref="SdmxDateExtensions.IsInRange"/>
        /// </summary>
        /// <param name="valueToCheck">The value to check.</param>
        /// <param name="timeRange">The time range.</param>
        /// <returns>The return value of <see cref="SdmxDateExtensions.IsInRange"/> </returns>
        [TestCaseSource(typeof(IsInRangeValueGenerator))]
        public bool TestIsInRange(DateTime valueToCheck, ITimeRange timeRange)
        {
            return timeRange.IsInRange(valueToCheck);
        }

        /// <summary>
        /// Generate values for <see cref="TestSdmxDateExtensions.TestIsInRange"/>
        /// </summary>
        /// <seealso cref="System.Collections.IEnumerable" />
        private class IsInRangeValueGenerator : IEnumerable
        {
            /// <summary>
            /// Returns an enumerator that iterates through a collection.
            /// </summary>
            /// <returns>
            /// An <see cref="T:System.Collections.IEnumerator" /> object that can be used to iterate through the collection.
            /// </returns>
            public IEnumerator GetEnumerator()
            {
                // no range
                var may2010 = new DateTime(2010, 05, 04, 0, 0, 0, DateTimeKind.Utc);
                var june2010 = new DateTime(2010, 06, 04, 0, 0, 0, DateTimeKind.Utc);
                var april2010 = new DateTime(2010, 04, 04, 0, 0, 0, DateTimeKind.Utc);
                var sdmxMay2010 = new SdmxDateCore(may2010);
                yield return new TestCaseData(may2010, new TimeRangeCore(false, sdmxMay2010, null, true, false)).Returns(true).SetName("{m}(Inclusive,no range,equals to start date)");
                yield return new TestCaseData(june2010, new TimeRangeCore(false, sdmxMay2010, null, true, false)).Returns(true).SetName("{m}(Inclusive, no range, after start date)");
                yield return new TestCaseData(may2010, new TimeRangeCore(false, sdmxMay2010, null, false, false)).Returns(false).SetName("{m}(Exclusive, no range, equals to start date)");
                yield return new TestCaseData(april2010, new TimeRangeCore(false, sdmxMay2010, null, false, false)).Returns(false).SetName("{m}(Exclusive, no range, before start date)");
                yield return new TestCaseData(april2010, new TimeRangeCore(false, sdmxMay2010, null, true, false)).Returns(false).SetName("{m}(Exclusive, no range, before start date)");
                yield return new TestCaseData(may2010, new TimeRangeCore(false, null, sdmxMay2010, true, true)).Returns(true).SetName("{m}(Inclusive, no range, equals to end date)");
                yield return new TestCaseData(april2010, new TimeRangeCore(false, null, sdmxMay2010, true, true)).Returns(true).SetName("{m}(Inclusive, no range, before end date)");
                yield return new TestCaseData(may2010, new TimeRangeCore(false, null, sdmxMay2010, false, false)).Returns(false).SetName("{m}(Exclusive, no range, equals to end date)");

                // range
                var april2015 = new DateTime(2015, 04, 02, 0, 0, 0, DateTimeKind.Utc);
                var endDate = new SdmxDateCore(april2015);
                var sdmxJune2010 = new SdmxDateCore(june2010);
                yield return new TestCaseData(may2010, new TimeRangeCore(true, sdmxMay2010, endDate, true, false)).Returns(true).SetName("{m}(Inclusive Start, range, equals to start date)");
                yield return new TestCaseData(may2010, new TimeRangeCore(true, sdmxMay2010, endDate, true, true)).Returns(true).SetName("{m}(Inclusive both, range, equals to start date)");
                yield return new TestCaseData(may2010, new TimeRangeCore(true, sdmxMay2010, endDate, false, true)).Returns(false).SetName("{m}(Inclusive End, range, equals to start date)");
                yield return new TestCaseData(june2010, new TimeRangeCore(true, sdmxMay2010, endDate, false, false)).Returns(true).SetName("{m}(Exclusive, range, after start date, before end date)");
                yield return new TestCaseData(april2015, new TimeRangeCore(true, sdmxMay2010, endDate, false, true)).Returns(true).SetName("{m}(Inclusive End, range, after start date, equal end date)");
                yield return new TestCaseData(april2015, new TimeRangeCore(true, sdmxMay2010, endDate, true, true)).Returns(true).SetName("{m}(Inclusive Both, range, after start date, equal end date)");
                yield return new TestCaseData(april2015, new TimeRangeCore(true, sdmxMay2010, endDate, true, false)).Returns(false).SetName("{m}(Exclusive End, range, after start date, equal end date)");
                yield return new TestCaseData(april2015, new TimeRangeCore(true, sdmxMay2010, sdmxJune2010, true, false)).Returns(false).SetName("{m}(Exclusive End, range, after start date, after end date)");
                yield return new TestCaseData(april2015, new TimeRangeCore(true, sdmxMay2010, sdmxJune2010, true, true)).Returns(false).SetName("{m}(Inclusive both, range, after start date, after end date)");
                yield return new TestCaseData(april2015, new TimeRangeCore(true, sdmxMay2010, sdmxJune2010, false, true)).Returns(false).SetName("{m}(Inclusive End, range, after start date, after end date)");

                yield return new TestCaseData(april2010, new TimeRangeCore(true, sdmxMay2010, sdmxJune2010, true, false)).Returns(false).SetName("{m}(Exclusive End, range, before start date, before end date)");
                yield return new TestCaseData(april2010, new TimeRangeCore(true, sdmxMay2010, sdmxJune2010, true, true)).Returns(false).SetName("{m}(Inclusive both, range, before start date, before end date)");
                yield return new TestCaseData(april2010, new TimeRangeCore(true, sdmxMay2010, sdmxJune2010, false, true)).Returns(false).SetName("{m}(Inclusive End, range, before start date, before end date)");
            }
        }
    }
}