﻿// -----------------------------------------------------------------------
// <copyright file="TestSdmxCategorisationExtension.cs" company="EUROSTAT">
//   Date Created : 2017-05-29
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace CustomRequests.Tests
{
    using Estat.Sdmxsource.Extension.Extension;

    using NUnit.Framework;

    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;

    public class TestSdmxCategorisationExtension
    {
        [TestCase("urn:sdmx:org.sdmx.infomodel.categoryscheme.Category=SDMX:SUBJECT_MATTER_DOMAINS(1.0).1.2", "urn:sdmx:org.sdmx.infomodel.categoryscheme.Category=SDMX:SUBJECT_MATTER_DOMAINS(1.0).1", true)]
        [TestCase("urn:sdmx:org.sdmx.infomodel.categoryscheme.Category=SDMX:SUBJECT_MATTER_DOMAINS(1.0).1.2.TEST123", "urn:sdmx:org.sdmx.infomodel.categoryscheme.Category=SDMX:SUBJECT_MATTER_DOMAINS(1.0).1.2.TEST", false)]
        [TestCase("urn:sdmx:org.sdmx.infomodel.categoryscheme.Category=SDMX:SUBJECT_MATTER_DOMAINS(1.0).2.TEST", "urn:sdmx:org.sdmx.infomodel.categoryscheme.Category=SDMX:SUBJECT_MATTER_DOMAINS(1.0).1.2.TEST", false)]
        [TestCase("urn:sdmx:org.sdmx.infomodel.categoryscheme.Category=SDMX:SUBJECT_MATTER_DOMAINS(1.0).2.TEST", "urn:sdmx:org.sdmx.infomodel.categoryscheme.Category=SDMX:SUBJECT_MATTER_DOMAINS(1.0).1.TEST", false)]
        [TestCase("urn:sdmx:org.sdmx.infomodel.categoryscheme.Category=SDMX:SUBJECT_MATTER_DOMAINS(1.0).11.22.33.44.55.66", "urn:sdmx:org.sdmx.infomodel.categoryscheme.Category=SDMX:SUBJECT_MATTER_DOMAINS(1.0).11.22.33.44.55", true)]
        [TestCase("urn:sdmx:org.sdmx.infomodel.categoryscheme.Category=SDMX:SUBJECT_MATTER_DOMAINS(1.0).11.22.33.44.55.66", "urn:sdmx:org.sdmx.infomodel.categoryscheme.Category=SDMX:SUBJECT_MATTER_DOMAINS(1.0).11.22.33.44.55.66", true)]
        [TestCase("urn:sdmx:org.sdmx.infomodel.categoryscheme.Category=SDMX:SUBJECT_MATTER_DOMAINS(1.0).11.22.33.44.55.66", "urn:sdmx:org.sdmx.infomodel.categoryscheme.CategoryScheme=SDMX:SUBJECT_MATTER_DOMAINS(1.0)", true)]
        [TestCase("urn:sdmx:org.sdmx.infomodel.categoryscheme.Category=SDMX:SUBJECT_MATTER_DOMAINS(1.0).11.22.33.44.55.66", "urn:sdmx:org.sdmx.infomodel.categoryscheme.CategoryScheme=SDMX:SUBJECT_MATTER_DOMAINS(2.0)", false)]
        public void TestIsChildOf(string childUrn, string parentUrn, bool expectedResult)
        {
           var childReference = new StructureReferenceImpl(childUrn); 
           var parentReference = new StructureReferenceImpl(parentUrn); 
           Assert.That(childReference.IsCategoryChildOf(parentReference), Is.EqualTo(expectedResult));
        }
    }
}