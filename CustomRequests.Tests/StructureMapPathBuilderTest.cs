﻿// -----------------------------------------------------------------------
// <copyright file="StructureMapPathBuilderTest.cs" company="EUROSTAT">
//   Date Created : 2017-07-10
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace CustomRequests.Tests
{
    using System.IO;
    using System.Linq;

    using Estat.Sdmxsource.Extension.Builder;
    using Estat.Sdmxsource.Extension.Extension;

    using NUnit.Framework;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Manager.Parse;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Mapping;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.MetadataStructure;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.Structureparser.Manager.Parsing;
    using Org.Sdmxsource.Sdmx.StructureRetrieval.Manager;
    using Org.Sdmxsource.Sdmx.Util.Extension;
    using Org.Sdmxsource.Sdmx.Util.Objects;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;

    [TestFixture]
    public class StructureMapPathBuilderTest
    {
        /// <summary>
        /// The builder
        /// </summary>
        private readonly StructureMapPathBuilder _builder = new StructureMapPathBuilder();

        /// <summary>
        /// The merge builder
        /// </summary>
        private readonly StructureSetMergeBuilder _mergeBuilder = new StructureSetMergeBuilder();

        /// <summary>
        /// The parsing manager
        /// </summary>
        private readonly IStructureParsingManager _parsingManager = new StructureParsingManager();

        /// <summary>
        /// The structure set builder
        /// </summary>
        private readonly StructureSetBuilder _structureSetBuilder = new StructureSetBuilder();

        [TestCase("tests/v21/Structure/MultipleStructureMap.xml", "urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=ESTAT:STS(7.0)", "urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=ESTAT:STS(7.2)", 28, 4)]
        public void ShouldBuildStructureSet(string filePath, string sourceUrn, string destUrn, int expectedComponentMap, int expectedCodelistMap)
        {
            var file = new FileInfo(filePath);
            var sdmxObejcts = file.GetSdmxObjects(this._parsingManager);
            ISdmxObjectRetrievalManager retrievalManager = new InMemoryRetrievalManager(sdmxObejcts);
            var sourceReference = new StructureReferenceImpl(sourceUrn);
            var targetReference = new StructureReferenceImpl(destUrn);
            var structureSetObject = this._structureSetBuilder.Build(sourceReference, targetReference, retrievalManager);
            Assert.That(structureSetObject, Is.Not.Null);
            Assert.That(structureSetObject.StructureMapList, Is.Not.Null.And.Not.Empty);
            Assert.That(structureSetObject.StructureMapList, Has.Count.EqualTo(1));
            Assert.That(structureSetObject.StructureMapList[0].Components, Has.Count.EqualTo(expectedComponentMap));
            Assert.That(structureSetObject.StructureMapList[0].Components.Where(o => o.RepMapRef != null).ToArray(), Has.Length.EqualTo(expectedCodelistMap));
            Assert.That(structureSetObject.CodelistMapList, Has.Count.EqualTo(expectedCodelistMap));
            Assert.That(structureSetObject.CodelistMapList.Where(o => o.Items.Count > 0).ToArray(), Has.Length.EqualTo(expectedCodelistMap));
        }

        /// <summary>
        /// Should the find structure map path.
        /// </summary>
        /// <param name="filePath">The file path.</param>
        /// <param name="sourceUrn">The source urn.</param>
        /// <param name="destUrn">The destination urn.</param>
        /// <param name="expectedPathCount">The expected path count.</param>
        [TestCase("tests/v21/Structure/ESTAT+SDMXREG-282+1.1.xml", "urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=ESTAT:STS(7.0)", "urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=ESTAT:STS(7.1)", 1)]
        [TestCase("tests/v21/Structure/MultipleStructureMap.xml", "urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=ESTAT:STS(7.0)", "urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=ESTAT:STS(7.2)", 2)]
        public void ShouldFindStructureMapPath(string filePath, string sourceUrn, string destUrn, int expectedPathCount)
        {
            var file = new FileInfo(filePath);
            var sdmxObejcts = file.GetSdmxObjects(this._parsingManager);
            var sourceDataflow = this.GetDataflow(sdmxObejcts, sourceUrn);
            var targetDataflow = this.GetDataflow(sdmxObejcts, destUrn);
            var structureSets = sdmxObejcts.StructureSets;
            var path = this._builder.BuildStructureMapPath(structureSets, sourceDataflow, targetDataflow);
            Assert.That(path, Is.Not.Null);
            Assert.That(path, Is.Not.Empty);
            Assert.That(path, Has.Length.EqualTo(expectedPathCount));

            Assert.That(sourceDataflow.IsStructureOrUsageMatch(path.First().SourceRef));
            Assert.That(targetDataflow.IsStructureOrUsageMatch(path.Last().TargetRef));

            for (int index = 1; index < path.Count; index++)
            {
                var previousMap = path[index - 1];
                var currentMap = path[index];
                Assert.That(previousMap.TargetRef, Is.EqualTo(currentMap.SourceRef));
            }
        }

        /// <summary>
        /// Should find structure map path cross.
        /// </summary>
        /// <param name="filePath">The file path.</param>
        /// <param name="sourceUrn">The source urn.</param>
        /// <param name="destUrn">The destination urn.</param>
        /// <param name="expectedPathCount">The expected path count.</param>
        [TestCase("tests/v21/Structure/MultipleStructureSet.xml", "urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=ESTAT:STS(7.0)", "urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=ESTAT:STS(7.3)", 3)]
        public void ShouldFindStructureMapPathCross(string filePath, string sourceUrn, string destUrn, int expectedPathCount)
        {
            var file = new FileInfo(filePath);
            var sdmxObejcts = file.GetSdmxObjects(this._parsingManager);
            var sourceDataflow = this.GetDataflow(sdmxObejcts, sourceUrn);
            var targetDataflow = this.GetDataflow(sdmxObejcts, destUrn);
            var structureSets = sdmxObejcts.StructureSets;
            var path = this._builder.BuildStructureMapPathCrossSet(structureSets, sourceDataflow, targetDataflow);
            Assert.That(path, Is.Not.Null);
            Assert.That(path, Is.Not.Empty);
            Assert.That(path, Has.Length.EqualTo(expectedPathCount));

            Assert.That(sourceDataflow.IsStructureOrUsageMatch(path.First().SourceRef));
            Assert.That(targetDataflow.IsStructureOrUsageMatch(path.Last().TargetRef));

            for (int index = 1; index < path.Count; index++)
            {
                var previousMap = path[index - 1];
                var currentMap = path[index];
                Assert.That(previousMap.TargetRef, Is.EqualTo(currentMap.SourceRef));
            }
        }

        [TestCase("tests/v21/Structure/MultipleStructureSet.xml", "urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=ESTAT:STS(7.0)", "urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=ESTAT:STS(7.3)")]
        [TestCase("tests/v21/Structure/ESTAT+SDMXREG-282+1.1.xml", "urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=ESTAT:STS(7.0)", "urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=ESTAT:STS(7.1)")]
        [TestCase("tests/v21/Structure/MultipleStructureMap.xml", "urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=ESTAT:STS(7.0)", "urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=ESTAT:STS(7.2)")]
        public void ShouldMergeStructureSets(string filePath, string sourceUrn, string destUrn)
        {
            var file = new FileInfo(filePath);
            var sdmxObejcts = file.GetSdmxObjects(this._parsingManager);
            var sourceDataflow = this.GetDataflow(sdmxObejcts, sourceUrn);
            var targetDataflow = this.GetDataflow(sdmxObejcts, destUrn);
            var structureSets = sdmxObejcts.StructureSets;
            var path = this._builder.BuildStructureMapPathCrossSet(structureSets, sourceDataflow, targetDataflow);

            ISdmxObjectRetrievalManager retrievalManager = new InMemoryRetrievalManager(sdmxObejcts);
            var mergedSet = this._mergeBuilder.Build(path, retrievalManager);
            Assert.That(mergedSet, Is.Not.Null);
            Assert.That(mergedSet.StructureMapList, Has.Count.EqualTo(1));
            var maxCodelists = structureSets.Select(o => o.CodelistMapList.Count).Max();
            Assert.That(mergedSet.CodelistMapList, Has.Count.GreaterThanOrEqualTo(maxCodelists));
            var firstStructureMap = path.First();
            var lastStructureMap = path.Last();
            var mergedStructureMap = mergedSet.StructureMapList.First();
            Assert.That(mergedStructureMap.SourceRef, Is.EqualTo(firstStructureMap.SourceRef));
            Assert.That(mergedStructureMap.TargetRef, Is.EqualTo(lastStructureMap.TargetRef));
        }

        [TestCase("tests/v21/Structure/MultipleStructureSet.xml", "urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=ESTAT:STS(7.0)", "urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=ESTAT:STS(7.3)")]
        [TestCase("tests/v21/Structure/ESTAT+SDMXREG-282+1.1.xml", "urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=ESTAT:STS(7.0)", "urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=ESTAT:STS(7.1)")]
        [TestCase("tests/v21/Structure/MultipleStructureMap.xml", "urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=ESTAT:STS(7.0)", "urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=ESTAT:STS(7.2)")]
        public void ShouldMergeStructureSetsComponentMaps(string filePath, string sourceUrn, string destUrn)
        {
            var file = new FileInfo(filePath);
            var sdmxObejcts = file.GetSdmxObjects(this._parsingManager);
            var sourceDataflow = this.GetDataflow(sdmxObejcts, sourceUrn);
            var targetDataflow = this.GetDataflow(sdmxObejcts, destUrn);
            var structureSets = sdmxObejcts.StructureSets;
            var path = this._builder.BuildStructureMapPathCrossSet(structureSets, sourceDataflow, targetDataflow);

            ISdmxObjectRetrievalManager retrievalManager = new InMemoryRetrievalManager(sdmxObejcts);
            var mergedSet = this._mergeBuilder.Build(path, retrievalManager);
            var mergedStructureMap = mergedSet.StructureMapList.First();

            Assert.That(mergedStructureMap.Components, Is.Not.Empty);
            foreach (var mergedComponentMap in mergedStructureMap.Components)
            {
                var componentMapPath = _builder.BuildComponentMapPath(path, mergedComponentMap.MapConceptRef);
                Assert.That(mergedComponentMap.MapConceptRef, Is.EqualTo(componentMapPath.First().MapConceptRef));
                Assert.That(mergedComponentMap.MapTargetConceptRef, Is.EqualTo(componentMapPath.Last().MapTargetConceptRef));
                var codelistMapObjects = componentMapPath.Select(o => o.GetCodelistMap()).Where(o => o != null).ToArray();

                var mergedCodelistMap = mergedComponentMap.GetCodelistMap();
                if (codelistMapObjects.Length > 0)
                {
                    Assert.That(mergedCodelistMap, Is.Not.Null);
                    var firstCodelistMap = codelistMapObjects.First();
                    Assert.That(mergedCodelistMap.SourceRef, Is.EqualTo(firstCodelistMap.SourceRef));
                    var lastCodelistMap = codelistMapObjects.Last();
                    Assert.That(mergedCodelistMap.TargetRef, Is.EqualTo(lastCodelistMap.TargetRef));
                    Assert.That(mergedCodelistMap.Items.Count, Is.GreaterThanOrEqualTo(firstCodelistMap.Items.Count));
                    Assert.That(mergedCodelistMap.Items.Count, Is.GreaterThanOrEqualTo(lastCodelistMap.Items.Count));
                    // TODO some how check if code maps are merged correctly
                }
                else
                {
                    Assert.That(mergedCodelistMap, Is.Null);
                }
            }
        }

        /// <summary>
        /// Gets the dataflow.
        /// </summary>
        /// <param name="sdmxObjects">The SDMX objects.</param>
        /// <param name="urn">The urn.</param>
        private IDataflowObject GetDataflow(ISdmxObjects sdmxObjects, string urn)
        {
            if (UrnUtil.GetIdentifiableType(urn).EnumType == SdmxStructureEnumType.Dataflow)
            {
                return sdmxObjects.GetDataflows(new StructureReferenceImpl(urn)).First();
            }

            if (UrnUtil.GetIdentifiableType(urn).EnumType == SdmxStructureEnumType.Dsd)
            {
                var dsd = sdmxObjects.GetDataStructures(new StructureReferenceImpl(urn)).First();
                return new DataflowObjectCore(new DataflowMutableCore(dsd));
            }

            Assert.Fail("input file issue. No Dataflow/Dsd related to '" + urn + "' found");
            return null;
        }
    }
}