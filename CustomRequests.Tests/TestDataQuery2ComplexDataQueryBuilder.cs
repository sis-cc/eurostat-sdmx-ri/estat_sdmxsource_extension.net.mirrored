﻿// -----------------------------------------------------------------------
// <copyright file="TestDataQuery2ComplexDataQueryBuilder.cs" company="EUROSTAT">
//   Date Created : 2016-06-15
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace CustomRequests.Tests
{
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;

    using Estat.Sdmxsource.Extension.Builder;

    using NUnit.Framework;

    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
    using Org.Sdmxsource.Sdmx.Api.Model.Data.Query;
    using Org.Sdmxsource.Sdmx.Api.Model.Data.Query.Complex;
    using Org.Sdmxsource.Sdmx.Structureparser.Manager.Parsing;
    using Org.Sdmxsource.Sdmx.StructureRetrieval.Manager;
    using Org.Sdmxsource.Sdmx.Util.Extension;
    using Org.Sdmxsource.Util.Io;

    /// <summary>
    /// The test data query 2 complex data query builder.
    /// </summary>
    [TestFixture]
    public class TestDataQuery2ComplexDataQueryBuilder
    {
        /// <summary>
        /// The _retrieval manager
        /// </summary>
        private readonly ISdmxObjectRetrievalManager _retrievalManager;

        /// <summary>
        /// The _parse manager
        /// </summary>
        private readonly IDataQueryParseManager _parseManager;

        /// <summary>
        /// Initializes a new instance of the <see cref="TestDataQuery2ComplexDataQueryBuilder"/> class.
        /// </summary>
        public TestDataQuery2ComplexDataQueryBuilder()
        {
            var dataFlowFile = new FileInfo("tests/V21/Structure/test-sdmxv2.1-ESTAT+SSTSCONS_PROD_M+2.0.xml");
            var dsdFile = new FileInfo("tests/V21/Structure/test-sdmxv2.1-ESTAT+STS+2.0.xml");
            var sdmxObjects = dataFlowFile.GetSdmxObjects(new StructureParsingManager());
            sdmxObjects.Merge(dsdFile.GetSdmxObjects(new StructureParsingManager()));
            this._retrievalManager = new InMemoryRetrievalManager(sdmxObjects);
            this._parseManager = new DataQueryParseManager();
        }

        /// <summary>
        /// Tests the build rest.
        /// </summary>
        /// <param name="url">The URL.</param>
        [TestCase("data/ESTAT,SSTSCONS_PROD_M/ALL")]
        [TestCase("data/ESTAT,SSTSCONS_PROD_M/M.LU.N.....")]
        [TestCase("data/ESTAT,SSTSCONS_PROD_M/M.LU.N...../?startPeriod=2013-03")]
        [TestCase("data/ESTAT,SSTSCONS_PROD_M/M.LU.N...../?endPeriod=2013-03")]
        [TestCase("data/ESTAT,SSTSCONS_PROD_M/M.LU.N...../?dimensionAtObservation=FREQ")]
        public void TestBuildRest(string url)
        {
            var restQuery = this._parseManager.ParseRestQuery(url, this._retrievalManager);
            Validate(new[] { restQuery }, new DataQuery2ComplexQueryBuilder(false));
        }

        /// <summary>
        /// Tests the build v2.
        /// </summary>
        /// <param name="file">The file.</param>
        [TestCase("tests/V20/Query_SSTSCONS_PROD_A_v2.0.xml")]
        public void TestBuildV2(string file)
        {
            IList<IDataQuery> dataQueries;
    
            using (var queryLocation = new FileReadableDataLocation(file))
            {
                dataQueries = this._parseManager.BuildDataQuery(queryLocation, this._retrievalManager);
            }

            DataQuery2ComplexQueryBuilder builder = new DataQuery2ComplexQueryBuilder(true);
            Validate(dataQueries, builder);
        }

        /// <summary>
        /// Validates the specified data queries.
        /// </summary>
        /// <param name="dataQueries">The data queries.</param>
        /// <param name="builder">The builder.</param>
        private static void Validate(IList<IDataQuery> dataQueries, DataQuery2ComplexQueryBuilder builder)
        {
            foreach (var dataQuery in dataQueries)
            {
                var complexDataQuery = builder.Build(dataQuery);
                Assert.NotNull(complexDataQuery);
                Assert.AreEqual(dataQuery.Dataflow, complexDataQuery.Dataflow);
                Assert.AreEqual(dataQuery.DataQueryDetail, complexDataQuery.DataQueryDetail);
                Assert.AreEqual(dataQuery.SelectionGroups.Count, complexDataQuery.SelectionGroups.Count);
                Assert.AreEqual(dataQuery.DimensionAtObservation, complexDataQuery.DimensionAtObservation);
                for (int index = 0; index < dataQuery.SelectionGroups.Count; index++)
                {
                    var dataQuerySelectionGroup = dataQuery.SelectionGroups[index];
                    var complexDataQuerySelectionGroup = complexDataQuery.SelectionGroups[index];
                    Assert.AreEqual(dataQuerySelectionGroup.DateFrom, complexDataQuerySelectionGroup.DateFrom);
                    Assert.AreEqual(dataQuerySelectionGroup.DateTo, complexDataQuerySelectionGroup.DateTo);
                    Assert.AreEqual(
                        dataQuerySelectionGroup.Selections.Count,
                        complexDataQuerySelectionGroup.Selections.Count);
                    foreach (var selection in dataQuerySelectionGroup.Selections)
                    {
                        var complexSelection =
                            complexDataQuerySelectionGroup.GetSelectionsForConcept(selection.ComponentId);
                        Assert.AreEqual(complexSelection.Values.Count, selection.Values.Count);
                        CollectionAssert.AreEquivalent(complexSelection.Values.Select(value => value.Value), selection.Values);
                    }
                }
            }
        }
    }
}