﻿// -----------------------------------------------------------------------
// <copyright file="TestComplexStructureQueryBuilderV21.cs" company="EUROSTAT">
//   Date Created : 2013-09-20
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace CustomRequests.Tests
{
    using System.Xml;

    using Estat.Sri.CustomRequests.Builder.QueryBuilder;

    using NUnit.Framework;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Complex;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference.Complex;
    using Org.Sdmxsource.Sdmx.Structureparser.Manager.Parsing;
    using Org.Sdmxsource.Sdmx.Structureparser.Workspace;
    using Org.Sdmxsource.Util.Io;
    using Org.Sdmxsource.XmlHelper;

    /// <summary>
    ///     Test unit for <see cref="ComplexStructureQueryBuilderV21" />
    /// </summary>
    [TestFixture]
    public class TestComplexStructureQueryBuilderV21
    {
        /// <summary>
        ///     Test unit for <see cref="ComplexStructureQueryBuilderV21.BuildComplexStructureQuery" />
        /// </summary>
        /// <param name="sdmxStructure">The SDMX structure.</param>
        [TestCase(SdmxStructureEnumType.CategoryScheme)]
        [TestCase(SdmxStructureEnumType.Dsd)]
        [TestCase(SdmxStructureEnumType.Dataflow)]
        [TestCase(SdmxStructureEnumType.CodeList)]
        public void TestType(SdmxStructureEnumType sdmxStructure)
        {
            var agency = new ComplexTextReferenceCore(null, TextSearch.GetFromEnum(TextSearchEnumType.Equal), "TEST");
            var id = new ComplexTextReferenceCore(null, TextSearch.GetFromEnum(TextSearchEnumType.Equal), "TEST");
            IComplexVersionReference versionRef = new ComplexVersionReferenceCore(
                TertiaryBool.ParseBoolean(false),
                "1.0",
                null,
                null);
            var complexStructureReferenceCore = new ComplexStructureReferenceCore(
                agency,
                id,
                versionRef,
                SdmxStructureType.GetFromEnum(sdmxStructure),
                null,
                null,
                null,
                null);
            var complexStructureQueryMetadataCore = new ComplexStructureQueryMetadataCore(
                true,
                ComplexStructureQueryDetail.GetFromEnum(ComplexStructureQueryDetailEnumType.Full),
                ComplexMaintainableQueryDetail.GetFromEnum(ComplexMaintainableQueryDetailEnumType.Full),
                StructureReferenceDetail.GetFromEnum(StructureReferenceDetailEnumType.All),
                new [] { SdmxStructureType.GetFromEnum(sdmxStructure) });
            IComplexStructureQuery complexStructureQuery = new ComplexStructureQueryCore(
                complexStructureReferenceCore,
                complexStructureQueryMetadataCore);

            var builder = new ComplexStructureQueryBuilderV21();
            var structureQuery = builder.BuildComplexStructureQuery(complexStructureQuery);
            var fileName = string.Format("test-ComplexStructureQueryBuilderV21-{0}-test-type.xml", sdmxStructure);
            structureQuery.Save(fileName);

            IQueryParsingManager parsingManager = new QueryParsingManager();
            IQueryWorkspace queryWorkspace;
            using (var readable = new FileReadableDataLocation(fileName))
            {
                queryWorkspace = parsingManager.ParseQueries(readable);
            }

            Assert.NotNull(queryWorkspace.ComplexStructureQuery);
            Assert.AreEqual(sdmxStructure, queryWorkspace.ComplexStructureQuery.StructureReference.ReferencedStructureType.EnumType);
        }

        /// <summary>
        ///     Test unit for <see cref="ComplexStructureQueryBuilderV21.BuildComplexStructureQuery" />
        /// </summary>
        /// <param name="sdmxStructure">The SDMX structure.</param>
        /// <param name="detail">The detail</param>
        /// <exception cref="SdmxSemmanticException">Reference Detail cannot be null.</exception>
        [TestCase(SdmxStructureEnumType.CategoryScheme, StructureReferenceDetailEnumType.All)]
        [TestCase(SdmxStructureEnumType.CategoryScheme, StructureReferenceDetailEnumType.Children)]
        [TestCase(SdmxStructureEnumType.CategoryScheme, StructureReferenceDetailEnumType.Descendants)]
        [TestCase(SdmxStructureEnumType.CategoryScheme, StructureReferenceDetailEnumType.None)]
        [TestCase(SdmxStructureEnumType.CategoryScheme, StructureReferenceDetailEnumType.Parents)]
        [TestCase(SdmxStructureEnumType.CategoryScheme, StructureReferenceDetailEnumType.ParentsSiblings)]
        [TestCase(SdmxStructureEnumType.CategoryScheme, StructureReferenceDetailEnumType.Specific)]
        [TestCase(SdmxStructureEnumType.Dsd, StructureReferenceDetailEnumType.All)]
        [TestCase(SdmxStructureEnumType.Dsd, StructureReferenceDetailEnumType.Children)]
        [TestCase(SdmxStructureEnumType.Dsd, StructureReferenceDetailEnumType.Descendants)]
        [TestCase(SdmxStructureEnumType.Dsd, StructureReferenceDetailEnumType.None)]
        [TestCase(SdmxStructureEnumType.Dsd, StructureReferenceDetailEnumType.Parents)]
        [TestCase(SdmxStructureEnumType.Dsd, StructureReferenceDetailEnumType.ParentsSiblings)]
        [TestCase(SdmxStructureEnumType.Dsd, StructureReferenceDetailEnumType.Specific)]
        [TestCase(SdmxStructureEnumType.Dataflow, StructureReferenceDetailEnumType.All)]
        [TestCase(SdmxStructureEnumType.Dataflow, StructureReferenceDetailEnumType.Children)]
        [TestCase(SdmxStructureEnumType.Dataflow, StructureReferenceDetailEnumType.Descendants)]
        [TestCase(SdmxStructureEnumType.Dataflow, StructureReferenceDetailEnumType.None)]
        [TestCase(SdmxStructureEnumType.Dataflow, StructureReferenceDetailEnumType.Parents)]
        [TestCase(SdmxStructureEnumType.Dataflow, StructureReferenceDetailEnumType.ParentsSiblings)]
        [TestCase(SdmxStructureEnumType.Dataflow, StructureReferenceDetailEnumType.Specific)]
        [TestCase(SdmxStructureEnumType.CodeList, StructureReferenceDetailEnumType.All)]
        [TestCase(SdmxStructureEnumType.CodeList, StructureReferenceDetailEnumType.Children)]
        [TestCase(SdmxStructureEnumType.CodeList, StructureReferenceDetailEnumType.Descendants)]
        [TestCase(SdmxStructureEnumType.CodeList, StructureReferenceDetailEnumType.None)]
        [TestCase(SdmxStructureEnumType.CodeList, StructureReferenceDetailEnumType.Parents)]
        [TestCase(SdmxStructureEnumType.CodeList, StructureReferenceDetailEnumType.ParentsSiblings)]
        [TestCase(SdmxStructureEnumType.CodeList, StructureReferenceDetailEnumType.Specific)]
        public void TestDetail(SdmxStructureEnumType sdmxStructure, StructureReferenceDetailEnumType detail)
        {
            var agency = new ComplexTextReferenceCore(null, TextSearch.GetFromEnum(TextSearchEnumType.Equal), "TEST");
            var id = new ComplexTextReferenceCore(null, TextSearch.GetFromEnum(TextSearchEnumType.Equal), "TEST");
            IComplexVersionReference versionRef = new ComplexVersionReferenceCore(
                TertiaryBool.ParseBoolean(false), 
                "1.0", 
                null, 
                null);
            var complexStructureReferenceCore = new ComplexStructureReferenceCore(
                agency, 
                id, 
                versionRef, 
                SdmxStructureType.GetFromEnum(sdmxStructure), 
                null, 
                null, 
                null, 
                null);
            
            SdmxStructureType[] referenceSpecificStructures = detail == StructureReferenceDetailEnumType.Specific ? new[] { SdmxStructureType.GetFromEnum(sdmxStructure) } : new SdmxStructureType[0]; 

            var complexStructureQueryMetadataCore = new ComplexStructureQueryMetadataCore(
                true, 
                ComplexStructureQueryDetail.GetFromEnum(ComplexStructureQueryDetailEnumType.Full), 
                ComplexMaintainableQueryDetail.GetFromEnum(ComplexMaintainableQueryDetailEnumType.Full), 
                StructureReferenceDetail.GetFromEnum(detail), 
                 referenceSpecificStructures);
            IComplexStructureQuery complexStructureQuery = new ComplexStructureQueryCore(
                complexStructureReferenceCore, 
                complexStructureQueryMetadataCore);

            var builder = new ComplexStructureQueryBuilderV21();
            var structureQuery = builder.BuildComplexStructureQuery(complexStructureQuery);
            var fileName = string.Format("test-ComplexStructureQueryBuilderV21-{0}-{1}.xml", sdmxStructure, detail);
            structureQuery.Save(fileName);
            
            IQueryParsingManager parsingManager = new QueryParsingManager();
            IQueryWorkspace queryWorkspace;
            using (var readable = new FileReadableDataLocation(fileName))
            {
                queryWorkspace = parsingManager.ParseQueries(readable);
            }

            Assert.NotNull(queryWorkspace.ComplexStructureQuery);
            Assert.AreEqual(detail, queryWorkspace.ComplexStructureQuery.StructureQueryMetadata.StructureReferenceDetail.EnumType);
        }

        /// <summary>
        /// Test unit for <see cref="ComplexStructureQueryBuilderV21.BuildComplexStructureQuery" />
        /// </summary>
        /// <param name="sdmxStructure">The SDMX structure.</param>
        /// <param name="detail">The detail</param>
        [TestCase(SdmxStructureEnumType.CategoryScheme, StructureReferenceDetailEnumType.All)]
        [TestCase(SdmxStructureEnumType.CategoryScheme, StructureReferenceDetailEnumType.Children)]
        [TestCase(SdmxStructureEnumType.CategoryScheme, StructureReferenceDetailEnumType.Descendants)]
        [TestCase(SdmxStructureEnumType.CategoryScheme, StructureReferenceDetailEnumType.None)]
        [TestCase(SdmxStructureEnumType.CategoryScheme, StructureReferenceDetailEnumType.Parents)]
        [TestCase(SdmxStructureEnumType.CategoryScheme, StructureReferenceDetailEnumType.ParentsSiblings)]
        [TestCase(SdmxStructureEnumType.CategoryScheme, StructureReferenceDetailEnumType.Specific)]
        [TestCase(SdmxStructureEnumType.Dsd, StructureReferenceDetailEnumType.All)]
        [TestCase(SdmxStructureEnumType.Dsd, StructureReferenceDetailEnumType.Children)]
        [TestCase(SdmxStructureEnumType.Dsd, StructureReferenceDetailEnumType.Descendants)]
        [TestCase(SdmxStructureEnumType.Dsd, StructureReferenceDetailEnumType.None)]
        [TestCase(SdmxStructureEnumType.Dsd, StructureReferenceDetailEnumType.Parents)]
        [TestCase(SdmxStructureEnumType.Dsd, StructureReferenceDetailEnumType.ParentsSiblings)]
        [TestCase(SdmxStructureEnumType.Dsd, StructureReferenceDetailEnumType.Specific)]
        [TestCase(SdmxStructureEnumType.Dataflow, StructureReferenceDetailEnumType.All)]
        [TestCase(SdmxStructureEnumType.Dataflow, StructureReferenceDetailEnumType.Children)]
        [TestCase(SdmxStructureEnumType.Dataflow, StructureReferenceDetailEnumType.Descendants)]
        [TestCase(SdmxStructureEnumType.Dataflow, StructureReferenceDetailEnumType.None)]
        [TestCase(SdmxStructureEnumType.Dataflow, StructureReferenceDetailEnumType.Parents)]
        [TestCase(SdmxStructureEnumType.Dataflow, StructureReferenceDetailEnumType.ParentsSiblings)]
        [TestCase(SdmxStructureEnumType.Dataflow, StructureReferenceDetailEnumType.Specific)]
        [TestCase(SdmxStructureEnumType.CodeList, StructureReferenceDetailEnumType.All)]
        [TestCase(SdmxStructureEnumType.CodeList, StructureReferenceDetailEnumType.Children)]
        [TestCase(SdmxStructureEnumType.CodeList, StructureReferenceDetailEnumType.Descendants)]
        [TestCase(SdmxStructureEnumType.CodeList, StructureReferenceDetailEnumType.None)]
        [TestCase(SdmxStructureEnumType.CodeList, StructureReferenceDetailEnumType.Parents)]
        [TestCase(SdmxStructureEnumType.CodeList, StructureReferenceDetailEnumType.ParentsSiblings)]
        [TestCase(SdmxStructureEnumType.CodeList, StructureReferenceDetailEnumType.Specific)]
        public void TestValidOoutput(SdmxStructureEnumType sdmxStructure, StructureReferenceDetailEnumType detail)
        {
            var agency = new ComplexTextReferenceCore(null, TextSearch.GetFromEnum(TextSearchEnumType.Equal), "TEST");
            var id = new ComplexTextReferenceCore(null, TextSearch.GetFromEnum(TextSearchEnumType.Equal), "TEST");
            IComplexVersionReference versionRef = new ComplexVersionReferenceCore(
                TertiaryBool.ParseBoolean(false),
                "1.0",
                null,
                null);
            var complexStructureReferenceCore = new ComplexStructureReferenceCore(
                agency,
                id,
                versionRef,
                SdmxStructureType.GetFromEnum(sdmxStructure),
                null,
                null,
                null,
                null);
            var complexStructureQueryMetadataCore = new ComplexStructureQueryMetadataCore(
                true,
                ComplexStructureQueryDetail.GetFromEnum(ComplexStructureQueryDetailEnumType.Full),
                ComplexMaintainableQueryDetail.GetFromEnum(ComplexMaintainableQueryDetailEnumType.Full),
                StructureReferenceDetail.GetFromEnum(detail),
                new [] { SdmxStructureType.GetFromEnum(sdmxStructure) });
            IComplexStructureQuery complexStructureQuery = new ComplexStructureQueryCore(
                complexStructureReferenceCore,
                complexStructureQueryMetadataCore);

            var builder = new ComplexStructureQueryBuilderV21();
            var structureQuery = builder.BuildComplexStructureQuery(complexStructureQuery);
            var fileName = string.Format("test-ComplexStructureQueryBuilderV21-{0}-validation-test.xml", sdmxStructure);
            structureQuery.Save(fileName);

            bool firstNode = false;
            using (var readable = new FileReadableDataLocation(fileName))
            using (var inputStream = readable.InputStream)
            using (var reader = XMLParser.CreateSdmxMlReader(inputStream, SdmxSchemaEnumType.VersionTwoPointOne))
            {
                while (reader.Read())
                {
                    switch (reader.NodeType)
                    {
                        case XmlNodeType.Element:
                            if (!firstNode)
                            {
                                firstNode = true;
                                Assert.AreEqual(SdmxConstants.MessageNs21, reader.NamespaceURI);
                                Assert.AreEqual(MessageEnumType.Query, MessageType.ParseString(reader.LocalName).EnumType);
                            }

                            break;
                    }
                }
            }
        }
    }
}