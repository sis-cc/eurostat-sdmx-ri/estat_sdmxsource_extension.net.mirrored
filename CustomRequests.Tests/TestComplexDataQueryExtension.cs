﻿// -----------------------------------------------------------------------
// <copyright file="TestComplexDataQueryExtension.cs" company="EUROSTAT">
//   Date Created : 2014-10-31
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace CustomRequests.Tests
{
    using Estat.Sri.CustomRequests.Extension;

    using NUnit.Framework;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Data.Query.Complex;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Data.Query.Complex;

    /// <summary>
    ///     Test Complex Data Query Extension
    /// </summary>
    [TestFixture]
    public class TestComplexDataQueryExtension
    {
        /// <summary>
        ///     Tests the should not use and.
        /// </summary>
        [Test]
        public void TestShouldNotUseAnd()
        {
            IComplexComponentValue[] cv =
            {
                new ComplexComponentValueImpl(
                    "M", 
                    OrderedOperator.GetFromEnum(OrderedOperatorEnumType.NotEqual), 
                    SdmxStructureEnumType.Dimension), 
                new ComplexComponentValueImpl(
                    "A", 
                    OrderedOperator.GetFromEnum(OrderedOperatorEnumType.Equal), 
                    SdmxStructureEnumType.Dimension), 
                new ComplexComponentValueImpl(
                    "B", 
                    OrderedOperator.GetFromEnum(OrderedOperatorEnumType.Equal), 
                    SdmxStructureEnumType.Dimension)
            };
            var freqCriteria = new ComplexDataQuerySelectionImpl("FREQ", cv);
            Assert.IsFalse(freqCriteria.ShouldUseAnd());
        }

        /// <summary>
        ///     Tests the should not use and2.
        /// </summary>
        [Test]
        public void TestShouldNotUseAnd2()
        {
            IComplexComponentValue[] cv =
            {
                new ComplexComponentValueImpl(
                    "M", 
                    OrderedOperator.GetFromEnum(OrderedOperatorEnumType.Equal), 
                    SdmxStructureEnumType.Dimension), 
                new ComplexComponentValueImpl(
                    "A", 
                    OrderedOperator.GetFromEnum(OrderedOperatorEnumType.Equal), 
                    SdmxStructureEnumType.Dimension), 
                new ComplexComponentValueImpl(
                    "B", 
                    OrderedOperator.GetFromEnum(OrderedOperatorEnumType.Equal), 
                    SdmxStructureEnumType.Dimension)
            };
            var freqCriteria = new ComplexDataQuerySelectionImpl("FREQ", cv);
            Assert.IsFalse(freqCriteria.ShouldUseAnd());
        }

        /// <summary>
        ///     Tests the should use and.
        /// </summary>
        [Test]
        public void TestShouldUseAnd()
        {
            IComplexComponentValue[] cv =
            {
                new ComplexComponentValueImpl(
                    "M", 
                    OrderedOperator.GetFromEnum(OrderedOperatorEnumType.NotEqual), 
                    SdmxStructureEnumType.Dimension), 
                new ComplexComponentValueImpl(
                    "A", 
                    OrderedOperator.GetFromEnum(OrderedOperatorEnumType.NotEqual), 
                    SdmxStructureEnumType.Dimension), 
                new ComplexComponentValueImpl(
                    "B", 
                    OrderedOperator.GetFromEnum(OrderedOperatorEnumType.NotEqual), 
                    SdmxStructureEnumType.Dimension)
            };
            var freqCriteria = new ComplexDataQuerySelectionImpl("FREQ", cv);
            Assert.IsTrue(freqCriteria.ShouldUseAnd());
        }
    }
}