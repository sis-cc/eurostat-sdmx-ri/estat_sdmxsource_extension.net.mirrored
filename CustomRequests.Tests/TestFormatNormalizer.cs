// -----------------------------------------------------------------------
// <copyright file="TestFormatNormalizer.cs" company="EUROSTAT">
//   Date Created : 2016-08-01
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace CustomRequests.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net.Mime;

    using Estat.Sdmxsource.Extension.Model;
    using Estat.Sdmxsource.Extension.Util;

    using NUnit.Framework;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Data;
    using Org.Sdmxsource.Sdmx.Api.Model.Format;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Builder.Common;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Data;

    /// <summary>
    /// The test format normalizer.
    /// </summary>
    [TestFixture]
    public class TestFormatNormalizer
    {
        /// <summary>
        /// The format normalizer
        /// </summary>
        private readonly FormatNormalizer _formatNormalizer = new FormatNormalizer();

        /// <summary>
        /// Tests the data default move first.
        /// </summary>
        [Test]
        public void TestDataDefaultMoveFirst()
        {
            List<IRestResponse<IDataFormat>> list = new List<IRestResponse<IDataFormat>>();

            list.Add(BuildDataRestResponse("text/json", DataEnumType.Json));
            list.Add(BuildDataRestResponse("application/vnd.sdmx.genericdata+xml;version=2.0", DataEnumType.Generic20));
            var defaultReposponse = BuildDataRestResponse("application/vnd.sdmx.genericdata+xml;version=2.1", DataEnumType.Generic21);
            list.Add(defaultReposponse);
            list.Add(BuildDataRestResponse("text/csv", DataEnumType.CrossSectional20));
            list.Add(BuildDataRestResponse("text/xml", DataEnumType.Generic21));
            Assert.AreEqual(this._formatNormalizer.NormalizeDataFormatOrder(list, RestApiVersion.V1).First(), defaultReposponse);
        }

        /// <summary>
        /// Tests the data default last move first.
        /// </summary>
        [Test]
        public void TestDataDefaultLastMoveFirst()
        {
            List<IRestResponse<IDataFormat>> list = new List<IRestResponse<IDataFormat>>();

            list.Add(BuildDataRestResponse("text/json", DataEnumType.Json));
            list.Add(BuildDataRestResponse("application/vnd.sdmx.genericdata+xml;version=2.0", DataEnumType.Generic20));
            var defaultReposponse = BuildDataRestResponse("application/vnd.sdmx.genericdata+xml;version=2.1", DataEnumType.Generic21);
            list.Add(defaultReposponse);
            Assert.AreEqual(this._formatNormalizer.NormalizeDataFormatOrder(list, RestApiVersion.V1).First(), defaultReposponse);
        }

        /// <summary>
        /// Tests the data default first move first.
        /// </summary>
        [Test]
        public void TestDataDefaultFirstMoveFirst()
        {
            List<IRestResponse<IDataFormat>> list = new List<IRestResponse<IDataFormat>>();
            var defaultReposponse = BuildDataRestResponse("application/vnd.sdmx.genericdata+xml;version=2.1", DataEnumType.Generic21);
            list.Add(defaultReposponse);
            list.Add(BuildDataRestResponse("text/json", DataEnumType.Json));
            list.Add(BuildDataRestResponse("application/vnd.sdmx.genericdata+xml;version=2.0", DataEnumType.Generic20));
        
            Assert.AreEqual(this._formatNormalizer.NormalizeDataFormatOrder(list, RestApiVersion.V1).First(), defaultReposponse);
        }

        /// <summary>
        /// Tests the data default empty.
        /// </summary>
        [Test]
        public void TestDataDefaultEmpty()
        {
            var normalizeDataFormatOrder = this._formatNormalizer.NormalizeDataFormatOrder(new List<IRestResponse<IDataFormat>>(), RestApiVersion.V1);
            Assert.NotNull(normalizeDataFormatOrder);
        }

        /// <summary>
        /// Tests the data no default.
        /// </summary>
        [Test]
        public void TestDataNoDefault()
        {
            List<IRestResponse<IDataFormat>> list = new List<IRestResponse<IDataFormat>>();

            list.Add(BuildDataRestResponse("text/json", DataEnumType.Json));
            list.Add(BuildDataRestResponse("application/vnd.sdmx.genericdata+xml;version=2.0", DataEnumType.Generic20));
            var normalizeDataFormatOrder = this._formatNormalizer.NormalizeDataFormatOrder(list, RestApiVersion.V1);
            Assert.NotNull(normalizeDataFormatOrder);
        }

        /// <summary>
        /// Tests the structure default move first.
        /// </summary>
        [Test]
        public void TestStructureDefaultMoveFirst()
        {
            List<IRestResponse<IStructureFormat>> list = new List<IRestResponse<IStructureFormat>>();

            list.Add(BuildStructureRestResponse("text/xml", StructureOutputFormatEnumType.SdmxV2RegistryQueryResponseDocument));
            list.Add(BuildStructureRestResponse("application/vnd.sdmx.structure+xml;version=2.0", StructureOutputFormatEnumType.SdmxV2StructureDocument));
            var defaultReposponse = BuildStructureRestResponse("application/vnd.sdmx.structure+xml;version=2.1", StructureOutputFormatEnumType.SdmxV21StructureDocument);
            list.Add(defaultReposponse);
            list.Add(BuildStructureRestResponse("text/csv", StructureOutputFormatEnumType.SdmxV21QueryResponseDocument));
            list.Add(BuildStructureRestResponse("text/xml", StructureOutputFormatEnumType.SdmxV21StructureDocument));
            Assert.AreEqual(this._formatNormalizer.NormalizeStructureFormatOrder(list, RestApiVersion.V1).First(), defaultReposponse);
        }

        /// <summary>
        /// Tests the structure default empty.
        /// </summary>
        [Test]
        public void TestStructureDefaultEmpty()
        {
            var normalizeDataFormatOrder = this._formatNormalizer.NormalizeStructureFormatOrder(new List<IRestResponse<IStructureFormat>>(), RestApiVersion.V1);
            Assert.NotNull(normalizeDataFormatOrder);
        }

        /// <summary>
        /// Tests the structure no default.
        /// </summary>
        [Test]
        public void TestStructureNoDefault()
        {
            var list = new List<IRestResponse<IStructureFormat>>();
            list.Add(BuildStructureRestResponse("text/xml", StructureOutputFormatEnumType.SdmxV2RegistryQueryResponseDocument));
            list.Add(BuildStructureRestResponse("application/vnd.sdmx.structure+xml;version=2.0", StructureOutputFormatEnumType.SdmxV2StructureDocument));
            var normalizeDataFormatOrder = this._formatNormalizer.NormalizeStructureFormatOrder(list, RestApiVersion.V1);
            Assert.NotNull(normalizeDataFormatOrder);
        }

        /// <summary>
        /// Builds the data rest response.
        /// </summary>
        /// <param name="contentType">
        /// Type of the content.
        /// </param>
        /// <param name="dataType">
        /// Type of the data.
        /// </param>
        /// <returns>
        /// The <see cref="RestResponse{IDataFormat}"/>.
        /// </returns>
        private static RestResponse<IDataFormat> BuildDataRestResponse(string contentType, DataEnumType dataType)
        {
            var responseContentHeaderJson = new ResponseContentHeader(new ContentType(contentType), null);
            var sdmxDataFormatCore = new SdmxDataFormatCore(DataType.GetFromEnum(dataType));
            var restResponse1 = new RestResponse<IDataFormat>(new Lazy<IDataFormat>(() => sdmxDataFormatCore), responseContentHeaderJson);
            return restResponse1;
        }

        /// <summary>
        /// Builds the structure rest response.
        /// </summary>
        /// <param name="contentType">
        /// Type of the content.
        /// </param>
        /// <param name="dataType">
        /// Type of the data.
        /// </param>
        /// <returns>
        /// The <see cref="RestResponse{IStructureFormat}"/>.
        /// </returns>
        private static RestResponse<IStructureFormat> BuildStructureRestResponse(string contentType, StructureOutputFormatEnumType dataType)
        {
            var responseContentHeaderJson = new ResponseContentHeader(new ContentType(contentType), null);
            var sdmxDataFormatCore = new SdmxStructureFormat(StructureOutputFormat.GetFromEnum(dataType));
            var restResponse1 = new RestResponse<IStructureFormat>(new Lazy<IStructureFormat>(() => sdmxDataFormatCore), responseContentHeaderJson);
            return restResponse1;
        }
    }
}