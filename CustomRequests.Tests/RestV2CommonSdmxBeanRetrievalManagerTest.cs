using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomRequests.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Estat.Sdmxsource.Extension.Manager;
    using Microsoft.VisualStudio.TestPlatform.CommunicationUtilities.Resources;
    using NSubstitute;
    using NUnit.Framework;
    using Org.Sdmxsource.Sdmx.Api.Builder.Common;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Factory;
    using Org.Sdmxsource.Sdmx.Api.Manager.Parse;
    using Org.Sdmxsource.Sdmx.Api.Manager.Query;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Common;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Builder.Common;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference.Common;
    using Org.Sdmxsource.Sdmx.Structureparser.Builder;
    using Org.Sdmxsource.Sdmx.Structureparser.Manager.Parsing;
    using Org.Sdmxsource.Util.Io;
    using static System.Net.WebRequestMethods;
    using Org.Sdmxsource.Sdmx.SdmxQueryBuilder.Manager;
    using Org.Sdmxsource.Sdmx.SdmxQueryBuilder.Factory;
    using Estat.Sdmxsource.Extension.Model;

    namespace SdmxStructureRetrievalTests
    {
        /**
     * Copyright (c) 2015 European Commission.
     *
     * Licensed under the EUPL, Version 1.1 or – as soon they
     * will be approved by the European Commission - subsequent
     * versions of the EUPL (the "Licence");
     * You may not use this work except in compliance with the
     * Licence.
     * You may obtain a copy of the Licence at:
     *
     * https://joinup.ec.europa.eu/software/page/eupl5
     *
     * Unless required by applicable law or agreed to in
     * writing, software distributed under the Licence is
     * distributed on an "AS IS" basis,
     * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
     * express or implied.
     * See the Licence for the specific language governing
     * permissions and limitations under the Licence.
     */
        [TestFixture]
        public class RestV2CommonSdmxBeanRetrievalManagerTest
        {
            private IStructureParsingManager structureParsingManager;
            private IReadableDataLocationFactory readableDataLocationFactory;
            private IStructureQueryWriterManager structureQueryWriterManager;
            private WsInfo _fusionSettings;
            private static string fusionUrl;

            private static string proxyHost;

            private static string proxyUserName;

            private static string proxyPassword;

            private static string fusionUser;

            private static string fusionPassword;

            [SetUp]
            public void SetUp()
            {
                this.structureParsingManager = new StructureParsingManager();
                this.readableDataLocationFactory = new ReadableDataLocationFactory();
                this.structureQueryWriterManager = new StructureQueryWriterManager(new[] { new RestStructureQueryWriterFactoryV2() });
                this._fusionSettings = new WsInfo()
                {
                    EndPoint = "http://10.240.126.65:8080/sdmx/v2",
                };
            }
            [Test]
            public void GetMaintainables()
            {
                RestV2CommonSdmxBeanRetrievalManager retrievalManager = GetRestV2CommonSdmxBeanRetrievalManager();
                ICommonStructureQuery structureQuery = CommonStructureQueryCore.Builder
                    .NewQuery(CommonStructureQueryType.Other, StructureOutputFormatEnumType.SdmxV21StructureDocument)
                    .SetMaintainableTarget(SdmxStructureEnumType.Dataflow)
                    .Build();
                
                var maintainables = retrievalManager.GetMaintainables(structureQuery);
                Assert.NotNull(maintainables);
                Assert.IsTrue(maintainables.Dataflows.Any());
            }

            private RestV2CommonSdmxBeanRetrievalManager GetRestV2CommonSdmxBeanRetrievalManager()
            {
                return new
                        RestV2CommonSdmxBeanRetrievalManager(_fusionSettings,
                        structureParsingManager,
                        readableDataLocationFactory, structureQueryWriterManager);
            }
            [Test]
            public void GetMaintainablesDoesntExists()
            {
                RestV2CommonSdmxBeanRetrievalManager retrievalManager = GetRestV2CommonSdmxBeanRetrievalManager();

                ICommonStructureQuery structureQuery = CommonStructureQueryCore.Builder
                    .NewQuery(CommonStructureQueryType.Other, StructureOutputFormatEnumType.SdmxV21StructureDocument)
                    .SetMaintainableTarget(SdmxStructureEnumType.Dataflow)
                    .SetMaintainableIds("DOESNOT_EXISTS")
                    .Build();

                Assert.Throws<SdmxNoResultsException>(() => retrievalManager.GetMaintainables(structureQuery));
    }

        [Test]
        public void GetMaintainablesWithCredentials()
        {
            this._fusionSettings.UserName = "root";
            this._fusionSettings.Password= "password";
            
                
            RestV2CommonSdmxBeanRetrievalManager retrievalManager = GetRestV2CommonSdmxBeanRetrievalManager();

            ICommonStructureQuery structureQuery = CommonStructureQueryCore.Builder
                        .NewQuery(CommonStructureQueryType.Other, StructureOutputFormatEnumType.SdmxV21StructureDocument)
                        .SetMaintainableTarget(SdmxStructureEnumType.Dataflow)
                        .Build();


            var maintainables = retrievalManager.GetMaintainables(structureQuery);
            Assert.NotNull(maintainables);
            Assert.IsTrue(maintainables.Dataflows.Any());
        }

        [Test]
        public void GetMaintainablesWithProxy()
        {
            this._fusionSettings.ProxyServer = "http://10.240.126.59:3128";
            this._fusionSettings.ProxyPassword = "d_UseThisAsACITNET_2FA_workaround";
            this._fusionSettings.ProxyUserName = "sdmx";
            RestV2CommonSdmxBeanRetrievalManager retrievalManager = GetRestV2CommonSdmxBeanRetrievalManager();

            ICommonStructureQuery structureQuery = CommonStructureQueryCore.Builder
                        .NewQuery(CommonStructureQueryType.Other, StructureOutputFormatEnumType.SdmxV21StructureDocument)
                        .SetMaintainableTarget(SdmxStructureEnumType.Dataflow)
                        .Build();


            var maintainables = retrievalManager.GetMaintainables(structureQuery);
            Assert.NotNull(maintainables);
            Assert.IsTrue(maintainables.Dataflows.Any());
        }
    }
}

}
