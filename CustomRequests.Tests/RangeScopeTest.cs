// -----------------------------------------------------------------------
// <copyright file="RangeScopeTest.cs" company="EUROSTAT">
//   Date Created : 2018-6-7
//   Copyright (c) 2009, 2018 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using NUnit.Framework;
using Org.Sdmxsource.Util;

namespace CustomRequests.Tests
{
    [TestFixture]
    public class RangeScopeTest
    {
        [TestCase("values=10-20", 10L, 20L)]
        [TestCase("values=1-10", 1L, 10L)]
        [TestCase("values=1-", 1L, null)]
        [TestCase("values= 1-10", 1L, 10L)]
        public void ShouldParseValidRangeHeader(string rangeHeaderValue, long lowerLimit, long? upperLimit)
        {
            var result = HeaderScope.ParseHeaderString(rangeHeaderValue);
            Assert.That(result, Is.Not.Null);
            Assert.That(result.Item1, Is.Not.Null);
            Assert.That(result.Item1.Value, Is.EqualTo(lowerLimit));
            Assert.That(result.Item2, Is.EqualTo(upperLimit));
        }

        [TestCase("values=20-10")]
        [TestCase("values:10-20")]
        [TestCase("values=10-20-30")]
        [TestCase("values=-10")]
        // valid but not supported
        [TestCase("values=10-20, 30-40")]
        public void ShouldNotParseUnsupportedRanges(string rangeHeaderValue)
        {
            var result = HeaderScope.ParseHeaderString(rangeHeaderValue);
            Assert.That(result, Is.Null);
        }
    }
}