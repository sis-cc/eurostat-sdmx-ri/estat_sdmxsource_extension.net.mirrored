// -----------------------------------------------------------------------
// <copyright file="CsvDataWriterEngineTests.cs" company="EUROSTAT">
//   Date Created : 2016-09-27
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using CsvHelper;
using Estat.Sdmxsource.Extension.Engine;
using Estat.Sdmxsource.Extension.Model;
using NUnit.Framework;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Engine;
using Org.Sdmxsource.Sdmx.Api.Manager.Parse;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Header;
using Org.Sdmxsource.Sdmx.Structureparser.Manager.Parsing;
using Org.Sdmxsource.Util.Io;

namespace CustomRequests.Tests
{
    [TestFixture]
    public class CsvDataWriterEngineTests
    {
        private StringWriter _stringWriter;

        [Test]
        public void TestWritingCsv()
        {
            _stringWriter = new StringWriter();
            IStructureParsingManager manager = new StructureParsingManager();
            IDataStructureObject dataStructure;
            using (var readable = new FileReadableDataLocation("tests/V21/Structure/test-sdmxv2.1-ESTAT+STS+2.0.xml"))
            {
                var structureWorkspace = manager.ParseStructures(readable);
                dataStructure = structureWorkspace.GetStructureObjects(false).DataStructures.First();
            }


           
                // initialize the data writing engine. It can be for SDMX versions 2.0 or 2.1
                IDataWriterEngine dataWriterEngine = new CsvDataWriterEngine(
                    new CsvDataFormat(DataType.GetFromEnum(DataEnumType.Compact21),new CsvWriterOptions(),Encoding.UTF8), _stringWriter);

                // write header
                dataWriterEngine.WriteHeader(new HeaderImpl("ZZ9", "ZZ9"));

                // start dataset
                dataWriterEngine.StartDataset(null, dataStructure,
                    new DatasetHeaderCore(null, DatasetAction.GetFromEnum(DatasetActionEnumType.Information),
                        new DatasetStructureReferenceCore(
                            null,
                            dataStructure.AsReference,
                            null,
                            null,
                            null)));

                // write dataset attributes
                dataWriterEngine.WriteAttributeValue("TITLE", "CompactDataWriter test");

                // write 2 group entries
                dataWriterEngine.StartGroup("SIBLING");
                dataWriterEngine.WriteGroupKeyValue("REF_AREA", "EL");
                dataWriterEngine.WriteGroupKeyValue("STS_INDICATOR", "PROD");
                dataWriterEngine.WriteGroupKeyValue("STS_ACTIVITY", "NS0030");
                dataWriterEngine.WriteGroupKeyValue("STS_INSTITUTION", "1");
                dataWriterEngine.WriteGroupKeyValue("ADJUSTMENT", "N");
                dataWriterEngine.WriteAttributeValue("COMPILATION", "test");

                dataWriterEngine.StartGroup("SIBLING");
                dataWriterEngine.WriteGroupKeyValue("REF_AREA", "EL");
                dataWriterEngine.WriteGroupKeyValue("STS_INDICATOR", "IND");
                dataWriterEngine.WriteGroupKeyValue("STS_ACTIVITY", "NS0030");
                dataWriterEngine.WriteGroupKeyValue("STS_INSTITUTION", "1");
                dataWriterEngine.WriteGroupKeyValue("ADJUSTMENT", "N");
                dataWriterEngine.WriteAttributeValue("COMPILATION", "test2");

                // write a series entry
                dataWriterEngine.StartSeries();
                dataWriterEngine.WriteSeriesKeyValue("FREQ", "A");
                dataWriterEngine.WriteSeriesKeyValue("REF_AREA", "EL");
                dataWriterEngine.WriteSeriesKeyValue("STS_INDICATOR", "PROD");
                dataWriterEngine.WriteSeriesKeyValue("STS_ACTIVITY", "NS0030");
                dataWriterEngine.WriteSeriesKeyValue("STS_INSTITUTION", "1");
                dataWriterEngine.WriteSeriesKeyValue("ADJUSTMENT", "N");
                dataWriterEngine.WriteAttributeValue("TIME_FORMAT", "P1Y");

                // write 2 observations for the abose series
                dataWriterEngine.WriteObservation("2001", "1.23");
                dataWriterEngine.WriteAttributeValue("OBS_STATUS", "A");
                dataWriterEngine.WriteAttributeValue("OBS_CONF", "F");

                dataWriterEngine.WriteObservation("2002", "4.56");
                dataWriterEngine.WriteAttributeValue("OBS_STATUS", "A");
                dataWriterEngine.WriteAttributeValue("OBS_CONF", "F");

                // write another series entry
            dataWriterEngine.StartSeries();
            dataWriterEngine.WriteSeriesKeyValue("FREQ", "A");
            dataWriterEngine.WriteSeriesKeyValue("REF_AREA", "EL");
            dataWriterEngine.WriteSeriesKeyValue("STS_INDICATOR", "IND");
            dataWriterEngine.WriteSeriesKeyValue("STS_ACTIVITY", "NS0030");
            dataWriterEngine.WriteSeriesKeyValue("STS_INSTITUTION", "1");
            dataWriterEngine.WriteSeriesKeyValue("ADJUSTMENT", "N");
            dataWriterEngine.WriteAttributeValue("TIME_FORMAT", "P1Y");

            // write 1 observation for the abose series
                dataWriterEngine.WriteObservation("2001", "7.89");
                dataWriterEngine.WriteAttributeValue("OBS_STATUS", "A");
                dataWriterEngine.WriteAttributeValue("OBS_CONF", "F");

                // close compact Writer
                dataWriterEngine.Close();


            string output = _stringWriter.GetStringBuilder().ToString().Replace("\r", string.Empty);
            var expectedOutput = @"FREQ,REF_AREA,STS_INDICATOR,STS_ACTIVITY,STS_INSTITUTION,ADJUSTMENT,TIME_FORMAT,TIME_PERIOD,OBS_VALUE,OBS_STATUS,OBS_CONF
A,EL,PROD,NS0030,1,N,P1Y,2001,1.23,A,F
A,EL,PROD,NS0030,1,N,P1Y,2002,4.56,A,F
A,EL,IND,NS0030,1,N,P1Y,2001,7.89,A,F
".Replace("\r", string.Empty);
            Assert.That(output,Is.EqualTo(expectedOutput));
        }


        [Test]
        public void TestConfiguration()
        {
            var csvWriter = new CsvWriter(Console.Out, System.Globalization.CultureInfo.InvariantCulture, false);

            //csvWriter.Configuration.Delimiter = "/";
            //csvWriter.Configuration.QuoteNoFields = true;
            //Console.WriteLine(csvWriter.Configuration.QuoteRequiredChars);
            csvWriter.WriteList(new List<string>()
            {
                "a","b","33,44,55",
            });
        }
    }
}