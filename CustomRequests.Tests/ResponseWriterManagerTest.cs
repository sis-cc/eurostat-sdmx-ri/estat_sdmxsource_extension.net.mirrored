// -----------------------------------------------------------------------
// <copyright file="ResponseWriterManagerTest.cs" company="EUROSTAT">
//   Date Created : 2017-11-16
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using Estat.Sdmxsource.Extension.Manager;
using Estat.Sdmxsource.Extension.Factory;
using Estat.Sdmxsource.Extension.Model.Error;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Util.Objects.Reference;
using Estat.Sdmxsource.Extension.Constant;
using NUnit.Framework;
using System.IO;
using Org.Sdmxsource.XmlHelper;
using System.Xml;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Header;
using System.Globalization;
using Org.Sdmxsource.Sdmx.Api.Factory;
using Org.Sdmxsource.Util.Io;
using Org.Sdmxsource.Sdmx.Util.Sdmx;
using System;

namespace CustomRequests.Tests
{
    /// <summary>
    /// Test for repsonse writer
    /// </summary>
    public class ResponseWriterManagerTest
    {
        /// <summary>
        /// The count
        /// </summary>
        private static int _count;

        /// <summary>
        /// The response writer manager
        /// </summary>
        private readonly IResponseWriterManager _responseWriterManager;

        /// <summary>
        /// The test input
        /// </summary>
        private readonly IList<IResponseWithStatusObject> _testInput;
        /// <summary>
        /// The header
        /// </summary>
        private readonly HeaderImpl _header;

        /// <summary>
        /// The readable data location factory
        /// </summary>
        private readonly IReadableDataLocationFactory _readableDataLocationFactory;

        /// <summary>
        /// Initializes a new instance of the <see cref="ResponseWriterManagerTest"/> class.
        /// </summary>
        public ResponseWriterManagerTest()
        {
            _responseWriterManager = new ResponseWriterManager(new ErrorMessage21WriterFactory(), new SubmitStructureResponseWriterFactory());
            _testInput = BuildTestInput();
            _header = new HeaderImpl(string.Format(CultureInfo.InvariantCulture, "IREF{0:000000}", _count), "ZZ9");
            _count++;
            _header.AddReciever(new PartyCore(null, "TEST", null, null));
            _readableDataLocationFactory = new ReadableDataLocationFactory();
        }

        /// <summary>
        /// Shoulds the produce valid error message.
        /// </summary>
        [Test]
        public void ShouldProduceValidErrorMessage()
        {
            var file = new FileInfo("ShouldProduceValidErrorMessage-should_be_valid.xml");

            using (var stream = file.Create())
            {
                var format = new ErrorMessageFormat(stream, Encoding.UTF8);
                _responseWriterManager.Write(format, _testInput);
                stream.Flush();
            }

            ValidateSdmxSchema(file);

            ValidateSdmxMessageType(file, MessageEnumType.Error);
        }

        /// <summary>
        /// Shoulds the content of the produce valid error message with specific.
        /// </summary>
        [Test]
        public void ShouldProduceValidErrorMessageWithSpecificContent()
        {
            var file = new FileInfo("ShouldProduceValidErrorMessageWithSpecificContent-should_be_valid.xml");

            using (var stream = file.Create())
            {
                var format = new ErrorMessageFormat(stream, Encoding.UTF8);
                _responseWriterManager.Write(format, _testInput);
                stream.Flush();
            }

            var expectedMessages = new Queue<IMessageObject>(_testInput.SelectMany(x => x.Messages));
            var rootIsError = false;
            IMessageObject current = null;
            int textIndex = 0;
            ITextTypeWrapper currentText = null;
            string xmlText = null;
            using (var stream = file.OpenRead())
            {
                using (var reader = XMLParser.CreateSdmxMlReader(stream, SdmxSchemaEnumType.VersionTwoPointOne))
                {
                    while(reader.Read())
                    {
                        var nodeType = reader.NodeType;
                        switch(nodeType)
                        {
                            case XmlNodeType.Element:
                                {
                                    var localName = reader.LocalName;
                                    switch(localName)
                                    {
                                        case "Error":
                                            Assert.That(!rootIsError);
                                            rootIsError = true;
                                            break;
                                        case "ErrorMessage":
                                            Assert.That(rootIsError);
                                            Assert.That(expectedMessages, Is.Not.Empty);
                                            current = expectedMessages.Dequeue();
                                            textIndex = 0;
                                            currentText = null;
                                            xmlText = null;
                                            var code = reader.GetAttribute("code");
                                            string expectedCode = null;
                                            if (current.ErrorCode == null)
                                            {
                                                expectedCode =((int)current.HttpCode).ToString(CultureInfo.InvariantCulture);
                                            }
                                            else
                                            {
                                                expectedCode = current.ErrorCode?.ClientErrorCode.ToString(CultureInfo.InvariantCulture);
                                            }

                                            Assert.That(code, Is.EqualTo(expectedCode));
                                            break;
                                        case "Text":
                                            Assert.That(rootIsError);
                                            Assert.That(current, Is.Not.Null);
                                            Assert.That(current.Text, Has.Count.GreaterThan(textIndex));
                                            currentText = current.Text[textIndex];
                                            textIndex++;
                                            var lang = reader.XmlLang;
                                            Assert.That(currentText.Locale, Is.EqualTo(lang));
                                            break;
                                    }
                               }
                                break;
                            case XmlNodeType.Text:
                                xmlText = reader.Value;
                                break;
                            case XmlNodeType.EndElement:
                                {
                                    var localName = reader.LocalName;
                                    switch(localName)
                                    {
                                        case "Text":
                                            Assert.That(xmlText, Is.Not.Null.And.Not.Empty);
                                            Assert.That(currentText, Is.Not.Null);
                                            Assert.That(xmlText, Is.EqualTo(currentText.Value));
                                            xmlText = null;
                                            currentText = null;
                                            break;
                                    }
                                }

                                break;
                        }
                    }
                }
            }

            Assert.That(rootIsError);
        }

        /// <summary>
        /// Shoulds the content of the produce valid submit structure response with.
        /// </summary>
        [Test]
        public void ShouldProduceValidSubmitStructureResponseWithContent()
        {
            var file = new FileInfo("ShouldProduceValidSubmitStructureResponseWithContent-should_be_valid.xml");

            using (var writer = XmlWriter.Create(file.FullName))
            {
                var format = new SubmitStructureResponse21Format(writer, _header, true);
                _responseWriterManager.Write(format, _testInput);
                writer.Flush();
            }

            ValidateSdmxSchema(file);
            ValidateSdmxMessageType(file, MessageEnumType.RegistryInterface);

            var expectedResponses = new Queue<IResponseWithStatusObject>(_testInput);
            var expectedMessages = new Queue<IMessageObject>();
            var rootIsError = false;
            IResponseWithStatusObject currentResponse = null;
            IMessageObject current = null;
            int textIndex = 0;
            ITextTypeWrapper currentText = null;
            string xmlText = null;
            using (var stream = file.OpenRead())
            {
                using (var reader = XMLParser.CreateSdmxMlReader(stream, SdmxSchemaEnumType.VersionTwoPointOne))
                {
                    while (reader.Read())
                    {
                        var nodeType = reader.NodeType;
                        switch (nodeType)
                        {
                            case XmlNodeType.Element:
                                {
                                    var localName = reader.LocalName;
                                    switch (localName)
                                    {
                                        case "SubmitStructureResponse":
                                            Assert.That(!rootIsError);
                                            rootIsError = true;
                                            break;
                                        case "SubmissionResult":
                                            currentResponse = expectedResponses.Dequeue();
                                            break;
                                        case "SubmittedStructure":
                                            Assert.That(currentResponse, Is.Not.Null);
                                            var action = reader.GetAttribute("action");
                                            Assert.That(currentResponse.Action.Action, Is.EqualTo(action));
                                            break;
                                        case "StatusMessage":
                                            Assert.That(currentResponse, Is.Not.Null);
                                            var status = reader.GetAttribute("status");
                                            Assert.That(currentResponse.Status.ToString(), Is.EqualTo(status));
                                            Assert.That(expectedMessages, Is.Empty);
                                            foreach (var item in currentResponse.Messages)
                                            {
                                                expectedMessages.Enqueue(item);
                                            }

                                            break;
                                        case "MessageText":
                                            Assert.That(currentResponse, Is.Not.Null);
                                            Assert.That(rootIsError);
                                            Assert.That(expectedMessages, Is.Not.Empty);
                                            current = expectedMessages.Dequeue();
                                            textIndex = 0;
                                            currentText = null;
                                            xmlText = null;
                                            var code = reader.GetAttribute("code");
                                            Assert.That(code, Is.EqualTo(current.ErrorCode?.ClientErrorCode.ToString(CultureInfo.InvariantCulture)));
                                            break;
                                        case "Text":
                                            Assert.That(rootIsError);
                                            Assert.That(current, Is.Not.Null);
                                            Assert.That(current.Text, Has.Count.GreaterThan(textIndex));
                                            currentText = current.Text[textIndex];
                                            textIndex++;
                                            var lang = reader.XmlLang;
                                            Assert.That(currentText.Locale, Is.EqualTo(lang));
                                            break;
                                    }
                                }
                                break;
                            case XmlNodeType.Text:
                                xmlText = reader.Value;
                                break;
                            case XmlNodeType.EndElement:
                                {
                                    var localName = reader.LocalName;
                                    switch (localName)
                                    {
                                        case "Text":
                                            Assert.That(xmlText, Is.Not.Null.And.Not.Empty);
                                            Assert.That(currentText, Is.Not.Null);
                                            Assert.That(xmlText, Is.EqualTo(currentText.Value));
                                            xmlText = null;
                                            currentText = null;
                                            break;
                                        case "URN":
                                            Assert.That(currentResponse, Is.Not.Null);
                                            Assert.That(xmlText, Is.EqualTo(currentResponse.StructureReference.TargetUrn.ToString()));
                                            xmlText = null;
                                            break;
                                    }
                                }

                                break;
                        }
                    }
                }
            }

            Assert.That(rootIsError);
        }

        /// <summary>
        /// Shoulds the produce valid submit structure response.
        /// </summary>
        [Test]
        public void ShouldProduceValidSubmitStructureResponse()
        {
            var file = new FileInfo("ShouldProduceValidSubmitStructureResponse-should_be_valid.xml");

            using (var writer = XmlWriter.Create(file.FullName))
            {
                var format = new SubmitStructureResponse21Format(writer, _header, true);
                _responseWriterManager.Write(format, _testInput);
                writer.Flush();
            }

            ValidateSdmxSchema(file);
            ValidateSdmxMessageType(file, MessageEnumType.RegistryInterface);
            ValidateSdmxMessageType(file, RegistryMessageEnumType.SubmitStructureResponse);
        }

        /// <summary>
        /// Validates the SDMX schema.
        /// </summary>
        /// <param name="file">The file.</param>
        private static void ValidateSdmxSchema(FileInfo file)
        {
            using (var stream = file.OpenRead())
            {
                XMLParser.ValidateXml(stream, SdmxSchemaEnumType.VersionTwoPointOne);
            }
        }

        /// <summary>
        /// Builds the test input.
        /// </summary>
        /// <returns></returns>
        private IList<IResponseWithStatusObject> BuildTestInput()
        {
            var list = new List<IResponseWithStatusObject>();

            var text = new List<ITextTypeWrapper>();
            text.Add(new TextTypeWrapperImpl("en", "Test 1", null));
            text.Add(new TextTypeWrapperImpl("el", "Δοκιμή 1", null));

            var messages1 = new List<IMessageObject>();
            messages1.Add(new MessageObject(text, SdmxErrorCode.GetFromEnum(SdmxErrorCodeEnumType.NoResultsFound)));
            messages1.Add(new MessageObject(text, SdmxErrorCode.GetFromEnum(SdmxErrorCodeEnumType.NoResultsFound)));
            messages1.Add(new MessageObject(new ITextTypeWrapper[0], SdmxErrorCode.GetFromEnum(SdmxErrorCodeEnumType.Conflict)));

            list.Add(new ResponseWithStatusObject(
                messages1,
                ResponseStatus.Failure,
                new StructureReferenceImpl("TESTAGENCY", "TEST_ID", "1.0", SdmxStructureEnumType.Dataflow),
                DatasetAction.GetFromEnum(DatasetActionEnumType.Append)));

            var messages2 = new List<IMessageObject>();
            list.Add(new ResponseWithStatusObject(
                new List<IMessageObject> { new MessageObject(new ITextTypeWrapper[] { new TextTypeWrapperImpl("en", "Updated: Codelist TESTAGENCY:TEST_CL(1.0)", null) }, System.Net.HttpStatusCode.Created) },
                ResponseStatus.Success,
                new StructureReferenceImpl("TESTAGENCY", "TEST_CL", "1.0", SdmxStructureEnumType.CodeList),
                DatasetAction.GetFromEnum(DatasetActionEnumType.Replace)));
            list.Add(new ResponseWithStatusObject(
                new List<IMessageObject> { new MessageObject(new ITextTypeWrapper[] { new TextTypeWrapperImpl("en", "Updated: Codelist TESTAGENCY:TEST_CL(1.1)", null) }, System.Net.HttpStatusCode.Created) },
                ResponseStatus.Success,
                new StructureReferenceImpl("TESTAGENCY", "TEST_CL", "1.1", SdmxStructureEnumType.CodeList),
                DatasetAction.GetFromEnum(DatasetActionEnumType.Replace)));
            list.Add(new ResponseWithStatusObject(
                new List<IMessageObject> { new MessageObject(new ITextTypeWrapper[] { new TextTypeWrapperImpl("en", "Updated: Codelist TESTAGENCY:TEST_CL(1.2)", null) }, System.Net.HttpStatusCode.Created) },
                ResponseStatus.Success,
                new StructureReferenceImpl("TESTAGENCY", "TEST_CL", "1.2", SdmxStructureEnumType.CodeList),
                DatasetAction.GetFromEnum(DatasetActionEnumType.Replace)));

            return list;
        }

        /// <summary>
        /// Validates the type of the SDMX message.
        /// </summary>
        /// <param name="file">The file.</param>
        /// <param name="messageType">Type of the message.</param>
        private void ValidateSdmxMessageType(FileInfo file, MessageEnumType messageType)
        {
            using (var dataLocation = _readableDataLocationFactory.GetReadableDataLocation(file))
            {
                var messageEnumType = SdmxMessageUtil.GetMessageType(dataLocation);
                Assert.That(messageEnumType, Is.Not.Null);
                Assert.That(messageEnumType, Is.EqualTo(messageType));
            }
        }

        /// <summary>
        /// Validates the type of the SDMX message.
        /// </summary>
        /// <param name="file">The file.</param>
        /// <param name="messageType">Type of the message.</param>
        private void ValidateSdmxMessageType(FileInfo file, RegistryMessageEnumType messageType)
        {
            using (var dataLocation = _readableDataLocationFactory.GetReadableDataLocation(file))
            {
                var messageEnumType = SdmxMessageUtil.GetRegistryMessageType(dataLocation);
                Assert.That(messageEnumType, Is.Not.Null);
                Assert.That(messageEnumType, Is.EqualTo(messageType));
            }
        }
    }
}
