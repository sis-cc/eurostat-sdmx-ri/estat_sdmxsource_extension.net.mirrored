// -----------------------------------------------------------------------
// <copyright file="SubmitRegistrationEngineTest.cs" company="EUROSTAT">
//   Date Created : 2017-12-4
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using Estat.Sdmxsource.Extension.Model;
using NUnit.Framework;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Registry;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Registry;
using Org.Sdmxsource.Sdmx.Util.Objects.Reference;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using log4net;
using log4net.Config;

namespace CustomRequests.Tests
{
    using Estat.Sdmxsource.Extension.Engine.WebService;
    using Org.Sdmxsource.Sdmx.Structureparser.Builder.XmlSerialization.V21.RegistryRequest;

    class SubmitRegistrationEngineTest
    {
        public SubmitRegistrationEngineTest()
        {
            var logRepository = LogManager.GetRepository(Assembly.GetEntryAssembly());
            XmlConfigurator.Configure(logRepository, new FileInfo("log4net.config"));
        }

        [Test, Ignore("Global registry dropped support for SOAP")]
        [Category("RequiresInternet")]
        public void SubmitRegistrationEngineShouldFailSubmittingSoapMessage()
        {
            WsInfo wsInfo = new WsInfo();
            wsInfo.EndPoint ="https://registry.sdmx.org/FusionRegistry/ws/soap/query";
            wsInfo.Wsdl = "https://registry.sdmx.org/FusionRegistry/ws/soap/sdmxSoap.wsdl";
            IMessageSubmitterEngine soapMessageWriter = new SoapRegistryInterfaceRequestSubmitter(new SoapMessageSubmitter(wsInfo, new SoapMessageWriterEngine()));
            SubmitRegistrationEngine engine = new SubmitRegistrationEngine();
            IRegistrationMutableObject registrationMutableObject = new RegistrationMutableCore();
            registrationMutableObject.Id = "TEST_ID";
            registrationMutableObject.ProvisionAgreementRef = new StructureReferenceImpl("TEST_AGENCY", "TEST_PA1", "1.0", SdmxStructureEnumType.ProvisionAgreement);
            registrationMutableObject.DataSource = new DataSourceMutableCore();
            registrationMutableObject.DataSource.DataUrl = new Uri("http://localhost/ws/rest/");
            registrationMutableObject.DataSource.RESTDatasource = true;

            Assert.Throws<WebException>(()=>engine.Register(soapMessageWriter, new[] { registrationMutableObject.ImmutableInstance }, DatasetActionEnumType.Append));
        }

        [Test, Ignore("TEST Global registry down")]
        [Category("RequiresInternet")]
        public void SubmitToGlobalRegistry()
        {
            WsInfo wsInfo = new WsInfo();
            wsInfo.EndPoint ="https://test2.registry.sdmx.org/FusionRegistry/ws/rest/";
            IMessageSubmitterEngine soapMessageWriter = new GlobalRegistryRestSubmitterEngine(wsInfo);
            SubmitRegistrationEngine engine = new SubmitRegistrationEngine();
            IRegistrationMutableObject registrationMutableObject = new RegistrationMutableCore();
            registrationMutableObject.Id = "REG-SOAP";
            registrationMutableObject.LastUpdated = new DateTime(2017, 10, 10);
            registrationMutableObject.ValidFrom = new DateTime(2017, 10, 10);
            registrationMutableObject.ValidTo = new DateTime(2018, 12, 31);
            registrationMutableObject.ProvisionAgreementRef = new StructureReferenceImpl("urn:sdmx:org.sdmx.infomodel.registry.ProvisionAgreement=ESTAT:DATA_REG_TEST(1.0)");
            registrationMutableObject.DataSource = new DataSourceMutableCore();
            registrationMutableObject.DataSource.DataUrl = new Uri("https://webgate.ec.europa.eu/nsi-jax-ws/rest/data/ESTAT,NAMAIN_IDC_N,1.9?detail=full&updatedAfter=2018-01-24T00:00:00.000");
            registrationMutableObject.DataSource.RESTDatasource = false;
            registrationMutableObject.DataSource.WebServiceDatasource = true;

            var responces = engine.Register(soapMessageWriter, new[] { registrationMutableObject.ImmutableInstance }, DatasetActionEnumType.Append);
            Assert.That(responces, Is.Not.Empty.And.Not.Null);
            Assert.That(!responces.Any(x => x.IsError),string.Join("\n", responces.Where(r => r.IsError).SelectMany(r => r.ErrorList.ErrorMessage)));

        }

        /// <summary>
        /// Shoulds the generate option1 request.
        /// </summary>
        [Test]
        public void ShouldGenerateOption1Request()
        {
            var builder = new SubmitRegistrationBuilder();
            IRegistrationMutableObject registrationMutableObject = new RegistrationMutableCore();
            registrationMutableObject.Id = "TEST_ID";
            registrationMutableObject.LastUpdated = new DateTime(2017, 10, 10);
            registrationMutableObject.ValidFrom = new DateTime(2017, 10, 10);
            registrationMutableObject.ValidTo = new DateTime(2018, 12, 31);
            registrationMutableObject.ProvisionAgreementRef = new StructureReferenceImpl("urn:sdmx:org.sdmx.infomodel.registry.ProvisionAgreement=ESTAT:DATA_REG_TEST(1.0)");
            registrationMutableObject.DataSource = new DataSourceMutableCore();
            registrationMutableObject.DataSource.DataUrl = new Uri("https://webgate.ec.europa.eu/nsi-jax-ws/rest/data/ESTAT,NAMAIN_IDC_N,1.9?detail=full&updatedAfter=2018-01-24T00:00:00.000");
            registrationMutableObject.DataSource.RESTDatasource = true;
            registrationMutableObject.DataSource.WebServiceDatasource = true;
            var output = builder.BuildRegistryInterfaceDocument(new[] { registrationMutableObject.ImmutableInstance }, DatasetActionEnumType.Append);
            output.Save("ShouldGenerateOption1Request.xml");
        }

        [Test, Ignore("Incomplete. The WebResponse needs to be mocked")]
        public void SubmitRegistrationEngineShouldWtriteSoapMessage()
        {
            ISoapMessageSubmitterEngine soapMessageWriter = new NoOpSoap(new SoapMessageWriterEngine());
            IMessageSubmitterEngine messageWriter = new SoapRegistryInterfaceRequestSubmitter(soapMessageWriter);
            SubmitRegistrationEngine engine = new SubmitRegistrationEngine();
            IRegistrationMutableObject registrationMutableObject = new RegistrationMutableCore();
            registrationMutableObject.Id = "TEST_ID";
            registrationMutableObject.ProvisionAgreementRef = new StructureReferenceImpl("TEST_AGENCY", "TEST_PA1", "1.0", SdmxStructureEnumType.ProvisionAgreement);
            registrationMutableObject.DataSource = new DataSourceMutableCore();
            registrationMutableObject.DataSource.DataUrl = new Uri("http://localhost/ws/rest/");
            registrationMutableObject.DataSource.RESTDatasource = true;

            var responces = engine.Register(messageWriter, new[] { registrationMutableObject.ImmutableInstance }, DatasetActionEnumType.Append);
        }

        private class NoOpSoap : ISoapMessageSubmitterEngine
        {
            private readonly ISoapMessageWriter _messageWriter;

            /// <summary>
            /// Initializes a new instance of the <see cref="NoOpSoap"/> class.
            /// </summary>
            /// <param name="messageWriter">The message writer.</param>
            public NoOpSoap(ISoapMessageWriter messageWriter)
            {
                _messageWriter = messageWriter;
            }

            /// <summary>
            /// Writes the specified body writer.
            /// </summary>
            /// <param name="bodyWriter">The body writer.</param>
            /// <param name="operation">The operation.</param>
            /// <returns>
            /// The WebResponse.
            /// </returns>
            public WebResponse Submit(Action<XmlWriter> bodyWriter, string operation)
            {
                FileInfo outputFile = new FileInfo("NoOpSoap.xml");
                using(var stream = outputFile.Create())
                {
                    SoapRequestInfo soapRequestInfo = new SoapRequestInfo();
                    soapRequestInfo.Operation = operation;
                    soapRequestInfo.ParameterName = null;
                    soapRequestInfo.Prefix = "web";
                    soapRequestInfo.WriteOperation = true;
                    soapRequestInfo.TargetNamespace = "http://change.me/uri";
                    _messageWriter.Write(bodyWriter, stream, soapRequestInfo);
                    stream.Flush();
                }

                using(var xml = XmlReader.Create(outputFile.FullName))
                {
                    while(xml.Read())
                    {

                    }
                }

                return null;
            }

            /// <summary>
            /// Writes the specified document.
            /// </summary>
            /// <param name="document">The document.</param>
            /// <param name="operation">The operation.</param>
            /// <returns></returns>
            public WebResponse Submit(XmlNode document, string operation)
            {
                return this.Submit(x => document.WriteTo(x), operation);
            }

            /// <summary>
            /// Writes the specified document.
            /// </summary>
            /// <param name="document">The document.</param>
            /// <param name="operation">The operation.</param>
            /// <returns></returns>
            public WebResponse Submit(XElement document, string operation)
            {
                return this.Submit(x => document.WriteTo(x), operation);
            }
        }
    }
}
