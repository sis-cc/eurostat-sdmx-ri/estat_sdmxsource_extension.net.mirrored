﻿// -----------------------------------------------------------------------
// <copyright file="SoapSdmxObjectRetrievalManagerTests.cs" company="EUROSTAT">
//   Date Created : 2017-06-14
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace CustomRequests.Tests
{
    using System;

    using Estat.Sdmxsource.Extension.Manager;
    using Estat.Sdmxsource.Extension.Model;

    using NUnit.Framework;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;

    [TestFixture]
    internal class SoapSdmxObjectRetrievalManagerTests
    {
        [Test]
        [Category("RequiresInternet")]
        public void ShouldGetSdmxObjects20()
        {
            //
            var soapSdmxObjectRetrievalManager = new SoapSdmxObjectRetrievalManager(new WsInfo
            {
                EndPoint = "https://ec.europa.eu/tools/cspa_services_global/sdmxregistry/v2.0/SdmxRegistryServicePS"
            },SdmxSchema.GetFromEnum(SdmxSchemaEnumType.VersionTwo));

            IMaintainableRefObject maintainableRefObjectImpl = new MaintainableRefObjectImpl
            {
                MaintainableId = "CL_UNIT",
                AgencyId = "ESTAT",
                Version = "1.0"

                //MaintainableId = registryQuery.Id
            };
            var sdmxStructureEnumType = SdmxStructureEnumType.CodeList;
            
            var structureReferenceImpl = new StructureReferenceImpl(maintainableRefObjectImpl, sdmxStructureEnumType);

            var sdmxObjects = soapSdmxObjectRetrievalManager.GetSdmxObjects(structureReferenceImpl, ResolveCrossReferences.DoNotResolve);
            Assert.That(sdmxObjects.Codelists.Count, Is.EqualTo(1));
        }

        [Test]
        [Category("RequiresInternet")]
        public void ShouldGetSdmxObjects21()
        {
            //
            var soapSdmxObjectRetrievalManager = new SoapSdmxObjectRetrievalManager(new WsInfo
            {
                EndPoint = "https://ec.europa.eu/tools/cspa_services_global/sdmxregistry/v2.1/SdmxRegistryServicePS"
            },SdmxSchema.GetFromEnum(SdmxSchemaEnumType.VersionTwoPointOne));

            IMaintainableRefObject maintainableRefObjectImpl = new MaintainableRefObjectImpl
            {
                MaintainableId = "CL_UNIT",
                AgencyId = "ESTAT",
                Version = "1.0"
            };

            var sdmxStructureEnumType = SdmxStructureEnumType.CodeList;
            var structureReferenceImpl = new StructureReferenceImpl(maintainableRefObjectImpl, sdmxStructureEnumType);
            var sdmxObjects = soapSdmxObjectRetrievalManager.GetSdmxObjects(structureReferenceImpl,ResolveCrossReferences.DoNotResolve);
            Assert.That(sdmxObjects.Codelists.Count, Is.EqualTo(1));
            var sdmxStructureEnumType2 = SdmxStructureEnumType.CodeList;

            var restStructureQueryCore = new RESTStructureQueryCore(StructureQueryDetail.GetFromEnum(StructureQueryDetailEnumType.Full),
                StructureReferenceDetail.GetFromEnum(StructureReferenceDetailEnumType.None),
                null, new StructureReferenceImpl(maintainableRefObjectImpl, sdmxStructureEnumType2),
                true);
            sdmxObjects = soapSdmxObjectRetrievalManager.GetMaintainables(restStructureQueryCore);

            Assert.That(sdmxObjects.Codelists.Count, Is.EqualTo(1));
        }
    }
}