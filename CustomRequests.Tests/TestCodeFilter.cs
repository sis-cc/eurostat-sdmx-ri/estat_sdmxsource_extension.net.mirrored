using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Estat.Sdmxsource.Extension.Util;
using NUnit.Framework;
using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Codelist;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Codelist;

namespace CustomRequests.Tests
{
    internal class TestCodeFilter
    {
        private static readonly string Locale = "en";
        private static ICodelistMutableObject smallCodelist = BuildTestCodelist();
        private static ICodelistMutableObject bigCodelist = BuildBigTestCodelist();



        [Test]
        public void ShouldReturnFullHierarchyOfOneCode()
        {
            ICodelistMutableObject codelist = smallCodelist;
            var filter = new CodelistFilter();

            ISet<string> available = new HashSet<string>(new  string [] {"270"});
            List<ICodeMutableObject> result = filter.For(codelist, available).ToList();
            Assert.That(result.Count, Is.EqualTo(5));
            Assert.That(result.Any(c => c.Id.Equals("270", StringComparison.Ordinal)), "270");
            Assert.That(result.Any(c => c.Id.Equals("011", StringComparison.Ordinal)), "011");
            Assert.That(result.Any(c => c.Id.Equals("202", StringComparison.Ordinal)), "202");
            Assert.That(result.Any(c => c.Id.Equals("002", StringComparison.Ordinal)), "002");
            Assert.That(result.Any(c => c.Id.Equals("001", StringComparison.Ordinal)), "001");
        }
        [Test, MaxTime(500)]
        public void ShouldRunFast()
        {
            ICodelistMutableObject codelist = bigCodelist;
            var filter = new CodelistFilter();

            ISet<string> available = new HashSet<string>(new  string [] {"270", "011", "818", "T1"}, StringComparer.Ordinal);
            filter.For(codelist, available);
        }

        [Test]
        public void ShouldReturnFullHierarchyOfManyCodes()
        {
            ICodelistMutableObject codelist = smallCodelist;
            var filter = new CodelistFilter();

            ISet<string> available = new HashSet<string>(new  string [] {"818", "204", "132"});
            List<ICodeMutableObject> result = filter.For(codelist, available).ToList();
            Assert.That(result.Count, Is.EqualTo(8));
            Assert.That(result.Any(c => c.Id.Equals("818", StringComparison.Ordinal)), "818");
            Assert.That(result.Any(c => c.Id.Equals("204", StringComparison.Ordinal)), "204");
            Assert.That(result.Any(c => c.Id.Equals("132", StringComparison.Ordinal)), "132");
            Assert.That(result.Any(c => c.Id.Equals("011", StringComparison.Ordinal)), "011");
            Assert.That(result.Any(c => c.Id.Equals("015", StringComparison.Ordinal)), "015");
            Assert.That(result.Any(c => c.Id.Equals("202", StringComparison.Ordinal)), "202");
            Assert.That(result.Any(c => c.Id.Equals("002", StringComparison.Ordinal)), "002");
            Assert.That(result.Any(c => c.Id.Equals("001", StringComparison.Ordinal)), "001");
        }
        [Test]
        public void ShouldReturnOnlyRoot()
        {
            ICodelistMutableObject codelist = smallCodelist;
            var filter = new CodelistFilter();

            ISet<string> available = new HashSet<string>(new  string [] {"001"});
            List<ICodeMutableObject> result = filter.For(codelist, available).ToList();
            Assert.That(result.Count, Is.EqualTo(1));
            Assert.That(result.Any(c => c.Id.Equals("001", StringComparison.Ordinal)), "001");
        }

        private static ICodelistMutableObject BuildTestCodelist()
        {
            ICodelistMutableObject codelist = new CodelistMutableCore { Id = "CL_REF_AREA", AgencyId = "TEST", Version = "1.0.0" };
            codelist.AddName(Locale, "Test");

            ICodeMutableObject world = new CodeMutableCore { Id = "001" };
            world.AddName(Locale, "World");
            codelist.AddItem(world);

            AddCode(codelist, "002", "Africa", "001");
            AddCode(codelist, "270", "Gambia", "011");
            AddCode(codelist, "011", "Western Africa", "202");
            AddCode(codelist, "202", "Sub-Saharan Africa", "002");
            AddCode(codelist, "204", "Benin", "011");
            AddCode(codelist, "132", "Cabo Verde", "011");
            AddCode(codelist, "015", "Northen Africa", "002");
            AddCode(codelist, "012", "Algeria", "015");
            AddCode(codelist, "818", "Egypt", "015");

            return codelist;

        }

        private static ICodelistMutableObject BuildBigTestCodelist()
        {
            ICodelistMutableObject codelist = BuildTestCodelist();
            string name = "test";
            for(int i=0; i< 1000000; i++)
            {
                AddCode(codelist, string.Format(CultureInfo.InvariantCulture, "T{0}", i), name, "015");
            }

            return codelist;
        }

        private static  void AddCode(ICodelistMutableObject codelist, string id, string name, string parent)
        {
            ICodeMutableObject africa = new CodeMutableCore { Id = id, ParentCode = parent };
            africa.AddName(Locale, name);
            codelist.AddItem(africa);
        }
    }
}
