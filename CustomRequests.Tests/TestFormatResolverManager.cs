// -----------------------------------------------------------------------
// <copyright file="TestFormatResolverManager.cs" company="EUROSTAT">
//   Date Created : 2016-08-01
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using NSubstitute;
using Org.Sdmxsource.Sdmx.Api.Exception;

namespace CustomRequests.Tests
{
    using System.Globalization;
    using System.Net;
    using System.Net.Mime;

    using Estat.Sdmxsource.Extension.Builder;
    using Estat.Sdmxsource.Extension.Factory;
    using Estat.Sdmxsource.Extension.Manager;
    using Estat.Sdmxsource.Extension.Model;

    using NUnit.Framework;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
    using Org.Sdmxsource.Sdmx.Api.Model.Data;
    using Org.Sdmxsource.Sdmx.Api.Model.Data.Query;
    using Org.Sdmxsource.Sdmx.Api.Model.Format;
    using Org.Sdmxsource.Sdmx.Api.Model.Query;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Builder.Common;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;

    /// <summary>
    /// The test format resolver manager.
    /// </summary>
    public class TestFormatResolverManager
    {
        /// <summary>
        /// Tests the rest data one format not the default.
        /// </summary>
        [Test]
        public void TestRestDataOneFormatNotTheDefault()
        {
            var dataWriterPluginContracts = new[]
                                                {
                                                    BuildDataWriterPluginContract(BuildRestResponse("text/json", DataEnumType.Json), BuildRestResponse("application/vnd.sdmx.data+json", DataEnumType.Json))
                                                };
            IFormatResolverManager formatResolverManager = new FormatResolverManager(new IStructureWriterPluginContract[0],  dataWriterPluginContracts, new SortedAcceptHeadersBuilder());

            var sdmxObjectRetrievalManager = Substitute.For<ISdmxObjectRetrievalManager>();
            var dataQuery = Substitute.For<IDataQuery>();
            var headers = Substitute.For<WebHeaderCollection>();
            headers[HttpRequestHeader.Accept].Returns(string.Empty);

            var restResponse = formatResolverManager.GetDataFormat(headers, sdmxObjectRetrievalManager, dataQuery, RestApiVersion.V1);
            Assert.NotNull(restResponse);
            Assert.AreEqual(restResponse.Format.SdmxDataFormat.EnumType, DataEnumType.Json);
        }

        /// <summary>
        /// Tests the rest data one format not the default.
        /// </summary>
        [Test]
        public void TestRestDataTwoFormatIncludingDefault()
        {
            var dataWriterPluginContracts = new[]
                                                {
                                                    BuildDataWriterPluginContract(BuildRestResponse("text/json", DataEnumType.Json), BuildRestResponse("application/vnd.sdmx.data+json", DataEnumType.Json)),
                                                    BuildDataWriterPluginContract(BuildRestResponse("text/xml", DataEnumType.Generic21), BuildRestResponse("application/vnd.sdmx.genericdata+xml;version=2.1", DataEnumType.Generic21), BuildRestResponse("application/vnd.sdmx.genericdata+xml;version=2.0", DataEnumType.Generic20), BuildRestResponse("application/vnd.sdmx.structurespecificdata+xml;version=2.1", DataEnumType.Compact21))
                                                };
            IFormatResolverManager formatResolverManager = new FormatResolverManager(new IStructureWriterPluginContract[0], dataWriterPluginContracts, new SortedAcceptHeadersBuilder());

            var sdmxObjectRetrievalManager = Substitute.For<ISdmxObjectRetrievalManager>();
            var dataQuery = Substitute.For<IDataQuery>();
            var headers = Substitute.For<WebHeaderCollection>();
            headers[HttpRequestHeader.Accept].Returns(string.Empty);

            var restResponse = formatResolverManager.GetDataFormat(headers, sdmxObjectRetrievalManager, dataQuery, RestApiVersion.V1);
            Assert.NotNull(restResponse);
            Assert.AreEqual(restResponse.Format.SdmxDataFormat.EnumType, DataEnumType.Generic21);
            Assert.AreEqual(restResponse.ResponseContentHeader.MediaType.MediaType, "application/vnd.sdmx.genericdata+xml");
            Assert.NotNull(restResponse.ResponseContentHeader.MediaType.Parameters);
            Assert.AreEqual(restResponse.ResponseContentHeader.MediaType.Parameters["version"], "2.1");
        }

        /// <summary>
        /// Tests the rest data one format not the default.
        /// </summary>
        /// <param name="acceptValue">The accept value.</param>
        /// <param name="expectedMediaType">Expected type of the media.</param>
        [TestCase("text/json", "text/json")]
        [TestCase("text/xml;q=1.0, text/json;q=0.1", "text/xml")]
        [TestCase("application/vnd.sdmx.structurespecificdata+xml;version=2.1", "application/vnd.sdmx.structurespecificdata+xml; version=2.1")]
        [TestCase("*/*", "application/vnd.sdmx.genericdata+xml; version=2.1")]
        [TestCase("text/xml;q=0.3,*/*;q=0.1,application/*;q=0.2", "text/xml")]
        [TestCase("text/*;q=0.3,*/*;q=0.001,application/*;q=0.301", "application/vnd.sdmx.genericdata+xml; version=2.1")]
        [TestCase("", "application/vnd.sdmx.genericdata+xml; version=2.1")]
        [TestCase("text/*;q=0.3,*/*;q=0.1,application/*;q=1", "application/vnd.sdmx.genericdata+xml; version=2.1")]
        [TestCase("text/xml, */*", "text/xml")]
        [TestCase("text/xml, paok/ole, */*", "text/xml")]
        [TestCase("text/xml;q=0.1, paok/ole;q=0.9, */*;q=0.02", "text/xml")]
        
        [TestCase("text/xml, */*,application/vnd.sdmx.genericdata+xml;version=2.1", "application/vnd.sdmx.genericdata+xml; version=2.1")]
        [TestCase("text/json, */*;q=0.5,application/vnd.sdmx.genericdata+xml;version=2.1;q=0.1", "text/json")]
        [TestCase("application/vnd.sdmx.structurespecificdata+xml;version=2.1;, */*,application/vnd.sdmx.genericdata+xml;version=2.1", "application/vnd.sdmx.structurespecificdata+xml; version=2.1")]
        [TestCase("application/vnd.sdmx.structurespecificdata+xml;version=2.1;q=0.1, */*;q=0.1,application/vnd.sdmx.genericdata+xml;version=2.1;q=0.01", "application/vnd.sdmx.structurespecificdata+xml; version=2.1")]
        public void TestRestDataWithAccept(string acceptValue, string expectedMediaType)
        {
            var dataWriterPluginContracts = new[]
                                                {
                                                    BuildDataWriterPluginContract(BuildRestResponse("text/json", DataEnumType.Json), BuildRestResponse("application/vnd.sdmx.data+json", DataEnumType.Json)),
                                                    BuildDataWriterPluginContract(BuildRestResponse("text/xml", DataEnumType.Generic21), BuildRestResponse("application/vnd.sdmx.genericdata+xml;version=2.1", DataEnumType.Generic21), BuildRestResponse("application/vnd.sdmx.genericdata+xml;version=2.0", DataEnumType.Generic20), BuildRestResponse("application/vnd.sdmx.structurespecificdata+xml;version=2.1", DataEnumType.Compact21))
                                                };
            IFormatResolverManager formatResolverManager = new FormatResolverManager(new IStructureWriterPluginContract[0], dataWriterPluginContracts, new SortedAcceptHeadersBuilder());

            var sdmxObjectRetrievalManager = Substitute.For<ISdmxObjectRetrievalManager>();
            var dataQuery = Substitute.For<IDataQuery>();
            var headers = Substitute.For<WebHeaderCollection>();
            headers[HttpRequestHeader.Accept].Returns(acceptValue);

            var restResponse = formatResolverManager.GetDataFormat(headers, sdmxObjectRetrievalManager, dataQuery, RestApiVersion.V1);
            Assert.AreEqual(expectedMediaType, restResponse.ResponseContentHeader.MediaType.ToString());
        }

        //[TestCase("paok/ole", null, typeof(WebFaultException<string>))]
        public void TestRestDataWithAcceptException(string acceptValue, string expectedMediaType,Type type)
        {
            Assert.Throws(type, (() => TestRestDataWithAccept(acceptValue, expectedMediaType)));
        }

        /// <summary>
        /// Tests for rest.
        /// </summary>
        /// <param name="acceptValue">The accept value.</param>
        /// <param name="expectedContentTypeValue">The expected content type value.</param>
        /// <param name="acceptCharSet">The accept character set.</param>
        [TestCase("*/*", "application/vnd.sdmx.genericdata+xml;version=2.1; charset=utf-8", "utf-8")]
        [TestCase("text/xml;q=0.3,*/*;q=0.001,application/*;q=0.1", "text/xml; charset=utf-8", "utf-8")]
        [TestCase("text/*;q=0.3,*/*;q=0.001,application/*", "application/vnd.sdmx.genericdata+xml;version=2.1; charset=utf-8", "utf-8")]
        [TestCase("text/*;q=0.3,*/*;q=0.001,application/*;q=1", "application/vnd.sdmx.genericdata+xml;version=2.1; charset=utf-8", "utf-8")]
        [TestCase("text/xml, */*;q=0.9", "text/xml; charset=utf-8", "utf-8")]
        [TestCase("text/xml, paok/ole", "text/xml; charset=utf-8", "utf-8")]
        [TestCase("text/xml;q=0.1, paok/ole;q=0.9, */*;q=0.01", "text/xml", null)]
        [TestCase("text/xml, */*;q=0.001,application/vnd.sdmx.genericdata+xml;version=2.1", "application/vnd.sdmx.genericdata+xml;version=2.1; charset=utf-8", "utf-8")]
        [TestCase("text/xml, */*;q=0.001,application/vnd.sdmx.genericdata+xml;version=2.0", "application/vnd.sdmx.genericdata+xml;version=2.0; charset=utf-8", "utf-8")]
        [TestCase("application/vnd.sdmx.structurespecificdata+xml;version=2.1;, */*;q=0.001,application/vnd.sdmx.genericdata+xml;version=2.1", "application/vnd.sdmx.structurespecificdata+xml;version=2.1; charset=utf-8", "utf-8")]
        [TestCase("application/vnd.sdmx.structurespecificdata+xml;version=2.1;q=0.1, */*;q=0.001,application/vnd.sdmx.genericdata+xml;version=2.1;q=0.01", "application/vnd.sdmx.structurespecificdata+xml;version=2.1; charset=utf-8", "utf-8")]
        [TestCase("application/vnd.sdmx.compactdata+xml;version=2.0;q=0.1, */*;q=0.001,application/vnd.sdmx.genericdata+xml;version=2.1;q=0.01", "application/vnd.sdmx.compactdata+xml;version=2.0; charset=utf-16", "utf-16")]
        //[TestCase("application/vnd.sdmx.compactdata+xml;version=2.0;q=0.1, */*;q=0.001,application/vnd.sdmx.genericdata+xml;version=2.1;q=0.01", "application/vnd.sdmx.compactdata+xml;version=2.0; charset=utf-16", "utf-8", ExpectedException = typeof(AssertionException))]
        //[TestCase("application/vnd.sdmx.compactdata+xml;version=2.1", "application/vnd.sdmx.compactdata+xml;version=2.1; charset=utf-8", "utf-8", ExpectedException = typeof(WebFaultException<string>))]
        public void TestForRestDataContentType(string acceptValue, string expectedContentTypeValue, string acceptCharSet)
        {
            var buildDataWriterPluginContract = BuildDataWriterPluginContract(
                BuildRestResponse("text/xml", DataEnumType.Generic21, acceptCharSet), 
                BuildRestResponse("application/vnd.sdmx.genericdata+xml;version=2.1", DataEnumType.Generic21, acceptCharSet), 
                BuildRestResponse("application/vnd.sdmx.genericdata+xml;version=2.0", DataEnumType.Generic20, acceptCharSet), 
                BuildRestResponse("application/vnd.sdmx.compactdata+xml;version=2.0", DataEnumType.Compact20, acceptCharSet), 
                BuildRestResponse("application/vnd.sdmx.structurespecificdata+xml;version=2.1", DataEnumType.Compact21, acceptCharSet));
            var dataWriterPluginContracts = new[]
                                                 {
                                                    BuildDataWriterPluginContract(BuildRestResponse("text/json", DataEnumType.Json, acceptCharSet), BuildRestResponse("application/vnd.sdmx.data+json", DataEnumType.Json, acceptCharSet)),
                                                    buildDataWriterPluginContract
                                                };
            IFormatResolverManager formatResolverManager = new FormatResolverManager(new IStructureWriterPluginContract[0], dataWriterPluginContracts, new SortedAcceptHeadersBuilder());

            var sdmxObjectRetrievalManager = Substitute.For<ISdmxObjectRetrievalManager>();
            var dataQuery = Substitute.For<IDataQuery>();
            var headers = Substitute.For<WebHeaderCollection>();
            headers[HttpRequestHeader.Accept].Returns(acceptValue);
            if (acceptCharSet != null)
            {
                headers[HttpRequestHeader.AcceptCharset].Returns(acceptCharSet);
            }

            var restResponse = formatResolverManager.GetDataFormat(headers, sdmxObjectRetrievalManager, dataQuery, RestApiVersion.V1);
            Assert.NotNull(restResponse);
            Assert.AreEqual(new ContentType(expectedContentTypeValue), restResponse.ResponseContentHeader.MediaType);
        }

        [TestCase("application/vnd.sdmx.paok+xml;version=2.0;q=0.1", null)]
        [TestCase("application/vnd.sdmx.compactdata+xml;version=2.1;charset=utf-8", "utf-8")]
        public void TestForRestDataContentTypeNotAcceptable(string acceptValue, string acceptCharSet)
        {
            var buildDataWriterPluginContract = BuildDataWriterPluginContract(
                BuildRestResponse("text/xml", DataEnumType.Generic21, acceptCharSet), 
                BuildRestResponse("application/vnd.sdmx.genericdata+xml;version=2.1", DataEnumType.Generic21, acceptCharSet), 
                BuildRestResponse("application/vnd.sdmx.genericdata+xml;version=2.0", DataEnumType.Generic20, acceptCharSet), 
                BuildRestResponse("application/vnd.sdmx.compactdata+xml;version=2.0", DataEnumType.Compact20, acceptCharSet), 
                BuildRestResponse("application/vnd.sdmx.structurespecificdata+xml;version=2.1", DataEnumType.Compact21, acceptCharSet));
            var dataWriterPluginContracts = new[]
                                                 {
                                                    BuildDataWriterPluginContract(BuildRestResponse("text/json", DataEnumType.Json, acceptCharSet), BuildRestResponse("application/vnd.sdmx.data+json", DataEnumType.Json, acceptCharSet)),
                                                    buildDataWriterPluginContract
                                                };
            IFormatResolverManager formatResolverManager = new FormatResolverManager(new IStructureWriterPluginContract[0], dataWriterPluginContracts, new SortedAcceptHeadersBuilder());

            var sdmxObjectRetrievalManager = Substitute.For<ISdmxObjectRetrievalManager>();
            var dataQuery = Substitute.For<IDataQuery>();
            var headers = Substitute.For<WebHeaderCollection>();
            headers[HttpRequestHeader.Accept].Returns(acceptValue);
            if (acceptCharSet != null)
            {
                headers[HttpRequestHeader.AcceptCharset].Returns(acceptCharSet);
            }

            Assert.Throws<SdmxNotAcceptableException>(()=> formatResolverManager.GetDataFormat(headers, sdmxObjectRetrievalManager, dataQuery, RestApiVersion.V1));
        }

        /// <summary>
        /// Tests the rest data one format not the default.
        /// </summary>
        [Test]
        public void TestRestStructureOneFormatNotTheDefault()
        {
            var dataWriterPluginContracts = new[]
                                                {
                                                    BuildStructureWriterPluginContract(BuildRestResponse("text/json", StructureOutputFormatEnumType.Edi), BuildRestResponse("application/vnd.sdmx.structure+json", StructureOutputFormatEnumType.Edi)),
                                                };
            IFormatResolverManager formatResolverManager = new FormatResolverManager(dataWriterPluginContracts, new IDataWriterPluginContract[0], new SortedAcceptHeadersBuilder());

            var headers = Substitute.For<WebHeaderCollection>();
            headers[HttpRequestHeader.Accept].Returns(string.Empty);

            var restResponse = formatResolverManager.GetStructureFormat(headers, RestApiVersion.V1);
            Assert.NotNull(restResponse);
            Assert.AreEqual(restResponse.Format.SdmxOutputFormat.EnumType, StructureOutputFormatEnumType.Edi);
        }

        /// <summary>
        /// Tests the rest structure with accept.
        /// </summary>
        /// <param name="acceptValue">The accept value.</param>
        /// <param name="expectedMediaType">Expected type of the media.</param>
        [TestCase("text/json", "text/json")]
        [TestCase("text/xml;q=1.0, text/json;q=0.1", "text/xml")]
        [TestCase("application/vnd.sdmx.structure+xml;version=2.1", "application/vnd.sdmx.structure+xml; version=2.1")]
        [TestCase("", "application/vnd.sdmx.structure+xml; version=2.1")]
        [TestCase("*/*", "application/vnd.sdmx.structure+xml; version=2.1")]
        [TestCase("text/xml;q=0.3,*/*;q=0.1,application/*;q=0.2", "text/xml")]
        [TestCase("text/*;q=0.3,*/*;q=0.001,application/*;q=0.301", "application/vnd.sdmx.structure+xml; version=2.1")]
        [TestCase("text/*;q=0.3,*/*;q=0.1,application/*;q=1", "application/vnd.sdmx.structure+xml; version=2.1")]
        [TestCase("text/xml, */*", "text/xml")]
        [TestCase("text/xml, paok/ole, */*", "text/xml")]
        [TestCase("text/xml;q=0.1, paok/ole;q=0.9, */*;q=0.02", "text/xml")]
        //[TestCase("paok/ole", null, ExpectedException = typeof(WebFaultException<string>))]
        [TestCase("text/xml, */*,application/vnd.sdmx.structure+xml;version=2.1", "application/vnd.sdmx.structure+xml; version=2.1")]
        [TestCase("text/json, */*;q=0.5,application/vnd.sdmx.structure+xml;version=2.1;q=0.1", "text/json")]
        [TestCase("application/vnd.sdmx.structure+xml;version=2.1;, */*,application/vnd.sdmx.structure+xml;version=2.0", "application/vnd.sdmx.structure+xml; version=2.1")]
        [TestCase("application/vnd.sdmx.structure+xml;version=2.1;q=0.1, */*;q=0.1,application/vnd.sdmx.structure+xml;version=2.0;q=0.01", "application/vnd.sdmx.structure+xml; version=2.1")]
        public void TestRestStructureWithAccept(string acceptValue, string expectedMediaType)
        {
            var dataWriterPluginContracts = new[]
                                                {
                                                    BuildStructureWriterPluginContract(BuildRestResponse("text/json", StructureOutputFormatEnumType.Edi), BuildRestResponse("application/vnd.sdmx.structure+json", StructureOutputFormatEnumType.Edi)),
                                                    BuildStructureWriterPluginContract(BuildRestResponse("text/xml", StructureOutputFormatEnumType.SdmxV21StructureDocument), BuildRestResponse("application/vnd.sdmx.structure+xml;version=2.1", StructureOutputFormatEnumType.SdmxV21StructureDocument), BuildRestResponse("application/vnd.sdmx.structure+xml;version=2.0", StructureOutputFormatEnumType.SdmxV2StructureDocument), BuildRestResponse("application/vnd.sdmx.structure.registry;version=2.1", StructureOutputFormatEnumType.SdmxV21RegistrySubmitDocument))
                                                };
            IFormatResolverManager formatResolverManager = new FormatResolverManager(dataWriterPluginContracts, new IDataWriterPluginContract[0], new SortedAcceptHeadersBuilder());

            var headers = Substitute.For<WebHeaderCollection>();
            headers[HttpRequestHeader.Accept].Returns(acceptValue);

            var restResponse = formatResolverManager.GetStructureFormat(headers, RestApiVersion.V1);
            Assert.AreEqual(expectedMediaType, restResponse.ResponseContentHeader.MediaType.ToString());
        }

        /// <summary>
        /// Builds the data writer plugin contract.
        /// </summary>
        /// <param name="supportedFormats">The supported formats.</param>
        /// <returns>
        /// The <see cref="IDataWriterPluginContract" />
        /// </returns>
        private static IDataWriterPluginContract BuildDataWriterPluginContract(params IRestResponse<IDataFormat>[] supportedFormats)
        {
            var dataFormatFactory = Substitute.For<IDataWriterPluginContract>();
            dataFormatFactory.GetDataFormats(Arg.Is<IRestDataRequest>(x=>x != null)).Returns(supportedFormats);
            return dataFormatFactory;
        }

        /// <summary>
        /// Builds the data writer plugin contract.
        /// </summary>
        /// <param name="supportedFormats">The supported formats.</param>
        /// <returns>The <see cref="IDataWriterPluginContract"/></returns>
        private static IStructureWriterPluginContract BuildStructureWriterPluginContract(params IRestResponse<IStructureFormat>[] supportedFormats)
        {
            var dataFormatFactory = Substitute.For<IStructureWriterPluginContract>();
            dataFormatFactory.GetStructureFormats(Arg.Is<WebHeaderCollection>(x=>x!=null), Arg.Is<ISortedAcceptHeaders>(x=>x!=null)).Returns(supportedFormats);
            return dataFormatFactory;
        }

        /// <summary>
        /// Builds the rest response.
        /// </summary>
        /// <param name="contentType">Type of the content.</param>
        /// <param name="dataEnumType">Type of the data .</param>
        /// <param name="charSet">The character set.</param>
        /// <returns>
        /// The <see cref="IRestResponse{IDataFormat}" />.
        /// </returns>
        private static IRestResponse<IDataFormat> BuildRestResponse(string contentType, DataEnumType dataEnumType, string charSet = null)
        {
            var restResponse = Substitute.For<IRestResponse<IDataFormat>>();
            var format = Substitute.For<IDataFormat>();
            format.FormatAsString.Returns(string.Format(CultureInfo.InvariantCulture, "{0}{1}", contentType, dataEnumType));
            format.SdmxDataFormat.Returns(DataType.GetFromEnum(dataEnumType));

            restResponse.Format.Returns(format);

            var responseContentHeader = Substitute.For<IResponseContentHeader>();
            if (charSet == null)
            {
                responseContentHeader.MediaType.Returns(new ContentType(contentType));
            }
            else
            {
                responseContentHeader.MediaType.Returns(new ContentType(contentType) { CharSet = charSet });
            }

            restResponse.ResponseContentHeader.Returns(responseContentHeader);
            return restResponse;
        }

        /// <summary>
        /// Builds the rest response.
        /// </summary>
        /// <param name="contentType">
        /// Type of the content.
        /// </param>
        /// <param name="dataEnumType">
        /// Type of the data .
        /// </param>
        /// <returns>
        /// The <see cref="IRestResponse{IDataFormat}"/>.
        /// </returns>
        private static IRestResponse<IStructureFormat> BuildRestResponse(string contentType, StructureOutputFormatEnumType dataEnumType)
        {
            var restResponse = Substitute.For<IRestResponse<IStructureFormat>>();
            var format = Substitute.For<IStructureFormat>();
            format.FormatAsString.Returns(string.Format(CultureInfo.InvariantCulture, "{0}{1}", contentType, dataEnumType));
            format.SdmxOutputFormat.Returns(StructureOutputFormat.GetFromEnum(dataEnumType));

            restResponse.Format.Returns(format);

            var responseContentHeader = Substitute.For<IResponseContentHeader>();
            responseContentHeader.MediaType.Returns(new ContentType(contentType));

            restResponse.ResponseContentHeader.Returns(responseContentHeader);
            return restResponse;
        }
    }
}