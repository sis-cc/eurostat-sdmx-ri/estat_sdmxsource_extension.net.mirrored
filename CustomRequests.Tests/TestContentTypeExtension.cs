// -----------------------------------------------------------------------
// <copyright file="TestContentTypeExtension.cs" company="EUROSTAT">
//   Date Created : 2016-07-01
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace CustomRequests.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Net.Mime;

    using Estat.Sdmxsource.Extension.Extension;
    using Estat.Sdmxsource.Extension.Model;

    using NUnit.Framework;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Data;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Data;

    /// <summary>
    /// Test <see cref="ContentTypeExtension"/>
    /// </summary>
    /// <remarks>SDMX REST supported content type : <![CDATA[
    /// application/vnd.sdmx.genericdata+xml;version=2.1
    /// application/vnd.sdmx.structurespecificdata+xml;version=2.1
    /// application/vnd.sdmx.generictimeseriesdata+xml;version=2.1
    /// application/vnd.sdmx.structurespecifictimeseriesdata+xml;version=2.1
    /// application/vnd.sdmx.genericmetadata+xml;version=2.1
    /// application/vnd.sdmx.structurespecificmetadata+xml;version=2.1
    /// application/vnd.sdmx.structure+xml;version=2.1
    /// application/vnd.sdmx.schema+xml;version=2.1]]></remarks>
    [TestFixture]
    public class TestContentTypeExtension
    {
        /// <summary>
        /// Tests the get score.
        /// </summary>
        /// <param name="acceptHeaderRaw">The accept header raw.</param>
        /// <param name="mediaType">Type of the media.</param>
        /// <param name="expectedValue">The expected value.</param>
        [TestCase("*/*", "application/vnd.sdmx.genericdata+xml;version=2.1", 1)]
        [TestCase("application/*", "application/vnd.sdmx.genericdata+xml;version=2.1", 101)]
        [TestCase("*/vnd.sdmx.genericdata+xml", "application/vnd.sdmx.genericdata+xml;version=2.1", 11)]
        [TestCase("*/vnd.sdmx.genericdata+xml;version=2.1", "application/vnd.sdmx.genericdata+xml;version=2.1", 12)]
        [TestCase("*/vnd.sdmx.genericdata+xml;version=2.0", "application/vnd.sdmx.genericdata+xml;version=2.1", 0)]
        [TestCase("application/vnd.sdmx.genericdata+xml;version=2.1", "application/vnd.sdmx.genericdata+xml;version=2.1", 112)]
        [TestCase("application/vnd.sdmx.genericdata+xml;version=2.1", "application/vnd.sdmx.genericdata+xml", 111)]
        [TestCase("application/vnd.sdmx.genericdata+xml", "application/vnd.sdmx.genericdata+xml;version=2.1", 111)]
        [TestCase("application/vnd.sdmx.genericdata+xml", "application/vnd.sdmx.genericdata+xml", 111)]
        [TestCase("application/vnd.sdmx.compactdata+xml; version=2.1", "application/vnd.sdmx.compactdata+xml; version=2.0", 0)]
        public void TestGetScore(string acceptHeaderRaw, string mediaType, int expectedValue)
        {
            var acceptHeaderTokens = acceptHeaderRaw.Split(',').Select(s => new ContentType(s));
            ContentType contentType = new ContentType(mediaType);
            IRestResponse<IDataFormat> response = new RestResponse<IDataFormat>(new Lazy<IDataFormat>(() => new SdmxDataFormatCore(DataType.GetFromEnum(DataEnumType.Compact21))), new ResponseContentHeader(contentType, null));
            var acceptHeaderToken = response.GetScore(acceptHeaderTokens);
            Assert.AreEqual(expectedValue, acceptHeaderToken.MatchScore);
        }

        /// <summary>
        /// Tests the get quality.
        /// </summary>
        /// <param name="acceptHeaderRaw">The accept header raw.</param>
        /// <param name="expectedValue">The expected value.</param>
        [TestCase("*/*",  1.0f)]
        [TestCase("*/*;q=0.1",  0.1f)]
        [TestCase("*/*;q=0.9",  0.9f)]
        [TestCase("*/*;q=1.9",  1f)]
        [TestCase("*/*;q=0",  0f)]
        public void TestGetQuality(string acceptHeaderRaw, float expectedValue)
        {
            ContentType contentType = new ContentType(acceptHeaderRaw);
            var quality = contentType.GetQuality();
            Assert.AreEqual(expectedValue, quality);
        }

        /// <summary>
        /// Tests the get best match.
        /// </summary>
        /// <param name="acceptHeaderRaw">The accept header raw.</param>
        /// <param name="expectedMedia">The expected media.</param>
        [TestCase("*/*", "application/vnd.sdmx.genericdata+xml")]
        [TestCase("text/xml;q=0.3,*/*;q=0.1,application/*;q=0.2", "text/xml")]
        [TestCase("text/*;q=0.3,*/*,application/*", "application/vnd.sdmx.genericdata+xml")]
        [TestCase("text/*;q=0.3,*/*,application/*;q=1", "application/vnd.sdmx.genericdata+xml")]
        [TestCase("text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8", "application/xml")]
        [TestCase("application/xml,application/xhtml+xml,text/html;q=0.9, text/plain;q=0.8,image/png,*/*;q=0.5", "application/xml")]
        [TestCase("text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8", "application/xml")]
        [TestCase("image/jpeg, application/x-ms-application, image/gif, application/xaml+xml, image/pjpeg, application/x-ms-xbap, application/x-shockwave-flash, application/msword, */*", "application/vnd.sdmx.genericdata+xml")]
        [TestCase("text/xml, */*", "text/xml")]
        [TestCase("text/xml, paok/ole, */*", "text/xml")]
        [TestCase("text/xml;q=0.1, paok/ole;q=0.9, */*;q=0.001", "text/xml")]
        [TestCase("paok/ole", null)]
        [TestCase("text/xml, */*,application/vnd.sdmx.genericdata+xml;version=2.1", "application/vnd.sdmx.genericdata+xml")]
        [TestCase("application/vnd.sdmx.structurespecificdata+xml;version=2.1;, */*,application/vnd.sdmx.genericdata+xml;version=2.1", "application/vnd.sdmx.structurespecificdata+xml")]
        [TestCase("application/vnd.sdmx.structurespecificdata+xml;version=2.1;q=0.9, application/vnd.sdmx.compactdata+xml;version=2.1;q=1.0", "application/vnd.sdmx.structurespecificdata+xml")]
        [TestCase("application/vnd.sdmx.structurespecificdata+xml;version=2.1;q=0.9, application/vnd.sdmx.compactdata+xml;version=2.0;q=1.0", "application/vnd.sdmx.compactdata+xml")]
        [TestCase("application/vnd.sdmx.structurespecificdata+xml;version=2.1;q=0.1, */*;q=0.001,application/vnd.sdmx.genericdata+xml;version=2.1;q=0.01", "application/vnd.sdmx.structurespecificdata+xml")]
        [TestCase("text/json, */*;q=0.5,application/vnd.sdmx.genericdata+xml;version=2.1;q=0.1", "text/json")]
        [TestCase("text/json, */*;q=0.5,application/vnd.sdmx.genericdata+xml;q=0.1", "text/json")]
        public void TestGetBestMatch(string acceptHeaderRaw, string expectedMedia)
        {
            var headerCollection = new UnRestrictedWebHeaderCollection();
            headerCollection.Add("Accept", acceptHeaderRaw);
            List<string> supportedTypes = new List<string>();
            supportedTypes.Add("application/vnd.sdmx.genericdata+xml;version=2.1");
            supportedTypes.Add("application/vnd.sdmx.genericdata+xml");
            supportedTypes.Add("application/vnd.sdmx.genericdata+xml;version=2.0");
            supportedTypes.Add("application/vnd.sdmx.structurespecificdata+xml;version=2.1");
            supportedTypes.Add("application/vnd.sdmx.structurespecificdata+xml");
            supportedTypes.Add("application/vnd.sdmx.compactdata+xml;version=2.0");
            supportedTypes.Add("application/vnd.sdmx.generictimeseriesdata+xml;version=2.1");
            supportedTypes.Add("application/vnd.sdmx.structurespecifictimeseriesdata+xml;version=2.1");
            supportedTypes.Add("application/vnd.sdmx.genericmetadata+xml;version=2.1");
            supportedTypes.Add("application/vnd.sdmx.structurespecificmetadata+xml;version=2.1");
            supportedTypes.Add("application/vnd.sdmx.structure+xml;version=2.1");
            supportedTypes.Add("application/vnd.sdmx.schema+xml;version=2.1");
            supportedTypes.Add("text/json");
            supportedTypes.Add("application/xml");
            supportedTypes.Add("text/xml");
            var acceptHeaderToken = headerCollection.GetContentBestMatch(supportedTypes.Select(s => new RestResponse<IDataFormat>(null, new ResponseContentHeader(new ContentType(s), null))));
            if (expectedMedia == null)
            {
                Assert.IsNull(acceptHeaderToken);
            }
            else
            {
                Assert.AreEqual(expectedMedia, acceptHeaderToken.RestResponse.ResponseContentHeader.MediaType.MediaType);
            }
        }

        /// <summary>
        /// An unrestricted web header collection. Not mocking too simple.
        /// </summary>
        private class UnRestrictedWebHeaderCollection : WebHeaderCollection
        {
            /// <summary>
            /// Inserts a header with the specified name and value into the collection.
            /// </summary>
            /// <param name="name">The header to add to the collection.</param>
            /// <param name="value">The content of the header.</param>
            public override void Add(string name, string value)
            {
                this.AddWithoutValidate(name, value);
            }
        }
    }
}