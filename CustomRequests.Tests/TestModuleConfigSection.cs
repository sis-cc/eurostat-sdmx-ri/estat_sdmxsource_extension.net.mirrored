﻿// -----------------------------------------------------------------------
// <copyright file="TestModuleConfigSection.cs" company="EUROSTAT">
//   Date Created : 2014-10-31
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace CustomRequests.Tests
{
    using Estat.Sdmxsource.Extension.Config;
    using Estat.Sdmxsource.Extension.Constant;

    using NUnit.Framework;

    /// <summary>
    ///     Test unit for <see cref="ModuleConfigSection" />
    /// </summary>
    [TestFixture]
    public class TestModuleConfigSection
    {
        /// <summary>
        ///     Test unit for <see cref="ModuleConfigSection.GetSection" />
        /// </summary>
        [Test]
        public void TestGetSection()
        {
            var section = ModuleConfigSection.GetSection();
            Assert.NotNull(section);
        }

        /// <summary>
        ///     Test unit for <see cref="ModuleConfigSection.Retrievers" />
        /// </summary>
        [Test]
        public void TestGetSectionOrder()
        {
            var section = ModuleConfigSection.GetSection();
            Assert.AreEqual("Test3", section.Retrievers[0].Name);
            Assert.AreEqual("B", section.Retrievers[1].Name);
            Assert.AreEqual("A", section.Retrievers[2].Name);
            Assert.AreEqual("C", section.Retrievers[3].Name);
        }

        /// <summary>
        ///     Test unit for <see cref="ModuleConfigSection.StructureRequestBehavior" />
        /// </summary>
        [Test]
        public void TestGetStructureBehavior()
        {
            var section = ModuleConfigSection.GetSection();
            Assert.AreEqual(RequestBehaviorType.First, section.StructureRequestBehavior);
        }
    }
}