// -----------------------------------------------------------------------
// <copyright file="RDFWritersTest" company="EUROSTAT">
//   Date Created : 2017-10-24
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using NUnit.Framework;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Engine;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Header;
using Org.Sdmxsource.Sdmx.Structureparser.Manager.Parsing;
using Org.Sdmxsource.Sdmx.StructureRetrieval.Manager;
using Org.Sdmxsource.Sdmx.Util.Extension;
using System;
using System.IO;
using System.Linq;
using System.Xml;
using Org.Sdmxsource.Sdmx.Api.Model.Objects;
using Org.Sdmxsource.Sdmx.Util.Objects.Container;

namespace CustomRequests.Tests
{
    internal class RDFWritersTest
    {
        private readonly ISdmxObjects _sdmxObjects;
        private readonly InMemoryRetrievalManager _retrievalManager;

        public RDFWritersTest()
        {
            var dataFlowFile = new FileInfo("tests/V21/Structure/test-sdmxv2.1-ESTAT+SSTSCONS_PROD_M+2.0.xml");
            var dsdFile = new FileInfo("tests/V21/Structure/test-sdmxv2.1-ESTAT+STS+2.0.xml");
            var sdmxObjects = dataFlowFile.GetSdmxObjects(new StructureParsingManager());
            sdmxObjects.Merge(dsdFile.GetSdmxObjects(new StructureParsingManager()));
            _sdmxObjects = sdmxObjects;
            this._retrievalManager = new InMemoryRetrievalManager(sdmxObjects);
        }

        [Test]
        public void TestRDFStructureWriterSdmxObjects()
        {
            FileInfo outputFile = new FileInfo("test-TestRDFStructureWriterSdmxObjects.rdf");

            using (var stream = outputFile.Create())
            {
                using (IStructureWriterEngine writer = new RDFPlugin.Engine.RDFStructureWriterEngine(stream, System.Text.Encoding.UTF8))
                {
                    writer.WriteStructures(_sdmxObjects);
                }

                stream.Flush();
            }

            ValidateRdf(outputFile);
        }


        [Test]
        public void TestRDFStructureWriterDataflowAndDsd()
        {
            FileInfo outputFile = new FileInfo("test-TestStructureWriterDataflowAndDsd.rdf");

            var dsd = _retrievalManager.GetMaintainableObjects<IDataStructureObject>().FirstOrDefault();
            var dataflow = _retrievalManager.GetMaintainableObjects<IDataflowObject>().FirstOrDefault();
            ISdmxObjects sdmxObjects = new SdmxObjectsImpl();
            sdmxObjects.Header = new HeaderImpl("RDFTTT1", "ZZ8");
            sdmxObjects.AddDataflow(dataflow);
            sdmxObjects.AddDataStructure(dsd);
            using (var stream = outputFile.Create())
            {
                using (IStructureWriterEngine writer = new RDFPlugin.Engine.RDFStructureWriterEngine(stream, System.Text.Encoding.UTF8))
                {
                    writer.WriteStructures(sdmxObjects);
                }

                stream.Flush();
            }

            ValidateRdf(outputFile);
        }

        [Test]
        public void TestRDFStructureWriterDsd()
        {
            FileInfo outputFile = new FileInfo("test-TestStructureWriterDsd.rdf");

            var dsd = _retrievalManager.GetMaintainableObjects<IDataStructureObject>().FirstOrDefault();
            using (var stream = outputFile.Create())
            {
                using (IStructureWriterEngine writer = new RDFPlugin.Engine.RDFStructureWriterEngine(stream, System.Text.Encoding.UTF8))
                {
                    writer.WriteStructure(dsd);
                }

                stream.Flush();
            }

            ValidateRdf(outputFile);
        }

        [Test]
        public void TestRDFStructureWriterDataflow()
        {
            FileInfo outputFile = new FileInfo("test-TestStructureWriterDataflow.rdf");

            var dataflow = _retrievalManager.GetMaintainableObjects<IDataflowObject>().FirstOrDefault();
            using (var stream = outputFile.Create())
            {
                using (IStructureWriterEngine writer = new RDFPlugin.Engine.RDFStructureWriterEngine(stream, System.Text.Encoding.UTF8))
                {
                    writer.WriteStructure(dataflow);
                }

                stream.Flush();
            }

            ValidateRdf(outputFile);
        }

        [Test]
        public void TestRDFWriteDataWithDataflowNoDataSetHeader()
        {
            var dsd = _retrievalManager.GetMaintainableObjects<IDataStructureObject>().FirstOrDefault();
            var dataflow = _retrievalManager.GetMaintainableObjects<IDataflowObject>().FirstOrDefault();
            FileInfo outputFile = new FileInfo("test-TestRDFWriteDataWithDataflowNoDataSetHeader.rdf");
            using (var stream = outputFile.Create())
            {
                using (RDFPlugin.Engine.RDFDataWriterEngine writer = new RDFPlugin.Engine.RDFDataWriterEngine(System.Text.Encoding.UTF8, stream))
                {
                    writer.WriteHeader(new HeaderImpl("RDF" + DateTime.Now.ToBinary(), "ZZ9"));
                    writer.StartDataset(dataflow, dsd, null);

                    writer.WriteSeriesKeyValue("FREQ", "Q");
                    writer.WriteSeriesKeyValue("REF_AREA", "CN");
                    writer.WriteSeriesKeyValue("ADJUSTMENT", "C");
                    writer.WriteSeriesKeyValue("STS_INDICATOR", "PROD");
                    writer.WriteSeriesKeyValue("STS_ACTIVITY", "NS0020");
                    writer.WriteSeriesKeyValue("STS_INSTITUTION", "1");
                    writer.WriteSeriesKeyValue("STS_BASE_YEAR", "2000");

                    writer.WriteAttributeValue("TIME_FORMAT", "P1M");

                    writer.WriteObservation("2005-Q1", "1.51");
                    writer.WriteAttributeValue("OBS_STATUS", "A");
                    writer.WriteObservation("2005-Q2", "1.52");
                    writer.WriteAttributeValue("OBS_STATUS", "A");

                    writer.WriteSeriesKeyValue("FREQ", "Q");
                    writer.WriteSeriesKeyValue("REF_AREA", "LU");
                    writer.WriteSeriesKeyValue("ADJUSTMENT", "C");
                    writer.WriteSeriesKeyValue("STS_INDICATOR", "PROD");
                    writer.WriteSeriesKeyValue("STS_ACTIVITY", "NS0020");
                    writer.WriteSeriesKeyValue("STS_INSTITUTION", "1");
                    writer.WriteSeriesKeyValue("STS_BASE_YEAR", "2000");

                    writer.WriteAttributeValue("TIME_FORMAT", "P1M");

                    writer.WriteObservation("2005-Q1", "2.51");
                    writer.WriteAttributeValue("OBS_STATUS", "A");
                    writer.WriteObservation("2005-Q2", "2.52");
                    writer.WriteAttributeValue("OBS_STATUS", "A");
                    writer.Close();
                }

                stream.Flush();
            }

            ValidateRdf(outputFile);
        }

        private static void ValidateRdf(FileInfo outputFile)
        {
            // TODO use a proper validation for RDF but currently it fails
            int count = 0;
            using (var stream = outputFile.OpenText())
            using (var reader = XmlReader.Create(stream))
            {
                while (reader.Read())
                {
                    count++;
                    var nodeType = reader.NodeType;
                    if (nodeType == XmlNodeType.Element)
                    {
                        for (int i = 0; i < reader.AttributeCount; i++)
                        {
                            var value = reader.GetAttribute(i);
                        }
                    }
                }
            }

            Assert.That(count, Is.GreaterThan(0));
        }

        [TestCase(typeof(ArgumentNullException))]
        public void TestRDFWriteDataWithNoDataflowNoDataSetHeader(Type type)
        {
            Assert.Throws(type, () =>
            {
                var dsd = _retrievalManager.GetMaintainableObjects<IDataStructureObject>().FirstOrDefault();
                FileInfo outputFile = new FileInfo("test-TestRDFWriteDataWithNoDataflowNoDataSetHeader.rdf");
                using (var stream = outputFile.Create())
                {
                    using (RDFPlugin.Engine.RDFDataWriterEngine writer =
                        new RDFPlugin.Engine.RDFDataWriterEngine(System.Text.Encoding.UTF8, stream))
                    {
                        writer.WriteHeader(new HeaderImpl("RDF" + DateTime.Now.ToBinary(), "ZZ9"));
                        writer.StartDataset(null, dsd, null);

                        writer.WriteSeriesKeyValue("FREQ", "Q");
                        writer.WriteSeriesKeyValue("REF_AREA", "CN");
                        writer.WriteSeriesKeyValue("ADJUSTMENT", "C");
                        writer.WriteSeriesKeyValue("STS_INDICATOR", "PROD");
                        writer.WriteSeriesKeyValue("STS_ACTIVITY", "NS0020");
                        writer.WriteSeriesKeyValue("STS_INSTITUTION", "1");
                        writer.WriteSeriesKeyValue("STS_BASE_YEAR", "2000");

                        writer.WriteAttributeValue("TIME_FORMAT", "P1M");

                        writer.WriteObservation("2005-Q1", "1.51");
                        writer.WriteAttributeValue("OBS_STATUS", "A");
                        writer.WriteObservation("2005-Q2", "1.52");
                        writer.WriteAttributeValue("OBS_STATUS", "A");

                        writer.WriteSeriesKeyValue("FREQ", "Q");
                        writer.WriteSeriesKeyValue("REF_AREA", "LU");
                        writer.WriteSeriesKeyValue("ADJUSTMENT", "C");
                        writer.WriteSeriesKeyValue("STS_INDICATOR", "PROD");
                        writer.WriteSeriesKeyValue("STS_ACTIVITY", "NS0020");
                        writer.WriteSeriesKeyValue("STS_INSTITUTION", "1");
                        writer.WriteSeriesKeyValue("STS_BASE_YEAR", "2000");

                        writer.WriteAttributeValue("TIME_FORMAT", "P1M");

                        writer.WriteObservation("2005-Q1", "2.51");
                        writer.WriteAttributeValue("OBS_STATUS", "A");
                        writer.WriteObservation("2005-Q2", "2.52");
                        writer.WriteAttributeValue("OBS_STATUS", "A");
                        writer.Close();
                    }

                    stream.Flush();
                }

                ValidateRdf(outputFile);
            });
        }

        [Test]
        public void TestRDFWriteDataWithDataflowDataSetHeader()
        {
            var dsd = _retrievalManager.GetMaintainableObjects<IDataStructureObject>().FirstOrDefault();
            var dataflow = _retrievalManager.GetMaintainableObjects<IDataflowObject>().FirstOrDefault();
            FileInfo outputFile = new FileInfo("test-TestRDFWriteDataWithDataflowDataSetHeader");
            using (var stream = outputFile.Create())
            {
                using (RDFPlugin.Engine.RDFDataWriterEngine writer = new RDFPlugin.Engine.RDFDataWriterEngine(System.Text.Encoding.UTF8, stream))
                {
                    writer.WriteHeader(new HeaderImpl("RDF" + DateTime.Now.ToBinary(), "ZZ9"));

                    DatasetAction action = DatasetAction.GetFromEnum(DatasetActionEnumType.Information);
                    var dsdRef = new DatasetStructureReferenceCore(dsd.AsReference);
                    Org.Sdmxsource.Sdmx.Api.Model.Header.IDatasetHeader datasetHeader = new DatasetHeaderCore("datasetId", action, dsdRef);
                    writer.StartDataset(dataflow, dsd, datasetHeader);

                    writer.WriteSeriesKeyValue("FREQ", "Q");
                    writer.WriteSeriesKeyValue("REF_AREA", "CN");
                    writer.WriteSeriesKeyValue("ADJUSTMENT", "C");
                    writer.WriteSeriesKeyValue("STS_INDICATOR", "PROD");
                    writer.WriteSeriesKeyValue("STS_ACTIVITY", "NS0020");
                    writer.WriteSeriesKeyValue("STS_INSTITUTION", "1");
                    writer.WriteSeriesKeyValue("STS_BASE_YEAR", "2000");

                    writer.WriteAttributeValue("TIME_FORMAT", "P1M");

                    writer.WriteObservation("2005-Q1", "1.51");
                    writer.WriteAttributeValue("OBS_STATUS", "A");
                    writer.WriteObservation("2005-Q2", "1.52");
                    writer.WriteAttributeValue("OBS_STATUS", "A");

                    writer.WriteSeriesKeyValue("FREQ", "Q");
                    writer.WriteSeriesKeyValue("REF_AREA", "LU");
                    writer.WriteSeriesKeyValue("ADJUSTMENT", "C");
                    writer.WriteSeriesKeyValue("STS_INDICATOR", "PROD");
                    writer.WriteSeriesKeyValue("STS_ACTIVITY", "NS0020");
                    writer.WriteSeriesKeyValue("STS_INSTITUTION", "1");
                    writer.WriteSeriesKeyValue("STS_BASE_YEAR", "2000");

                    writer.WriteAttributeValue("TIME_FORMAT", "P1M");

                    writer.WriteObservation("2005-Q1", "2.51");
                    writer.WriteAttributeValue("OBS_STATUS", "A");
                    writer.WriteObservation("2005-Q2", "2.52");
                    writer.WriteAttributeValue("OBS_STATUS", "A");
                    writer.Close();
                }

                stream.Flush();
            }

            ValidateRdf(outputFile);
        }
    }
}