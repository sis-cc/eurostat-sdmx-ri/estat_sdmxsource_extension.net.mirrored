// -----------------------------------------------------------------------
// <copyright file="TestToolIndicator.cs" company="EUROSTAT">
//   Date Created : 2016-09-07
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using Estat.Sdmxsource.Extension.Engine;
using NSubstitute;
using NUnit.Framework;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Manager.Parse;
using Org.Sdmxsource.Sdmx.Api.Model.Header;
using Org.Sdmxsource.Sdmx.Api.Model.Objects;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
using Org.Sdmxsource.Sdmx.DataParser.Engine;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Header;
using Org.Sdmxsource.Sdmx.Structureparser.Engine.Writing;
using Org.Sdmxsource.Sdmx.Structureparser.Manager.Parsing;
using Org.Sdmxsource.Sdmx.Structureparser.Model;
using Org.Sdmxsource.Sdmx.Util.Objects.Container;
using Org.Sdmxsource.Util.Io;
using Org.Sdmxsource.XmlHelper;

namespace CustomRequests.Tests
{
    [TestFixture]
    public class TestToolIndicator
    {
        private IToolIndicator toolIndicator;
        private AbstractWritingEngine decorated;
        private Stream stream;
        private string comment;
        private ISdmxObjects objects;
        private IDataflowObject _dataFlow;
        private IDataStructureObject _dataStructure;
        private IDatasetHeader datasetHeader;

        [SetUp]
        public void SetUp()
        {
            toolIndicator = Substitute.For<IToolIndicator>();
            comment = "This is a comment";
            stream = new FileStream(@"tests/test.xml", FileMode.OpenOrCreate);
            toolIndicator.GetComment().Returns(comment);
            decorated = new StructureWriterEngineV21(stream);

            objects = new SdmxObjectsImpl();
            IStructureParsingManager manager = new StructureParsingManager();
            using (var readable = new FileReadableDataLocation("tests/V21/Structure/test-sdmxv2.1-ESTAT+STS+2.0.xml"))
            {
                var structureWorkspace = manager.ParseStructures(readable);
                _dataStructure = structureWorkspace.GetStructureObjects(false).DataStructures.First();
            }

            using (
                var readable =
                    new FileReadableDataLocation("tests/V21/Structure/test-sdmxv2.1-ESTAT+SSTSCONS_PROD_M+2.0.xml"))
            {
                var structureWorkspace = manager.ParseStructures(readable);
                _dataFlow = structureWorkspace.GetStructureObjects(false).Dataflows.First();
            }

            objects.AddDataStructure(_dataStructure);
            objects.AddDataflow(_dataFlow);

            datasetHeader = new DatasetHeaderCore(
                        "UPDATE", 
                        DatasetAction.GetFromEnum(DatasetActionEnumType.Replace),
                        new DatasetStructureReferenceCore(_dataStructure.AsReference));
        }

        public WritingEngineDecorator CreateSUT()
        {
            return new WritingEngineDecorator(toolIndicator,decorated,SdmxSchemaEnumType.VersionTwoPointOne,stream,true);
        }

        [Test]
        public void TestTheEngine()
        {
            using (stream)
            {
                var writingEngineDecorator = CreateSUT();
                var node = writingEngineDecorator.Build(objects);
                //XMLParser.ValidateXml(stream,SdmxSchemaEnumType.VersionTwoPointOne);
                var comments = node.Document.Nodes().Where(x => x.NodeType == XmlNodeType.Comment);
            }
        }


        [Test]
        public void TestCompactDataWriterEngine()
        {
            using (stream)
            {
                var compactDataWriterEngine = new CompactDataWriterEngine(stream, SdmxSchema.GetFromEnum(SdmxSchemaEnumType.VersionTwoPointOne));
                compactDataWriterEngine.GeneratedFileComment = toolIndicator.GetComment();
                compactDataWriterEngine.StartDataset(_dataFlow, _dataStructure, datasetHeader, null);
                compactDataWriterEngine.WriteSeriesKeyValue("FREQ", "M");
                compactDataWriterEngine.WriteSeriesKeyValue("REF_AREA", "DE");
                compactDataWriterEngine.WriteSeriesKeyValue("ADJUSTMENT", "N");
                compactDataWriterEngine.WriteSeriesKeyValue("STS_INDICATOR", "PROD");
                compactDataWriterEngine.WriteSeriesKeyValue("STS_ACTIVITY", "NS0030");
                compactDataWriterEngine.WriteSeriesKeyValue("STS_INSTITUTION", "1");
                compactDataWriterEngine.WriteSeriesKeyValue("STS_BASE_YEAR", "2000");
                compactDataWriterEngine.WriteAttributeValue("TIME_FORMAT", GetTimeFormat("M"));
                var readToEnd = new StreamReader(stream).ReadToEnd();
            }
        }


        /// <summary>
        /// Writes the time format.
        /// </summary>
        /// <param name="key">The frequency code.</param>
        /// <returns>The TIme format.</returns>
        private static string GetTimeFormat(string key)
        {
            switch (key)
            {
                case "Q":

                    return "P3M";
                case "A":

                    return "P1Y";
                case "M":

                    return "P1M";
                default:
                    Assert.Fail("Test bug. Check CL_FREQ codes");
                    break;
            }

            return null;
        }
    }
}