#!/bin/bash

set -u
set -e

if (( $# == 0 )); then
    echo "USAGE: $0 <local nuget folder>"
    exit -1
fi

NUGET_REPO="$1"

if [[  -f nuget.config ]]; then
    rm nuget.config
fi

if [[ ! -e "$NUGET_REPO" ]]; then
    mkdir -p "$NUGET_REPO"
fi

dotnet new nugetconfig
dotnet nuget add source "$NUGET_REPO" -n local_repo --configfile nuget.config