﻿// -----------------------------------------------------------------------
// <copyright file="SdmxExtension.cs" company="EUROSTAT">
//   Date Created : 2016-07-07
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sdmxsource.Extension.Constant
{
    using System;

    /// <summary>
    /// The enumeration of SDMX extension for SDMX v2.0.
    /// </summary>
    [Flags]
    public enum SdmxExtension
    {
        /// <summary>
        /// No extension should be used.
        /// </summary>
        None = 0,

        /// <summary>
        /// Add the attribute "conceptSchemeVersion" in Components
        /// </summary>
        ConceptSchemeVersion = 1,

        /// <summary>
        /// Add the attribute "representationSchemeVersion" in MetadataAttribute and RepresentationScheme
        /// </summary>
        RepresentationSchemeVersion = 2,

        /// <summary>
        /// Add "TimeDimension" as an Target Identifier in MSDs
        /// </summary>
        TimeDimensionTargetIdentifier = 4,

        /// <summary>
        /// The ESTAT extensions
        /// </summary>
        EstatExtensions = ConceptSchemeVersion | RepresentationSchemeVersion | TimeDimensionTargetIdentifier
    }
}