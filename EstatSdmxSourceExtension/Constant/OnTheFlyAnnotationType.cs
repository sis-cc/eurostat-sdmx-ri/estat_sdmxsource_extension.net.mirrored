// -----------------------------------------------------------------------
// <copyright file="OnTheFlyAnnotationType.cs" company="EUROSTAT">
//   Date Created : 2018-1-9
//   Copyright (c) 2009, 2018 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sdmxsource.Extension.Constant
{
    using System;

    /// <summary>
    /// The enumeration of on the fly annotations
    /// </summary>
    [Flags]
    public enum OnTheFlyAnnotationType
    {
        /// <summary>
        /// Disable any on the fly annotation (default)
        /// </summary>
        None = 0,

        /// <summary>
        /// Provide local data annotations
        /// </summary>
        LocalData = 1
    }
}