// -----------------------------------------------------------------------
// <copyright file="PermissionType.cs" company="EUROSTAT">
//   Date Created : 2017-05-29
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sdmxsource.Extension.Constant
{
    using System;

    /// <summary>
    /// The permission attributes and roles
    /// </summary>
    [Flags]
    public enum PermissionType
    {
        /// <summary>
        /// The none
        /// </summary>
        None = 0,

        /// <summary>
        /// The can read structural metadata attribute
        /// </summary>
        CanReadStructuralMetadata = 1,

        /// <summary>
        /// The can read data attribute
        /// </summary>
        CanReadData = 2,

        /// <summary>
        /// The can ignore production flag attribute
        /// </summary>
        CanIgnoreProductionFlag = 4,

        /// <summary>
        /// The can perform internal mapping configuration attribute
        /// </summary>
        CanPerformInternalMappingConfig = 8,

        /// <summary>
        /// The can import structures attribute
        /// </summary>
        CanImportStructures = 16,

        /// <summary>
        /// The can import data attribute
        /// </summary>
        CanImportData = 32,

        /// <summary>
        /// The can modify store settings attribute
        /// </summary>
        CanModifyStoreSettings = 64,

        /// <summary>
        /// The can update structural metadata attribute
        /// </summary>
        CanUpdateStructuralMetadata = 128,

        /// <summary>
        /// The can update data attribute
        /// </summary>
        CanUpdateData = 256,

        /// <summary>
        /// The can delete structural metadata attribute
        /// </summary>
        CanDeleteStructuralMetadata = 512,

        /// <summary>
        /// The can delete data attribute
        /// </summary>
        CanDeleteData = 1024,

        /// <summary>
        /// The can read point in time data attribute
        /// </summary>
        CanReadPitData = 2048,

        /// <summary>
        /// The Web Service external user role
        /// </summary>
        WsUserRole = CanReadStructuralMetadata | CanReadData,

        /// <summary>
        /// The domain/internal user, that can map between DDB and SDMX. In SDMX RI the Mapping Assistant user role
        /// </summary>
        DomainUserRole = WsUserRole | CanIgnoreProductionFlag | CanPerformInternalMappingConfig,

        /// <summary>
        /// The structure importer role
        /// </summary>
        StructureImporterRole = CanReadStructuralMetadata | CanImportStructures | CanUpdateStructuralMetadata | CanDeleteStructuralMetadata,

        /// <summary>
        /// The data importer role
        /// </summary>
        DataImporterRole = CanReadStructuralMetadata | CanReadData | CanImportData | CanUpdateData | CanDeleteData,

        /// <summary>
        /// The structure importer role for update
        /// </summary>
        StructureImporterRole_U = CanReadStructuralMetadata | CanImportStructures | CanUpdateStructuralMetadata,

        /// <summary>
        /// The data importer role for update
        /// </summary>
        DataImporterRole_U = CanReadStructuralMetadata | CanReadData | CanImportData | CanUpdateData,

        // <summary>
        /// The structure importer role for update and delete
        /// </summary>
        StructureImporterRole_UD = CanReadStructuralMetadata | CanImportStructures | CanUpdateStructuralMetadata | CanDeleteStructuralMetadata,

        /// <summary>
        /// The data importer role for update and delete
        /// </summary>
        DataImporterRole_UD = CanReadStructuralMetadata | CanReadData | CanImportData | CanUpdateData | CanDeleteData,

        /// <summary>
        /// The administrator (root) user role
        /// </summary>
        AdminRole = WsUserRole | DomainUserRole | StructureImporterRole | DataImporterRole | CanModifyStoreSettings | CanUpdateStructuralMetadata | CanUpdateData | CanDeleteStructuralMetadata | CanDeleteData | CanReadPitData
    }
}