namespace Estat.Sdmxsource.Extension.Constant
{
    internal static class HttpConstants
    {
        /// <summary>
        ///     HTTP Header content type
        /// </summary>
        public const string Content = "text/xml;charset=\"utf-8\"";
        public const string ApplicationXml = "application/xml;charset=\"utf-8\"";

        /// <summary>
        ///     HTTP POST
        /// </summary>
        public const string Post = "POST";

        /// <summary>
        ///     HTTP GET
        /// </summary>
        public const string Get = "GET";
    }
}