// -----------------------------------------------------------------------
// <copyright file="CustomAnnotationTypeExtensions.cs" company="EUROSTAT">
//   Date Created : 2013-07-15
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sdmxsource.Extension.Constant
{
    using System;
    using System.Linq;
    using System.Xml;

    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;

    /// <summary>
    ///     The custom annotation type extensions.
    /// </summary>
    public static class CustomAnnotationTypeExtensions
    {
        /// <summary>
        /// The _english locale
        /// </summary>
        private const string EnglishLocale = "en";

        /// <summary>
        ///     The category scheme node order.
        /// </summary>
        private const string CategoryschemeNodeOrderDefault = "CategoryScheme_node_order";

        /// <summary>
        ///     a constant holding the annotation text for crossX DSDs retrieved in 2.1 queries
        /// </summary>
        private const string Crossx21AnnotationText =
            "This is something that can be queried using only SDMX v2.0 endpoints";

        /// <summary>
        ///     a constant holding the annotation title for crossX DSDs retrieved in 2.1 queries
        /// </summary>
        private const string Crossx21AnnotationTitle = "Only for SDMX v2.0";

        /// <summary>
        ///     The non production dataflow
        /// </summary>
        private const string ExternalUsageDataflow = "ExternalUsageDataflow";

        /// <summary>
        ///     The non production dataflow
        /// </summary>
        private const string NonProductionDataflow = "NonProductionDataflow";

        private const string Crossx21AnnotationType = "SDMX_v2_0_XS_DSD";

        /// <summary>
        ///     Returns the <see cref="CustomAnnotationType" /> from annotation.
        /// </summary>
        /// <param name="annotation">
        ///     The annotation.
        /// </param>
        /// <returns>
        ///     The <see cref="CustomAnnotationType" />.
        /// </returns>
        public static CustomAnnotationType FromAnnotation(this IAnnotationMutableObject annotation)
        {
            if (annotation != null)
            {
                if (!string.IsNullOrWhiteSpace(annotation.Type))
                {
                    if (annotation.Type.Equals(CategoryNodeOrderType))
                    {
                        return CustomAnnotationType.CategorySchemeNodeOrder;
                    }
                    else
                    {
                        switch (annotation.Type)
                        {
                            case NonProductionDataflow:
                                return CustomAnnotationType.NonProductionDataflow;
                            case ExternalUsageDataflow:
                                return CustomAnnotationType.ExternalUsageDataflow;
                            case Crossx21AnnotationType:
                                return CustomAnnotationType.SDMXv20Only;
                        }
                    }
                }
                else
                {
                    switch (annotation.Title)
                    {
                        case Crossx21AnnotationTitle:
                            return CustomAnnotationType.SDMXv20Only;
                    }
                }
            }

            return CustomAnnotationType.None;
        }

        /// <summary>
        ///     Returns the <see cref="CustomAnnotationType" /> from annotation.
        /// </summary>
        /// <param name="annotation">
        ///     The annotation.
        /// </param>
        /// <returns>
        ///     The <see cref="CustomAnnotationType" />.
        /// </returns>
        public static CustomAnnotationType FromAnnotation(this IAnnotation annotation)
        {
            if (annotation != null)
            {
                if (!string.IsNullOrWhiteSpace(annotation.Type))
                {
                    if (annotation.Type.Equals(CategoryNodeOrderType))
                    {
                        return CustomAnnotationType.CategorySchemeNodeOrder;
                    }
                    else
                    {
                        switch (annotation.Type)
                        {
                            case NonProductionDataflow:
                                return CustomAnnotationType.NonProductionDataflow;
                            case ExternalUsageDataflow:
                                return CustomAnnotationType.ExternalUsageDataflow;
                            case Crossx21AnnotationType:
                                return CustomAnnotationType.SDMXv20Only;
                        }
                    }
                }
                else
                {
                    switch (annotation.Title)
                    {
                        case Crossx21AnnotationTitle:
                            return CustomAnnotationType.SDMXv20Only;
                    }
                }
            }

            return CustomAnnotationType.None;
        }

        /// <summary>
        ///     Determines whether the specified dataflow is external usage
        /// </summary>
        /// <param name="dataflow">The dataflow.</param>
        /// <returns>true if the dataflow is external usage; otherwise true.</returns>
        public static bool IsExternalUsage(this IAnnotableMutableObject dataflow)
        {
            if (dataflow == null)
            {
                throw new ArgumentNullException("dataflow");
            }

            return
                dataflow.Annotations.Any(
                    o =>
                        o.FromAnnotation() == CustomAnnotationType.ExternalUsageDataflow
                        && o.ValueFromAnnotation().Equals(XmlConvert.ToString(true)));
        }

        /// <summary>
        ///     Determines whether the specified dataflow is used external
        /// </summary>
        /// <param name="dataflow">The dataflow.</param>
        /// <returns>
        ///     true if the dataflow is external usage; otherwise true.
        /// </returns>
        public static bool IsExternalUsage(this IAnnotableObject dataflow)
        {
            if (dataflow == null)
            {
                throw new ArgumentNullException("dataflow");
            }

            return
                dataflow.Annotations.Any(
                    o =>
                        o.FromAnnotation() == CustomAnnotationType.ExternalUsageDataflow
                        && o.ValueFromAnnotation().Equals(XmlConvert.ToString(true)));
        }

        /// <summary>
        ///     Determines whether the specified dataflow is not in production
        /// </summary>
        /// <param name="dataflow">The dataflow.</param>
        /// <returns>true if the dataflow is not in production; otherwise true.</returns>
        public static bool IsNonProduction(this IAnnotableMutableObject dataflow)
        {
            if (dataflow == null)
            {
                throw new ArgumentNullException("dataflow");
            }

            return
                dataflow.Annotations.Any(
                    o =>
                        o.FromAnnotation() == CustomAnnotationType.NonProductionDataflow
                        && o.ValueFromAnnotation().Equals(XmlConvert.ToString(true)));
        }

        /// <summary>
        ///     Determines whether the specified dataflow is not in production
        /// </summary>
        /// <param name="dataflow">The dataflow.</param>
        /// <returns>true if the dataflow is not in production; otherwise true.</returns>
        public static bool IsNonProduction(this IAnnotableObject dataflow)
        {
            if (dataflow == null)
            {
                throw new ArgumentNullException("dataflow");
            }

            return
                dataflow.Annotations.Any(
                    o =>
                        o.FromAnnotation() == CustomAnnotationType.NonProductionDataflow
                        && o.ValueFromAnnotation().Equals(XmlConvert.ToString(true)));
        }

        /// <summary>
        ///     Sets the annotation external usage to a dataflow.
        /// </summary>
        /// <param name="dataflowMutableObject">The dataflow mutable object.</param>
        public static void SetExternalUsage(this IAnnotableMutableObject dataflowMutableObject)
        {
            if (dataflowMutableObject == null)
            {
                throw new ArgumentNullException("dataflowMutableObject");
            }

            var annotation =
                CustomAnnotationType.ExternalUsageDataflow.ToAnnotation<AnnotationMutableCore>(
                    XmlConvert.ToString(true));
            dataflowMutableObject.AddAnnotation(annotation);
        }

        /// <summary>
        ///     Sets the annotation non production to a dataflow.
        /// </summary>
        /// <param name="dataflowMutableObject">The dataflow mutable object.</param>
        public static void SetNonProduction(this IAnnotableMutableObject dataflowMutableObject)
        {
            if (dataflowMutableObject == null)
            {
                throw new ArgumentNullException("dataflowMutableObject");
            }

            var annotation =
                CustomAnnotationType.NonProductionDataflow.ToAnnotation<AnnotationMutableCore>(
                    XmlConvert.ToString(true));
            dataflowMutableObject.AddAnnotation(annotation);
        }

        /// <summary>
        ///     Create and return an annotation of type <typeparamref name="T" />
        /// </summary>
        /// <param name="customAnnotation">
        ///     The custom annotation.
        /// </param>
        /// <typeparam name="T">
        ///     The concrete type of <see cref="IAnnotationMutableObject" />
        /// </typeparam>
        /// <returns>
        ///     The <typeparamref name="T" />.
        /// </returns>
        public static T ToAnnotation<T>(this CustomAnnotationType customAnnotation)
            where T : IAnnotationMutableObject, new()
        {
            var annotation = new T();
            switch (customAnnotation)
            {
                case CustomAnnotationType.CategorySchemeNodeOrder:
                    annotation.Type = customAnnotation.ToStringValue();
                    break;
                case CustomAnnotationType.NonProductionDataflow:
                    annotation.Type = customAnnotation.ToStringValue();
                    break;
                case CustomAnnotationType.ExternalUsageDataflow:
                    annotation.Type = customAnnotation.ToStringValue();
                    break;
                case CustomAnnotationType.SDMXv20Only:
                    annotation = new T { Title = customAnnotation.ToStringValue() };
                    annotation.AddText(EnglishLocale, Crossx21AnnotationText);
                    annotation.Type = Crossx21AnnotationType;
                    break;
                case CustomAnnotationType.SDMXv21Only:
                    break;
                case CustomAnnotationType.ReportingPeriod:
                    annotation.Type = customAnnotation.ToString();
                    break;
            }

            return annotation;
        }

        /// <summary>
        ///     Create and return an annotation of type <typeparamref name="T" />
        /// </summary>
        /// <param name="customAnnotation">
        ///     The custom annotation.
        /// </param>
        /// <param name="value">
        ///     The value.
        /// </param>
        /// <typeparam name="T">
        ///     The concrete type of <see cref="IAnnotationMutableObject" />
        /// </typeparam>
        /// <returns>
        ///     The <typeparamref name="T" />.
        /// </returns>
        public static T ToAnnotation<T>(this CustomAnnotationType customAnnotation, string value)
            where T : IAnnotationMutableObject, new()
        {
            var annotation = customAnnotation.ToAnnotation<T>();
            annotation.AddText(EnglishLocale, value);

            return annotation;
        }

        /// <summary>
        ///     Returns the string value associated with <paramref name="customAnnotation" />
        /// </summary>
        /// <param name="customAnnotation">
        ///     The custom annotation.
        /// </param>
        /// <returns>
        ///     The string value associated with <paramref name="customAnnotation" />
        /// </returns>
        public static string ToStringValue(this CustomAnnotationType customAnnotation)
        {
            if (customAnnotation.Equals(CategoryNodeOrderType))
            {
                return CategoryNodeOrderType;
            }
            switch (customAnnotation)
            {
                case CustomAnnotationType.NonProductionDataflow:
                    return NonProductionDataflow;
                case CustomAnnotationType.ExternalUsageDataflow:
                    return ExternalUsageDataflow;
                case CustomAnnotationType.SDMXv20Only:
                    return Crossx21AnnotationTitle;
                default:
                    return null;
            }
        }

        /// <summary>
        ///     Returns  value from annotation.
        /// </summary>
        /// <param name="annotation">
        ///     The annotation.
        /// </param>
        /// <returns>
        ///     The <see cref="string" />.
        /// </returns>
        public static string ValueFromAnnotation(this IAnnotationMutableObject annotation)
        {
            if(annotation.Type == CategoryNodeOrderType && CategoryNodeOrderOptions.ReadOrderFrom == "Title")
            {
                return annotation.Title;
            }
            if (annotation != null && !string.IsNullOrWhiteSpace(annotation.Type) && annotation.Text.Count > 0)
            {
                return annotation.Text[0].Value;
            }

            return null;
        }

        /// <summary>
        ///     Returns  value from annotation.
        /// </summary>
        /// <param name="annotation">
        ///     The annotation.
        /// </param>
        /// <returns>
        ///     The <see cref="string" />.
        /// </returns>
        public static string ValueFromAnnotation(this IAnnotation annotation)
        {
            if (annotation.Type == CategoryNodeOrderType && CategoryNodeOrderOptions.ReadOrderFrom == "Title")
            {
                return annotation.Title;
            }
            if (annotation != null && !string.IsNullOrWhiteSpace(annotation.Type) && annotation.Text.Count > 0)
            {
                return annotation.Text[0].Value;
            }

            return null;
        }

        public static CategoryNodeOrderOptions CategoryNodeOrderOptions { get; set; }
        
        public static string CategoryNodeOrderType
        {
            get
            {
                if (CategoryNodeOrderOptions != null)
                {
                    return CategoryNodeOrderOptions.Type;
                }
                return CategoryschemeNodeOrderDefault;
            }
        }

        public static bool IsCustomAnnotation(this IAnnotation annotation)
        {
            return annotation.FromAnnotation() !=  CustomAnnotationType.None;
        }
    }
}