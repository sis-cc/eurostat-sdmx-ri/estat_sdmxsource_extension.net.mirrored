﻿// -----------------------------------------------------------------------
// <copyright file="SoapConstants.cs" company="EUROSTAT">
//   Date Created : 2017-6-15
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sdmxsource.Extension.Constant
{
    internal static class SoapConstants
    {
        /// <summary>
        ///     SOAP 1.1 namespace
        /// </summary>
        public const string Soap11Ns = "http://schemas.xmlsoap.org/soap/envelope/";

        /// <summary>
        ///     This field holds a template for soap 1.1 request envelope
        /// </summary>
        public const string SoapRequest =
            "<soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:{0}=\"{1}\"><soap:Header/><soap:Body></soap:Body></soap:Envelope>";

        /// <summary>
        ///     SOAP Body tag
        /// </summary>
        public const string Body = "Body";

        /// <summary>
        ///     SOAPAction HTTP header
        /// </summary>
        public const string SoapAction = "SOAPAction";

        /// <summary>
        /// The SOAP prefix
        /// </summary>
        public const string SoapPrefix = "soap";

        /// <summary>
        /// The envelope tag
        /// </summary>
        public const string Envelope = "Envelope";

        /// <summary>
        /// The header tag
        /// </summary>
        public const string Header = "Header";
    }
}