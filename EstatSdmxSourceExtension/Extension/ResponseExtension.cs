// -----------------------------------------------------------------------
// <copyright file="ResponseExtension.cs" company="EUROSTAT">
//   Date Created : 2017-11-17
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sdmxsource.Extension.Extension
{
    using Estat.Sdmxsource.Extension.Model.Error;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;

    /// <summary>
    /// A set of extension methods for <see cref="IResponseWithStatusObject"/>
    /// </summary>
    public static class ResponseExtension
    {
        /// <summary>
        /// Gets the HTTP code.
        /// </summary>
        /// <param name="responseWithStatusList">The response with status list.</param>
        /// <returns>The HTTP Status code</returns>
        public static int GetHttpCode(this IEnumerable<IResponseWithStatusObject> responseWithStatusList)
        {
            var lookup = responseWithStatusList.ToLookup(x => x.Status);

            int success = 0;
            int warning = 0;
            int error = 0;
            var errorCodes = new HashSet<HttpStatusCode>();
            foreach(var response in lookup)
            {
                var currentErrorCodes = response.SelectMany(x => x.Messages).Select(m => m.HttpCode).ToArray();
                switch(response.Key)
                {
                    case Constant.ResponseStatus.Success:
                        success++;
                        errorCodes.UnionWith(currentErrorCodes);
                        break;
                    case Constant.ResponseStatus.Warning:
                        warning++;
                        errorCodes.UnionWith(currentErrorCodes);
                        break;
                    case Constant.ResponseStatus.Failure:
                        error++;
                        errorCodes.UnionWith(currentErrorCodes);
                        break;
                }
            }

            // if we have many http codes we have multi status
            if (errorCodes.Count > 1)
            {
                if (error == 0 && warning == 0)
                {
                    // Examples:
                    // non-final arterfact delete and insert
                    // multiple actions, updating, deleting and inserting artefacts
                    if (errorCodes.Contains(HttpStatusCode.Created))
                    {
                        return (int)HttpStatusCode.Created;
                    }

                    return (int)HttpStatusCode.OK;
                }

                // multi status only for mixed success with warning/error
                return 207; // MultiStatus
            }

            if (errorCodes.Count == 1)
            {
                return (int)errorCodes.First();
            }

            // if there are no SdmxError Codes and no success, then it is most probable that we hit some legacy checks that do not generate an exception
            if (warning > 0)
            {
                if (error > 0)
                {
                    return 207; // MultiStatus
                }

                return (int)HttpStatusCode.BadRequest;
            }

            return (int)HttpStatusCode.InternalServerError;
        }
    }
}
