﻿// -----------------------------------------------------------------------
// <copyright file="DataTypeExtension.cs" company="EUROSTAT">
//   Date Created : 2015-12-17
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sdmxsource.Extension.Extension
{
    using Org.Sdmxsource.Sdmx.Api.Constants;

    /// <summary>
    /// Extensions related to Data types
    /// </summary>
    public static class DataTypeExtension
    {
        /// <summary>
        /// Gets the corresponding <see cref="DataType"/> of a <see cref="BaseDataFormat"/> from the specified <paramref name="sdmxSchema"/>
        /// </summary>
        /// <param name="dataFormat">The data format.</param>
        /// <param name="sdmxSchema">The SDMX schema.</param>
        /// <returns>The <see cref="DataType"/>.</returns>
        public static DataType GetDataType(this BaseDataFormatEnumType dataFormat, SdmxSchemaEnumType sdmxSchema)
        {
            foreach (var dataType in DataType.Values)
            {
                if (dataType.BaseDataFormat.EnumType == dataFormat && dataType.SchemaVersion.EnumType == sdmxSchema)
                {
                    return dataType;
                }
            }

            return null;
        }

        /// <summary>
        /// Gets the corresponding <see cref="DataType"/> of a <see cref="BaseDataFormat"/> from the specified <paramref name="sdmxSchema"/>
        /// </summary>
        /// <param name="dataFormat">The data format.</param>
        /// <param name="sdmxSchema">The SDMX schema.</param>
        /// <returns>The <see cref="DataType"/>.</returns>
        public static DataType GetDataType(this BaseDataFormat dataFormat, SdmxSchemaEnumType sdmxSchema)
        {
            if (dataFormat == null)
            {
                return null;
            }

            return dataFormat.EnumType.GetDataType(sdmxSchema);
        }

        /// <summary>
        /// Gets the corresponding <see cref="DataType"/> of a <see cref="BaseDataFormat"/> from the specified <paramref name="sdmxSchema"/>
        /// </summary>
        /// <param name="dataFormat">The data format.</param>
        /// <param name="sdmxSchema">The SDMX schema.</param>
        /// <returns>The <see cref="DataType"/>.</returns>
        public static DataType GetDataType(this BaseDataFormat dataFormat, SdmxSchema sdmxSchema)
        {
            if (dataFormat == null)
            {
                return null;
            }

            return dataFormat.EnumType.GetDataType(sdmxSchema);
        }
    }
}