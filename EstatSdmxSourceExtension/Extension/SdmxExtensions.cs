﻿// -----------------------------------------------------------------------
// <copyright file="SdmxExtensions.cs" company="EUROSTAT">
//   Date Created : 2017-07-10
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sdmxsource.Extension.Extension
{
    using System;
    using System.Linq;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Mapping;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;

    /// <summary>
    /// A collection of SDMX Object related extensions
    /// </summary>
    public static class SdmxExtensions
    {
        /// <summary>
        /// Determines whether the <paramref name="dataflow"/> matches the specified <paramref name="structureOrUsageReference"/> either by it self or by it's <see cref="IDataflowObject.DataStructureRef"/>
        /// </summary>
        /// <param name="dataflow">The dataflow.</param>
        /// <param name="structureOrUsageReference">The structure or usage reference.</param>
        /// <returns>
        ///   <c>true</c> if the <paramref name="dataflow"/> matches the specified <paramref name="structureOrUsageReference"/> either by it self or by it's <see cref="IDataflowObject.DataStructureRef"/>; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsStructureOrUsageMatch(this IDataflowObject dataflow, IStructureReference structureOrUsageReference)
        {
            if (dataflow == null)
            {
                throw new ArgumentNullException(nameof(dataflow));
            }

            if (structureOrUsageReference == null)
            {
                throw new ArgumentNullException(nameof(structureOrUsageReference));
            }

            // Check if structure usage is a dataflow and matches the provided dataflow or if it matches the DSD reference
            return (structureOrUsageReference.TargetReference.EnumType == SdmxStructureEnumType.Dataflow && structureOrUsageReference.IsMatch(dataflow))
                || structureOrUsageReference.Equals(dataflow.DataStructureRef);
        }

        /// <summary>
        /// Gets the codelist map.
        /// </summary>
        /// <param name="componentMap">The component map.</param>
        /// <returns></returns>
        /// <exception cref="ArgumentNullException">componentMap is null</exception>
        public static ICodelistMapObject GetCodelistMap(this IComponentMapObject componentMap)
        {
            if (componentMap == null)
            {
                throw new ArgumentNullException(nameof(componentMap));
            }

            var crossReference = componentMap.RepMapRef?.CodelistMap;
            if (crossReference == null)
            {
                return null;
            }

            IStructureSetObject structureSet = (IStructureSetObject)componentMap.MaintainableParent;
            var codelistMapObject = structureSet.CodelistMapList.FirstOrDefault(o => crossReference.IsMatch(o));
            return codelistMapObject;
        }
    }
}