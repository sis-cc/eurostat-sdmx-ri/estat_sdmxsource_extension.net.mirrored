// -----------------------------------------------------------------------
// <copyright file="XmlExtension.cs" company="EUROSTAT">
//   Date Created : 2018-1-18
//   Copyright (c) 2009, 2018 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sdmxsource.Extension.Extension
{
    using System;
    using System.Xml;
    using System.Xml.Linq;

    /// <summary>
    /// XML related extensions
    /// </summary>
    public static class XmlExtension
    {
        /// <summary>
        /// Gets the writer action.
        /// </summary>
        /// <param name="document">The document.</param>
        /// <returns>The action that writes the <paramref name="document"/> to a <see cref="XmlWriter"/></returns>
        public static Action<XmlWriter> GetWriterAction(this XmlNode document)
        {
            return document.WriteTo;
        }

        /// <summary>
        /// Gets the writer action.
        /// </summary>
        /// <param name="document">The document.</param>
        /// <returns>The action that writes the <paramref name="document"/> to a <see cref="XmlWriter"/></returns>
        public static Action<XmlWriter> GetWriterAction(this XElement document)
        {
            return document.WriteTo;
        }
    }
}