// -----------------------------------------------------------------------
// <copyright file="QueryExtensions.cs" company="EUROSTAT">
//   Date Created : 2013-10-04
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sdmxsource.Extension.Extension
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Complex;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference.Complex;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;

    /// <summary>
    ///     Various Structure query related extensions
    /// </summary>
    public static class QueryExtensions
    {
        /// <summary>
        ///     Gets the complex query detail from the specified <paramref name="returnStub" />
        /// </summary>
        /// <param name="returnStub">
        ///     The return stub.
        /// </param>
        /// <returns>
        ///     The <see cref="ComplexStructureQueryDetailEnumType" />.
        /// </returns>
        public static ComplexStructureQueryDetailEnumType GetComplexQueryDetail(this bool returnStub)
        {
            return returnStub ? ComplexStructureQueryDetailEnumType.Stub : ComplexStructureQueryDetailEnumType.Full;
        }

        /// <summary>
        ///     Returns a <see cref="IMaintainableRefObject" /> from the specified <paramref name="complexStructureQuery" />
        /// </summary>
        /// <param name="complexStructureQuery">
        ///     The complex structure query.
        /// </param>
        /// <returns>
        ///     The <see cref="IMaintainableRefObject" />.
        /// </returns>
        /// <remarks>This method currently assumes all operators are <c>EQUAL</c></remarks>
        public static IMaintainableRefObject GetMaintainableRefObject(
            this IComplexStructureReferenceObject complexStructureQuery)
        {
            if (complexStructureQuery == null)
            {
                throw new ArgumentNullException("complexStructureQuery");
            }

            string version = complexStructureQuery.VersionReference.Version;

            string id = complexStructureQuery.Id != null ? complexStructureQuery.Id.SearchParameter : null;

            string agencyId = complexStructureQuery.AgencyId != null
                ? complexStructureQuery.AgencyId.SearchParameter
                : null;

            return new MaintainableRefObjectImpl(agencyId, id, version);
        }

        /// <summary>
        ///     Gets the return stub value.
        /// </summary>
        /// <param name="detail">
        ///     The detail.
        /// </param>
        /// <returns>
        ///     The return stub value
        /// </returns>
        public static bool GetReturnStub(this BaseConstantType<StructureQueryDetailEnumType> detail)
        {
            return detail == StructureQueryDetailEnumType.AllStubs;
        }

        /// <summary>
        ///     Gets the complex query detail from the specified <paramref name="returnStub" />
        /// </summary>
        /// <param name="returnStub">
        ///     The return stub.
        /// </param>
        /// <returns>
        ///     The <see cref="ComplexStructureQueryDetailEnumType" />.
        /// </returns>
        public static StructureQueryDetail GetStructureQueryDetail(this bool returnStub)
        {
            return
                StructureQueryDetail.GetFromEnum(
                    returnStub ? StructureQueryDetailEnumType.AllStubs : StructureQueryDetailEnumType.Full);
        }

        /// <summary>
        ///     Gets the structure query detail from the specified <see cref="ComplexStructureQueryDetail" /> />
        /// </summary>
        /// <param name="complexStructureQueryDetail">
        ///     The complex structure query detail
        /// </param>
        /// <returns>
        ///     The <see cref="StructureQueryDetail" />.
        /// </returns>
        public static StructureQueryDetail GetStructureQueryDetail(this ComplexStructureQueryDetail complexStructureQueryDetail)
        {
            StructureQueryDetailEnumType structureQueryDetail;
            switch (complexStructureQueryDetail.EnumType)
            {
                case ComplexStructureQueryDetailEnumType.CompleteStub:
                    structureQueryDetail = StructureQueryDetailEnumType.AllCompleteStubs;
                    break;
                case ComplexStructureQueryDetailEnumType.Stub:
                    structureQueryDetail = StructureQueryDetailEnumType.AllStubs;
                    break;
                case ComplexStructureQueryDetailEnumType.Null:
                    structureQueryDetail = StructureQueryDetailEnumType.Null;
                    break;
                default:
                    structureQueryDetail = StructureQueryDetailEnumType.Full;
                    break;
            }

            return StructureQueryDetail.GetFromEnum(structureQueryDetail);
        }

        /// <summary>
        ///     Convert to <see cref="IComplexStructureReferenceObject" />.
        /// </summary>
        /// <param name="reference">The reference.</param>
        /// <returns>The <see cref="IComplexStructureReferenceObject" />.</returns>
        public static IComplexStructureReferenceObject ToComplex(this IStructureReference reference)
        {
            if (reference == null)
            {
                throw new ArgumentNullException("reference");
            }

            var maintainableRef = reference.MaintainableReference;
            IComplexTextReference id = null;
            if (maintainableRef.HasMaintainableId())
            {
                id = new ComplexTextReferenceCore(
                    null, 
                    TextSearch.GetFromEnum(TextSearchEnumType.Equal), 
                    maintainableRef.MaintainableId);
            }

            IComplexTextReference agency = null;
            if (maintainableRef.HasAgencyId())
            {
                agency = new ComplexTextReferenceCore(
                    null, 
                    TextSearch.GetFromEnum(TextSearchEnumType.Equal), 
                    maintainableRef.AgencyId);
            }

            IComplexVersionReference version = maintainableRef.HasVersion()
                ? new ComplexVersionReferenceCore(
                    TertiaryBool.ParseBoolean(false), 
                    maintainableRef.Version, 
                    null, 
                    null)
                : new ComplexVersionReferenceCore(
                    TertiaryBool.ParseBoolean(true), 
                    null, 
                    null, 
                    null);

            return new ComplexStructureReferenceCore(
                agency, 
                id, 
                version, 
                reference.MaintainableStructureEnumType, 
                null, 
                null, 
                null, 
                null);
        }

        /// <summary>
        ///     Convert to <see cref="IComplexStructureReferenceObject" />.
        /// </summary>
        /// <param name="reference">The reference.</param>
        /// <returns>The <see cref="IComplexStructureReferenceObject" />.</returns>
        public static IComplexStructureReferenceObject ToComplex(this IStructureReference reference, ISet<string> childrenItems)
        {
            if (reference == null)
            {
                throw new ArgumentNullException("reference");
            }

            var maintainableRef = reference.MaintainableReference;
            IComplexTextReference id = null;
            if (maintainableRef.HasMaintainableId())
            {
                id = new ComplexTextReferenceCore(
                    null,
                    TextSearch.GetFromEnum(TextSearchEnumType.Equal),
                    maintainableRef.MaintainableId);
            }

            IComplexTextReference agency = null;
            if (maintainableRef.HasAgencyId())
            {
                agency = new ComplexTextReferenceCore(
                    null,
                    TextSearch.GetFromEnum(TextSearchEnumType.Equal),
                    maintainableRef.AgencyId);
            }

            IComplexVersionReference version = maintainableRef.HasVersion()
                ? new ComplexVersionReferenceCore(
                    TertiaryBool.ParseBoolean(false),
                    maintainableRef.Version,
                    null,
                    null)
                : new ComplexVersionReferenceCore(
                    TertiaryBool.ParseBoolean(true),
                    null,
                    null,
                    null);
            List<IComplexIdentifiableReferenceObject> childReferences = null;
            if (childrenItems != null)
            {
                childReferences = new List<IComplexIdentifiableReferenceObject>();
               foreach(string specificItem in childrenItems.Where(x => !string.IsNullOrWhiteSpace(x)).SelectMany(x => x.Split('.')))
                {
                    if (string.IsNullOrWhiteSpace(specificItem))
                    {
                        continue;
                    }

                    var complexTextReferenceCore = new ComplexTextReferenceCore(
                        "en",
                        TextSearch.GetFromEnum(TextSearchEnumType.Equal),
                        specificItem);
                    var complexIdentifiableReferenceCore = new ComplexIdentifiableReferenceCore(
                        complexTextReferenceCore,
                        reference.TargetReference,
                        null,
                        null,
                        null,
                        null);
                    childReferences.Add(complexIdentifiableReferenceCore);
                }
            }

            return new ComplexStructureReferenceCore(
                agency,
                id,
                version,
                reference.MaintainableStructureEnumType,
                null,
                null,
                null,
                childReferences);
        }

        /// <summary>
        ///     Convert to <see cref="ComplexStructureQueryDetail" />
        /// </summary>
        /// <param name="detail">The detail.</param>
        /// <returns>The <see cref="ComplexStructureQueryDetail" /></returns>
        public static ComplexStructureQueryDetail ToComplex(this BaseConstantType<StructureQueryDetailEnumType> detail)
        {
            if (detail == null)
            {
                throw new ArgumentNullException("detail");
            }

            ComplexStructureQueryDetailEnumType complexStructureQueryDetail;
            switch (detail.EnumType)
            {
                case StructureQueryDetailEnumType.AllStubs:
                    complexStructureQueryDetail = ComplexStructureQueryDetailEnumType.Stub;
                    break;
                case StructureQueryDetailEnumType.AllCompleteStubs:
                    complexStructureQueryDetail = ComplexStructureQueryDetailEnumType.CompleteStub;
                    break;
                default:
                    complexStructureQueryDetail = ComplexStructureQueryDetailEnumType.Full;
                    break;
            }

            return ComplexStructureQueryDetail.GetFromEnum(complexStructureQueryDetail);
        }

        /// <summary>
        ///     Convert to <see cref="ComplexStructureQueryDetail" /> for referenced maintainables
        /// </summary>
        /// <param name="detail">The detail.</param>
        /// <returns>The <see cref="ComplexStructureQueryDetail" /></returns>
        public static ComplexStructureQueryDetail ToComplexForReferences(this BaseConstantType<StructureQueryDetailEnumType> detail)
        {
            if (detail == null)
            {
                throw new ArgumentNullException("detail");
            }

            ComplexStructureQueryDetailEnumType complexStructureQueryDetail;
            switch (detail.EnumType)
            {
                case StructureQueryDetailEnumType.AllStubs:
                case StructureQueryDetailEnumType.ReferencedStubs:
                    complexStructureQueryDetail = ComplexStructureQueryDetailEnumType.Stub;
                    break;
                case StructureQueryDetailEnumType.AllCompleteStubs:
                    complexStructureQueryDetail = ComplexStructureQueryDetailEnumType.CompleteStub;
                    break;
                default:
                    complexStructureQueryDetail = ComplexStructureQueryDetailEnumType.Full;
                    break;
            }

            return ComplexStructureQueryDetail.GetFromEnum(complexStructureQueryDetail);
        }

        /// <summary>
        ///     Gets <see cref="StructureQueryDetail" />  for referenced maintainables
        /// </summary>
        /// <param name="detail">The detail.</param>
        /// <returns>The <see cref="StructureQueryDetail" /></returns>
        public static StructureQueryDetail ToReferenceQueryDetail(this BaseConstantType<StructureQueryDetailEnumType> detail)
        {
            if (detail == null)
            {
                throw new ArgumentNullException("detail");
            }

            StructureQueryDetailEnumType structureQueryDetail;

            structureQueryDetail = detail.EnumType == StructureQueryDetailEnumType.ReferencedStubs ?
                                     StructureQueryDetailEnumType.AllStubs :
                                     detail;
                
            return StructureQueryDetail.GetFromEnum(structureQueryDetail);
        }

        /// <summary>
        ///     Convert to <see cref="ComplexMaintainableQueryDetail" />
        /// </summary>
        /// <param name="detail">
        ///     The detail.
        /// </param>
        /// <returns>
        ///     The <see cref="ComplexMaintainableQueryDetail" />.
        /// </returns>
        public static ComplexMaintainableQueryDetail ToComplexReference(
            this BaseConstantType<StructureQueryDetailEnumType> detail)
        {
            if (detail == null)
            {
                throw new ArgumentNullException("detail");
            }

            ComplexMaintainableQueryDetailEnumType complexStructureQueryDetail;
            switch (detail.EnumType)
            {
                case StructureQueryDetailEnumType.AllStubs:
                    complexStructureQueryDetail = ComplexMaintainableQueryDetailEnumType.Stub;
                    break;
                default:
                    complexStructureQueryDetail = ComplexMaintainableQueryDetailEnumType.Full;
                    break;
            }

            return ComplexMaintainableQueryDetail.GetFromEnum(complexStructureQueryDetail);
        }
    }
}