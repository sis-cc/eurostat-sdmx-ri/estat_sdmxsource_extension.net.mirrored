﻿// -----------------------------------------------------------------------
// <copyright file="EncodingExtension.cs" company="EUROSTAT">
//   Date Created : 2017-06-12
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sdmxsource.Extension.Extension
{
    /// <summary>
    /// Base 64 encoding extensions
    /// </summary>
    public static class EncodingExtension
    {
        /// <summary>
        /// Convert the <paramref name="base64"/> to the base64 URL safe string.
        /// </summary>
        /// <param name="base64">The base64.</param>
        /// <returns>The URL Safe (RFC4648)</returns>
        public static string ToBase64UrlSafe(this string base64)
        {
            return base64?.TrimEnd('=').Replace('+', '-').Replace('/', '_');
        }
    }
}