﻿// -----------------------------------------------------------------------
// <copyright file="SdmxCategorisationExtension.cs" company="EUROSTAT">
//   Date Created : 2017-05-29
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sdmxsource.Extension.Extension
{
    using System;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;

    /// <summary>
    /// Extensions for SDMX Categorisations and Categories
    /// </summary>
    public static class SdmxCategorisationExtension
    {
        /// <summary>
        /// Determines whether the <paramref name="childReference"/> is a child of the<paramref name="parentReference"/>
        /// </summary>
        /// <param name="childReference">The child category reference.</param>
        /// <param name="parentReference">The parent category or category scheme reference.</param>
        /// <returns>
        ///   <c>true</c> if [is category child of] [the specified parent reference]; otherwise, <c>false</c>.
        /// </returns>
        /// <exception cref="System.ArgumentNullException">
        /// childReference
        /// or
        /// parentReference
        /// </exception>
        public static bool IsCategoryChildOf(this IStructureReference childReference, IStructureReference parentReference)
        {
            if (childReference == null)
            {
                throw new ArgumentNullException("childReference");
            }

            if (parentReference == null)
            {
                throw new ArgumentNullException("parentReference");
            }

            if (parentReference.MaintainableStructureEnumType != SdmxStructureEnumType.CategoryScheme || childReference.TargetReference.EnumType != SdmxStructureEnumType.Category)
            {
                return false;
            }

            if (!childReference.MaintainableReference.Equals(parentReference.MaintainableReference))
            {
                return false;
            }

            if (!parentReference.HasChildReference())
            {
                return true;
            }

            if (!childReference.HasChildReference())
            {
                return false;
            }

            var parentCategoryIds = parentReference.IdentifiableIds;
            var childCategoryIds = childReference.IdentifiableIds;

            var parentCategoryPathCount = parentCategoryIds.Count;
            if (parentCategoryPathCount > childCategoryIds.Count)
            {
                return false;
            }

            for (int i = 0; i < parentCategoryPathCount; i++)
            {
                var childCategoryId = childCategoryIds[i];
                var parentCategoryId = parentCategoryIds[i];
                if (!childCategoryId.Equals(parentCategoryId))
                {
                    return false;
                }
            }

            return true;
        }
    }
}