// -----------------------------------------------------------------------
// <copyright file="ContentTypeExtension.cs" company="EUROSTAT">
//   Date Created : 2016-06-30
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sdmxsource.Extension.Extension
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Net;
    using System.Net.Mime;

    using Estat.Sdmxsource.Extension.Builder;
    using Estat.Sdmxsource.Extension.Model;

    using Org.Sdmxsource.Sdmx.Api.Exception;

    /// <summary>
    /// The content type extension.
    /// </summary>
    public static class ContentTypeExtension
    {
        /// <summary>
        /// The _content type builder
        /// </summary>
        private static readonly ContentTypeBuilder _contentTypeBuilder = new ContentTypeBuilder();

        /// <summary>
        /// Gets the accept header content types.
        /// </summary>
        /// <param name="headers">
        /// The headers.
        /// </param>
        /// <returns>
        /// The  accept header content types.
        /// </returns>
        public static IEnumerable<ContentType> GetAcceptHeaderContentTypes(this WebHeaderCollection headers)
        {
            return _contentTypeBuilder.Build(headers);
        }

        /// <summary>
        /// Gets the content best match.
        /// </summary>
        /// <typeparam name="TOutputFormat">The type of the output format.</typeparam>
        /// <param name="headers">The headers.</param>
        /// <param name="supportedContentTypes">The supported content types.</param>
        /// <returns>
        /// The <see cref="AcceptHeaderToken{T}" />.
        /// </returns>
        public static AcceptHeaderToken<TOutputFormat> GetContentBestMatch<TOutputFormat>(
            this WebHeaderCollection headers,
            IEnumerable<IRestResponse<TOutputFormat>> supportedContentTypes)
        {
            var acceptHeaderContentTypes = headers.GetAcceptHeaderContentTypes();
            return supportedContentTypes.Select(type => type.GetScore(acceptHeaderContentTypes)).Where(token => token.MatchScore > 0).Max(token => token);
        }

        /// <summary>
        /// Gets the match score of <paramref name="validMimeType" /> against <paramref name="acceptHeaderValues" />.
        /// </summary>
        /// <typeparam name="TOutputFormat">The type of the output format.</typeparam>
        /// <param name="validMimeType">Type of the valid MIME.</param>
        /// <param name="acceptHeaderValues">The accept header values.</param>
        /// <returns>
        /// The <see cref="AcceptHeaderToken{T}" />.
        /// </returns>
        /// <exception cref="System.ArgumentNullException">validMimeType
        /// or
        /// acceptHeaderValues</exception>
        public static AcceptHeaderToken<TOutputFormat> GetScore<TOutputFormat>(
            this IRestResponse<TOutputFormat> validMimeType,
            IEnumerable<ContentType> acceptHeaderValues)
        {
            if (validMimeType == null)
            {
                throw new ArgumentNullException("validMimeType");
            }

            if (acceptHeaderValues == null)
            {
                throw new ArgumentNullException("acceptHeaderValues");
            }

            var validTypeSubType = validMimeType.ResponseContentHeader.MediaType.GetMediaType();
            var typePredicate = BuildPredicate(validTypeSubType.Item1);
            var subTypePredicate = BuildPredicate(validTypeSubType.Item2);
            var parameterMap = validMimeType.ResponseContentHeader.MediaType.GetParametersDictionary();

            var bestMatch = new AcceptHeaderToken<TOutputFormat>(0, 0, validMimeType, -1);

            int position = 0;
            foreach (var acceptHeaderValue in acceptHeaderValues)
            {
                position++;
                var mediaType = acceptHeaderValue.GetMediaType();
                if (typePredicate(mediaType.Item1) && subTypePredicate(mediaType.Item2) && MatchingParameterValues(acceptHeaderValue, validMimeType.ResponseContentHeader.MediaType))
                {
                    var quality = acceptHeaderValue.GetQuality();
                    int currentScore = 1; // there was some match
                    currentScore += mediaType.Item1.Equals(validTypeSubType.Item1) ? 100 : 0;
                    currentScore += mediaType.Item2.Equals(validTypeSubType.Item2) ? 10 : 0;
                    var acceptParameters = acceptHeaderValue.GetParametersDictionary();
                    foreach (var validParam in parameterMap)
                    {
                        string value;
                        if (acceptParameters.TryGetValue(validParam.Key, out value)
                            && string.Equals(value, validParam.Value))
                        {
                            currentScore++;
                        }
                    }

                    if (bestMatch.MatchScore < currentScore || (bestMatch.MatchScore == currentScore && bestMatch.Quality < quality))
                    {
                        bestMatch = new AcceptHeaderToken<TOutputFormat>(currentScore, quality, validMimeType, position);
                    }
                }
            }

            return bestMatch;
        }

        /// <summary>
        /// Gets the type of the media.
        /// </summary>
        /// <param name="contentType">The content type.</param>
        /// <returns>
        /// A <see cref="Tuple" /> with type and subtype.
        /// </returns>
        /// <exception cref="System.ArgumentNullException">The <paramref name="contentType"/> is null</exception>
        /// <exception cref="SdmxSyntaxException">Invalid content type :  + split.FirstOrDefault()</exception>
        /// <exception cref="ArgumentNullException"></exception>
        public static Tuple<string, string> GetMediaType(this ContentType contentType)
        {
            if (contentType == null)
            {
                throw new ArgumentNullException("contentType");
            }

            var split = contentType.MediaType.Split('/');
            if (split.Length < 2)
            {
                throw new SdmxSyntaxException("Invalid content type : " + split.FirstOrDefault());
            }

            return new Tuple<string, string>(split[0], split[1]);
        }

        /// <summary>
        /// Gets the quality value.
        /// </summary>
        /// <param name="contentType">Type of the content.</param>
        /// <returns>The <c>q</c> parameter value if present and valid; otherwise the default value 1</returns>
        public static float GetQuality(this ContentType contentType)
        {
            float actualValue;
            if (contentType.Parameters == null
                || string.IsNullOrWhiteSpace(contentType.Parameters["q"])
                || !float.TryParse(contentType.Parameters["q"].Trim(), NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture, out actualValue)
                || actualValue > 1)
            {
                return 1;
            }

            return actualValue;
        }

        /// <summary>
        /// Gets the parameters dictionary.
        /// </summary>
        /// <param name="contentType">
        /// Type of the content.
        /// </param>
        /// <returns>
        /// The <see cref="ContentType.Parameters"/> as a <see cref="IDictionary"/>.
        /// </returns>
        public static IDictionary<string, string> GetParametersDictionary(this ContentType contentType)
        {
            var output = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);
            if (contentType.Parameters != null)
            {
                foreach (DictionaryEntry parameter in contentType.Parameters)
                {
                    output[(string)parameter.Key] = (string)parameter.Value;
                }
            }

            return output;
        }

        /// <summary>
        /// Check if the specific <see cref="ContentType"/> have matching the parameter value. Ignoring any missing parameters
        /// </summary>
        /// <param name="accepType">Type of the accept.</param>
        /// <param name="supportedType">Type of the supported.</param>
        /// <returns>True if there is a matching parameter value; otherwise false.</returns>
        private static bool MatchingParameterValues(ContentType accepType, ContentType supportedType)
        {
            if (accepType.Parameters == null || supportedType.Parameters == null)
            {
                return true;
            }

            var notMatchingParameterValues = from DictionaryEntry parameter in accepType.Parameters
                                             let value = supportedType.Parameters[(string)parameter.Key]
                                             where value != null && parameter.Value != null && !string.Equals(value, (string)parameter.Value, StringComparison.OrdinalIgnoreCase)
                                             select parameter;
            if (notMatchingParameterValues.Any())
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Builds the predicate.
        /// </summary>
        /// <param name="mediaTypeToken">The media type token.</param>
        /// <returns>
        /// The <see cref="Predicate{String}" />.
        /// </returns>
        private static Predicate<string> BuildPredicate(string mediaTypeToken)
        {
            return mediaTypeToken.Equals("*")
                       ? (Predicate<string>)(s => true)
                       : (s => s.Equals("*") || s.Equals(mediaTypeToken));
        }
    }
}