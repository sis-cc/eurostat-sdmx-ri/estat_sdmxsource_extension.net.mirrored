﻿// -----------------------------------------------------------------------
// <copyright file="ConfigurationMissingException.cs" company="EUROSTAT">
//   Date Created : 2017-11-24
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sdmxsource.Extension.Exception
{
    using System;
    using System.Runtime.Serialization;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;

    /// <summary>
    /// The Configuration Missing Exception
    /// </summary>
    [Serializable]
    public class ConfigurationMissingException : SdmxException
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ConfigurationMissingException"/> class.
        /// </summary>
        /// <param name="errorMessage">The error message that explains the reason for the exception.</param>
        /// <param name="innerException">The exception that is the cause of the current exception, or a null reference (Nothing in Visual Basic) if no inner
        /// exception is specified.</param>
        public ConfigurationMissingException(string errorMessage, Exception innerException)
            : base(errorMessage, innerException)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ConfigurationMissingException"/> class.
        /// </summary>
        /// <param name="message">The message that describes the error.</param>
        public ConfigurationMissingException(string message)
            : base(message)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ConfigurationMissingException"/> class.
        /// </summary>
        public ConfigurationMissingException()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ConfigurationMissingException"/> class.
        /// </summary>
        /// <param name="code">The exception code</param>
        public ConfigurationMissingException(ExceptionCode code) : base(code)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ConfigurationMissingException"/> class.
        /// </summary>
        /// <param name="errorMessage">The error message</param>
        /// <param name="errorCode">The error code</param>
        public ConfigurationMissingException(string errorMessage, SdmxErrorCode errorCode) : base(errorMessage, errorCode)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ConfigurationMissingException"/> class.
        /// </summary>
        /// <param name="nestedException">The exception</param>
        /// <param name="errorCode">The error code</param>
        /// <param name="message">the message</param>
        public ConfigurationMissingException(Exception nestedException, SdmxErrorCode errorCode, string message) : base(nestedException, errorCode, message)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ConfigurationMissingException"/> class.
        /// </summary>
        /// <param name="exception">The exception</param>
        /// <param name="code">The exception code</param>
        /// <param name="args">The arguments</param>
        public ConfigurationMissingException(Exception exception, ExceptionCode code, params object[] args) : base(exception, code, args)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ConfigurationMissingException"/> class.
        /// </summary>
        /// <param name="errorCode">The error code</param>
        /// <param name="code">The exception code</param>
        /// <param name="args">The arguments</param>
        public ConfigurationMissingException(SdmxErrorCode errorCode, ExceptionCode code, params object[] args) : base(errorCode, code, args)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ConfigurationMissingException"/> class.
        /// </summary>
        /// <param name="nestedException">The exception</param>
        /// <param name="errorCode">The error code</param>
        /// <param name="code">The exception code</param>
        /// <param name="args">The arguments</param>
        public ConfigurationMissingException(Exception nestedException, SdmxErrorCode errorCode, ExceptionCode code, params object[] args) : base(nestedException, errorCode, code, args)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ConfigurationMissingException"/> class.
        /// </summary>
        /// <param name="info">The <see cref="T:System.Runtime.Serialization.SerializationInfo" /> that holds the serialized object data about the
        /// exception being thrown.</param>
        /// <param name="context">The <see cref="T:System.Runtime.Serialization.StreamingContext" /> that contains contextual information about the
        /// source or destination.</param>
        protected ConfigurationMissingException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
