﻿// -----------------------------------------------------------------------
// <copyright file="SubmitStructureException.cs" company="EUROSTAT">
//   Date Created : 2016-08-08
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sdmxsource.Extension.Exception
{
    using System;
    using System.Runtime.Serialization;

    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Estat.Sdmxsource.Extension.Model.Error;

    /// <summary>
    /// The submit structure exception.
    /// </summary>
    [Serializable]
    public class SubmitStructureException : SdmxException
    {
        /// <summary>
        /// The structure reference
        /// </summary>
        private readonly IStructureReference _structureReference;

        /// <summary>
        /// The response
        /// </summary>
        private readonly IResponseWithStatusObject _response;

        /// <summary>
        /// Initializes a new instance of the <see cref="SubmitStructureException"/> class.
        /// </summary>
        public SubmitStructureException()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SubmitStructureException"/> class.
        /// </summary>
        /// <param name="errorMessage">The error message that explains the reason for the exception.</param>
        /// <param name="innerException">The exception that is the cause of the current exception, or a null reference (Nothing in Visual Basic) if no inner
        /// exception is specified.</param>
        public SubmitStructureException(string errorMessage, Exception innerException)
            : base(errorMessage, innerException)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SubmitStructureException"/> class.
        /// </summary>
        /// <param name="message">The message that describes the error.</param>
        public SubmitStructureException(string message)
            : base(message)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SubmitStructureException"/> class. 
        /// Creates Exception from an error String and an Error code
        /// </summary>
        /// <param name="errorMessage">
        /// The error message
        /// </param>
        /// <param name="structureReference">
        /// The structure reference.
        /// </param>
        public SubmitStructureException(string errorMessage, IStructureReference structureReference)
            : base(errorMessage)
        {
            this._structureReference = structureReference;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SubmitStructureException"/> class.
        /// </summary>
        /// <param name="response">The response.</param>
        /// <exception cref="ArgumentNullException">response is null</exception>
        public SubmitStructureException(IResponseWithStatusObject response)
        {
            if (response == null)
            {
                throw new ArgumentNullException(nameof(response));
            }

            _response = response;
            _structureReference = response.StructureReference;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SubmitStructureException"/> class.
        /// </summary>
        /// <param name="serializationInfo">The serialization information.</param>
        /// <param name="context">The context.</param>
        protected SubmitStructureException(SerializationInfo serializationInfo, StreamingContext context) : base(serializationInfo, context)
        {
            this._structureReference = (IStructureReference)serializationInfo.GetValue("structureReference", typeof(IStructureReference));
            this._response = (IResponseWithStatusObject)serializationInfo.GetValue("response", typeof(IResponseWithStatusObject));
        }

        /// <summary>
        /// Gets the structure reference.
        /// </summary>
        /// <value>
        /// The structure reference.
        /// </value>
        public IStructureReference StructureReference
        {
            get
            {
                return this._structureReference;
            }
        }

        /// <summary>
        /// Gets the response.
        /// </summary>
        /// <value>
        /// The response.
        /// </value>
        public IResponseWithStatusObject Response
        {
            get
            {
                return _response;
            }
        }

        /// <summary>
        /// When overridden in a derived class, sets the <see cref="T:System.Runtime.Serialization.SerializationInfo" /> with
        /// information about the exception.
        /// </summary>
        /// <param name="info">The <see cref="T:System.Runtime.Serialization.SerializationInfo" /> that holds the serialized object
        /// data about the exception being thrown.</param>
        /// <param name="context">The <see cref="T:System.Runtime.Serialization.StreamingContext" /> that contains contextual
        /// information about the source or destination.</param>
        /// <exception cref="T:System.ArgumentNullException">The <paramref name="info" /> parameter is a null reference (Nothing in
        /// Visual Basic).</exception>
        /// <PermissionSet><IPermission class="System.Security.Permissions.FileIOPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Read="*AllFiles*" PathDiscovery="*AllFiles*" /><IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="SerializationFormatter" /></PermissionSet>
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
            info.AddValue("structureReference", this._structureReference);
            info.AddValue("response", this._response);
        }
    }
}