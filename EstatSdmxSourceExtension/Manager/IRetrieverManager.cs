// -----------------------------------------------------------------------
// <copyright file="IRetrieverManager.cs" company="EUROSTAT">
//   Date Created : 2015-11-20
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sdmxsource.Extension.Manager
{
    using System;
    using Estat.Sdmxsource.Extension.Model;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Engine;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Common;
    using Org.Sdmxsource.Sdmx.Api.Model.Query;
    using Org.Sdmxsource.Sdmx.Api.Util;
    using Org.Sdmxsource.Sdmx.DataParser.Engine;

    /// <summary>
    /// The interface responsible for retrieving data and structures
    /// </summary>
    public interface IRetrieverManager
    {
        // TODO ?
        // Java uses only
        // ISdmxObjects ParseRequest(IRestStructureQuery query) & 
        // ISdmxObjects ParseRequest(IRestAvailableConstraintQuery query)

        /// <summary>
        /// Gets the SDMX data response.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <returns>The <see cref="IDataResponse{TWriter}"/>.</returns>
        IDataResponse<IDataWriterEngine> GetDataResponseComplex(IReadableDataLocation input);

        /// <summary>
        /// Gets the SDMX v2.0 CrossSectional data response for SOAP requests.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <returns>The <see cref="IDataResponse{ICrossSectionalWriterEngine}"/>.</returns>
        IDataResponse<ICrossSectionalWriterEngine> GetDataResponseCross(IReadableDataLocation input);

        /// <summary>
        /// Gets the SDMX v2.0 CrossSectional data response for REST requests.
        /// </summary>
        /// <param name="request">The input.</param>
        /// <returns>The <see cref="IDataResponse{ICrossSectionalWriterEngine}"/>.</returns>
        IDataResponse<ICrossSectionalWriterEngine> GetDataResponseCross(DataRequest request);

        /// <summary>
        /// Gets the data response simple.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <returns>The <see cref="IDataResponse{IDataWriterEngine}"/>.</returns>
        IDataResponse<IDataWriterEngine> GetDataResponseSimple(IReadableDataLocation input);

        /// <summary>
        /// Gets the data response simple.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The <see cref="IDataResponse{IDataWriterEngine}"/>.</returns>
        IDataResponse<IDataWriterEngine> GetDataResponseSimple(DataRequest request);

        /// <summary>
        /// Gets the structure response for SDMX SOAP.
        /// </summary>
        /// <param name="sdmxSchema">The SDMX schema.</param>
        /// <param name="input">The input.</param>
        /// <returns>The <see cref="ISdmxObjects"/>.</returns>
        ISdmxObjects ParseRequest(SdmxSchemaEnumType sdmxSchema, IReadableDataLocation input);

        /// <summary>
        /// Parses the request.
        /// </summary>
        /// <param name="sdmxSchema">The SDMX schema.</param>
        /// <param name="query">The query.</param>
        /// <returns>The <see cref="ISdmxObjects"/>.</returns>
        [Obsolete("Use the overload with the ICommonStructureQuery")]
        ISdmxObjects ParseRequest(SdmxSchema sdmxSchema, IRestStructureQuery query);

        /// <summary>
        /// Parses the request.
        /// </summary>
        /// <param name="sdmxSchema">The SDMX schema.</param>
        /// <param name="query">The query.</param>
        /// <returns>The <see cref="ISdmxObjects"/>.</returns>
        ISdmxObjects ParseRequest(ICommonStructureQuery query);

        /// <summary>
        /// Parses the request.
        /// </summary>
        /// <param name="sdmxSchema">The SDMX schema.</param>
        /// <param name="query">The query.</param>
        /// <returns>The <see cref="ISdmxObjects"/>.</returns>
        ISdmxObjects ParseRequest(SdmxSchema sdmxSchema, IRestAvailableConstraintQuery query);

        /// <summary>
        /// Parses the data request.
        /// </summary>
        /// <param name="dataQuery">
        /// The data query.
        /// </param>
        /// <returns>
        /// The <see cref="DataRequest"/>.
        /// </returns>
        DataRequest ParseDataRequest(IRestDataQuery dataQuery);
    }
}