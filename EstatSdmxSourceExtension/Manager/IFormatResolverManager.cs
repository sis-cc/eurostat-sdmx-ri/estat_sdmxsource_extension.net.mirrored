// -----------------------------------------------------------------------
// <copyright file="IFormatResolverManager.cs" company="EUROSTAT">
//   Date Created : 2016-07-01
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System.Collections.Generic;

namespace Estat.Sdmxsource.Extension.Manager
{
    using System.Net;
    using System.Xml;

    using Estat.Sdmxsource.Extension.Model;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
    using Org.Sdmxsource.Sdmx.Api.Model.Data;
    using Org.Sdmxsource.Sdmx.Api.Model.Data.Query;
    using Org.Sdmxsource.Sdmx.Api.Model.Format;
    using Org.Sdmxsource.Sdmx.Api.Model.Query;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Builder.Common;

    /// <summary>
    /// The FormatResolverManager interface.
    /// </summary>
    public interface IFormatResolverManager
    {
        /// <summary>
        /// Gets the data format.
        /// </summary>
        /// <param name="headers">
        /// The headers.
        /// </param>
        /// <param name="retrievalManager">
        /// The retrieval manager.
        /// </param>
        /// <param name="dataQuery">
        /// The data query.
        /// </param>
        /// <param name="restApiVersion">The rest API version.</param>
        /// <returns>
        /// The <see cref="IRestResponse{IDataFormat}"/>.
        /// </returns>
        IRestResponse<IDataFormat> GetDataFormat(WebHeaderCollection headers, ISdmxObjectRetrievalManager retrievalManager, IDataQuery dataQuery, RestApiVersion restApiVersion);

        /// <summary>
        /// Gets the available response formats for REST data requests.
        /// </summary>
        /// <returns>The list of supported formats.</returns>
        IEnumerable<string> GetAvailableDataFormats();

        /// <summary>
        /// Gets the structure format.
        /// </summary>
        /// <param name="headers">The headers.</param>
        /// <param name="structureType">Type of the structure.</param>
        /// <param name="restApiVersion">The rest API version.</param>
        /// <returns>
        /// The <see cref="IRestResponse{IStructureFormat}" />.
        /// </returns>
        IRestResponse<IStructureFormat> GetStructureFormat(WebHeaderCollection headers, RestApiVersion restApiVersion);
        
        /// <summary>
        /// Gets teh available response formats for REST structure requests.
        /// </summary>
        /// <returns>The list of supported formats.</returns>
        IEnumerable<string> GetAvailableStructureFormats();

        /// <summary>
        /// Gets the data format.
        /// </summary>
        /// <param name="dataType">Type of the data.</param>
        /// <param name="xmlWriter">The XML writer.</param>
        /// <returns>
        /// The <see cref="IDataFormat" />.
        /// </returns>
        IDataFormat GetDataFormat(ISoapRequest<DataType> dataType, XmlWriter xmlWriter);

        /// <summary>
        /// Gets the structure format.
        /// </summary>
        /// <param name="format">The format.</param>
        /// <param name="xmlWriter">The XML writer.</param>
        /// <returns>
        /// The <see cref="IStructureFormat" />.
        /// </returns>
        IStructureFormat GetStructureFormat(ISoapRequest<StructureOutputFormat> format, XmlWriter xmlWriter);
    }
}