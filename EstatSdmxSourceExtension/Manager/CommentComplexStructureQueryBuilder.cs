﻿// -----------------------------------------------------------------------
// <copyright file="CommentComplexStructureQueryBuilder.cs" company="EUROSTAT">
//   Date Created : 2016-08-18
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.CustomRequests.Manager
{
    using System.Xml.Linq;

    using Estat.Sri.CustomRequests.Builder;

    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Complex;
    using Org.Sdmxsource.Sdmx.Structureparser.Model;

    /// <summary>
    /// The Comment Complex Structure Query Builder
    /// </summary>
    public class CommentComplexStructureQueryBuilder : IComplexStructureQueryBuilder<XDocument>
    {
        /// <summary>
        /// The _complex structure query builder.
        /// </summary>
        private readonly IComplexStructureQueryBuilder<XDocument> _complexStructureQueryBuilder;

        /// <summary>
        /// The _tool indicator.
        /// </summary>
        private readonly IToolIndicator _toolIndicator;

        /// <summary>
        /// Initializes a new instance of the <see cref="CommentComplexStructureQueryBuilder"/> class. 
        /// Initializes a new instance of the <see cref="T:System.Object"/> class.
        /// </summary>
        /// <param name="toolIndicator">
        /// The tool Indicator.
        /// </param>
        /// <param name="complexStructureQueryBuilder">
        /// The complex Structure Query Builder.
        /// </param>
        public CommentComplexStructureQueryBuilder(
            IToolIndicator toolIndicator,
            IComplexStructureQueryBuilder<XDocument> complexStructureQueryBuilder)
        {
            this._toolIndicator = toolIndicator;
            this._complexStructureQueryBuilder = complexStructureQueryBuilder;
        }

        /// <summary>
        /// Builds a ComplexStructureQuery that matches the passed in format
        /// </summary>
        /// <param name="query">
        /// The query.
        /// </param>
        /// <returns>
        /// The complex structure
        /// </returns>
        public XDocument BuildComplexStructureQuery(IComplexStructureQuery query)
        {
            var doc = this._complexStructureQueryBuilder.BuildComplexStructureQuery(query);
            if (this._toolIndicator == null || doc.Root == null)
            {
                return doc;
            }

            var comm = new XComment(this._toolIndicator.GetComment());
            doc.Root.FirstNode.AddBeforeSelf(comm);
            return doc;
        }
    }
}