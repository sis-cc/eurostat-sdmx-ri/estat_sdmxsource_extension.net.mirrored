// -----------------------------------------------------------------------
// <copyright file="IAvailableDataManager.cs" company="EUROSTAT">
//   Date Created : 2018-04-18
//   Copyright (c) 2009, 2018 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sdmxsource.Extension.Manager
{
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable;
    using Org.Sdmxsource.Sdmx.Api.Model.Query;

    /// <summary>
    /// Available Data Manager
    /// </summary>
    public interface IAvailableDataManager
    {
        /// <summary>
        /// Retrieves the structural metadata constrained by available data
        /// </summary>
        /// <param name="query">The query.</param>
        /// <param name="objects">The objects to fill.</param>
        /// <param name="multipleReferences">Indicates whether is part of retrieving multiple structure refernces.</param>
        /// <returns>
        /// The
        /// </returns>
        IMutableObjects Retrieve(IAvailableConstraintQuery query, IMutableObjects objects = null, bool multipleReferences = false);

        /// <summary>
        /// Gets the maximum possible observation count for the specified query.
        /// </summary>
        /// <param name="query">The query.</param>
        /// <param name="includeHistory">if set to <c>true</c> [include history].</param>
        /// <returns>
        /// The number of observations
        /// </returns>
        long GetCount(IAvailableConstraintQuery query,bool includeHistory);
    }
}