﻿// -----------------------------------------------------------------------
// <copyright file="IResponseWriterManager.cs" company="EUROSTAT">
//   Date Created : 2017-11-15
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using Estat.Sdmxsource.Extension.Model.Error;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Estat.Sdmxsource.Extension.Manager
{
    /// <summary>
    /// The Response writer manager
    /// </summary>
    public interface IResponseWriterManager
    {
        /// <summary>
        /// Writes the specified format.
        /// </summary>
        /// <param name="format">The format.</param>
        /// <param name="statusObjects">The status objects.</param>
        void Write(IResponseFormat format, IList<IResponseWithStatusObject> statusObjects);
    }
}
