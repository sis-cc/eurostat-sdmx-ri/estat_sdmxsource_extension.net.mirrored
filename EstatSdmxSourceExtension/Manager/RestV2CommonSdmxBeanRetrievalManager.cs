using System;
using System.Collections.Generic;
using System.Text;
using CsvHelper.Configuration;
using log4net.Repository.Hierarchy;
using log4net;
using Microsoft.Extensions.Primitives;
using Org.Sdmxsource.Sdmx.Api.Exception;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Common;
using Org.Sdmxsource.Sdmx.Api.Util;
using Org.Sdmxsource.Sdmx.Structureparser.Manager.Parsing;
using Org.Sdmxsource.Sdmx.Structureparser.Workspace;
using Org.Sdmxsource.Util.Io;
using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
using Org.Sdmxsource.Sdmx.Api.Factory;
using Org.Sdmxsource.Sdmx.Api.Manager.Parse;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Model.Objects;
using Org.Sdmxsource.Sdmx.SdmxQueryBuilder.Model;
using Org.Sdmxsource.Sdmx.Api.Model;
using Org.Sdmxsource.Sdmx.Api.Builder;
using Org.Sdmxsource.Sdmx.Api.Manager.Query;
using System.IO;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net;
using Estat.Sdmxsource.Extension.Model;

namespace Estat.Sdmxsource.Extension.Manager
{
    public class RestV2CommonSdmxBeanRetrievalManager : ICommonSdmxObjectRetrievalManager
    {

        private static readonly ILog _log = LogManager.GetLogger(typeof(RestV2CommonSdmxBeanRetrievalManager));
        private readonly WsInfo fusionSettings;
        private IStructureParsingManager structureParsingManager;
        private IReadableDataLocationFactory readableDataLocationFactory;

        private IStructureQueryWriterManager structureQueryWriterManager;

        public RestV2CommonSdmxBeanRetrievalManager(WsInfo fusionSettings,
                                                    IStructureParsingManager structureParsingManager,
                                                    IReadableDataLocationFactory readableDataLocationFactory,
                                                    IStructureQueryWriterManager structureQueryWriterManager)
        {
            this.fusionSettings = fusionSettings;
            this.structureParsingManager = structureParsingManager;
            this.readableDataLocationFactory = readableDataLocationFactory;
            this.structureQueryWriterManager = structureQueryWriterManager;
        }


        public ISdmxObjects GetMaintainables(ICommonStructureQuery structureQuery)
        {
            StringBuilder stringBuilder = new StringBuilder(this.fusionSettings.EndPoint);
            Dictionary<string, string> headers = new Dictionary<string, string>();
            string acceptHeader = "Accept";
            stringBuilder.Append('/');
            RestV2QueryWithWriterFormat format = new RestV2QueryWithWriterFormat(stringBuilder);
            structureQueryWriterManager.WriteStructureQuery(structureQuery, format);
            _log.Info($"Build URL {stringBuilder.ToString()}");
            // the fusion registry supports the HTTP Accept header
            if (structureQuery.StructureOutputFormat != null)
            {
                switch (structureQuery.StructureOutputFormat.EnumType)
                {
                    case StructureOutputFormatEnumType.SdmxV2StructureDocument:
                        headers.Add(acceptHeader, "application/vnd.sdmx.structure+xml;version=2.0");
                        break;
                    case StructureOutputFormatEnumType.SdmxV21StructureDocument:
                        headers.Add(acceptHeader, "application/vnd.sdmx.structure+xml;version=2.1");
                        break;
                    case StructureOutputFormatEnumType.SdmxV3StructureDocument:
                        headers.Add(acceptHeader, "application/vnd.sdmx.structure+xml;version=3.0.0");
                        break;
                    case StructureOutputFormatEnumType.JsonV10:
                        headers.Add(acceptHeader, "application/vnd.sdmx.structure+json");
                        break;
                    default:
                        _log.Info("No format added to header parameters");
                        break;
                        // we use the WS default which is normally SDMX-JSON 2.0.0
                }
            }
            return RequestObjects(stringBuilder.ToString(), headers);
        }


        private ISdmxObjects RequestObjects(string registryBaseUrl, Dictionary<string, string> headers)
        {

            var sdmxHttpClient = new SdmxHttpClient(fusionSettings);

            var response = sdmxHttpClient.GetStructuresResponseAsync(registryBaseUrl, headers).Result;

            // Process the response
            if (response.IsSuccessStatusCode)
            {
                var stream = response.Content.ReadAsStreamAsync();
                return ParseStructures(stream.Result);
            }
            else
            {
                switch ((int)response.StatusCode)
                {
                    case 501:
                        throw new SdmxNotImplementedException(response.ReasonPhrase);
                    case 204:
                    case 404:
                        throw new SdmxNoResultsException(); // TODO message could be XML
                    case 422:
                    case 400:
                    case 403:
                        throw new SdmxSemmanticException(); // TODO message could be XML
                    default:
                        throw new SdmxInternalServerException(response.ReasonPhrase);
                }
            }
        }


        private ISdmxObjects ParseStructures(Stream response)
        {
            // 1. Write request message into a readable data location.
            using (var requestLocation = readableDataLocationFactory.GetReadableDataLocation(response))
            {

                // 2. Parse the structures into a workspace. StructureParsingManager
                // performs message XML validation.
                IStructureWorkspace workspace = structureParsingManager.ParseStructures(requestLocation);

                // 3. Read workspace to get the structure beans.
                return workspace.GetStructureObjects(true);
            }
        }
    }
}