﻿// -----------------------------------------------------------------------
// <copyright file="DataflowAuthorizationManager.cs" company="EUROSTAT">
//   Date Created : 2016-02-11
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sdmxsource.Extension.Manager
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Security.Principal;

    using Estat.Sdmxsource.Extension.Engine;
    using Estat.Sdmxsource.Extension.Factory;

    /// <summary>
    /// The <see cref="DataflowAuthorizationManager"/> will go through a list of <see cref="IDataflowAuthorizationFactory"/>,
    /// pass the <see cref="IPrincipal"/> to the <see cref="IDataflowAuthorizationFactory"/> and return the first non-null <see cref="IDataflowAuthorizationEngine"/>
    /// </summary>
    public class DataflowAuthorizationManager : IDataflowAuthorizationManager
    {
        /// <summary>
        /// The _dataflow authorization factories
        /// </summary>
        private readonly IList<IDataflowAuthorizationFactory> _dataflowAuthorizationFactories;

        /// <summary>
        /// Initializes a new instance of the <see cref="DataflowAuthorizationManager"/> class.
        /// </summary>
        /// <param name="dataflowAuthorizationFactories">The dataflow authorization factories.</param>
        /// <exception cref="ArgumentNullException"><paramref name="dataflowAuthorizationFactories"/> is <see langword="null" />.</exception>
        public DataflowAuthorizationManager(IEnumerable<IDataflowAuthorizationFactory> dataflowAuthorizationFactories)
        {
            if (dataflowAuthorizationFactories == null)
            {
                throw new ArgumentNullException("dataflowAuthorizationFactories");
            }

            this._dataflowAuthorizationFactories = dataflowAuthorizationFactories.Where(factory => factory != null).ToArray();
        }

        /// <summary>
        /// Gets the authorization engine.
        /// </summary>
        /// <param name="principal">The principal.</param>
        /// <returns>The <see cref="IDataflowAuthorizationEngine"/>; otherwise null.</returns>
        public IDataflowAuthorizationEngine GetAuthorizationEngine(IPrincipal principal)
        {
            foreach (var dataflowAuthorizationFactory in this._dataflowAuthorizationFactories)
            {
                var dataflowAuthorizationEngine = dataflowAuthorizationFactory.GetEngine(principal);
                if (dataflowAuthorizationEngine != null)
                {
                    return dataflowAuthorizationEngine;
                }
            }

            return null;
        }
    }
}