// -----------------------------------------------------------------------
// <copyright file="MutableStructureSearchManagerWithAuth.cs" company="EUROSTAT">
//   Date Created : 2013-06-12
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sdmxsource.Extension.Manager
{
    using System;
    using System.Collections.Generic;
    using Estat.Sdmxsource.Extension.Extension;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval.Mutable;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Api.Model.Query;

    /// <summary>
    /// A delegating class that wraps a <see cref="IAuthMutableStructureSearchManager"/>.
    /// </summary>
    [Obsolete("After migrating to MSDB 7.0, use ICommonSdmxObjectRetrievalManager & ICommonSdmxObjectRetrievalManagerSoap20")]
    public class MutableStructureSearchManagerWithAuth : IMutableStructureSearchManager
    {
        /// <summary>
        /// The dataflow authorization mutable structure search manager
        /// </summary>
        private readonly IAuthMutableStructureSearchManager _authMutableStructureSearchManager;

        /// <summary>
        /// The _authorized dataflows
        /// </summary>
        private readonly IList<IMaintainableRefObject> _authorizedDataflows;

        /// <summary>
        /// Initializes a new instance of the <see cref="MutableStructureSearchManagerWithAuth"/> class.
        /// </summary>
        /// <param name="authMutableStructureSearchManager">The authentication mutable structure search manager.</param>
        /// <param name="authorizedDataflows">The authorized dataflows.</param>
        public MutableStructureSearchManagerWithAuth(IAuthMutableStructureSearchManager authMutableStructureSearchManager, IList<IMaintainableRefObject> authorizedDataflows)
        {
            this._authMutableStructureSearchManager = authMutableStructureSearchManager;
            this._authorizedDataflows = authorizedDataflows;
        }

        /// <summary>
        /// Returns the latest version of the maintainable for the given maintainable input
        /// </summary>
        /// <param name="maintainableObject">
        /// The maintainable Object.
        /// </param>
        /// <returns>
        /// The <see cref="IMaintainableMutableObject"/>.
        /// </returns>
        public IMaintainableMutableObject GetLatest(IMaintainableMutableObject maintainableObject)
        {
            return this._authMutableStructureSearchManager.GetLatest(maintainableObject, this._authorizedDataflows);
        }

        /// <summary>
        /// Returns a set of maintainable that match the given query parameters
        /// </summary>
        /// <param name="structureQuery">
        /// The structure Query.
        /// </param>
        /// <returns>
        /// The <see cref="IMutableObjects"/>.
        /// </returns>
        public IMutableObjects GetMaintainables(IRestStructureQuery structureQuery)
        {
            return this._authMutableStructureSearchManager.GetMaintainables(structureQuery, this._authorizedDataflows);
        }

        /// <summary>
        /// Retrieves all structures that match the given query parameters in the list of query objects.  The list
        ///     must contain at least one StructureQueryObject.
        /// </summary>
        /// <param name="queries">
        /// The queries.
        /// </param>
        /// <param name="resolveReferences">
        /// - if set to true then any cross referenced structures will also be available in the SdmxObjects container
        /// </param>
        /// <param name="returnStub">
        /// - if set to true then only stubs of the returned objects will be returned.
        /// </param>
        /// <returns>
        /// The <see cref="IMutableObjects"/>.
        /// </returns>
        public IMutableObjects RetrieveStructures(IList<IStructureReference> queries, bool resolveReferences, bool returnStub)
        {
            var queryDetail = returnStub.GetStructureQueryDetail();

            return this._authMutableStructureSearchManager.RetrieveStructures(queries, resolveReferences, queryDetail, this._authorizedDataflows);
        }
    }
}