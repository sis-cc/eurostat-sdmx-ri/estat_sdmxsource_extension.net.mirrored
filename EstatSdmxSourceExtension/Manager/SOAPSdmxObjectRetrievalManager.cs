// -----------------------------------------------------------------------
// <copyright file="SOAPSdmxObjectRetrievalManager.cs" company="EUROSTAT">
//   Date Created : 2017-06-09
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sdmxsource.Extension.Manager
{
    using System.Collections.Generic;
    using System.IO;
    using System.Net;
    using System.Xml;
    using System.Xml.Linq;

    using Builder;

    using Model;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Manager.Parse;
    using Org.Sdmxsource.Sdmx.Api.Model;
    using Org.Sdmxsource.Sdmx.Api.Model.Format;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Complex;
    using Org.Sdmxsource.Sdmx.Api.Model.Query;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Structureparser.Manager.Parsing;
    using Org.Sdmxsource.Sdmx.StructureRetrieval.Manager;
    using Org.Sdmxsource.Sdmx.Util.Xml;
    using Org.Sdmxsource.Util.Io;

    using Sri.CustomRequests.Factory;
    using Sri.CustomRequests.Manager;
    using Sri.CustomRequests.Model;
    using System.Diagnostics;

    using Estat.Sdmxsource.Extension.Engine.WebService;
    using Estat.Sdmxsource.Extension.Extension;
    using Estat.Sdmxsource.Extension.Util;
    using System.Linq;

    /// <summary>
    /// Retrieve artifacts from a SOAP endpoint
    /// </summary>
    /// <seealso cref="Org.Sdmxsource.Sdmx.StructureRetrieval.Manager.BaseSdmxObjectRetrievalManager" />
    public class SoapSdmxObjectRetrievalManager : BaseSdmxObjectRetrievalManager
    {
        private readonly WsInfo _config;
        private readonly SdmxSchema _sdmxSchema;
        private readonly SoapMessageSubmitter _soapWriter;

        /// <summary>
        /// Test
        /// </summary>
        /// <param name="config"></param>
        /// <param name="sdmxSchema"></param>
        public SoapSdmxObjectRetrievalManager(WsInfo config, SdmxSchema sdmxSchema)
        {
            this._config = config;
            this._sdmxSchema = sdmxSchema;
             _soapWriter = new SoapMessageSubmitter(_config, new SoapMessageWriterEngine());
        }

        /// <summary>
        /// Get all the maintainable that match the <paramref name="restquery"/>
        /// </summary>
        /// <param name="restquery">The REST structure query.</param>
        /// <returns>
        /// the maintainable that match the <paramref name="restquery"/>
        /// </returns>
        /// <exception cref="T:System.ArgumentNullException"><paramref name="restquery"/> is <see langword="null"/>.</exception><exception cref="T:Org.Sdmxsource.Sdmx.Api.Exception.SdmxNoResultsException">if the maintainable could not be resolved from the given endpoint</exception><exception cref="T:Org.Sdmxsource.Sdmx.Api.Exception.SdmxNotImplementedException">Cannot return stubs since no ServiceRetrievalManager has been supplied!</exception>
        public override ISdmxObjects GetMaintainables(IRestStructureQuery restquery)
        {
            // Issue 427 / SDMXRI-1441
            var returnDetails = true;
            var structureQueryDetail = restquery.StructureQueryMetadata.StructureQueryDetail.EnumType;
            if (structureQueryDetail == StructureQueryDetailEnumType.AllCompleteStubs ||
                structureQueryDetail == StructureQueryDetailEnumType.AllStubs)
            {
                returnDetails = false;
            }

            if (this._sdmxSchema.EnumType == SdmxSchemaEnumType.VersionTwo)
            {
                switch (restquery.StructureQueryMetadata.StructureReferenceDetail.EnumType)
                {
                    case StructureReferenceDetailEnumType.None:
                    case StructureReferenceDetailEnumType.Null:
                        return this.GetSdmxObjects20(new List<IStructureReference> { restquery.StructureReference }, false, returnDetails);
                    case StructureReferenceDetailEnumType.Children:
                        return this.GetSdmxObjects20(new List<IStructureReference> { restquery.StructureReference }, true, returnDetails);
                    default:
                        throw new SdmxNotImplementedException(string.Format("StructureReferenceDetail {0} not supported", restquery.StructureQueryMetadata.StructureReferenceDetail.EnumType));
                }
            }
            if (this._sdmxSchema.EnumType == SdmxSchemaEnumType.VersionTwoPointOne)
            {
                return this.GetSdmxObjects21(restquery);
            }
            return null;
        }
        public ISdmxObjects GetMaintainables(RESTStructureQueryCoreMultipleReferences restquery)
        {
            if (this._sdmxSchema.EnumType == SdmxSchemaEnumType.VersionTwo)
            {
                switch (restquery.StructureQueryMetadata.StructureReferenceDetail.EnumType)
                {
                    case StructureReferenceDetailEnumType.None:
                    case StructureReferenceDetailEnumType.Null:
                        return this.GetSdmxObjects20(restquery.StructureReferences, false);
                    case StructureReferenceDetailEnumType.Children:
                        return this.GetSdmxObjects20(restquery.StructureReferences, true);
                    default:
                        throw new SdmxNotImplementedException(string.Format("StructureReferenceDetail {0} not supported", restquery.StructureQueryMetadata.StructureReferenceDetail.EnumType));
                }
            }
            else
            {
                throw new SdmxSemmanticException("queries for multiple references are only available for SDMX 2.0");
            }
        }

        /// <summary>
        /// Gets the SDMX objects.
        /// </summary>
        /// <param name="structureReference">The <see cref="T:Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.IStructureReference"/> which must not be null.</param><param name="resolveCrossReferences">either 'do not resolve', 'resolve all' or 'resolve all excluding agencies'. If not
        ///                 set to 'do not resolve' then all the structures that are referenced by the resulting structures are also returned
        ///                 (and also their children).  This will be equivalent to descendants for a <c>RESTful</c> query..
        ///             </param>
        /// <returns>
        /// Returns a <see cref="T:Org.Sdmxsource.Sdmx.Api.Model.Objects.ISdmxObjects"/> container containing all the Maintainable Objects that match the query
        ///                 parameters as defined by the <paramref name="structureReference"/>.
        /// </returns>
        /// <exception cref="T:Org.Sdmxsource.Sdmx.Api.Exception.SdmxSyntaxException">Unknown Maintainable Type</exception><exception cref="T:System.ArgumentNullException"><paramref name="structureReference"/> is <see langword="null"/>.</exception><exception cref="T:Org.Sdmxsource.Sdmx.Api.Exception.SdmxNotImplementedException">Unknown condition encountered.</exception>
        public override ISdmxObjects GetSdmxObjects(IStructureReference structureReference, ResolveCrossReferences resolveCrossReferences)
        {

            return this.GetMaintainables(new RESTStructureQueryCore(structureReference));
        }

        /// <summary>
        /// Gets the SDMX objects20.
        /// </summary>
        /// <param name="reference"></param>
        /// <param name="resolveReferences">if set to <c>true</c> [resolve references].</param>
        /// <param name="returnDetails">It true by default. Set it to <c>false</c> to avoid large responses (Issue 427 / SDMXRI-1441).</param>
        /// <returns></returns>
        private ISdmxObjects GetSdmxObjects20(List<IStructureReference> references, bool resolveReferences, bool returnDetails = true)
        {
            var queryStructureRequestBuilderManager = new QueryStructureRequestBuilderManager(new FakeToolIndicator());

            IStructureQueryFormat<XDocument> queryFormat = new QueryStructureRequestFormat();
            var wdoc = queryStructureRequestBuilderManager.BuildStructureQuery(references, queryFormat, resolveReferences);

            // Issue 427 / SDMXRI-1441
            if (!returnDetails)
            {
                var queryStructureRequestElement = wdoc.Descendants().Where(d => d.Name.LocalName.Equals("QueryStructureRequest")).FirstOrDefault();
                if (queryStructureRequestElement == null)
                {
                    throw new SdmxInternalServerException("QueryStructureRequest element not fould in the request body.");
                }
                queryStructureRequestElement.Add(new XAttribute("returnDetails", false));
            }

            var doc = new XmlDocument();
            doc.LoadXml(wdoc.ToString());

            var tempFileName = Path.GetTempFileName();

            try
            {
                this.SendRequest(doc, "QueryStructure", tempFileName);

                ISdmxObjects structureObjects;
                IStructureParsingManager parsingManager = new StructureParsingManager(SdmxSchemaEnumType.Null);
                using (var dataLocation = new FileReadableDataLocation(tempFileName))
                {
                    var structureWorkspace = parsingManager.ParseStructures(dataLocation);
                    structureObjects = structureWorkspace.GetStructureObjects(false);
                }

                return structureObjects;
            }
            finally
            {
                // delete the temporary file
                File.Delete(tempFileName);
            }
        }

        /// <summary>
        /// Gets the SDMX objects21.
        /// </summary>
        /// <param name="restStructureQuery">The structure references.</param>
        /// <returns></returns>
        private ISdmxObjects GetSdmxObjects21(IRestStructureQuery restStructureQuery)
        {
            var complexStructureQueryDataflow = new StructureQuery2ComplexQueryBuilder().Build(restStructureQuery);
            return this.SendQueryStructureRequest(complexStructureQueryDataflow, this.GetSoapAction(restStructureQuery.StructureReference.MaintainableStructureEnumType.EnumType));
        }

        private string GetSoapAction(SdmxStructureEnumType structure)
        {
            switch (structure)
            {
                case SdmxStructureEnumType.Dataflow:
                    return "GetDataflow";
                case SdmxStructureEnumType.CodeList:
                    return "GetCodelist";
                case SdmxStructureEnumType.Categorisation:
                    return "GetCategorisation";
                case SdmxStructureEnumType.CategoryScheme:
                    return "GetCategoryScheme";
                case SdmxStructureEnumType.ConceptScheme:
                    return "GetConceptScheme";
                case SdmxStructureEnumType.Dsd:
                    return "GetDataStructure";
                case SdmxStructureEnumType.HierarchicalCodelist:
                    return "GetHierarchicalCodelist";
                case SdmxStructureEnumType.ReportingTaxonomy:
                    return "GetReportingTaxonomy";
                case SdmxStructureEnumType.OrganisationScheme:
                    return "GetOrganisationScheme";
                case SdmxStructureEnumType.Constraint:
                    return "GetConstraint";
                case SdmxStructureEnumType.Process:
                    return "GetProcess";
                case SdmxStructureEnumType.ProvisionAgreement:
                    return "GetProvisionAgreement";
                case SdmxStructureEnumType.StructureSet:
                    return "GetStructureSet";
                case SdmxStructureEnumType.MetadataFlow:
                    return "GetMetadataflow";
                case SdmxStructureEnumType.Msd:
                    return "GetMetadataStructure";
                default:
                    return "GetStructures";
            }
        }

        private ISdmxObjects SendQueryStructureRequest(
            IComplexStructureQuery complexStructureQuery,
            string methodName)
        {
            IStructureQueryFormat<XDocument> queryFormat = new ComplexQueryFormatV21();

            IComplexStructureQueryFactory<XDocument> factory = new ComplexStructureQueryFactoryV21(new FakeToolIndicator());
            IComplexStructureQueryBuilderManager<XDocument> complexStructureQueryBuilderManager = new ComplexStructureQueryBuilderManager<XDocument>(factory);
            var wdoc = complexStructureQueryBuilderManager.BuildComplexStructureQuery(complexStructureQuery, queryFormat);
            var doc = new XmlDocument();
            doc.LoadXml(wdoc.ToString());

            var tempFileName = Path.GetTempFileName();

            try
            {
                this.SendRequest(doc, methodName, tempFileName);

                ISdmxObjects structureObjects;
                IStructureParsingManager parsingManager = new StructureParsingManager(SdmxSchemaEnumType.Null);
                using (var dataLocation = new FileReadableDataLocation(tempFileName))
                {
                    var structureWorkspace = parsingManager.ParseStructures(dataLocation);
                    structureObjects = structureWorkspace.GetStructureObjects(false);
                }

                return structureObjects;
            }
            finally
            {
                // Delete the temporary file
                File.Delete(tempFileName);
            }
        }

        /// <summary>
        /// Sends the request.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="webServiceOperation">The web service operation.</param>
        /// <param name="tempFileName">Name of the temporary file.</param>
        private void SendRequest(XmlDocument request, string webServiceOperation, string tempFileName)
        {
            try
            {
                using (var response = _soapWriter.Submit(request.GetWriterAction(), webServiceOperation))
                using (var stream = response.GetResponseStream())
                {
                    if (stream != null)
                    {
                        var settings = new XmlWriterSettings();
                        settings.Indent = true;
                        using (var writer = XmlWriter.Create(tempFileName, settings))
                        using (var readableDataLocation = new ReadableDataLocationTmp(stream))
                        {
                            SoapUtils.ExtractSdmxMessageWithComment(readableDataLocation, writer);
                        }
                    }
                }
            }
            catch (WebException e)
            {
                NsiClientHelper.HandleSoapFault(e);

                if (e.Status == WebExceptionStatus.ProtocolError)
                {
                    WebResponse resp = e.Response;
                    using (StreamReader sr = new StreamReader(resp.GetResponseStream()))
                    {
                        var readToEnd = sr.ReadToEnd();

                        Trace.WriteLine(readToEnd);
                    }
                }
                throw;
            }
        }
    }
}