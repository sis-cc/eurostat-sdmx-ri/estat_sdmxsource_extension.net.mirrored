namespace Estat.Sdmxsource.Extension.Manager
{
    using Org.Sdmxsource.Sdmx.Structureparser.Model;

    public class FakeToolIndicator : IToolIndicator
    {
        /// <summary>
        /// Gets the comment.
        /// </summary>
        /// <returns>
        /// Generates a comment.
        /// </returns>
        public string GetComment()
        {
            return string.Empty;
        }
    }
}