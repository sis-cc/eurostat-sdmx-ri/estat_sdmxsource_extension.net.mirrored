﻿// -----------------------------------------------------------------------
// <copyright file="ResponseWriterManager.cs" company="EUROSTAT">
//   Date Created : 2017-11-15
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Estat.Sdmxsource.Extension.Model.Error;
using Estat.Sdmxsource.Extension.Factory;
using Org.Sdmxsource.Sdmx.Api.Exception;

namespace Estat.Sdmxsource.Extension.Manager
{
    /// <summary>
    /// A manager class for writing SDMX repsonses for registry and non-registry requests
    /// </summary>
    public class ResponseWriterManager : IResponseWriterManager
    {
        private readonly IResponseWriterFactory[] _factories;

        /// <summary>
        /// Initializes a new instance of the <see cref="ResponseWriterManager"/> class.
        /// </summary>
        /// <param name="factories">The factories.</param>
        /// <exception cref="ArgumentNullException">factories</exception>
        public ResponseWriterManager(params IResponseWriterFactory[] factories)
        {
            if (factories == null)
            {
                throw new ArgumentNullException(nameof(factories));
            }

            _factories = factories.ToArray();
        }

        /// <summary>
        /// Writes the specified format.
        /// </summary>
        /// <param name="format">The format.</param>
        /// <param name="statusObjects">The status objects.</param>
        /// <exception cref="ArgumentNullException">format</exception>
        /// <exception cref="SdmxNotImplementedException"><paramref name="format"/> is not supported</exception>
        public void Write(IResponseFormat format, IList<IResponseWithStatusObject> statusObjects)
        {
            if (format == null)
            {
                throw new ArgumentNullException(nameof(format));
            }

            if (statusObjects == null)
            {
                throw new ArgumentNullException(nameof(statusObjects));
            }

            using (var engine = _factories.Select(x => x.GetEngine(format)).FirstOrDefault(x => x != null))
            {
                if (engine == null)
                {
                    throw new SdmxNotImplementedException(format.ToString());
                }

                engine.Write(statusObjects);
            }
        }
    }
}
