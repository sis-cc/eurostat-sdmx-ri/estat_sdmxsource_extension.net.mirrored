// -----------------------------------------------------------------------
// <copyright file="AuthorizationManager.cs" company="EUROSTAT">
//   Date Created : 2017-06-08
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sdmxsource.Extension.Manager
{
    using System.Security.Principal;

    using Estat.Sdmxsource.Extension.Constant;

    using Org.Sdmxsource.Sdmx.Api.Exception;

    /// <summary>
    /// The authorization manager.
    /// </summary>
    public class AuthorizationManager : IAuthorizationManager
    {
        /// <summary>
        /// The dataflow principal
        /// </summary>
        private readonly IDataflowPrincipalManager _dataflowPrincipal;

        /// <summary>
        /// Initializes a new instance of the <see cref="AuthorizationManager"/> class.
        /// </summary>
        /// <param name="dataflowPrincipal">The dataflow principal.</param>
        public AuthorizationManager(IDataflowPrincipalManager dataflowPrincipal)
        {
            this._dataflowPrincipal = dataflowPrincipal;
        }

        /// <summary>
        /// Check authorization access with the specified permission type.
        /// </summary>
        /// <param name="permissionType">Type of the permission.</param>
        /// <param name="authorizationBehaviorType">Type of the authorization behavior.</param>
        /// <exception cref="SdmxUnauthorisedException">Unauthorized access not allowed
        /// or
        /// Resource access permission denied</exception>
        public void Authorize(PermissionType permissionType, AuthorizationBehaviorType authorizationBehaviorType)
        {
            IPrincipal principal = this._dataflowPrincipal.GetCurrentPrincipal();
            if (principal == null)
            {
                RequirePrincipal(authorizationBehaviorType);
                return;
            }

            // handle windows anonymous user
            if (principal.Identity != null && principal.Identity.GetType().Name.Contains("WindowsIdentity"))
            {
                dynamic identity = principal.Identity;
                if (identity.IsAnonymous is bool && identity.IsAnonymous)
                {
                    RequirePrincipal(authorizationBehaviorType);
                    return;
                }
            }

            if (!principal.IsInRole(permissionType.ToString()))
            {
                throw new SdmxUnauthorisedException("Resource access permission denied");
            }
        }

        /// <summary>
        /// Requires the principal.
        /// </summary>
        /// <param name="authorizationBehaviorType">Type of the authorization behavior.</param>
        /// <exception cref="SdmxUnauthorisedException">Unauthorized access not allowed</exception>
        private static void RequirePrincipal(AuthorizationBehaviorType authorizationBehaviorType)
        {
            if (authorizationBehaviorType != AuthorizationBehaviorType.RequirePrincipal)
            {
                return;
            }

            // TODO change to forbidden
            throw new SdmxUnauthorisedException("Unauthorized access not allowed");
        }
    }
}