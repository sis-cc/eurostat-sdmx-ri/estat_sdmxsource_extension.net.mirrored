// -----------------------------------------------------------------------
// <copyright file="IDataRegistrator.cs" company="EUROSTAT">
//   Date Created : 2017-11-28
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sdmxsource.Extension.Manager
{
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Registry;
    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Registry;

    /// <summary>
    /// The Data Registrator interface is responsible for sending registrations to a Registry, and storing them in a local store
    /// </summary>
    public interface IDataRegistrator
    {
        /// <summary>
        /// Sends the SDMX submit registration request containign the specified <paramref name="registrations"/> to a SDMX v2.1 compliant Registry and store the response in a local store
        /// </summary>
        /// <param name="registrations">The registrations.</param>
        void SendSubmitRegistrationsRequest(Dictionary<DatasetActionEnumType, IList<IRegistrationMutableObject>> registrations);
    }
}
