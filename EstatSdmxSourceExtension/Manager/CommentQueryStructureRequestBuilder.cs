﻿// -----------------------------------------------------------------------
// <copyright file="CommentQueryStructureRequestBuilder.cs" company="EUROSTAT">
//   Date Created : 2016-08-18
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.CustomRequests.Manager
{
    using System.Collections.Generic;
    using System.Xml.Linq;

    using Estat.Sri.CustomRequests.Builder;

    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Structureparser.Model;

    /// <summary>
    /// The Comment Query Structure Request Builder
    /// </summary>
    public class CommentQueryStructureRequestBuilder : IQueryStructureRequestBuilder<XDocument>
    {
        /// <summary>
        /// The _query structure request builder.
        /// </summary>
        private readonly IQueryStructureRequestBuilder<XDocument> _queryStructureRequestBuilder;

        /// <summary>
        /// The _tool indicator.
        /// </summary>
        private readonly IToolIndicator _toolIndicator;

        /// <summary>
        /// Initializes a new instance of the <see cref="CommentQueryStructureRequestBuilder"/> class. 
        /// Initializes a new instance of the <see cref="T:System.Object"/> class.
        /// </summary>
        /// <param name="toolIndicator">
        /// The tool Indicator.
        /// </param>
        /// <param name="queryStructureRequestBuilder">
        /// The query Structure Request Builder.
        /// </param>
        public CommentQueryStructureRequestBuilder(
            IToolIndicator toolIndicator,
            IQueryStructureRequestBuilder<XDocument> queryStructureRequestBuilder)
        {
            this._toolIndicator = toolIndicator;
            this._queryStructureRequestBuilder = queryStructureRequestBuilder;
        }

        /// <summary>
        /// Builds a <c>QueryStructureRequest</c> that matches the passed in format
        /// </summary>
        /// <param name="queries">
        /// The queries.
        /// </param>
        /// <param name="resolveReferences">
        /// Set to <c>True</c> to resolve references.
        /// </param>
        /// <returns>
        /// The <see cref="XDocument"/> from <paramref name="queries"/>.
        /// </returns>
        public XDocument BuildStructureQuery(IEnumerable<IStructureReference> queries, bool resolveReferences)
        {
            var doc = this._queryStructureRequestBuilder.BuildStructureQuery(queries, resolveReferences);
            if (this._toolIndicator != null && doc.Root != null)
            {
                var comm = new XComment(this._toolIndicator.GetComment());
                doc.Root.FirstNode.AddBeforeSelf(comm);
            }

            return doc;
        }
    }
}