// -----------------------------------------------------------------------
// <copyright file="CrossDataWriterManager.cs" company="EUROSTAT">
//   Date Created : 2016-07-04
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or � as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sdmxsource.Extension.Manager
{
    using System.Collections.Generic;
    using System.IO;

    using Estat.Sdmxsource.Extension.Factory;

    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Data;
    using Org.Sdmxsource.Sdmx.DataParser.Engine;
    using Org.Sdmxsource.Util;

    /// <summary>
    /// The default implementation of the <see cref="ICrossDataWriterManager"/> interface.
    /// It will iterate through a list of <see cref="ICrossDataWriterFactory"/> and return the first not null result of <see cref="ICrossDataWriterFactory.GetWriterEngine"/>
    /// </summary>
    /// <seealso cref="Estat.Sdmxsource.Extension.Manager.ICrossDataWriterManager" />
    public class CrossDataWriterManager : ICrossDataWriterManager
    {
        /// <summary>
        /// The _factories
        /// </summary>
        private readonly IList<ICrossDataWriterFactory> _factories;

        /// <summary>
        /// Initializes a new instance of the <see cref="CrossDataWriterManager"/> class.
        /// </summary>
        /// <param name="factories">
        /// The factories.
        /// </param>
        public CrossDataWriterManager(params ICrossDataWriterFactory[] factories)
        {
            this._factories = factories;
        }

        /// <summary>
        /// Gets the SDMX v2.0 Cross Sectional data writer engine.
        /// </summary>
        /// <param name="dataFormat">
        /// The data format. The implementation may have extra properties.
        /// </param>
        /// <param name="outputStream">
        /// The output stream. It can be null. But in this case the output should be provided in the <paramref name="dataFormat"/> implementation.
        /// </param>
        /// <returns>
        /// The <see cref="ICrossSectionalWriterEngine"/>
        /// </returns>
        /// <exception cref="SdmxNotImplementedException">
        /// SDMX v2.0 Cross Sectional format is not supported. No factories provided.
        /// or
        /// SDMX v2.0 Cross Sectional format is not supported. No suitable factories found.
        /// </exception>
        public ICrossSectionalWriterEngine GetWriterEngine(IDataFormat dataFormat, Stream outputStream)
        {
            if (!ObjectUtil.ValidCollection(this._factories))
            {
                throw new SdmxNotImplementedException(
                    "SDMX v2.0 Cross Sectional format is not supported. No factories provided.");
            }

            foreach (var crossDataWriterFactory in this._factories)
            {
                var dataEngine = crossDataWriterFactory.GetWriterEngine(dataFormat, outputStream);
                if (dataEngine != null)
                {
                    return dataEngine;
                }
            }

            throw new SdmxNotImplementedException(
                "SDMX v2.0 Cross Sectional format is not supported. No suitable factories found.");
        }
    }
}