﻿// -----------------------------------------------------------------------
// <copyright file="IDataflowAuthorizationManager.cs" company="EUROSTAT">
//   Date Created : 2013-03-22
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sdmxsource.Extension.Manager
{
    using System.Security.Principal;

    using Estat.Sdmxsource.Extension.Engine;

    /// <summary>
    /// The Dataflow Authorization manager. Implementations will provide an instance of the <see cref="IDataflowAuthorizationEngine"/>
    /// based on the type of <see cref="IPrincipal"/>
    /// </summary>
    public interface IDataflowAuthorizationManager
    {
        /// <summary>
        /// Gets the authorization engine.
        /// </summary>
        /// <param name="principal">The principal.</param>
        /// <returns>The <see cref="IDataflowAuthorizationEngine"/>.</returns>
        IDataflowAuthorizationEngine GetAuthorizationEngine(IPrincipal principal);
    }
}