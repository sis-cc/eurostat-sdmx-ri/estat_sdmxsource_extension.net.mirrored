// -----------------------------------------------------------------------
// <copyright file="FormatResolverManager.cs" company="EUROSTAT">
//   Date Created : 2016-07-06
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System.Net.Mime;

namespace Estat.Sdmxsource.Extension.Manager
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Net;
    using System.Xml;

    using Estat.Sdmxsource.Extension.Extension;
    using Estat.Sdmxsource.Extension.Factory;
    using Estat.Sdmxsource.Extension.Model;
    using Estat.Sdmxsource.Extension.Util;
    using log4net;

    using Org.Sdmxsource.Sdmx.Api.Builder;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
    using Org.Sdmxsource.Sdmx.Api.Model.Data;
    using Org.Sdmxsource.Sdmx.Api.Model.Data.Query;
    using Org.Sdmxsource.Sdmx.Api.Model.Format;
    using Org.Sdmxsource.Sdmx.Api.Model.Query;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Builder.Common;

    /// <summary>
    /// The format resolver manager.
    /// </summary>
    public class FormatResolverManager : IFormatResolverManager
    {
        /// <summary>
        /// The _log
        /// </summary>
        private static readonly ILog _log = LogManager.GetLogger(typeof(FormatResolverManager));

        /// <summary>
        /// The _data writer plugin contracts
        /// </summary>
        private readonly IList<IDataWriterPluginContract> _dataWriterPluginContracts;

        /// <summary>
        /// The _structure writer plugin contracts
        /// </summary>
        private readonly IList<IStructureWriterPluginContract> _structureWriterPluginContracts;

        /// <summary>
        /// The _sorted accept headers
        /// </summary>
        private readonly IBuilder<ISortedAcceptHeaders, WebHeaderCollection> _sortedAcceptHeaders;

        /// <summary>
        /// The format normalizer
        /// </summary>
        private readonly FormatNormalizer _formatNormalizer = new FormatNormalizer();

        /// <summary>
        /// Initializes a new instance of the <see cref="FormatResolverManager" /> class.
        /// </summary>
        /// <param name="structureWriterPluginContracts">The structure writer plugin contracts.</param>
        /// <param name="dataWriterPluginContracts">The data writer plugin contracts.</param>
        /// <param name="sortedAcceptHeaders">The build that sorts the Accept headers and creates a <see cref="ISortedAcceptHeaders"/>.</param>
        /// <exception cref="System.ArgumentNullException">structureWriterPluginContracts
        /// or
        /// dataWriterPluginContracts</exception>
        public FormatResolverManager(IList<IStructureWriterPluginContract> structureWriterPluginContracts, IList<IDataWriterPluginContract> dataWriterPluginContracts, IBuilder<ISortedAcceptHeaders, WebHeaderCollection> sortedAcceptHeaders)
        {
            if (structureWriterPluginContracts == null)
            {
                throw new ArgumentNullException("structureWriterPluginContracts");
            }

            if (dataWriterPluginContracts == null)
            {
                throw new ArgumentNullException("dataWriterPluginContracts");
            }

            if (sortedAcceptHeaders == null)
            {
                throw new ArgumentNullException("sortedAcceptHeaders");
            }

            this._structureWriterPluginContracts = structureWriterPluginContracts.Select(contract => (IStructureWriterPluginContract)new SafeStructureWritingPlugin(contract)).ToArray();
            this._dataWriterPluginContracts = dataWriterPluginContracts.Select(contract => (IDataWriterPluginContract)new SafeDataWriterPluginContract(contract)).ToArray();
            this._sortedAcceptHeaders = sortedAcceptHeaders;
        }

        /// <summary>
        /// Gets the data format.
        /// </summary>
        /// <param name="headers">The headers.</param>
        /// <param name="retrievalManager">The retrieval manager.</param>
        /// <param name="dataQuery">The data query.</param>
        /// <returns>
        /// The <see cref="IRestResponse{IDataFormat}"/>.
        /// </returns>
        /// <exception cref="System.ArgumentNullException">
        /// headers
        /// or
        /// retrievalManager
        /// or
        /// dataQuery
        /// </exception>
        public IRestResponse<IDataFormat> GetDataFormat(WebHeaderCollection headers, ISdmxObjectRetrievalManager retrievalManager, IDataQuery dataQuery, RestApiVersion restApiVersion)
        {
            if (headers == null)
            {
                throw new ArgumentNullException("headers");
            }

            if (retrievalManager == null)
            {
                throw new ArgumentNullException("retrievalManager");
            }

            if (dataQuery == null)
            {
                throw new ArgumentNullException("dataQuery");
            }

            // TODO replace with .NET 4.5 System.Http.Headers
            ISortedAcceptHeaders acceptHeaders = this._sortedAcceptHeaders.Build(headers);
            IRestDataRequest requestInfo = new RestDataRequest(
                headers,
                        retrievalManager,
                        dataQuery,
                        acceptHeaders);
            var supportedFormats = this._formatNormalizer.NormalizeDataFormatOrder(this._dataWriterPluginContracts.SelectMany(contract => contract.GetDataFormats(requestInfo)).Where(response => response != null && response.ResponseContentHeader.MediaType != null), restApiVersion);
            
            return GetRestResponse(headers, supportedFormats);
        }

        public IEnumerable<string> GetAvailableDataFormats()
        {
            return _dataWriterPluginContracts.SelectMany(c => c.GetAvailableDataFormats());
        }

        public IEnumerable<string> GetAvailableStructureFormats()
        {
            return _structureWriterPluginContracts.SelectMany(c => c.GetAvailableStructureFormats());
        }

        /// <summary>
        /// Gets the data format.
        /// </summary>
        /// <param name="dataType">
        ///     Type of the data.
        /// </param>
        /// <param name="xmlWriter">
        ///     The XML writer.
        /// </param>
        /// <returns>
        /// The <see cref="IDataFormat"/>.
        /// </returns>
        /// <exception cref="SdmxNotImplementedException">
        /// The <paramref name="dataType"/> is not supported.
        /// </exception>
        public IDataFormat GetDataFormat(ISoapRequest<DataType> dataType, XmlWriter xmlWriter)
        {
            if (dataType == null)
            {
                throw new ArgumentNullException("dataType");
            }

            if (xmlWriter == null)
            {
                throw new ArgumentNullException("xmlWriter");
            }

            foreach (var dataWriterPluginContract in this._dataWriterPluginContracts)
            {
                var restDataResponse = dataWriterPluginContract.GetDataFormat(dataType, xmlWriter);
                if (restDataResponse != null)
                {
                    return restDataResponse;
                }
            }

            throw new SdmxNotImplementedException(string.Format(CultureInfo.InvariantCulture, "Support for format {0} is not implemented.", dataType));
        }

        /// <summary>
        /// Gets the structure format.
        /// </summary>
        /// <param name="headers">The headers.</param>
        /// <param name="structureType">The type of the requested structure</param>
        /// <param name="restApiVersion">The REST API version.</param>
        /// <returns>
        /// The <see cref="IStructureFormat" />.
        /// </returns>
        /// <exception cref="System.ArgumentNullException">headers is null</exception>
        public IRestResponse<IStructureFormat> GetStructureFormat(WebHeaderCollection headers, RestApiVersion restApiVersion)
        {
            if (headers == null)
            {
                throw new ArgumentNullException("headers");
            }

            // TODO replace with .NET 4.5 System.Http.Headers
            ISortedAcceptHeaders acceptHeaders = this._sortedAcceptHeaders.Build(headers);
            var restResponses = this._structureWriterPluginContracts.SelectMany(contract => contract.GetStructureFormats(headers, acceptHeaders))
                .Where(response => response != null && response.Format != null && response.ResponseContentHeader.MediaType != null);
            var supportedFormats = this._formatNormalizer.NormalizeStructureFormatOrder(restResponses, restApiVersion);
            return GetRestResponse(headers, supportedFormats);
        }

        /// <summary>
        /// Gets the structure format.
        /// </summary>
        /// <param name="format">
        ///     The format.
        /// </param>
        /// <param name="xmlWriter">
        ///     The XML writer.
        /// </param>
        /// <returns>
        /// The <see cref="IStructureFormat"/>.
        /// </returns>
        public IStructureFormat GetStructureFormat(ISoapRequest<StructureOutputFormat> format, XmlWriter xmlWriter)
        {
            if (format == null)
            {
                throw new ArgumentNullException("format");
            }

            if (xmlWriter == null)
            {
                throw new ArgumentNullException("xmlWriter");
            }

            foreach (var structureWriterPluginContract in this._structureWriterPluginContracts)
            {
                if (structureWriterPluginContract != null)
                {
                    var structureFormat = structureWriterPluginContract.GetStructureFormat(format, xmlWriter);
                    if (structureFormat != null)
                    {
                        return structureFormat;
                    }
                }
            }

            throw new SdmxNotImplementedException(string.Format(CultureInfo.InvariantCulture, "Support for format {0} is not implemented.", format.FormatType.EnumType));
        }

        /// <summary>
        /// Gets the rest response.
        /// </summary>
        /// <typeparam name="TOutputFormat">
        /// The type of the output format.
        /// </typeparam>
        /// <param name="headers">
        /// The headers.
        /// </param>
        /// <param name="supportedFormats">
        /// The supported formats.
        /// </param>
        /// <returns>
        /// The <see cref="IRestResponse{TOutputFormat}"/>.
        /// </returns>
        private static IRestResponse<TOutputFormat> GetRestResponse<TOutputFormat>(WebHeaderCollection headers, IList<IRestResponse<TOutputFormat>> supportedFormats)
        {
            var acceptHeaderToken = headers.GetContentBestMatch(supportedFormats);
            
            if (acceptHeaderToken != null)
            {
                if (acceptHeaderToken.RestResponse == null || acceptHeaderToken.RestResponse.Format.IsDefaultValue()
                    || acceptHeaderToken.RestResponse.ResponseContentHeader.MediaType == null)
                {
                    _log.WarnFormat("Best matching format does not have a format or media type set {0} ", acceptHeaderToken);
                }
                else
                {
                    return acceptHeaderToken.RestResponse;
                }
            }

            throw GenerateNotAcceptedException(headers, supportedFormats.Select(response => response.ResponseContentHeader.MediaType.ToString()));
        }

        /// <summary>
        /// Generates the not accepted exception.
        /// </summary>
        /// <param name="headers">
        ///     The headers.
        /// </param>
        /// <param name="supportedFormats">The list of supported formats</param>
        /// <returns>
        /// The <see cref="SdmxNotAcceptableException"/>.
        /// </returns>
        private static SdmxNotAcceptableException GenerateNotAcceptedException(
            WebHeaderCollection headers,
            IEnumerable<string> supportedFormats)
        {
            var requestAccept = headers[HttpRequestHeader.Accept];
            _log.Warn("Cannot serve content type: " + requestAccept);
            var message = string.Format(CultureInfo.InvariantCulture, "acceptable: {0}" + Environment.NewLine + "optional : urn=true", string.Join(", ", supportedFormats));
            return new SdmxNotAcceptableException(message);
        }
    }
}