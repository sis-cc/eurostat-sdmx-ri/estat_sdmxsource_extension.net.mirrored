﻿// -----------------------------------------------------------------------
// <copyright file="CommentComplexDataQueryBuilder.cs" company="EUROSTAT">
//   Date Created : 2016-08-18
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.CustomRequests.Manager
{
    using System.Xml.Linq;

    using Estat.Sri.CustomRequests.Builder;

    using Org.Sdmxsource.Sdmx.Api.Model.Data.Query.Complex;
    using Org.Sdmxsource.Sdmx.Structureparser.Model;

    /// <summary>
    /// The  Comment ComplexDataQueryBuilder
    /// </summary>
    public class CommentComplexDataQueryBuilder : IComplexDataQueryBuilder
    {
        /// <summary>
        /// The _complex data query builder.
        /// </summary>
        private readonly IComplexDataQueryBuilder _complexDataQueryBuilder;

        /// <summary>
        /// The _tool indicator.
        /// </summary>
        private readonly IToolIndicator _toolIndicator;

        /// <summary>
        /// Initializes a new instance of the <see cref="CommentComplexDataQueryBuilder"/> class.
        /// </summary>
        /// <param name="complexDataQueryBuilder">
        /// The complex data query builder.
        /// </param>
        /// <param name="toolIndicator">
        /// The tool indicator.
        /// </param>
        public CommentComplexDataQueryBuilder(
            IComplexDataQueryBuilder complexDataQueryBuilder,
            IToolIndicator toolIndicator)
        {
            this._complexDataQueryBuilder = complexDataQueryBuilder;
            this._toolIndicator = toolIndicator;
        }

        /// <summary>
        /// Builds a ComplexDataQuery that matches the passed in format
        /// </summary>
        /// <param name="query">
        /// The query.
        /// </param>
        /// <returns>
        /// The document
        /// </returns>
        public XDocument BuildComplexDataQuery(IComplexDataQuery query)
        {
            var doc = this._complexDataQueryBuilder.BuildComplexDataQuery(query);
            if (this._toolIndicator != null && doc.Root != null)
            {
                var comm = new XComment(this._toolIndicator.GetComment());
                doc.Root.FirstNode.AddBeforeSelf(comm);
            }

            return doc;
        }
    }
}