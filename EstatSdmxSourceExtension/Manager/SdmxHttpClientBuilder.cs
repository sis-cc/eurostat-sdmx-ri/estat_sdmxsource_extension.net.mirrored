using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net;
using System.Security.Policy;
using System.Text;
using Org.Sdmxsource.Util;
using System.IO;
using Org.Sdmxsource.Sdmx.Api.Exception;
using System.Threading.Tasks;
using Estat.Sdmxsource.Extension.Model;

namespace Estat.Sdmxsource.Extension.Manager
{
    internal class SdmxHttpClient
    {
        private readonly HttpClientHandler _handler;
        private Uri _baseAdress;

        public SdmxHttpClient(WsInfo fusionSettings)
        {
            this._handler = new HttpClientHandler();
            this._handler.Credentials = new NetworkCredential(fusionSettings.UserName, fusionSettings.Password);
            this._baseAdress = new Uri(fusionSettings.EndPoint);
            if (string.IsNullOrEmpty(fusionSettings.ProxyServer))
            {
                this._handler.Proxy = new WebProxy(fusionSettings.ProxyServer);
                this._handler.Proxy.Credentials = new NetworkCredential(fusionSettings.ProxyUserName, fusionSettings.ProxyPassword);

            }
        }

        public async Task<HttpResponseMessage> GetStructuresResponseAsync(string request,Dictionary<string,string> headers)
        {
            using (HttpClient httpClient = new HttpClient(this._handler))
            {
                httpClient.Timeout = TimeSpan.FromSeconds(10);
                httpClient.BaseAddress = _baseAdress;
                foreach(var header in headers)
                {
                    httpClient.DefaultRequestHeaders.Add(header.Key, header.Value);
                }

                 return await httpClient.SendAsync(new HttpRequestMessage(HttpMethod.Get, request));
            }

        }
    }
}
