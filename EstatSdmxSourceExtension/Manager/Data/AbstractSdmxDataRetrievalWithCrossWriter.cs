// -----------------------------------------------------------------------
// <copyright file="AbstractSdmxDataRetrievalWithCrossWriter.cs" company="EUROSTAT">
//   Date Created : 2014-07-08
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System.Threading.Tasks;

namespace Estat.Sdmxsource.Extension.Manager.Data
{
    using System;

    using Org.Sdmxsource.Sdmx.Api.Model.Data.Query;
    using Org.Sdmxsource.Sdmx.DataParser.Engine;
    using Org.Sdmxsource.Sdmx.DataParser.Manager;

    /// <summary>
    ///     The decorator class for <see cref="ISdmxDataRetrievalWithCrossWriter" />
    /// </summary>
    public abstract class AbstractSdmxDataRetrievalWithCrossWriter : ISdmxDataRetrievalWithCrossWriter
    {
        /// <summary>
        ///     The _data retrieval with cross writer.
        /// </summary>
        private readonly ISdmxDataRetrievalWithCrossWriter _dataRetrievalWithCrossWriter;

        /// <summary>
        ///     Initializes a new instance of the <see cref="AbstractSdmxDataRetrievalWithCrossWriter" /> class.
        /// </summary>
        /// <param name="dataRetrievalWithCrossWriter">
        ///     The data retrieval with cross writer.
        /// </param>
        protected AbstractSdmxDataRetrievalWithCrossWriter(
            ISdmxDataRetrievalWithCrossWriter dataRetrievalWithCrossWriter)
        {
            if (dataRetrievalWithCrossWriter == null)
            {
                throw new ArgumentNullException("dataRetrievalWithCrossWriter");
            }

            this._dataRetrievalWithCrossWriter = dataRetrievalWithCrossWriter;
        }

        /// <summary>
        ///     Gets the data.
        /// </summary>
        /// <param name="dataQuery">
        ///     The data query.
        /// </param>
        /// <param name="dataWriter">
        ///     The data writer.
        /// </param>
        public virtual void GetData(IDataQuery dataQuery, ICrossSectionalWriterEngine dataWriter)
        {
            this._dataRetrievalWithCrossWriter.GetData(dataQuery, dataWriter);
        }

        /// <summary>
        ///     Gets the data asynchronous.
        /// </summary>
        /// <param name="dataQuery">
        ///     The data query.
        /// </param>
        /// <param name="dataWriter">
        ///     The data writer.
        /// </param>
        public virtual async Task GetDataAsync(IDataQuery dataQuery, ICrossSectionalWriterEngine dataWriter)
        {
            await this._dataRetrievalWithCrossWriter.GetDataAsync(dataQuery, dataWriter);
        }
    }
}