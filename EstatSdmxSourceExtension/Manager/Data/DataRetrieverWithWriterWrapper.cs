// -----------------------------------------------------------------------
// <copyright file="DataRetrieverWithWriterWrapper.cs" company="EUROSTAT">
//   Date Created : 2016-10-05
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System.Threading.Tasks;

namespace Estat.Sdmxsource.Extension.Manager.Data
{
    using System;

    using Estat.Sdmxsource.Extension.Builder;

    using Org.Sdmxsource.Sdmx.Api.Builder;
    using Org.Sdmxsource.Sdmx.Api.Engine;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval.Data;
    using Org.Sdmxsource.Sdmx.Api.Model.Data.Query;
    using Org.Sdmxsource.Sdmx.Api.Model.Data.Query.Complex;

    /// <summary>
    /// A <see cref="ISdmxDataRetrievalWithWriter"/> implementation that decorates a <see cref="IAdvancedSdmxDataRetrievalWithWriter"/>.
    /// It uses a builder to convert from <see cref="IDataQuery"/> to <see cref="IComplexDataQuery"/>. 
    /// By default it uses the <see cref="DataQuery2ComplexQueryBuilder"/> but can overridden in the constructor.
    /// </summary>
    public class DataRetrieverWithWriterWrapper : ISdmxDataRetrievalWithWriter
    {
        /// <summary>
        /// The advanced SDMX data retrieval with writer
        /// </summary>
        private readonly IAdvancedSdmxDataRetrievalWithWriter _advancedSdmxDataRetrievalWithWriter;

        /// <summary>
        /// The builder
        /// </summary>
        private readonly IBuilder<IComplexDataQuery, IDataQuery> _builder;

        /// <summary>
        /// Initializes a new instance of the <see cref="DataRetrieverWithWriterWrapper"/> class.
        /// </summary>
        /// <param name="advancedSdmxDataRetrievalWithWriter">The advanced SDMX data retrieval with writer.</param>
        /// <param name="builder">The builder.</param>
        public DataRetrieverWithWriterWrapper(IAdvancedSdmxDataRetrievalWithWriter advancedSdmxDataRetrievalWithWriter, IBuilder<IComplexDataQuery, IDataQuery> builder)
        {
            if (advancedSdmxDataRetrievalWithWriter == null)
            {
                throw new ArgumentNullException("advancedSdmxDataRetrievalWithWriter");
            }

            this._builder = builder ?? new DataQuery2ComplexQueryBuilder(false);
            this._advancedSdmxDataRetrievalWithWriter = advancedSdmxDataRetrievalWithWriter;
        }

        /// <summary>
        /// Queries for data conforming to the parameters defined by the DataQuery,
        /// the response is written to the DataWriterEngine
        /// </summary>
        /// <param name="dataQuery">The data query.</param>
        /// <param name="dataWriter">The data writer.</param>
        /// <exception cref="System.ArgumentNullException">
        /// dataQuery
        /// or
        /// dataWriter
        /// </exception>
        public void GetData(IDataQuery dataQuery, IDataWriterEngine dataWriter)
        {
            if (dataQuery == null)
            {
                throw new ArgumentNullException("dataQuery");
            }

            if (dataWriter == null)
            {
                throw new ArgumentNullException("dataWriter");
            }

            var complexQuery = this._builder.Build(dataQuery);
            this._advancedSdmxDataRetrievalWithWriter.GetData(complexQuery, dataWriter);
        }

        /// <summary>
        /// Queries for data conforming to the parameters defined by the DataQuery,
        /// the response is written to the DataWriterEngine
        /// </summary>
        /// <param name="dataQuery">The data query.</param>
        /// <param name="dataWriter">The data writer.</param>
        /// <exception cref="System.ArgumentNullException">
        /// dataQuery
        /// or
        /// dataWriter
        /// </exception>
        public async Task GetDataAsync(IDataQuery dataQuery, IDataWriterEngine dataWriter)
        {
            if (dataQuery == null)
            {
                throw new ArgumentNullException("dataQuery");
            }

            if (dataWriter == null)
            {
                throw new ArgumentNullException("dataWriter");
            }

            var complexQuery = this._builder.Build(dataQuery);
            await this._advancedSdmxDataRetrievalWithWriter.GetDataAsync(complexQuery, dataWriter);
        }
    }
}