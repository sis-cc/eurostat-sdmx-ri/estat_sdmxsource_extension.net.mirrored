﻿// -----------------------------------------------------------------------
// <copyright file="AdvancedMutableStructureSearchManagerWithAuth.cs" company="EUROSTAT">
//   Date Created : 2016-02-11
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sdmxsource.Extension.Manager
{
    using System;
    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval.Mutable;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Complex;

    /// <summary>
    /// A delegating class that wraps a <see cref="IAuthAdvancedMutableStructureSearchManager"/>.
    /// </summary>
    public class AdvancedMutableStructureSearchManagerWithAuth : IAdvancedMutableStructureSearchManager
    {
        /// <summary>
        /// The dataflow authorization advanced mutable structure search manager
        /// </summary>
        private readonly IAuthAdvancedMutableStructureSearchManager _authAdvancedMutableStructureSearchManager;

        /// <summary>
        /// The _authorized dataflows
        /// </summary>
        private readonly IList<IMaintainableRefObject> _authorizedDataflows;

        /// <summary>
        /// Initializes a new instance of the <see cref="AdvancedMutableStructureSearchManagerWithAuth"/> class.
        /// </summary>
        /// <param name="authAdvancedMutableStructureSearchManager">The authentication advanced mutable structure search manager.</param>
        /// <param name="authorizedDataflows">The authorized dataflows.</param>
        /// <exception cref="ArgumentNullException"><paramref name="authAdvancedMutableStructureSearchManager"/> is <see langword="null" />.</exception>
        public AdvancedMutableStructureSearchManagerWithAuth(IAuthAdvancedMutableStructureSearchManager authAdvancedMutableStructureSearchManager, IList<IMaintainableRefObject> authorizedDataflows)
        {
            if (authAdvancedMutableStructureSearchManager == null)
            {
                throw new ArgumentNullException("authAdvancedMutableStructureSearchManager");
            }

            this._authAdvancedMutableStructureSearchManager = authAdvancedMutableStructureSearchManager;
            this._authorizedDataflows = authorizedDataflows;
        }

        /// <summary>
        /// Process the specified <paramref name="structureQuery"/> returning an <see cref="IMutableObjects"/> container which contains the Maintainable Structure hat correspond to the <paramref name="structureQuery"/> query parameters.
        /// </summary>
        /// <param name="structureQuery">
        /// The structure query.
        /// </param>
        /// <returns>
        /// The <see cref="IMutableObjects"/>.
        /// </returns>
        public IMutableObjects GetMaintainables(IComplexStructureQuery structureQuery)
        {
            return this._authAdvancedMutableStructureSearchManager.GetMaintainables(structureQuery, this._authorizedDataflows);
        }
    }
}