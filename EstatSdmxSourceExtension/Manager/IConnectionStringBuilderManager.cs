// -----------------------------------------------------------------------
// <copyright file="IConnectionStringBuilderManager.cs" company="EUROSTAT">
//   Date Created : 2018-6-6
//   Copyright (c) 2009, 2018 by the European Commission, represented by Eurostat.   All rights reserved.
//
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sdmxsource.Extension.Manager
{
    using System.Configuration;
    using System.Security.Principal;

    /// <summary>
    /// Build the <see cref="ConnectionStringSettings"/>
    /// </summary>
    public interface IConnectionStringBuilderManager
    {
        /// <summary>
        /// Builds the <see cref="ConnectionStringSettings"/>
        /// </summary>
        /// <returns>The <see cref="ConnectionStringSettings"/>.</returns>
        ConnectionStringSettings Build();

        /// <summary>
        /// Builds the <see cref="ConnectionStringSettings" />
        /// </summary>
        /// <param name="storeId">The store identifier.</param>
        /// <returns>The <see cref="ConnectionStringSettings" />.</returns>
        ConnectionStringSettings Build(string storeId);

        /// <summary>
        /// Builds the the <see cref="ConnectionStringSettings"/> from the specified principal.
        /// </summary>
        /// <param name="principal">The principal.</param>
        /// <returns>The <see cref="ConnectionStringSettings"/>.</returns>
        ConnectionStringSettings Build(IPrincipal principal);
    }
}