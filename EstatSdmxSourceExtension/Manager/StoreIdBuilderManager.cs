// -----------------------------------------------------------------------
// <copyright file="StoreIdBuilderManager.cs" company="EUROSTAT">
//   Date Created : 2018-6-6
//   Copyright (c) 2009, 2018 by the European Commission, represented by Eurostat.   All rights reserved.
//
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sdmxsource.Extension.Manager
{
    using System;
    using System.Linq;
    using System.Security.Principal;
    using Estat.Sdmxsource.Extension.Builder;

    /// <summary>
    /// Build the store id. In SDMX RI it is the connection string name
    /// </summary>
    /// <seealso cref="Estat.Sdmxsource.Extension.Manager.IStoreIdBuilderManager" />
    public class StoreIdBuilderManager : IStoreIdBuilderManager
    {
        /// <summary>
        /// The store identifier builders
        /// </summary>
        private readonly IStoreIdBuilder[] _storeIdBuilders;

        /// <summary>
        /// Initializes a new instance of the <see cref="StoreIdBuilderManager"/> class.
        /// </summary>
        /// <param name="storeIdBuilders">The store identifier builders.</param>
        /// <exception cref="ArgumentNullException">
        /// storeIdBuilders
        /// </exception>
        public StoreIdBuilderManager(params IStoreIdBuilder[] storeIdBuilders)
        {
            if (storeIdBuilders == null)
            {
                throw new ArgumentNullException(nameof(storeIdBuilders));
            }

            _storeIdBuilders = storeIdBuilders;
        }

        /// <summary>
        /// Builds the store id using the default principal or implementation specific method.
        /// </summary>
        /// <returns>The store ID or null</returns>
        public string Build()
        {
            return _storeIdBuilders.Select(b => b.Build()).FirstOrDefault(id => id != null);
        }

        /// <summary>
        /// Builds the store id using the specified principal.
        /// </summary>
        /// <param name="principal">The principal.</param>
        /// <returns>The store ID or null</returns>
        public string Build(IPrincipal principal)
        {
            return _storeIdBuilders.Select(b => b.Build(principal)).FirstOrDefault(id => id != null);
        }
    }
}