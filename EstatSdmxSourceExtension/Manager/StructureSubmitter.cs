﻿// -----------------------------------------------------------------------
// <copyright file="StructureSubmitter.cs" company="EUROSTAT">
//   Date Created : 2017-11-16
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sdmxsource.Extension.Manager
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Estat.Sdmxsource.Extension.Model.Error;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Estat.Sdmxsource.Extension.Factory;
    using Org.Sdmxsource.Sdmx.Api.Exception;

    /// <summary>
    /// The default implementation of <see cref="IStructureSubmitter"/>
    /// </summary>
    public class StructureSubmitter : IStructureSubmitter
    {
        /// <summary>
        /// The factories
        /// </summary>
        private readonly IList<IStructureSubmitFactory> _factories;

        /// <summary>
        /// Initializes a new instance of the <see cref="StructureSubmitter" /> class.
        /// </summary>
        /// <param name="factories">The factories.</param>
        /// <exception cref="ArgumentNullException">factories is <see langword="null"/></exception>
        public StructureSubmitter(params IStructureSubmitFactory[] factories)
        {
            if (factories == null)
            {
                throw new ArgumentNullException(nameof(factories));
            }

            _factories = factories.ToArray();
        }

        /// <summary>
        /// Submits the <paramref name="sdmxObjects" /> to the store identified by <paramref name="storeId" />. The action is determined by the <paramref name="sdmxObjects" /> <see cref="ISdmxObjects.Action" />
        /// </summary>
        /// <param name="storeId">The store identifier.</param>
        /// <param name="sdmxObjects">The SDMX objects.</param>
        /// <returns>
        /// The list of <see cref="IResponseWithStatusObject" />
        /// </returns>
        /// <exception cref="ArgumentNullException"><paramref name="sdmxObjects"/> is <c>null</c>.</exception>
        /// <exception cref="SdmxNotImplementedException">No factory found that  supports the specified store id</exception>
        public IList<IResponseWithStatusObject> SubmitStructures(string storeId, ISdmxObjects sdmxObjects)
        {
            if (sdmxObjects == null)
                throw new ArgumentNullException(nameof(sdmxObjects));

            var engine = _factories.Select(f => f.GetEngine(storeId)).FirstOrDefault(e => e != null);
            if (engine == null)
            {
                throw new SdmxNotImplementedException("No factory found that supports the specified store Id");
            }

            return engine.SubmitStructures(sdmxObjects);
        }
    }
}
