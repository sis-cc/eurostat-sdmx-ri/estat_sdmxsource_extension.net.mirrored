// -----------------------------------------------------------------------
// <copyright file="CodelistFilter.cs" company="EUROSTAT">
//   Date Created : 2018-05-29
//   Copyright (c) 2009, 2018 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sdmxsource.Extension.Util
{
    using System;
    using System.CodeDom;
    using System.Collections.Generic;
    using System.Linq;

    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Codelist;

    /// <summary>
    /// Code Filter
    /// </summary>
    public class CodelistFilter
    {
        /// <summary>
        /// Returns codes in a list and their parents
        /// </summary>
        /// <param name="codelist"></param>
        /// <param name="ids">The id's of codes that there is data</param>
        /// <returns></returns>
        public IEnumerable<ICodeMutableObject> For(ICodelistMutableObject codelist, ISet<string> ids)
        {
            // include all selected codes in the output
            var outputCodes = new List<ICodeMutableObject>(codelist.Items.Where(c => ids.Contains(c.Id)));

            var codeMutableObjects = codelist.Items.ToDictionary(item => item.Id, item => item, StringComparer.Ordinal);

            // include all codes that data exists
            var stack = new Stack<ICodeMutableObject>(outputCodes);

            // perform tree search of parent codes, and parent of parents etc of the available codes in order to include those also in the output
            // Note avoid recursion is libraries
            while (stack.Count > 0)
            {
                var code = stack.Pop();
                // if parent code is set and not in the list of available codes
                if (!string.IsNullOrEmpty(code.ParentCode) && !ids.Contains(code.ParentCode))
                {
                    // and parent was not visited (and exist in the codelist)
                    if (codeMutableObjects.TryGetValue(code.ParentCode, out ICodeMutableObject parentCode))
                    {
                        stack.Push(parentCode); // include to the stack to visit
                        codeMutableObjects.Remove(parentCode.Id); //ensure we visit parent code only once
                        outputCodes.Add(parentCode); // include in the output

                    }
                }
            }

            //return codes.Distinct();
            return outputCodes;
        }
    }
}