// -----------------------------------------------------------------------
// <copyright file="TextFormatValidator.cs" company="EUROSTAT">
//   Date Created : 2019-10-14
//   Copyright (c) 2009, 2019 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Estat.Sdmxsource.Extension.Constant;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Exception;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
using Org.Sdmxsource.Sdmx.Util.Date;
using Org.Sdmxsource.Sdmx.Util.Objects;

namespace Estat.Sdmxsource.Extension.Util
{
    /// <summary>
    /// validator for text format
    /// </summary>
    public class TextFormatValidator
    {
        public static List<string> ValidateTextFormat(ITextFormat textFormat, string value, SdmxSchema structureVersion,
            DataVersion dataVersionCompatibility, List<string> errorMessages)
        {
            if (textFormat.TextType != null)
            {
                errorMessages = ValidateTextType(textFormat.TextType, value, structureVersion, dataVersionCompatibility,
                    errorMessages);
            }

            if (textFormat.Decimals != null)
            {
                try
                {
                    var valAsDouble = double.Parse(value);
                    int num = GetNumberOfDecimalPlace(valAsDouble);
                    if (num > textFormat.Decimals.Value)
                    {
                        errorMessages = AddErrorMessage(errorMessages,
                            " exceeds the number of allowed decimal places of " + textFormat.Decimals);
                    }
                }
                catch (FormatException e)
                {
                    errorMessages = AddErrorMessage(errorMessages, " is expected to be numerical");
                }
            }

            if (textFormat.MinLength != null)
            {
                if ((value != null) && (value.Length < textFormat.MinLength.Value))
                {
                    errorMessages = AddErrorMessage(errorMessages,
                        " is shorter then the minimum allowed length of '" + textFormat.MinLength + "'");
                }
            }

            if (textFormat.MaxLength != null)
            {
                if ((value != null) && (value.Length > textFormat.MaxLength.Value))
                {
                    errorMessages = AddErrorMessage(errorMessages,
                        " is greater then the maximum allowed length of '" + textFormat.MaxLength + "'");
                }
            }

            if (textFormat.StartValue != null)
            {
                var valueAsDecimal = ParseBigDecimal(value);
                if (valueAsDecimal.CompareTo(textFormat.StartValue) < 0)
                {
                    errorMessages = AddErrorMessage(errorMessages,
                        " is less then the specified start value of " + textFormat.StartValue);
                }

                if (textFormat.Interval != null)
                {
                    if (decimal.Remainder(decimal.Subtract(valueAsDecimal, textFormat.StartValue.Value),
                            textFormat.Interval.Value).CompareTo(0) != 0)
                    {
                        errorMessages = AddErrorMessage(errorMessages,
                            " do not fit the allowed intervals starting from " + textFormat.StartValue +
                            " and increasing by " + textFormat.Interval);
                    }
                }
            }

            if (textFormat.EndValue != null)
            {
                if (ParseBigDecimal(value).CompareTo(textFormat.EndValue) > 0)
                {
                    errorMessages = AddErrorMessage(errorMessages,
                        " is greater then the specified end value of " + textFormat.EndValue);
                }
            }

            if (textFormat.MinValue != null)
            {
                if (ParseBigDecimal(value).CompareTo(textFormat.MinValue) < 0)
                {
                    errorMessages = AddErrorMessage(errorMessages,
                        " is less then the specified min value of " + textFormat.MinValue);
                }
            }

            if (textFormat.MaxValue != null)
            {
                if (ParseBigDecimal(value).CompareTo(textFormat.MaxValue) > 0)
                {
                    errorMessages = AddErrorMessage(errorMessages,
                        " is greater then the specified end value of " + textFormat.MaxValue);
                }
            }

            if (textFormat.StartTime != null)
            {
                try
                {
                    var dte = DateUtil.FormatDate(value, true);
                    if (dte < textFormat.StartTime.Date)
                    {
                        errorMessages = AddErrorMessage(errorMessages,
                            " is before the allowed start time of " + textFormat.StartTime.DateInSdmxFormat);
                    }
                }
                catch (FormatException e)
                {
                    errorMessages = AddErrorMessage(errorMessages, " is not of expected type 'Date'");
                }
            }

            if (textFormat.EndTime != null)
            {
                try
                {
                    var dte = DateUtil.FormatDate(value, true);
                    if (dte > textFormat.EndTime.Date)
                    {
                        errorMessages = AddErrorMessage(errorMessages,
                            " is after the allowed start time of " + textFormat.StartTime.DateInSdmxFormat);
                    }
                }
                catch (FormatException e)
                {
                    errorMessages = AddErrorMessage(errorMessages, " is not of expected type 'Date'");
                }
            }

            if (textFormat.Pattern != null)
            {
                var textFormatPattern = Regex.Match(value, textFormat.Pattern, RegexOptions.IgnoreCase);
                if (!textFormatPattern.Success)
                {
                    errorMessages = AddErrorMessage(errorMessages,
                        " does not respect the expected pattern " + textFormat.Pattern);
                }
            }

            return errorMessages;
        }

        /**
         * Adds an error message to the list, if the list is null then creates a new list, adds the error, and returns it.
         * @param errorList
         * @return
         */
        private static List<string> AddErrorMessage(List<string> errorList, string error)
        {
            if (errorList == null)
            {
                errorList = new List<string>();
            }

            errorList.Add(error);
            return errorList;
        }

        /**
         * BasicTimePeriod is a union of GregorianTimePeriod and DateTime
         * @param value
         * @param dataVersionCompatibility
         * @param structureVersion
         * @param timeFormat
         * @param errorMessages
         * @return
         */
        private static List<string> BasicTimePeriodCheck(string value, DataVersion dataVersionCompatibility,
            SdmxSchema structureVersion, TimeFormat timeFormat, List<string> errorMessages, string additionalFormat)
        {
            //We need to add the string of the accepted formats for the error messages.
            additionalFormat += " ,'Date Time'";
            if (timeFormat != TimeFormat.DateTime)
                GregorianTimePeriodCheck(value, dataVersionCompatibility, structureVersion, timeFormat, errorMessages,
                    additionalFormat);

            return errorMessages;
        }


        private static int GetNumberOfDecimalPlace(double value)
        {
            //For whole numbers like 0
            if (Math.Round(value) == value) return 0;
            string s = value.ToString();
            int index = s.IndexOf('.');
            if (index < 0)
            {
                return 0;
            }

            return s.Length - 1 - index;
        }

        /**
         * GregorianTimePeriod is a union of GregorianYear, GregorianMonth, and GregorianDay.
         * @param value
         * @param dataVersionCompatibility
         * @param structureVersion
         * @param timeFormat
         * @param errorMessages
         * @return
         */
        private static List<string> GregorianTimePeriodCheck(string value, DataVersion dataVersionCompatibility,
            SdmxSchema structureVersion, TimeFormat timeFormat, List<string> errorMessages, string additionalFormat)
        {
            switch (timeFormat.EnumType)
            {
                case TimeFormatEnumType.Year:
                case TimeFormatEnumType.Month:
                case TimeFormatEnumType.Date:
                    break;
                default:
                    errorMessages = AddErrorMessage(errorMessages,
                        " is not of expected type 'Year', 'Month', 'Date'" + additionalFormat);
                    break;
            }

            return errorMessages;
        }

        /**
         * Turn string into BigDecimal, to try and process it against other rules
         * @param value
         * @return
         */
        private static decimal ParseBigDecimal(string value)
        {
            try
            {
                return decimal.Parse(value);
            }
            catch (FormatException e)
            {
                throw new SdmxSemmanticException("Value '" + value + "' expected to be numerical");
            }
        }

        /**
         * Method to determine if a date which represents date time period is to be reported or not.
         * <ul> 
         * <li> For SDMX 2.0 formats (CompactData, GenericData 2.0), CrossSectionalData, UtilityData) 
         * 		we report always dates with the format 'yyyy-Dddd'.</li>
         * <li> For SDMX 2.1 formats (StructureSpecificData, GenericData 2.1) 
         * 		we report dates with the format 'yyyy-Dddd',
         * 		when structure has version 2.0.</li>
         * <li> For SDMX_CSV format we report dates with the format 'yyyy-Dddd',
         * 		when structure has version 2.0.</li>
         * <li> For Single level CSV, Multi-level CSV, FLR, Excel formats
         * 		we report dates with the format 'yyyy-Dddd',
         * 		when structure has version 2.0.</li>
         * </ul>
         * @see <a href="https://webgate.ec.europa.eu/CITnet/jira/browse/SDMXCONV-792">SDMXCONV-792</a>
         * @param value
         * @param dataVersionCompatibility
         * @param structureVersion
         * @param errorMessages
         * @return errors
         */
        private static List<string> ReportDateCheck(string value, DataVersion dataVersionCompatibility,
            SdmxSchema structureVersion, List<string> errorMessages)
        {
            //In case of SDMX 2.0 formats we always report the time_period with format 'yyyy-Dddd'
            if (dataVersionCompatibility == DataVersion.SDMX_20_COMPATIBLE)
            {
                errorMessages = AddErrorMessage(errorMessages, " is not acceptable type 'yyyy-Dddd'");
            }

            //In case of SDMX 2.1 and SDMX_CSV
            //we check the version of structure to decide if we will report ''yyyy-D1''
            if (dataVersionCompatibility == DataVersion.SDMX_21_COMPATIBLE &&
                structureVersion != SdmxSchemaEnumType.VersionTwoPointOne)
            {
                errorMessages = AddErrorMessage(errorMessages, " is not of expected type 'yyyy-Dddd'");
            }

            //In case of CSV, FLR, EXCEL
            //we check the version of structure to decide if we will report ''yyyy-D1''
            if (dataVersionCompatibility == DataVersion.NON_SDMX &&
                structureVersion != SdmxSchemaEnumType.VersionTwoPointOne)
            {
                errorMessages = AddErrorMessage(errorMessages, " is not of expected type 'yyyy-Dddd'");
            }

            return errorMessages;
        }

        /**
         * Method to determine if a date which represents Half of Year time period is to be reported or not.
         * <ul> 
         * <li> For SDMX 2.0 formats (CompactData, GenericData 2.0), CrossSectionalData, UtilityData) 
         * 		we report always dates with the format 'yyyy-Ss' and we don't report 'yyyy-Bs'.</li>
         * <li> For SDMX 2.1 formats (StructureSpecificData, GenericData 2.1), SDMX_CSV
         * 		we report always dates with the format 'yyyy-Bs' and we don't report 'yyyy-Ss'.</li>
         * <li> For Single level CSV, Multi-level CSV, FLR, Excel formats
         * 		we report dates with the format 'yyyy-Ss' and we don't report 'yyyy-Bs' when structure has version 2.0.
         * 		And we report 'yyyy-Bs' and we don't report 'yyyy-Ss when structure has version 2.1.'</li>
         * </ul>
         * @see <a href="https://webgate.ec.europa.eu/CITnet/jira/browse/SDMXCONV-792">SDMXCONV-792</a>
         * @param value - the input value for time period
         * @param dataVersionCompatibility - the version of the input data
         * @param structureVersion - the version of the structure given
         * @param errorMessages - the list of errors
         * @return errors
         */
        private static List<string> ReportHalfOfYearCheck(string value, DataVersion dataVersionCompatibility,
            SdmxSchema structureVersion, List<string> errorMessages)
        {
            if (value.ElementAt(5) == 'S')
            {
                //In case of SDMX 2.0 formats we always report the time_period with format yyyy-Ss 
                if (dataVersionCompatibility == DataVersion.SDMX_20_COMPATIBLE)
                {
                    errorMessages = AddErrorMessage(errorMessages, " format 'yyyy-Ss' is not acceptable");
                }

                //In case of CSV, FLR, EXCEL
                //we check the version of structure to decide if we will report yyyy-Ss 
                if (dataVersionCompatibility == DataVersion.NON_SDMX &&
                    structureVersion != SdmxSchemaEnumType.VersionTwoPointOne)
                {
                    errorMessages = AddErrorMessage(errorMessages, " format 'yyyy-Ss' is not acceptable");
                }
            }

            if ((value.ElementAt(5) == 'B'))
            {
                //In case of SDMX 2.0 formats we always report the time_period with format yyyy-Bs 
                if (dataVersionCompatibility == DataVersion.SDMX_21_COMPATIBLE)
                {
                    errorMessages = AddErrorMessage(errorMessages, " format 'yyyy-Bs' is not acceptable");
                }

                //In case of CSV, FLR, EXCEL
                //we check the version of structure to decide if we will report yyyy-Bs 
                if (dataVersionCompatibility == DataVersion.NON_SDMX &&
                    structureVersion == SdmxSchemaEnumType.VersionTwoPointOne)
                {
                    errorMessages = AddErrorMessage(errorMessages, " format 'yyyy-Bs' is not acceptable");
                }
            }

            return errorMessages;
        }

        /**
         * Method to determine if a date which represents monthly time period is to be reported or not.
         * <ul> 
         * <li> For SDMX 2.0 formats (CompactData, GenericData 2.0), CrossSectionalData, UtilityData) 
         * 		we report always dates with the format 'yyyy-Mmm'.</li>
         * <li> For SDMX 2.1 formats (StructureSpecificData, GenericData 2.1) , SDMX_CSV
         * 		we report dates with the format 'yyyy-Mmm',
         * 		when structure has version 2.0.</li>
         * <li> For Single level CSV, Multi-level CSV, FLR, Excel formats
         * 		we report dates with the format 'yyyy-Mmm',
         * 		when structure has version 2.0.</li>
         * </ul>
         * @see <a href="https://webgate.ec.europa.eu/CITnet/jira/browse/SDMXCONV-792">SDMXCONV-792</a>
         * @param value
         * @param dataVersionCompatibility
         * @param structureVersion
         * @param errorMessages
         * @return errors
         */
        private static List<string> ReportMonthCheck(string value, DataVersion dataVersionCompatibility,
            SdmxSchemaEnumType structureVersion, List<string> errorMessages)
        {
            //In case of SDMX 2.0 formats we always report the time_period with format 'yyyy-Mmm'
            if (dataVersionCompatibility == DataVersion.SDMX_20_COMPATIBLE)
            {
                errorMessages = AddErrorMessage(errorMessages, " is not of expected type 'yyyy-mm'");
            }

            //In case of SDMX 2.1 and SDMX_CSV
            //we check the version of structure to decide if we will report ''yyyy-Mmm''
            if (dataVersionCompatibility == DataVersion.SDMX_21_COMPATIBLE &&
                structureVersion != SdmxSchemaEnumType.VersionTwoPointOne)
            {
                errorMessages = AddErrorMessage(errorMessages, " is not of expected type 'yyyy-mm'");
            }

            //In case of CSV, FLR, EXCEL
            //we check the version of structure to decide if we will report ''yyyy-Mmm''
            if (dataVersionCompatibility == DataVersion.NON_SDMX &&
                structureVersion != SdmxSchemaEnumType.VersionTwoPointOne)
            {
                errorMessages = AddErrorMessage(errorMessages, " is not of expected type 'yyyy-mm'");
            }

            return errorMessages;
        }

        /**
         * Method to determine if a date which represents Time Range time period is to be reported or not.
         * <ul> 
         * <li> For SDMX 2.0 formats (CompactData, GenericData 2.0, CrossSectionalData, UtilityData) 
         * 		we report always dates with the format 'yyyy-mm-ddThh:mm:ss/duration' or 'yyyy-mm-dd/duration'.</li>
         * <li> For SDMX 2.1 formats (StructureSpecificData, GenericData 2.1), SDMX_CSV
         * 		we report dates with the format 'yyyy-mm-ddThh:mm:ss/duration' or 'yyyy-mm-dd/duration',
         * 		when structure has version 2.0.</li>
         * <li> For Single level CSV, Multi-level CSV, FLR, Excel formats
         * 		we report dates with the format 'yyyy-mm-ddThh:mm:ss/duration' or 'yyyy-mm-dd/duration',
         * 		when structure has version 2.0.</li>
         * </ul>
         * @see <a href="https://webgate.ec.europa.eu/CITnet/jira/browse/SDMXCONV-792">SDMXCONV-792</a>
         * @param value
         * @param dataVersionCompatibility
         * @param structureVersion
         * @param errorMessages
         * @return errors
         */
        private static List<string> ReportTimeRangeCheck(string value, DataVersion dataVersionCompatibility,
            SdmxSchema structureVersion, List<string> errorMessages)
        {
            //In case of SDMX 2.0 formats we always report the time_period with format yyyy-mm-ddThh:mm:ss/duration or yyyy-mm-dd/duration
            if (dataVersionCompatibility == DataVersion.SDMX_20_COMPATIBLE)
            {
                errorMessages = AddErrorMessage(errorMessages,
                    " is not acceptable type 'yyyy-mm-ddThh:mm:ss' or 'yyyy-mm-dd'");
            }

            //In case of SDMX 2.1 and SDMX_CSV
            //we check the version of structure to decide if we will report yyyy-mm-ddThh:mm:ss/duration or yyyy-mm-dd/duration
            if (dataVersionCompatibility == DataVersion.SDMX_21_COMPATIBLE &&
                structureVersion != SdmxSchemaEnumType.VersionTwoPointOne)
            {
                errorMessages = AddErrorMessage(errorMessages,
                    " is not acceptable type 'yyyy-mm-ddThh:mm:ss' or 'yyyy-mm-dd'");
            }

            //In case of CSV, FLR, EXCEL
            //we check the version of structure to decide if we will report yyyy-mm-ddThh:mm:ss/duration or yyyy-mm-dd/duration
            if (dataVersionCompatibility == DataVersion.NON_SDMX &&
                structureVersion != SdmxSchemaEnumType.VersionTwoPointOne)
            {
                errorMessages = AddErrorMessage(errorMessages,
                    " is not acceptable type 'yyyy-mm-ddThh:mm:ss' or 'yyyy-mm-dd'");
            }

            return errorMessages;
        }

        /**
         * Method to determine if a date which represents Weekly time period is to be reported or not.
         * <ul> 
         * <li> For SDMX 2.0 formats (CompactData, GenericData 2.0), CrossSectionalData, UtilityData) 
         * 		we report always dates with the format 'yyyy-W01-09'.</li>
         * <li> For SDMX 2.1 formats (StructureSpecificData, GenericData 2.1) and SDMX_CSV
         * 		we report dates with the format 'yyyy-W1-9'</li>
         * <li> For Single level CSV, Multi-level CSV, FLR, Excel formats
         * 		we report dates with the format 'yyyy-W01-09',
         * 		when structure has version 2.0. And we report 'yyyy-W1-9' when structure has version 2.1.
         * </li>
         * </ul>
         * @see <a href="https://webgate.ec.europa.eu/CITnet/jira/browse/SDMXCONV-792">SDMXCONV-792</a>
         * @param value
         * @param dataVersionCompatibility
         * @param structureVersion
         * @param errorMessages
         * @return errors
         */
        private static List<string> ReportWeekCheck(string value, DataVersion dataVersionCompatibility,
            SdmxSchema structureVersion, List<string> errorMessages)
        {
            // We expect the second one to be the Www or Ww
            if (value.Length == 7)
            {
                //is of the format yyyy-W1-9
                if (dataVersionCompatibility == DataVersion.SDMX_21_COMPATIBLE)
                {
                    errorMessages = AddErrorMessage(errorMessages, " is not acceptable type 'yyyy-W01-09'");
                }

                if (dataVersionCompatibility == DataVersion.NON_SDMX &&
                    structureVersion == SdmxSchemaEnumType.VersionTwoPointOne)
                {
                    errorMessages = AddErrorMessage(errorMessages, " is not acceptable type 'yyyy-W01-09'");
                }
            }

            if (value.Length > 7)
            {
                //is of the format yyyy-W01-09
                if (dataVersionCompatibility == DataVersion.SDMX_20_COMPATIBLE)
                {
                    errorMessages = AddErrorMessage(errorMessages, " is not acceptable type 'yyyy-W1-9'");
                }

                if (dataVersionCompatibility == DataVersion.NON_SDMX &&
                    structureVersion != SdmxSchemaEnumType.VersionTwoPointOne)
                {
                    errorMessages = AddErrorMessage(errorMessages, " is not acceptable type 'yyyy-W1-9'");
                }
            }

            return errorMessages;
        }

        /**
         * Method to determine if a date which represents yearly time period is to be reported or not.
         * <ul> 
         * <li> For SDMX 2.0 formats (CompactData, GenericData 2.0), CrossSectionalData, UtilityData) 
         * 		we report always dates with the format 'yyyy-A1'.</li>
         * <li> For SDMX 2.1 formats (StructureSpecificData, GenericData 2.1) 
         * 		we report dates with the format 'yyyy-A1',
         * 		when structure has version 2.0.</li>
         * <li> For SDMX_CSV format we report dates with the format 'yyyy-A1',
         * 		when structure has version 2.0.</li>
         * <li> For Single level CSV, Multi-level CSV, FLR, Excel formats
         * 		we report dates with the format 'yyyy-A1',
         * 		when structure has version 2.0.</li>
         * </ul>
         * @see <a href="https://webgate.ec.europa.eu/CITnet/jira/browse/SDMXCONV-792">SDMXCONV-792</a>
         * @param value
         * @param dataVersionCompatibility
         * @param structureVersion
         * @param errorMessages
         * @return errors
         */
        private static List<string> ReportYearCheck(string value, DataVersion dataVersionCompatibility,
            SdmxSchema structureVersion, List<string> errorMessages)
        {
            //In case of SDMX 2.0 formats we always report the time_period with format 'yyyy-A1'
            if (dataVersionCompatibility == DataVersion.SDMX_20_COMPATIBLE)
            {
                errorMessages = AddErrorMessage(errorMessages, " is not of expected type 'yyyy'");
            }

            //In case of SDMX 2.1 and SDMX_CSV
            //we check the version of structure to decide if we will report ''yyyy-A1''
            if (dataVersionCompatibility == DataVersion.SDMX_21_COMPATIBLE &&
                structureVersion != SdmxSchemaEnumType.VersionTwoPointOne)
            {
                errorMessages = AddErrorMessage(errorMessages, " is not of expected type 'yyyy'");
            }

            //In case of CSV, FLR, EXCEL
            //we check the version of structure to decide if we will report ''yyyy-A1''
            if (dataVersionCompatibility == DataVersion.NON_SDMX &&
                structureVersion != SdmxSchemaEnumType.VersionTwoPointOne)
            {
                errorMessages = AddErrorMessage(errorMessages, " is not of expected type 'yyyy'");
            }

            return errorMessages;
        }

        /**
         * StandardTimePeriod is a union of BasicTimePeriod and Time Range
         * @param value
         * @param dataVersionCompatibility
         * @param structureVersion
         * @param timeFormat
         * @param errorMessages
         * @return
         */
        private static List<string> StandardTimePeriodCheck(string value, DataVersion dataVersionCompatibility,
            SdmxSchema structureVersion, TimeFormat timeFormat, List<string> errorMessages, string additionalFormat)
        {
            //We need to add the string of the accepted formats for the error messages.
            additionalFormat += " ,'Time Range'";
            if (timeFormat != TimeFormat.TimeRange)
            {
                BasicTimePeriodCheck(value, dataVersionCompatibility, structureVersion, timeFormat, errorMessages,
                    additionalFormat);
            }
            else
            {
                errorMessages = ReportTimeRangeCheck(value, dataVersionCompatibility, structureVersion, errorMessages);
            }

            return errorMessages;
        }

        private static List<string> ValidateTextType(TextEnumType textType, string value, SdmxSchema structureVersion,
            DataVersion dataVersionCompatibility, List<string> errorMessages)
        {
            switch (textType)
            {
                case TextEnumType.Double:
                    try
                    {
                        // we check for whitespaces at the beginning or at the end of the value
                        // (because Double.parseDouble trims these whitespaces) and it has been
                        // reported as a bug (see SDMXCONV-523)
                        if (value.StartsWith(" ") || value.EndsWith(" "))
                        {
                            throw new FormatException(value + " with whitespaces");
                        }

                        double.Parse(value);
                    }
                    catch (FormatException e)
                    {
                        errorMessages = AddErrorMessage(errorMessages, " is not of expected type 'Double'");
                    }

                    break;
                case TextEnumType.BigInteger:
                    try
                    {
                        int.Parse(value);
                    }
                    catch (FormatException e)
                    {
                        errorMessages = AddErrorMessage(errorMessages, " is not of expected type 'Big Integer'");
                    }

                    break;
                case TextEnumType.Integer:
                    try
                    {
                        int.Parse(value);
                    }
                    catch (FormatException e)
                    {
                        errorMessages = AddErrorMessage(errorMessages, " is not of expected type 'Integer'");
                    }

                    break;
                case TextEnumType.Long:
                    try
                    {
                        long.Parse(value);
                    }
                    catch (FormatException e)
                    {
                        errorMessages = AddErrorMessage(errorMessages, " is not of expected type 'Long'");
                    }

                    break;
                case TextEnumType.Short:
                    try
                    {
                        short.Parse(value);
                    }
                    catch (FormatException e)
                    {
                        errorMessages = AddErrorMessage(errorMessages, " is not of expected type 'Short'");
                    }

                    break;
                case TextEnumType.Decimal:
                    try
                    {
                        decimal.Parse(value);
                    }
                    catch (FormatException e)
                    {
                        errorMessages = AddErrorMessage(errorMessages, " is not of expected type 'Decimal'");
                    }

                    break;
                case TextEnumType.Float:
                    try
                    {
                        float.Parse(value);
                    }
                    catch (FormatException e)
                    {
                        errorMessages = AddErrorMessage(errorMessages, " is not of expected type 'Float'");
                    }

                    break;
                case TextEnumType.Boolean:
                    if (!value.ToLower().Equals("true") && !value.ToLower().Equals("false"))
                    {
                        errorMessages = AddErrorMessage(errorMessages, " is not of expected type 'Boolean'");
                    }

                    break;
                case TextEnumType.Date:
                    try
                    {
                        DateUtil.GetTimeFormatOfDate(value);
                    }
                    catch (SdmxSemmanticException e)
                    {
                        errorMessages = AddErrorMessage(errorMessages, " is not of expected type 'Date'");
                    }

                    break;
                case TextEnumType.DateTime:
                    try
                    {
                        DateUtil.GetTimeFormatOfDate(value);
                    }
                    catch (SdmxSemmanticException e)
                    {
                        errorMessages = AddErrorMessage(errorMessages, " is not of expected type 'Date'");
                    }

                    break;
                case TextEnumType.Year:
                    try
                    {
                        if (value.Length != 4)
                        {
                            errorMessages = AddErrorMessage(errorMessages,
                                " is not of expected type 'Year', expected year length of 4");
                        }

                        int.Parse(value);
                    }
                    catch (System.Exception e)
                    {
                        errorMessages = AddErrorMessage(errorMessages, " is not of expected type 'Year'");
                    }

                    break;
                case TextEnumType.Time:
                    try
                    {
                        DateUtil.GetTimeFormatOfDate(value);
                    }
                    catch (System.Exception e)
                    {
                        errorMessages = AddErrorMessage(errorMessages, " is not of expected type 'Date'");
                    }

                    break;
                case TextEnumType.GregorianDay:
                    try
                    {
                        if (DateUtil.GetTimeFormatOfDate(value) != TimeFormat.Date)
                        {
                            errorMessages = AddErrorMessage(errorMessages, " is not of expected type 'Date'");
                        }
                    }
                    catch (System.Exception e)
                    {
                        errorMessages = AddErrorMessage(errorMessages, " is not of expected type 'Date'");
                    }

                    break;
                case TextEnumType.GregorianYear:
                    try
                    {
                        if (DateUtil.GetTimeFormatOfDate(value) != TimeFormat.Year)
                        {
                            errorMessages = AddErrorMessage(errorMessages, " is not of expected type 'Year'");
                        }
                    }
                    catch (System.Exception e)
                    {
                        errorMessages = AddErrorMessage(errorMessages, " is not of expected type 'Date'");
                    }

                    break;
                case TextEnumType.GregorianYearMonth:
                    try
                    {
                        if (DateUtil.GetTimeFormatOfDate(value) != TimeFormat.Month)
                        {
                            errorMessages = AddErrorMessage(errorMessages, " is not of expected type 'Year-Month'");
                        }
                    }
                    catch (System.Exception e)
                    {
                        errorMessages = AddErrorMessage(errorMessages, " is not of expected type 'Date'");
                    }

                    break;
                case TextEnumType.GregorianTimePeriod:
                    try
                    {
                        TimeFormat timeFormat = DateUtil.GetTimeFormatOfDate(value);
                        errorMessages = GregorianTimePeriodCheck(value, dataVersionCompatibility, structureVersion,
                            timeFormat, errorMessages, "");
                    }
                    catch (System.Exception e)
                    {
                        errorMessages = AddErrorMessage(errorMessages, " is not of expected type 'Date'");
                    }

                    break;
                case TextEnumType.BasicTimePeriod:
                    try
                    {
                        var timeFormat = DateUtil.GetTimeFormatOfDate(value);
                        errorMessages = BasicTimePeriodCheck(value, dataVersionCompatibility, structureVersion,
                            timeFormat, errorMessages, "");
                    }
                    catch (System.Exception e)
                    {
                        errorMessages = AddErrorMessage(errorMessages, " is not of expected type 'Date'");
                    }

                    break;
                case TextEnumType.StandardTimePeriod:
                    try
                    {
                        var timeFormat = DateUtil.GetTimeFormatOfDate(value);
                        errorMessages = StandardTimePeriodCheck(value, dataVersionCompatibility, structureVersion,
                            timeFormat, errorMessages, "");
                    }
                    catch (System.Exception e)
                    {
                        errorMessages = AddErrorMessage(errorMessages, " is not of expected type 'Date'");
                    }

                    break;
                case TextEnumType.ReportingMonth:
                    try
                    {
                        var timeFormat = DateUtil.GetTimeFormatOfDate(value);
                        if (timeFormat != TimeFormat.ReportingMonth)
                        {
                            errorMessages = AddErrorMessage(errorMessages, " is not of expected type 'Month'");
                        }
                        else
                        {
                            errorMessages = ReportMonthCheck(value, dataVersionCompatibility, structureVersion,
                                errorMessages);
                        }
                    }
                    catch (System.Exception e)
                    {
                        errorMessages = AddErrorMessage(errorMessages, " is not of expected type 'Date'");
                    }

                    break;
                case TextEnumType.ReportingQuarter:
                    try
                    {
                        var timeFormat = DateUtil.GetTimeFormatOfDate(value);
                        if (timeFormat != TimeFormat.QuarterOfYear)
                        {
                            errorMessages = AddErrorMessage(errorMessages, " is not of expected type Quarter of Year");
                        }
                    }
                    catch (System.Exception e)
                    {
                        errorMessages = AddErrorMessage(errorMessages, " is not of expected type 'Date'");
                    }

                    break;
                case TextEnumType.ReportingSemester:
                    try
                    {
                        var timeFormat = DateUtil.GetTimeFormatOfDate(value);
                        if (timeFormat != TimeFormat.HalfOfYear)
                        {
                            errorMessages = AddErrorMessage(errorMessages, " is not of expected type Half of Year");
                        }
                        else
                        {
                            errorMessages = ReportHalfOfYearCheck(value, dataVersionCompatibility, structureVersion,
                                errorMessages);
                        }
                    }
                    catch (System.Exception e)
                    {
                        errorMessages = AddErrorMessage(errorMessages, " is not of expected type 'Date'");
                    }

                    break;
                case TextEnumType.ReportingTimePeriod:
                    try
                    {
                        var timeFormat = DateUtil.GetTimeFormatOfDate(value);
                        switch (timeFormat.EnumType)
                        {
                            case TimeFormatEnumType.ReportingMonth:
                                errorMessages = ReportMonthCheck(value, dataVersionCompatibility, structureVersion,
                                    errorMessages);
                                break;
                            case TimeFormatEnumType.ReportingYear:
                                errorMessages = ReportYearCheck(value, dataVersionCompatibility, structureVersion,
                                    errorMessages);
                                break;
                            case TimeFormatEnumType.ReportingDay:
                                errorMessages = ReportDateCheck(value, dataVersionCompatibility, structureVersion,
                                    errorMessages);
                                break;
                            case TimeFormatEnumType.HalfOfYear:
                                errorMessages = ReportHalfOfYearCheck(value, dataVersionCompatibility, structureVersion,
                                    errorMessages);
                                break;
                            case TimeFormatEnumType.ThirdOfYear:
                            case TimeFormatEnumType.QuarterOfYear:
                                break;
                            case TimeFormatEnumType.Week:
                                errorMessages = ReportWeekCheck(value, dataVersionCompatibility, structureVersion,
                                    errorMessages);
                                break;
                            default:
                                errorMessages = AddErrorMessage(errorMessages, " is not of expected type 'Date'");
                                break;
                        }
                    }
                    catch (System.Exception e)
                    {
                        errorMessages = AddErrorMessage(errorMessages, " is not of expected type 'Date'");
                    }

                    break;
                case TextEnumType.ReportingTrimester:
                    try
                    {
                        var timeFormat = DateUtil.GetTimeFormatOfDate(value);
                        if (timeFormat != TimeFormat.QuarterOfYear)
                        {
                            errorMessages = AddErrorMessage(errorMessages, " is not of expected type Quarter of Year");
                        }
                    }
                    catch (System.Exception e)
                    {
                        errorMessages = AddErrorMessage(errorMessages, " is not of expected type 'Date'");
                    }

                    break;
                case TextEnumType.ReportingWeek:
                    try
                    {
                        var timeFormat = DateUtil.GetTimeFormatOfDate(value);
                        if (timeFormat != TimeFormat.Week)
                        {
                            errorMessages = AddErrorMessage(errorMessages, " is not of expected type Week");
                        }
                        else
                        {
                            errorMessages = ReportWeekCheck(value, dataVersionCompatibility, structureVersion,
                                errorMessages);
                        }
                    }
                    catch (System.Exception e)
                    {
                        errorMessages = AddErrorMessage(errorMessages, " is not of expected type 'Date'");
                    }

                    break;
                case TextEnumType.ReportingYear:
                    try
                    {
                        var timeFormat = DateUtil.GetTimeFormatOfDate(value);
                        if (timeFormat != TimeFormat.ReportingYear)
                        {
                            errorMessages = AddErrorMessage(errorMessages, " is not of expected type Year");
                        }
                        else
                        {
                            errorMessages = ReportYearCheck(value, dataVersionCompatibility, structureVersion,
                                errorMessages);
                        }
                    }
                    catch (System.Exception e)
                    {
                        errorMessages = AddErrorMessage(errorMessages, " is not of expected type 'Date'");
                    }

                    break;
                case TextEnumType.TimesRange:
                    try
                    {
                        var timeFormat = DateUtil.GetTimeFormatOfDate(value);
                        if (timeFormat != TimeFormat.TimeRange)
                        {
                            errorMessages = AddErrorMessage(errorMessages, " is not of expected type 'Time Range'");
                        }
                        else
                        {
                            errorMessages = ReportTimeRangeCheck(value, dataVersionCompatibility, structureVersion,
                                errorMessages);
                        }
                    }
                    catch (System.Exception e)
                    {
                        errorMessages = AddErrorMessage(errorMessages, " is not of expected type 'Date'");
                    }

                    break;
                // SDMXCONV-792
                case TextEnumType.ObservationalTimePeriod:
                    try
                    {
                        var timeFormat = DateUtil.GetTimeFormatOfDate(value);
                        switch (timeFormat.EnumType)
                        {
                            case TimeFormatEnumType.HalfOfYear:
                                errorMessages = ReportHalfOfYearCheck(value, dataVersionCompatibility, structureVersion,
                                    errorMessages);
                                break;
                            case TimeFormatEnumType.Week:
                                errorMessages = ReportWeekCheck(value, dataVersionCompatibility, structureVersion,
                                    errorMessages);
                                break;
                            case TimeFormatEnumType.ReportingMonth:
                                errorMessages = ReportMonthCheck(value, dataVersionCompatibility, structureVersion,
                                    errorMessages);
                                break;
                            case TimeFormatEnumType.ReportingYear:
                                errorMessages = ReportYearCheck(value, dataVersionCompatibility, structureVersion,
                                    errorMessages);
                                break;
                            case TimeFormatEnumType.ReportingDay:
                                errorMessages = ReportDateCheck(value, dataVersionCompatibility, structureVersion,
                                    errorMessages);
                                break;
                            case TimeFormatEnumType.TimeRange:
                                errorMessages = ReportTimeRangeCheck(value, dataVersionCompatibility, structureVersion,
                                    errorMessages);
                                break;
                        }
                    }
                    catch (System.Exception e)
                    {
                        errorMessages = AddErrorMessage(errorMessages, " is not of expected type 'Date'");
                    }

                    break;
                case TextEnumType.IdentifiableReference:
                    try
                    {
                        var targetStructure = UrnUtil.GetIdentifiableType(value);
                        UrnUtil.ValidateURN(value, targetStructure);
                    }
                    catch (System.Exception e)
                    {
                        errorMessages = AddErrorMessage(errorMessages,
                            " is not of expected type 'Identifiable Reference' a valid URN is expected");
                    }

                    break;
                case TextEnumType.Uri:
                    try
                    {
                        new Uri(value);
                    }
                    catch (UriFormatException e)
                    {
                        errorMessages = AddErrorMessage(errorMessages, "is not of expected type 'URI'");
                    }

                    break;
            }

            return errorMessages;
        }
    }
}