﻿// -----------------------------------------------------------------------
// <copyright file="SdmxContentTypeComparer.cs" company="EUROSTAT">
//   Date Created : 2016-08-01
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sdmxsource.Extension.Util
{
    using System.Collections.Generic;
    using System.Net.Mime;

    /// <summary>
    /// The SDMX content type comparer. It compares two <see cref="ContentType"/> only against the <see cref="ContentType.MediaType"/> and a <c>version</c> parameter value if it exists
    /// </summary>
    public class SdmxContentTypeComparer : IEqualityComparer<ContentType>
    {
        /// <summary>
        /// The SDMX media with version comparer
        /// </summary>
        private static readonly SdmxContentTypeComparer _sdmxMediaWithVersionComparer;

        /// <summary>
        /// Initializes static members of the <see cref="SdmxContentTypeComparer"/> class.
        /// </summary>
        static SdmxContentTypeComparer()
        {
            _sdmxMediaWithVersionComparer = new SdmxContentTypeComparer();
        }

        /// <summary>
        /// Gets the SDMX media with version comparer.
        /// </summary>
        /// <value>
        /// The SDMX media with version comparer.
        /// </value>
        public static IEqualityComparer<ContentType> SdmxMediaWithVersionComparer
        {
            get
            {
                return _sdmxMediaWithVersionComparer;
            }
        }

        /// <summary>
        /// Determines whether the specified objects are equal.
        /// </summary>
        /// <param name="x">The first object of type <see cref="ContentType"/> to compare.</param>
        /// <param name="y">The second object of type <see cref="ContentType"/> to compare.</param>
        /// <returns>
        /// true if the specified objects are equal; otherwise, false.
        /// </returns>
        public bool Equals(ContentType x, ContentType y)
        {
            if (ReferenceEquals(x, y))
            {
                return true;
            }

            if (ReferenceEquals(x, null) || ReferenceEquals(y, null))
            {
                return false;
            }

            return EqualMediaWithVersion(x, y);
        }

        /// <summary>
        /// Returns a hash code for this instance.
        /// </summary>
        /// <param name="obj">The object.</param>
        /// <returns>
        /// A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. 
        /// </returns>
        public int GetHashCode(ContentType obj)
        {
            if (obj != null)
            {
                var hash = obj.MediaType.GetHashCode();
                var version = GetVersion(obj);
                hash = (hash * 397) ^ (version != null ? version.GetHashCode() : 0);
                return hash;
            }

            return 0;
        }

        /// <summary>
        /// Check if the specified parameters have the same content type Media type with the same version parameter value.
        /// </summary>
        /// <param name="defaultContentType">Default type of the content.</param>
        /// <param name="response">The response.</param>
        /// <returns>True if the specified parameters have the same content type Media type with the same version parameter value.</returns>
        private static bool EqualMediaWithVersion(ContentType defaultContentType, ContentType response)
        {
            return response.MediaType.Equals(defaultContentType.MediaType) && HaveSameVersion(defaultContentType, response);
        }

        /// <summary>
        /// Check if the specified <see cref="ContentType"/> have the same version parameter value.
        /// </summary>
        /// <param name="defaultContentType">Default type of the content.</param>
        /// <param name="supportedContentType">Type of the supported content.</param>
        /// <returns>True if the specified <see cref="ContentType"/> have the same version parameter value; otherwise false</returns>
        private static bool HaveSameVersion(ContentType defaultContentType, ContentType supportedContentType)
        {
            var defaultVersion = GetVersion(defaultContentType);
            var supportedVersion = GetVersion(supportedContentType);
            return string.Equals(defaultVersion, supportedVersion);
        }

        /// <summary>
        /// Gets the version.
        /// </summary>
        /// <param name="defaultContentType">Default type of the content.</param>
        /// <returns>The version if it exists; otherwise null</returns>
        private static string GetVersion(ContentType defaultContentType)
        {
            return defaultContentType.Parameters != null ? defaultContentType.Parameters["version"] : null;
        }
    }
}