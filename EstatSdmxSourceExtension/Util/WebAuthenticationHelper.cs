﻿// -----------------------------------------------------------------------
// <copyright file="WebAuthenticationHelper.cs" company="EUROSTAT">
//   Date Created : 2017-11-28
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sdmxsource.Extension.Util
{
    using Estat.Sdmxsource.Extension.Model;
    using System.Net;

    /// <summary>
    /// This class contains a collection of utility methods related to setting up authentication and proxy configuration
    /// </summary>
    static class WebAuthenticationHelper
    {
        /// <summary>
        /// Setup the specified <paramref name="webRequest" /> network authentication and proxy configuration
        /// </summary>
        /// <param name="wsInfo">The ws information.</param>
        /// <param name="webRequest">The <see cref="WebRequest" /></param>
        public static void SetupWebRequestAuth(WsInfo wsInfo, WebRequest webRequest)
        {
            if (wsInfo.EnableHTTPAuthentication)
            {
                webRequest.Credentials = new NetworkCredential(wsInfo.UserName, wsInfo.Password, wsInfo.Domain);
            }

            if (wsInfo.EnableProxy)
            {
                if (wsInfo.UseSystemProxy)
                {
                    webRequest.Proxy = WebRequest.DefaultWebProxy;
                }
                else
                {
                    string host = wsInfo.ProxyServer;
                    int port = wsInfo.ProxyServerPort;
                    WebProxy proxy = new WebProxy(host, port);
                    if (!string.IsNullOrEmpty(wsInfo.ProxyUserName) || !string.IsNullOrEmpty(wsInfo.ProxyPassword))
                    {
                        proxy.Credentials = new NetworkCredential(wsInfo.ProxyUserName, wsInfo.ProxyPassword);
                    }

                    webRequest.Proxy = proxy;
                }
            }
        }
    }
}
