// -----------------------------------------------------------------------
// <copyright file="FormatNormalizer.cs" company="EUROSTAT">
//   Date Created : 2016-08-01
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sdmxsource.Extension.Util
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Net.Mime;

    using Estat.Sdmxsource.Extension.Model;

    using Org.Sdmxsource.Sdmx.Api.Model.Data;
    using Org.Sdmxsource.Sdmx.Api.Model.Format;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Builder.Common;

    /// <summary>
    /// The SDMX format normalizer.
    /// </summary>
    public class FormatNormalizer
    {
        /// <summary>
        /// The default format for REST data requests as defined in SDMX v2.1 Web Service Guidelines
        /// </summary>
        private const string DefaultSdmxData = "application/vnd.sdmx.genericdata+xml;version=2.1";

        /// <summary>
        /// The default data content type
        /// </summary>
        private readonly ContentType _defaultDataContentType;

        /// <summary>
        /// The default structure content type
        /// </summary>
        private ContentType GetDefaultStructureContentType(RestApiVersion restApiVersion)
        {
            string defaultStructure = restApiVersion == RestApiVersion.V2
                ? "application/vnd.sdmx.structure+json;version=2.0"
                : "application/vnd.sdmx.structure+xml;version=2.1";
            return new ContentType(defaultStructure);
        }
        private ContentType GetDefaultDataContentType(RestApiVersion restApiVersion)
        {
            string defaultStructure = restApiVersion == RestApiVersion.V2
                ? "application/vnd.sdmx.data+json;version=2.0"
                : DefaultSdmxData;
            return new ContentType(defaultStructure);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="FormatNormalizer"/> class.
        /// </summary>
        public FormatNormalizer()
        {
            this._defaultDataContentType = new ContentType(DefaultSdmxData);
        }

        /// <summary>
        /// Normalizes the data format order.
        /// </summary>
        /// <param name="supportedFormats">The supported formats.</param>
        /// <returns>The list of data rest response with the one with the default value first</returns>
        public IList<IRestResponse<IDataFormat>> NormalizeDataFormatOrder(IEnumerable<IRestResponse<IDataFormat>> supportedFormats, RestApiVersion restApiVersion)
        {
            var restResponses = supportedFormats.ToArray();
            MoveDefaultFirst(restResponses, this.GetDefaultDataContentType(restApiVersion));
            return restResponses;
        }

        /// <summary>
        /// Normalizes the structure format order.
        /// </summary>
        /// <param name="supportedFormats">The supported formats.</param>
        /// <param name="restApiVersion">The rest API version.</param>
        /// <returns>The list of structure rest response with the one with the default value first</returns>
        public IList<IRestResponse<IStructureFormat>> NormalizeStructureFormatOrder(
            IEnumerable<IRestResponse<IStructureFormat>> supportedFormats, RestApiVersion restApiVersion)
        {
            var restResponses = supportedFormats.ToArray();
            MoveDefaultFirst(restResponses, this.GetDefaultStructureContentType(restApiVersion));
            return restResponses;
        }

        /// <summary>
        /// Moves the default first.
        /// </summary>
        /// <typeparam name="TOutputFormat">The type of the output format.</typeparam>
        /// <param name="supportedFormats">The supported formats.</param>
        /// <param name="defaultContentType">Default type of the content.</param>
        private static void MoveDefaultFirst<TOutputFormat>(IList<IRestResponse<TOutputFormat>> supportedFormats, ContentType defaultContentType)
        {
            if (supportedFormats.Count > 0)
            {
                // A shorter version of IndexOf. Get the index of the first entry that matches. Else it will return an index == to count
                var index =
                    supportedFormats.TakeWhile(type => !SdmxContentTypeComparer.SdmxMediaWithVersionComparer.Equals(defaultContentType, type.ResponseContentHeader.MediaType)).Count();
                if (index > 0 && index < supportedFormats.Count)
                {
                    var defaultFormat = supportedFormats[index];
                    supportedFormats[index] = supportedFormats[0];
                    supportedFormats[0] = defaultFormat;
                }
            }
        }
    }
}