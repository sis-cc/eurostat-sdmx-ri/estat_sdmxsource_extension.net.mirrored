// -----------------------------------------------------------------------
// <copyright file="NsiClientHelper.cs" company="EUROSTAT">
//   Date Created : 2016-03-16
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sdmxsource.Extension.Util
{
    using System;
    using System.IO;
    using System.Net;
    using System.Text;
    using System.Xml;
    using log4net;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Estat.Sdmxsource.Extension.Model;

    using Org.Sdmxsource.Sdmx.Api.Util;
    using Org.Sdmxsource.Sdmx.Util.Xml;
    using Org.Sdmxsource.Util.Io;

    /// <summary>
    /// NSIClientHelper class
    /// </summary>
    public static class NsiClientHelper
    {
        /// <summary>
        ///     Log class
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(NsiClientHelper));

        /// <summary>
        /// Gets the response data location from SOAP request.
        /// </summary>
        /// <param name="response">The response.</param>
        /// <returns>The <see cref="IReadableDataLocation"/>. Please note it is disposable</returns>
        public static IReadableDataLocation GetResponseDataLocationFromSoap(WebResponse response)
        {
            var writeableLocation = new WriteableDataLocationTmp();
            try
            {
                using (var input = response.GetResponseStream())
                using (var outputStream = writeableLocation.OutputStream)
                using (var tempWriter = XmlWriter.Create(outputStream))
                {
                    SoapUtils.ExtractSdmxMessage(input, tempWriter);
                    tempWriter.Flush();
                    outputStream.Flush();
                }

                return writeableLocation;
            }
            catch (Exception)
            {
                writeableLocation.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Gets the response data location from SOAP request.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The <see cref="IReadableDataLocation"/>. Please note it is disposable</returns>
        public static IReadableDataLocation GetResponseDataLocationFromSoap(HttpWebRequest request)
        {
            var writeableLocation = new WriteableDataLocationTmp();
            try
            {
                using (var response = request.GetResponse())
                using (var input = response.GetResponseStream())
                using (var outputStream = writeableLocation.OutputStream)
                using (var tempWriter = XmlWriter.Create(outputStream))
                {
                    SoapUtils.ExtractSdmxMessage(input, tempWriter);
                    tempWriter.Flush();
                    outputStream.Flush();
                }

                return writeableLocation;
            }
            catch (Exception)
            {
                writeableLocation.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Gets the response data location from REST request.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The <see cref="IReadableDataLocation"/>. Please note it is disposable</returns>
        public static IReadableDataLocation GetResponseDataLocationFromRest(HttpWebRequest request)
        {
            using (var response = request.GetResponse())
            using (var input = response.GetResponseStream())
            {
                return new ReadableDataLocationTmp(input);
            }
        }

        /// <summary>
        ///     Handle a SOAP Fault from the WS. It will parse the soap details and throw an NSIClientException
        /// </summary>
        /// <param name="ex">
        ///     The soap fault
        /// </param>
        /// <exception cref="SdmxUnauthorisedException">
        ///     It is always thrown
        /// </exception>
        public static void HandleSoapFault(WebException ex)
        {
            var error = new StringBuilder();
            error.AppendLine(ex.Message);
            if (ex.Response != null)
            {
                if (ex.Response is HttpWebResponse &&
                    ((HttpWebResponse)ex.Response).StatusCode == HttpStatusCode.Unauthorized)
                {
                    throw new SdmxUnauthorisedException(string.Format("StructureReferenceDetail {0} not supported", ex.ToString()));
                }

                error.AppendLine(ex.ToString());
                XmlDocument fault = null;
                using (Stream stream = ex.Response.GetResponseStream())
                {
                    if (stream != null)
                    {
                        fault = new XmlDocument();
                        fault.Load(stream);
                        Logger.Error(fault.InnerXml);
                    }
                }

                if (fault != null)
                {
                    Logger.Error(fault.InnerText);
                    SdmxFault sdmxFault = SdmxFault.GetErrorNumber(fault);
                    if (sdmxFault.ErrorNumber == 110 ||
                        sdmxFault.ErrorMessage.Equals("Unauthorized", StringComparison.OrdinalIgnoreCase)
                        || sdmxFault.ErrorMessage.Equals("No results found", StringComparison.OrdinalIgnoreCase))
                    {
                        throw new SdmxNoResultsException("No results found", ex);
                    }
                    else
                    {
                        throw new SdmxInternalServerException("Internal server error",ex);
                    }
                }
            }

            Logger.Error(error.ToString());
            Logger.Error(ex.Message, ex);
            throw new SdmxInternalServerException(ex.ToString());
        }
    }
}