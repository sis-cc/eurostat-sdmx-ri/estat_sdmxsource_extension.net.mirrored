﻿// -----------------------------------------------------------------------
// <copyright file="OnTheFlyAnnotationScope.cs" company="EUROSTAT">
//   Date Created : 2018-1-9
//   Copyright (c) 2009, 2018 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sdmxsource.Extension.Util
{
    using System;

    using Estat.Sdmxsource.Extension.Constant;

    /// <summary>
    /// A single level scope for storing the requested annotation types to be generated
    /// </summary>
    /// <seealso cref="System.IDisposable" />
    public class OnTheFlyAnnotationScope : IDisposable
    {
        /// <summary>
        /// The annotation types
        /// </summary>
        [ThreadStatic]
        private static OnTheFlyAnnotationType _annotationTypes;

        /// <summary>
        /// Initializes a new instance of the <see cref="OnTheFlyAnnotationScope"/> class.
        /// </summary>
        /// <param name="onTheFlyAnnotationTypes">The on the fly annotation types.</param>
        /// <exception cref="InvalidOperationException">A scope is already running in this thread. Dispose existing before starting a new one</exception>
        public OnTheFlyAnnotationScope(OnTheFlyAnnotationType onTheFlyAnnotationTypes)
        {
            if (_annotationTypes != OnTheFlyAnnotationType.None)
            {
                throw  new InvalidOperationException("A scope is already running in this thread. Dispose existing before starting a new one");
            }

            _annotationTypes = onTheFlyAnnotationTypes;
        }

        /// <summary>
        /// Determines whether the specified annotation type was requested.
        /// </summary>
        /// <param name="annotationType">Type of the annotation.</param>
        /// <returns>
        ///   <c>true</c> if the specified annotation type was requested; otherwise, <c>false</c>.
        /// </returns>
        public static bool HasAnnotationType(OnTheFlyAnnotationType annotationType)
        {
            return _annotationTypes.HasFlag(annotationType);
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                _annotationTypes = OnTheFlyAnnotationType.None;
            }
        }
    }
}