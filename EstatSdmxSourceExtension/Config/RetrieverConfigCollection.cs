﻿// -----------------------------------------------------------------------
// <copyright file="RetrieverConfigCollection.cs" company="EUROSTAT">
//   Date Created : 2015-12-08
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sdmxsource.Extension.Config
{
    using System.Configuration;

    /// <summary>
    ///     A collection of RetrieverConfigCollection(s).
    /// </summary>
    public sealed class RetrieverConfigCollection : ConfigurationElementCollection
    {
        /// <summary>
        ///     Retrieve and item in the collection by index.
        /// </summary>
        /// <param name="index">The index.</param>
        /// <returns>The <see cref="RetrieverConfig" />.</returns>
        public RetrieverConfig this[int index]
        {
            get
            {
                return (RetrieverConfig)this.BaseGet(index);
            }

            set
            {
                if (this.BaseGet(index) != null)
                {
                    this.BaseRemoveAt(index);
                }

                BaseAdd(index, value);
            }
        }

        /// <summary>
        ///     Adds a RetrieverConfigCollection to the configuration file.
        /// </summary>
        /// <param name="element">The RetrieverConfigCollection to add.</param>
        public void Add(RetrieverConfig element)
        {
            BaseAdd(element);
        }

        /// <summary>
        ///     Removes a RetrieverConfigCollection with the given name.
        /// </summary>
        /// <param name="name">The name of the RetrieverConfigCollection to remove.</param>
        public void Remove(string name)
        {
            this.BaseRemove(name);
        }

        /// <summary>
        ///     Creates a new RetrieverConfigCollection.
        /// </summary>
        /// <returns>A new <c>RetrieverConfigCollection</c></returns>
        protected override ConfigurationElement CreateNewElement()
        {
            return new RetrieverConfig();
        }

        /// <summary>
        ///     Gets the key of an element based on it's Id.
        /// </summary>
        /// <param name="element">Element to get the key of.</param>
        /// <returns>The key of <c>element</c>.</returns>
        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((RetrieverConfig)element).Name;
        }
    }
}