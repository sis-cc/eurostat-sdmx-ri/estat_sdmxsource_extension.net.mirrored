// -----------------------------------------------------------------------
// <copyright file="ProxySettingsConfig.cs" company="EUROSTAT">
//   Date Created : 2019-05-02
//   Copyright (c) 2009, 2019 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System.Configuration;

namespace Estat.Sdmxsource.Extension.Config
{
    public class ProxySettingsConfig : ConfigurationSection
    {
        [ConfigurationProperty("Authentication", IsRequired = false)]
        public bool Authentication
        {
            get { return (bool)this["Authentication"]; }
            set { this["Authentication"] = value; }
        }

        [ConfigurationProperty("EnableProxy", IsRequired = false)]
        public bool EnableProxy
        {
            get { return (bool)this["EnableProxy"]; }
            set { this["EnableProxy"] = value; }
        }

        [ConfigurationProperty("Password", IsRequired = false)]
        public string Password
        {
            get { return (string)this["Password"]; }
            set { this["Password"] = value; }
        }

        [ConfigurationProperty("Url", IsRequired = false)]
        public string Url
        {
            get { return (string)this["Url"]; }
            set { this["Url"] = value; }
        }

        [ConfigurationProperty("Username", IsRequired = false)]
        public string Username
        {
            get { return (string)this["Username"]; }
            set { this["Username"] = value; }
        }
    }
}