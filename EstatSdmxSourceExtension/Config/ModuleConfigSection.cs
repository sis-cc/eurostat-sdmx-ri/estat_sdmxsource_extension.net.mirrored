// -----------------------------------------------------------------------
// <copyright file="ModuleConfigSection.cs" company="EUROSTAT">
//   Date Created : 2015-12-08
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;

namespace Estat.Sdmxsource.Extension.Config
{
    using System.Configuration;

    using Constant;

    /// <summary>
    ///     Configuration section ModuleConfigSection
    /// </summary>
    /// <remarks>
    ///     Assign properties to your child class that has the attribute
    ///     <c>[ConfigurationProperty]</c> to store said properties in the xml.
    /// </remarks>
    public sealed class ModuleConfigSection : ConfigurationSection
    {
        /// <summary>
        ///     The property name
        /// </summary>
        private const string PropertyName = "retrievers";

        /// <summary>
        ///     The section name
        /// </summary>
        private const string SectionName = "estat.sri/module";

        /// <summary>
        ///     The structure request behavior property name
        /// </summary>
        private const string StructureRequestBehaviorName = "structureRequestBehavior";

        /// <summary>
        ///     Prevents a default instance of the <see cref="ModuleConfigSection" /> class from being created.
        /// </summary>
        private ModuleConfigSection()
        {
            // Allow this section to be stored in user.app. By default this is forbidden.
            this.SectionInformation.AllowExeDefinition = ConfigurationAllowExeDefinition.MachineToLocalUser;
        }

        /// <summary>
        ///     Gets the Collection of <see cref="RetrieverConfig" />
        ///     A custom XML section for an applications configuration file.
        /// </summary>
        [ConfigurationProperty(PropertyName)]
        public RetrieverConfigCollection Retrievers
        {
            get { return (RetrieverConfigCollection)this[PropertyName]; }
        }

        /// <summary>
        ///     Gets the structure request behavior.
        /// </summary>
        /// <value>The structure request behavior.</value>
        [ConfigurationProperty(StructureRequestBehaviorName, DefaultValue = RequestBehaviorType.First)]
        public RequestBehaviorType StructureRequestBehavior
        {
            get { return (RequestBehaviorType)this[StructureRequestBehaviorName]; }
        }

        /// <summary>
        ///     Gets the current applications &lt;module&gt; section.
        /// </summary>
        /// <returns>
        ///     The configuration file's &lt;module&gt; section.
        /// </returns>
        public static ModuleConfigSection GetSection()
        {
            return (ModuleConfigSection)ConfigurationManager.GetSection(SectionName) ?? new ModuleConfigSection();
        }
    }
}