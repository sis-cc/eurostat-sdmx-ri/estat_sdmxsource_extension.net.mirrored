﻿// -----------------------------------------------------------------------
// <copyright file="SafeDataWriterPluginContract.cs" company="EUROSTAT">
//   Date Created : 2016-07-26
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sdmxsource.Extension.Factory
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml;

    using Estat.Sdmxsource.Extension.Model;

    using log4net;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Data;

    /// <summary>
    /// The safe data writer plugin contract.
    /// </summary>
    public class SafeDataWriterPluginContract : IDataWriterPluginContract
    {
        /// <summary>
        /// The _log
        /// </summary>
        private static readonly ILog _log = LogManager.GetLogger(typeof(SafeDataWriterPluginContract));

        /// <summary>
        /// The _decorated contract
        /// </summary>
        private readonly IDataWriterPluginContract _decoratedContract;

        /// <summary>
        /// Initializes a new instance of the <see cref="SafeDataWriterPluginContract"/> class.
        /// </summary>
        /// <param name="decoratedContract">The decorated contract.</param>
        public SafeDataWriterPluginContract(IDataWriterPluginContract decoratedContract)
        {
            if (decoratedContract == null)
            {
                throw new ArgumentNullException("decoratedContract");
            }

            this._decoratedContract = decoratedContract;
        }

        /// <summary>
        /// Gets the supported formats and header values.
        /// </summary>
        /// <param name="restDataRequest">The rest data request.</param>
        /// <returns>
        /// The list of supported formats - <see cref="IRestResponse{IDataFormat}" />.
        /// </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes", Justification = "It is OK. The propose of this class is to catch exceptions thrown by 3rd party code")]
        public IEnumerable<IRestResponse<IDataFormat>> GetDataFormats(IRestDataRequest restDataRequest)
        {
            if (restDataRequest == null)
            {
                throw new ArgumentNullException("restDataRequest");
            }

            try
            {
                return this._decoratedContract.GetDataFormats(restDataRequest);
            }
            catch (Exception e)
            {
                _log.Error(string.Format("While executing {0} with {1}", this._decoratedContract, restDataRequest), e);
            }

            return Enumerable.Empty<IRestResponse<IDataFormat>>();
        }

        /// <inheritdoc cref="IDataWriterPluginContract.GetAvailableDataFormats"/>
        public IEnumerable<string> GetAvailableDataFormats()
        {
            return _decoratedContract.GetAvailableDataFormats();
        }

        /// <summary>
        /// Gets the data format.
        /// </summary>
        /// <param name="soapRequest">Type of the data.</param>
        /// <param name="xmlWriter">The XML writer.</param>
        /// <returns>
        /// The <see cref="IDataFormat" />.
        /// </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes", Justification = "It is OK. The propose of this class is to catch exceptions thrown by 3rd party code")]
        public IDataFormat GetDataFormat(ISoapRequest<DataType> soapRequest, XmlWriter xmlWriter)
        {
            if (soapRequest == null)
            {
                throw new ArgumentNullException("soapRequest");
            }

            if (xmlWriter == null)
            {
                throw new ArgumentNullException("xmlWriter");
            }

            try
            {
                return this._decoratedContract.GetDataFormat(soapRequest, xmlWriter);
            }
            catch (Exception e)
            {
                _log.Error(string.Format("While executing {0} with {1}", this._decoratedContract, soapRequest), e);
            }

            return null;
        }
    }
}