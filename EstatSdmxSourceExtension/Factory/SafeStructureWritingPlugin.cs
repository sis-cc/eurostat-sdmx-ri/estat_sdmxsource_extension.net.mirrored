// -----------------------------------------------------------------------
// <copyright file="SafeStructureWritingPlugin.cs" company="EUROSTAT">
//   Date Created : 2016-07-26
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------



namespace Estat.Sdmxsource.Extension.Factory
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Xml;

    using Estat.Sdmxsource.Extension.Model;
    using Org.Sdmxsource.Sdmx.Api.Model.Query;
    using log4net;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Format;

    /// <summary>
    /// The safe structure writing plugin.
    /// </summary>
    public class SafeStructureWritingPlugin : IStructureWriterPluginContract
    {
        /// <summary>
        /// The _log
        /// </summary>
        private static readonly ILog _log = LogManager.GetLogger(typeof(SafeDataWriterPluginContract));

        /// <summary>
        /// The _decorated contract
        /// </summary>
        private readonly IStructureWriterPluginContract _decoratedContract;

        /// <summary>
        /// Initializes a new instance of the <see cref="SafeStructureWritingPlugin"/> class.
        /// </summary>
        /// <param name="decoratedContract">The decorated contract.</param>
        public SafeStructureWritingPlugin(IStructureWriterPluginContract decoratedContract)
        {
            if (decoratedContract == null)
            {
                throw new ArgumentNullException("decoratedContract");
            }

            this._decoratedContract = decoratedContract;
        }

        /// <summary>
        /// Gets the structure format.
        /// </summary>
        /// <param name="headers">The headers.</param>
        /// <param name="sortedAcceptHeaders">The sorted accept headers.</param>
        /// <param name="structureType">The type of the requested structure</param>
        /// <returns>
        /// The list of supported formats - <see cref="IRestResponse{IStructureFormat}" />.
        /// </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes", Justification = "It is OK. The propose of this class is to catch exceptions thrown by 3rd party code")]
        public IEnumerable<IRestResponse<IStructureFormat>> GetStructureFormats(WebHeaderCollection headers, ISortedAcceptHeaders sortedAcceptHeaders)
        {
            try
            {
                return this._decoratedContract.GetStructureFormats(headers, sortedAcceptHeaders);
            }
            catch (Exception e)
            {
                _log.Error(string.Format("While executing {0} with {1}", this._decoratedContract, sortedAcceptHeaders), e);
            }

            return Enumerable.Empty<IRestResponse<IStructureFormat>>();
        }

        /// <inheritdoc cref="IStructureWriterPluginContract.GetAvailableStructureFormats"/>
        public IEnumerable<string> GetAvailableStructureFormats()
        {
            return _decoratedContract.GetAvailableStructureFormats();
        }

        /// <summary>
        /// Gets the structure format.
        /// </summary>
        /// <param name="soapRequest">Type of the structure.</param>
        /// <param name="xmlWriter">The XML writer.</param>
        /// <returns>
        /// The <see cref="IStructureFormat" />.
        /// </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes", Justification = "It is OK. The propose of this class is to catch exceptions thrown by 3rd party code")]
        public IStructureFormat GetStructureFormat(ISoapRequest<StructureOutputFormat> soapRequest, XmlWriter xmlWriter)
        {
            try
            {
                return this._decoratedContract.GetStructureFormat(soapRequest, xmlWriter);
            }
            catch (Exception e)
            {
                _log.Error(string.Format("While executing {0} with {1}", this._decoratedContract, soapRequest), e);
            }

            return null;
        }
    }
}