﻿// -----------------------------------------------------------------------
// <copyright file="CSVDataWriterFactory.cs" company="EUROSTAT">
//   Date Created : 2016-09-28
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------



namespace Estat.Sdmxsource.Extension.Factory
{
    using System;
    using System.IO;
    using Engine;
    using Model;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Engine;
    using Org.Sdmxsource.Sdmx.Api.Factory;
    using Org.Sdmxsource.Sdmx.Api.Model.Data;

    /// <summary>
    /// CSVDataWriterFactory
    /// </summary>
    public class CsvDataWriterFactory : IDataWriterFactory
    {
        /// <summary>
        ///     Gets the data writer engine.
        /// </summary>
        /// <param name="dataFormat">
        ///     The data format.
        /// </param>
        /// <param name="outStream">
        ///     The output stream.
        /// </param>
        /// <returns>
        ///     The <see cref="IDataWriterEngine" />.
        /// </returns>
        public IDataWriterEngine GetDataWriterEngine(IDataFormat dataFormat, Stream outStream)
        {
            var csvDataFormat = dataFormat as CsvDataFormat;
            if (csvDataFormat == null)
            {
                return null;
            }
            if (csvDataFormat.SdmxDataFormat != null &&
                csvDataFormat.SdmxDataFormat.BaseDataFormat != null &&
                csvDataFormat.SdmxDataFormat.BaseDataFormat.EnumType == BaseDataFormatEnumType.Csv)
            {
                return new CsvDataWriterEngine(csvDataFormat, new StreamWriter(outStream));
            }

            return null;
        }
    }
}