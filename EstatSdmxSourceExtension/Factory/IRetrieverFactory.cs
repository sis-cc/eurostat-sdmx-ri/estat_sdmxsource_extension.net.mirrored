// -----------------------------------------------------------------------
// <copyright file="IRetrieverFactory.cs" company="EUROSTAT">
//   Date Created : 2015-12-08
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sdmxsource.Extension.Factory
{
    using System;
    using System.Collections.Generic;
    using System.Security.Principal;

    using Estat.Sdmxsource.Extension.Manager;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval.Data;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval.Mutable;
    using Org.Sdmxsource.Sdmx.Api.Model.Data.Query;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.DataParser.Manager;

    /// <summary>
    /// Interface for retrieving instances of Structure and Data retrieval interfaces.
    /// </summary>
    public interface IRetrieverFactory
    {
        /// <summary>
        /// Gets the unique identifier of the implementation.
        /// </summary>
        /// <value>The identifier.</value>
        string Id { get; }

        /// <summary>
        /// Gets an instance of <see cref="ISdmxDataRetrievalWithWriter" /> using the provided SDMX version.
        /// </summary>
        /// <param name="sdmxSchema">The SDMX schema version.</param>
        /// <param name="principal">The principal. Can be <c>null</c>.</param>
        /// <returns>The <see cref="ISdmxDataRetrievalWithWriter" />.</returns>
        ISdmxDataRetrievalWithWriter GetDataRetrieval(SdmxSchema sdmxSchema, IPrincipal principal);

        /// <summary>
        /// Gets an instance of <see cref="ISdmxDataRetrievalWithCrossWriter" /> using the provided SDMX version.
        /// </summary>
        /// <param name="sdmxSchema">The SDMX schema version.</param>
        /// <param name="principal">The principal. Can be <c>null</c>.</param>
        /// <returns>The <see cref="ISdmxDataRetrievalWithCrossWriter" />.</returns>
        ISdmxDataRetrievalWithCrossWriter GetDataRetrievalWithCross(SdmxSchema sdmxSchema, IPrincipal principal);

        /// <summary>
        /// Gets an instance of <see cref="ISdmxDataRetrievalWithCrossWriter" /> (SDMX v2.1 only).
        /// </summary>
        /// <param name="principal">The principal. Can be <c>null</c>.</param>
        /// <returns>The <see cref="IAdvancedSdmxDataRetrievalWithWriter" />.</returns>
        IAdvancedSdmxDataRetrievalWithWriter GetAdvancedDataRetrieval(IPrincipal principal);

        /// <summary>
        /// Used for sdmx 2.0 cross sectional requests (coming either from rest or soap requests)
        /// </summary>
        /// <param name="principal">the principal.</param>
        /// <returns>The structure retrieval manager.</returns>
        ICommonSdmxObjectRetrievalManager GetCommonSdmxObjectRetrievalManager(IPrincipal principal);

        /// <summary>
        /// Used only for Soap 2.0 requests.
        /// </summary>
        /// <param name="principal">the principal.</param>
        /// <returns>The structure retrieval manager.</returns>
        ICommonSdmxObjectRetrievalManagerSoap20 GetCommonSdmxObjectRetrievalManagerSoap20(IPrincipal principal);

        /// <summary>
        /// Gets the SDMX object retrieval.
        /// </summary>
        /// <param name="principal">The principal. Can be <c>null</c>.</param>
        /// <returns>The <see cref="ISdmxObjectRetrievalManager"/>.</returns>
        ISdmxObjectRetrievalManager GetSdmxObjectRetrieval(IPrincipal principal);

        /// <summary>
        /// Gets the data number of records.
        /// </summary>
        /// <param name="dataQuery">The data query.</param>
        /// <returns></returns>
        int GetNumberOfObservations(IDataQuery dataQuery);

        /// <summary>S
        /// Gets the available data manager.
        /// </summary>
        /// <param name="sdmxVersion">The SDMX version.</param>
        /// <param name="principal">The principal.</param>
        /// <returns></returns>
        IAvailableDataManager GetAvailableDataManager(SdmxSchema sdmxVersion, IPrincipal principal);
    }
}