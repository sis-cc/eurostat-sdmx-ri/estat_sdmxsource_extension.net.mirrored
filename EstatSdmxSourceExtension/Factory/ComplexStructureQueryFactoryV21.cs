﻿// -----------------------------------------------------------------------
// <copyright file="ComplexStructureQueryFactoryV21.cs" company="EUROSTAT">
//   Date Created : 2013-08-01
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.CustomRequests.Factory
{
    using System.Xml.Linq;

    using Estat.Sri.CustomRequests.Builder;
    using Estat.Sri.CustomRequests.Builder.QueryBuilder;
    using Estat.Sri.CustomRequests.Manager;
    using Estat.Sri.CustomRequests.Model;

    using Org.Sdmxsource.Sdmx.Api.Model.Format;
    using Org.Sdmxsource.Sdmx.Structureparser.Model;

    /// <summary>
    /// Complex Structure Query factory
    /// </summary>
    /// <seealso cref="IComplexStructureQueryFactory{XDocument}" />
    public class ComplexStructureQueryFactoryV21 : IComplexStructureQueryFactory<XDocument>
    {
        /// <summary>
        /// The tool indicator
        /// </summary>
        private readonly IToolIndicator _toolIndicator;

        /// <summary>
        /// Initializes a new instance of the <see cref="ComplexStructureQueryFactoryV21"/> class.
        /// </summary>
        /// <param name="toolIndicator">The tool indicator.</param>
        public ComplexStructureQueryFactoryV21(IToolIndicator toolIndicator)
        {
            this._toolIndicator = toolIndicator;
        }

        /// <summary>
        ///     Returns a ComplexStructureQueryBuilder only if this factory understands the StructureQueryFormat.
        ///     If the format is unknown, null will be returned
        /// </summary>
        /// <param name="format">The format</param>
        /// <returns>
        ///     ComplexStructureQueryBuilder is this factory knows how to build this query format, or null if it doesn't
        /// </returns>
        public IComplexStructureQueryBuilder<XDocument> GetComplexStructureQueryBuilder(
            IStructureQueryFormat<XDocument> format)
        {
            if (format is ComplexQueryFormatV21)
            {
                return new CommentComplexStructureQueryBuilder(this._toolIndicator, new ComplexStructureQueryBuilderV21());
            }

            return null;
        }
    }
}