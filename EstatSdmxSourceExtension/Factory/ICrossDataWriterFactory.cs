﻿// -----------------------------------------------------------------------
// <copyright file="ICrossDataWriterFactory.cs" company="EUROSTAT">
//   Date Created : 2016-07-04
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sdmxsource.Extension.Factory
{
    using System.IO;
    using System.Xml;

    using Org.Sdmxsource.Sdmx.Api.Model.Data;
    using Org.Sdmxsource.Sdmx.DataParser.Engine;

    /// <summary>
    /// The Factory interface for <see cref="ICrossSectionalWriterEngine"/>
    /// </summary>
    public interface ICrossDataWriterFactory
    {
        /// <summary>
        /// Gets the SDMX v2.0 Cross Sectional data writer engine.
        /// </summary>
        /// <param name="dataFormat">The data format. The implementation may have extra properties. </param>
        /// <param name="outputStream">The output stream. It can be null. But in this case the output should be provided in the <paramref name="dataFormat"/> implementation.</param>
        /// <returns>
        /// The <see cref="ICrossSectionalWriterEngine" />.
        /// </returns>
        ICrossSectionalWriterEngine GetWriterEngine(IDataFormat dataFormat, Stream outputStream);
    }
}