﻿// -----------------------------------------------------------------------
// <copyright file="RestDataRequest.cs" company="EUROSTAT">
//   Date Created : 2016-07-22
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sdmxsource.Extension.Factory
{
    using System.Net;

    using Estat.Sdmxsource.Extension.Model;

    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
    using Org.Sdmxsource.Sdmx.Api.Model.Data.Query;

    /// <summary>
    ///     The REST data request information.
    /// </summary>
    public class RestDataRequest : IRestDataRequest
    {
        /// <summary>
        ///     The _data query
        /// </summary>
        private readonly IDataQuery _dataQuery;

        /// <summary>
        ///     The _headers
        /// </summary>
        private readonly WebHeaderCollection _headers;

        /// <summary>
        ///     The _retrieval manager
        /// </summary>
        private readonly ISdmxObjectRetrievalManager _retrievalManager;

        /// <summary>
        ///     The _accept headers
        /// </summary>
        private readonly ISortedAcceptHeaders _sortedAcceptHeaders;

        /// <summary>
        /// Initializes a new instance of the <see cref="RestDataRequest"/> class.
        /// </summary>
        /// <param name="headers">
        /// The headers.
        /// </param>
        /// <param name="retrievalManager">
        /// The retrieval manager.
        /// </param>
        /// <param name="dataQuery">
        /// The data query.
        /// </param>
        /// <param name="sortedAcceptHeaders">
        /// The sorted Accept Headers.
        /// </param>
        public RestDataRequest(
            WebHeaderCollection headers,
            ISdmxObjectRetrievalManager retrievalManager,
            IDataQuery dataQuery,
            ISortedAcceptHeaders sortedAcceptHeaders)
        {
            this._headers = headers;
            this._retrievalManager = retrievalManager;
            this._dataQuery = dataQuery;
            this._sortedAcceptHeaders = sortedAcceptHeaders;
        }

        /// <summary>
        /// Gets the data query.
        /// </summary>
        /// <value>
        /// The data query.
        /// </value>
        public IDataQuery DataQuery
        {
            get
            {
                return this._dataQuery;
            }
        }

        /// <summary>
        /// Gets the headers from the request. Note they are mutable.
        /// Use the <see cref="SortedAcceptHeaders" /> to get a sorted by quality and order Accept, Accept-Language and Accept-CharSet
        /// </summary>
        /// <value>
        /// The headers.
        /// </value>
        public WebHeaderCollection Headers
        {
            get
            {
                return this._headers;
            }
        }

        /// <summary>
        ///     Gets the retrieval manager.
        /// </summary>
        /// <value>
        ///     The retrieval manager.
        /// </value>
        public ISdmxObjectRetrievalManager RetrievalManager
        {
            get
            {
                return this._retrievalManager;
            }
        }

        /// <summary>
        ///     Gets the sorted accept headers.
        /// </summary>
        /// <value>
        ///     The sorted accept headers.
        /// </value>
        public ISortedAcceptHeaders SortedAcceptHeaders
        {
            get
            {
                return this._sortedAcceptHeaders;
            }
        }
    }
}