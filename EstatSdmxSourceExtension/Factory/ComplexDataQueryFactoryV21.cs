﻿// -----------------------------------------------------------------------
// <copyright file="ComplexDataQueryFactoryV21.cs" company="EUROSTAT">
//   Date Created : 2013-08-01
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.CustomRequests.Factory
{
    using System.Xml.Linq;

    using Estat.Sri.CustomRequests.Builder;
    using Estat.Sri.CustomRequests.Builder.QueryBuilder;
    using Estat.Sri.CustomRequests.Manager;
    using Estat.Sri.CustomRequests.Model;

    using Org.Sdmxsource.Sdmx.Api.Model.Format;
    using Org.Sdmxsource.Sdmx.Structureparser.Model;

    /// <summary>
    ///     TODO: Update summary.
    /// </summary>
    public class ComplexDataQueryFactoryV21 : IComplexDataQueryFactory
    {
        /// <summary>
        ///     Returns a ComplexDataQueryBuilder only if this factory understands the DataQueryFormat.
        ///     If the format is unknown, null will be returned
        /// </summary>
        /// <param name="format">The format</param>
        /// <returns>
        ///     IComplexDataQueryBuilder is this factory knows how to build this query format, or null if it doesn't
        /// </returns>
        public IComplexDataQueryBuilder GetComplexDataQueryBuilder(IDataQueryFormat<XDocument> format)
        {
            var toolIndicator = this.ExtractToolIndicator(format);
            
            if (format is GenericTimeSeriesDataFormatV21)
            {
                return new CommentComplexDataQueryBuilder(new GenericTimeSeriesDataQueryBuilderV21(), toolIndicator);
            }

            if (format is GenericDataDocumentFormatV21)
            {
                return new CommentComplexDataQueryBuilder(new GenericDataQueryBuilderV21(), toolIndicator);
            }

            if (format is StructSpecificDataFormatV21)
            {
                return new CommentComplexDataQueryBuilder(new StructSpecificDataQueryBuilderV21(), toolIndicator);
            }

            if (format is StructSpecificTimeSeriesDataFormatV21)
            {
                return new CommentComplexDataQueryBuilder(new StructSpecificTimeSeriesQueryBuilderV21(), toolIndicator);
            }

            return null;
        }

        /// <summary>
        /// Extracts the tool indicator.
        /// </summary>
        /// <param name="dataQueryFormat">
        /// The data query format.
        /// </param>
        /// <returns>
        /// The <see cref="IToolIndicator"/>.
        /// </returns>
        private IToolIndicator ExtractToolIndicator(IDataQueryFormat<XDocument> dataQueryFormat)
        {
            var genericDataDocumentFormatV21 = dataQueryFormat as GenericDataDocumentFormatV21;
            if (genericDataDocumentFormatV21 != null)
            {
                return genericDataDocumentFormatV21.ToolIndicator;
            }

            var structSpecificDataFormatV21 = dataQueryFormat as StructSpecificDataFormatV21;
            if (structSpecificDataFormatV21 != null)
            {
                return structSpecificDataFormatV21.ToolIndicator;
            }

            return null;
        }
    }
}