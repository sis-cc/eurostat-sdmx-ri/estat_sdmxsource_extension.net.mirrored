﻿// -----------------------------------------------------------------------
// <copyright file="SubmitStructureResponseWriterFactory.cs" company="EUROSTAT">
//   Date Created : 2017-11-15
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using Org.Sdmxsource.Sdmx.Api.Model.Header;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using Estat.Sdmxsource.Extension.Engine;
using Estat.Sdmxsource.Extension.Model.Error;

namespace Estat.Sdmxsource.Extension.Factory
{
    /// <summary>
    /// SDMX v2.1 Registry Interface SubmitStructure Response writer factory
    /// </summary>
    /// <seealso cref="Estat.Sdmxsource.Extension.Factory.IResponseWriterFactory" />
    public class SubmitStructureResponseWriterFactory : IResponseWriterFactory
    {
        /// <summary>
        /// Gets the engine for the specified <paramref name="format" />
        /// </summary>
        /// <param name="format">The format.</param>
        /// <returns>
        /// The <see cref="IResponseMessageWriter" /> for the specified <paramref name="format" /> if it is supported;otherwise null
        /// </returns>
        public IResponseMessageWriter GetEngine(IResponseFormat format)
        {
            var submitStructure = format as SubmitStructureResponse21Format;
            if (submitStructure != null)
            {
                return new SubmitStructureResponseWriter(submitStructure.Writer, submitStructure.Header, submitStructure.RegistryInterfaceIsRootElement);
            }

            return null;
        }
    }
}
