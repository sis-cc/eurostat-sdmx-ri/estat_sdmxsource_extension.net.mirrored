// -----------------------------------------------------------------------
// <copyright file="AbstractRetrieverFactory.cs" company="EUROSTAT">
//   Date Created : 2015-12-08
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sdmxsource.Extension.Factory
{
    using System.Security.Principal;

    using Estat.Sdmxsource.Extension.Manager;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval.Data;
    using Org.Sdmxsource.Sdmx.Api.Model.Data.Query;
    using Org.Sdmxsource.Sdmx.DataParser.Manager;

    /// <summary>
    /// Abstract decorator class for <see cref="IRetrieverFactory"/>.
    /// </summary>
    public abstract class AbstractRetrieverFactory : IRetrieverFactory
    {
        /// <summary>
        /// The _decorated <see cref="IRetrieverFactory"/>
        /// </summary>
        private readonly IRetrieverFactory _decorated;

        /// <summary>
        /// Initializes a new instance of the <see cref="AbstractRetrieverFactory"/> class.
        /// </summary>
        /// <param name="decorated">The decorated.</param>
        protected AbstractRetrieverFactory(IRetrieverFactory decorated)
        {
            this._decorated = decorated;
        }

        /// <summary>
        /// Gets the unique identifier of the implementation.
        /// </summary>
        /// <value>The identifier.</value>
        public virtual string Id
        {
            get
            {
                return this._decorated.Id;
            }
        }

        /// <summary>
        /// Gets an instance of <see cref="ISdmxDataRetrievalWithCrossWriter" /> (SDMX v2.1 only).
        /// </summary>
        /// <param name="principal">The principal. Can be <c>null</c>.</param>
        /// <returns>The <see cref="IAdvancedSdmxDataRetrievalWithWriter" />.</returns>
        public virtual IAdvancedSdmxDataRetrievalWithWriter GetAdvancedDataRetrieval(IPrincipal principal)
        {
            return this._decorated.GetAdvancedDataRetrieval(principal);
        }

        /// <summary>
        /// Gets an instance of <see cref="ISdmxDataRetrievalWithWriter" /> using the provided SDMX version.
        /// </summary>
        /// <param name="sdmxSchema">The SDMX schema version.</param>
        /// <param name="principal">The principal. Can be <c>null</c>.</param>
        /// <returns>The <see cref="ISdmxDataRetrievalWithWriter" />.</returns>
        public virtual ISdmxDataRetrievalWithWriter GetDataRetrieval(SdmxSchema sdmxSchema, IPrincipal principal)
        {
            return this._decorated.GetDataRetrieval(sdmxSchema, principal);
        }

        /// <summary>
        /// Gets an instance of <see cref="ISdmxDataRetrievalWithCrossWriter" /> using the provided SDMX version.
        /// </summary>
        /// <param name="sdmxSchema">The SDMX schema version.</param>
        /// <param name="principal">The principal. Can be <c>null</c>.</param>
        /// <returns>The <see cref="ISdmxDataRetrievalWithCrossWriter" />.</returns>
        public virtual ISdmxDataRetrievalWithCrossWriter GetDataRetrievalWithCross(SdmxSchema sdmxSchema, IPrincipal principal)
        {
            return this._decorated.GetDataRetrievalWithCross(sdmxSchema, principal);
        }

        /// <summary>
        /// Gets the SDMX object retrieval.
        /// </summary>
        /// <param name="principal">The principal. Can be <c>null</c>.</param>
        /// <returns>The <see cref="ISdmxObjectRetrievalManager" />.</returns>
        public virtual ISdmxObjectRetrievalManager GetSdmxObjectRetrieval(IPrincipal principal)
        {
            return this._decorated.GetSdmxObjectRetrieval(principal);
        }

        /// <inheritdoc />
        /// <summary>
        /// Gets the data number of records.
        /// </summary>
        /// <param name="dataQuery">The data query.</param>
        /// <returns></returns>
        public int GetNumberOfObservations(IDataQuery dataQuery)
        {
            return this._decorated.GetNumberOfObservations(dataQuery);
        }

        /// <inheritdoc />
        public IAvailableDataManager GetAvailableDataManager(SdmxSchema sdmxVersion, IPrincipal principal)
        {
            return this._decorated.GetAvailableDataManager(sdmxVersion, principal);
        }

        #region Structure retrieval managers

        /// <summary>
        /// For all REST, SOAP and other requests EXCEPT SOAP 2.0
        /// </summary>
        /// <param name="principal"></param>
        /// <returns></returns>
        public ICommonSdmxObjectRetrievalManager GetCommonSdmxObjectRetrievalManager(IPrincipal principal)
        {
            return this._decorated.GetCommonSdmxObjectRetrievalManager(principal);
        }

        /// <summary>
        /// Only for SOAP 2.0 requests - until end of support.
        /// </summary>
        /// <param name="principal"></param>
        /// <returns></returns>
        public ICommonSdmxObjectRetrievalManagerSoap20 GetCommonSdmxObjectRetrievalManagerSoap20(IPrincipal principal)
        {
            return this._decorated.GetCommonSdmxObjectRetrievalManagerSoap20(principal);
        }

        #endregion
    }
}