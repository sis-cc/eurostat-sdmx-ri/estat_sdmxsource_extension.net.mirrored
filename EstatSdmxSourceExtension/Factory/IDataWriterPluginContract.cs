﻿// -----------------------------------------------------------------------
// <copyright file="IDataWriterPluginContract.cs" company="EUROSTAT">
//   Date Created : 2016-06-30
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sdmxsource.Extension.Factory
{
    using System.Collections.Generic;
    using System.Xml;

    using Estat.Sdmxsource.Extension.Model;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Data;

    /// <summary>
    /// The DataWriterPluginContract interface.
    /// </summary>
    public interface IDataWriterPluginContract
    {
        /// <summary>
        /// Gets the supported formats and header values. It is used for REST.
        /// It returns a list of supported content types e.g. <c>“application/json”</c>, together with the <see cref="IDataFormat"/> implementation and the  HTTP response content information, ContentType and Content-Language. 
        /// </summary>
        /// <param name="restDataRequest">The rest data request.</param>
        /// <returns>The list of supported formats - <see cref="IRestResponse{IDataFormat}" />.</returns>
        IEnumerable<IRestResponse<IDataFormat>> GetDataFormats(IRestDataRequest restDataRequest);

        /// <summary>
        /// Gets the supported response formats for REST.
        /// </summary>
        /// <returns>The list of supported formats.</returns>
        IEnumerable<string> GetAvailableDataFormats();

        /// <summary>
        /// Gets the data format. This is used for SOAP. 
        /// </summary>
        /// <param name="soapRequest">
        /// The SOAP request metadata including the requested <see cref="DataType"/>.
        /// </param>
        /// <param name="xmlWriter">
        /// The XML writer. The XML Writer for writing into the body of the SOAP message. This needs to be used for writing. It should not be null.
        /// </param>
        /// <returns>
        /// The <see cref="IDataFormat"/>.if the <paramref name="soapRequest"/> is supported; otherwise <c>null</c>
        /// </returns>
        IDataFormat GetDataFormat(ISoapRequest<DataType> soapRequest, XmlWriter xmlWriter);
    }
}