﻿// -----------------------------------------------------------------------
// <copyright file="AbstractDataWriterPluginContract.cs" company="EUROSTAT">
//   Date Created : 2016-08-22
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sdmxsource.Extension.Factory
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net.Mime;
    using System.Text;
    using System.Xml;

    using Estat.Sdmxsource.Extension.Model;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Data;

    /// <summary>
    /// The abstract data writer plugin contract.
    /// </summary>
    /// <remarks>An abstract class which can be extended to implement the IDataWriterPluginContract. At minimum it needs two things 1. At the constructor the  list of supported media types. E.g. “text/csv; version=2.0”,  should be provided. 2, The abstract method GetDataFormat needs be implemented. </remarks>
    public abstract class AbstractDataWriterPluginContract : IDataWriterPluginContract
    {
        /// <summary>
        /// The _supported media types.
        /// </summary>
        private readonly IList<string> _supportedMediaTypes;

        /// <summary>
        /// Initializes a new instance of the <see cref="AbstractDataWriterPluginContract"/> class.
        /// </summary>
        /// <param name="supportedMediaTypes">The supported media types.</param>
        protected AbstractDataWriterPluginContract(IList<string> supportedMediaTypes)
        {
            if (supportedMediaTypes == null)
            {
                throw new ArgumentNullException("supportedMediaTypes");
            }

            if (supportedMediaTypes.Count == 0)
            {
                throw new ArgumentException("Supported types are empty", "supportedMediaTypes");
            }

            this._supportedMediaTypes = supportedMediaTypes;
        }

        /// <inheritdoc cref="IDataWriterPluginContract.GetAvailableDataFormats"/>
        public IEnumerable<string> GetAvailableDataFormats()
        {
            return this._supportedMediaTypes;
        }

        /// <summary>
        /// Gets the data format.
        /// </summary>
        /// <param name="soapRequest">
        /// Type of the data.
        /// </param>
        /// <param name="xmlWriter">
        /// The XML writer.
        /// </param>
        /// <returns>
        /// The <see cref="IDataFormat"/>.
        /// </returns>
        public virtual IDataFormat GetDataFormat(ISoapRequest<DataType> soapRequest, XmlWriter xmlWriter)
        {
            return null;
        }

        /// <summary>
        /// Gets the supported formats and header values.
        /// </summary>
        /// <param name="restDataRequest">The rest data request.</param>
        /// <returns>
        /// The list of supported formats - <see cref="IRestResponse{IDataFormat}" />.
        /// </returns>
        public virtual IEnumerable<IRestResponse<IDataFormat>> GetDataFormats(IRestDataRequest restDataRequest)
        {
            var encoding =
                this.BuildEncoding(restDataRequest);

            return
                this._supportedMediaTypes.Select(
                    pair =>
                    new RestResponse<IDataFormat>(
                        new Lazy<IDataFormat>(() => this.BuildFormat(pair, encoding, restDataRequest)),
                        this.BuildResponseContentHeader(restDataRequest, pair, encoding)));
        }

        /// <summary>
        /// Builds the encoding.  Override this when you need to override the way the <see cref="Encoding"/> is selected.
        /// </summary>
        /// <param name="restDataRequest">
        /// The rest data request.
        /// </param>
        /// <returns>
        /// The <see cref="Encoding"/>.
        /// </returns>
        protected virtual Encoding BuildEncoding(IRestDataRequest restDataRequest)
        {
            return restDataRequest.SortedAcceptHeaders.CharSets.Select(quality => quality.Value).FirstOrDefault()
                   ?? new UTF8Encoding(false);
        }

        /// <summary>
        /// Gets the language.  Override this to return a different language 
        /// </summary>
        /// <param name="restDataRequest">
        /// The rest data request.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        protected virtual string GetLanguage(IRestDataRequest restDataRequest)
        {
            return null;
        }

        /// <summary>
        /// Builds the format. This is used only for REST. This needs to be overridden. The <paramref name="mediaType"/> and the <paramref name="encoding"/> are the most important.
        /// </summary>
        /// <param name="mediaType">Type of the media.</param>
        /// <param name="encoding">The encoding.</param>
        /// <param name="dataRequest">The data request.</param>
        /// <returns>
        /// The <see cref="IDataFormat" />.
        /// </returns>
        protected abstract IDataFormat BuildFormat(string mediaType, Encoding encoding, IRestDataRequest dataRequest);

        /// <summary>
        /// Builds the response content header.
        /// </summary>
        /// <param name="restDataRequest">The rest data request.</param>
        /// <param name="mediaType">Type of the media.</param>
        /// <param name="encoding">The encoding.</param>
        /// <returns>
        /// The <see cref="IResponseContentHeader" />.
        /// </returns>
        private IResponseContentHeader BuildResponseContentHeader(
            IRestDataRequest restDataRequest,
            string mediaType,
            Encoding encoding)
        {
            return new ResponseContentHeader(
                new ContentType(mediaType) { CharSet = encoding.WebName },
                new Lazy<string>(() => this.GetLanguage(restDataRequest)));
        }
    }
}