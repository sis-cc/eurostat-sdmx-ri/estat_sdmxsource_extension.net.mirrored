﻿// -----------------------------------------------------------------------
// <copyright file="IStructureSubmitFactory.cs" company="EUROSTAT">
//   Date Created : 2017-11-16
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using Estat.Sdmxsource.Extension.Engine;

namespace Estat.Sdmxsource.Extension.Factory
{
    /// <summary>
    /// The interface responsible for creating <see cref="IStructureSubmitterEngine"/>
    /// </summary>
    public interface IStructureSubmitFactory
    {
        /// <summary>
        /// Gets the engine for the specific store identified by <paramref name="storeId"/>.
        /// </summary>
        /// <param name="storeId">The store identifier.</param>
        /// <returns>The <see cref="IStructureSubmitterEngine"/> if <paramref name="storeId"/> is supported; otherwise null.</returns>
        IStructureSubmitterEngine GetEngine(string storeId);
    }
}
