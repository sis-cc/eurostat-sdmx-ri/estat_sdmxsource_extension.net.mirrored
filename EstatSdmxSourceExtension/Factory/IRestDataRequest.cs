﻿// -----------------------------------------------------------------------
// <copyright file="IRestDataRequest.cs" company="EUROSTAT">
//   Date Created : 2016-07-22
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sdmxsource.Extension.Factory
{
    using System.Net;

    using Estat.Sdmxsource.Extension.Model;

    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
    using Org.Sdmxsource.Sdmx.Api.Model.Data.Query;

    /// <summary>
    /// The REST Data Request interface.
    /// </summary>
    public interface IRestDataRequest
    {
        /// <summary>
        /// Gets the data query.
        /// This may be needed by formats like SDMX JSON but it is not needed for formats like SDMX v2.0/v2.1 XML 
        /// </summary>
        /// <value>
        /// The data query.
        /// </value>
        IDataQuery DataQuery { get; }

        /// <summary>
        /// Gets the headers from the request. Note they are mutable.
        ///  Use the <see cref="SortedAcceptHeaders"/> to get a sorted by quality and order Accept, Accept-Language and Accept-CharSet
        /// </summary>
        /// <value>
        /// The headers.
        /// </value>
        WebHeaderCollection Headers { get; }

        /// <summary>
        /// Gets the retrieval manager.
        /// This may be needed by formats like SDMX JSON but it is not needed for formats like SDMX v2.0/v2.1 XML
        /// </summary>
        /// <value>
        /// The retrieval manager.
        /// </value>
        ISdmxObjectRetrievalManager RetrievalManager { get; }

        /// <summary>
        /// Gets the sorted accept headers.
        /// </summary>
        /// <value>
        /// The sorted accept headers.
        /// </value>
        ISortedAcceptHeaders SortedAcceptHeaders { get; }
    }
}