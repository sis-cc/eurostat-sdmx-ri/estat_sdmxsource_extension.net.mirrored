// -----------------------------------------------------------------------
// <copyright file="AbstractStructureWriterPluginContract.cs" company="EUROSTAT">
//   Date Created : 2016-08-22
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sdmxsource.Extension.Factory
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Net.Mime;
    using System.Text;
    using System.Xml;

    using Estat.Sdmxsource.Extension.Model;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Format;
    using Org.Sdmxsource.Sdmx.Api.Model.Query;

    /// <summary>
    /// The abstract data writer plugin contract.
    /// </summary>
    public abstract class AbstractStructureWriterPluginContract : IStructureWriterPluginContract
    {
        /// <summary>
        /// The _supported media types.
        /// </summary>
        private readonly IList<string> _supportedMediaTypes;

        /// <summary>
        /// Initializes a new instance of the <see cref="AbstractStructureWriterPluginContract"/> class.
        /// </summary>
        /// <param name="supportedMediaTypes">
        /// The supported media types.
        /// </param>
        protected AbstractStructureWriterPluginContract(IList<string> supportedMediaTypes)
        {
            if (supportedMediaTypes == null)
            {
                throw new ArgumentNullException("supportedMediaTypes");
            }

            if (supportedMediaTypes.Count == 0)
            {
                throw new ArgumentException("Supported types are empty", "supportedMediaTypes");
            }

            this._supportedMediaTypes = supportedMediaTypes;
        }

        /// <summary>
        /// Gets the supported formats and header values.
        /// </summary>
        /// <param name="headers">The headers.</param>
        /// <param name="sortedAcceptHeaders">The sorted accept headers.</param>
        /// <param name="query">The query.</param>
        /// <returns>
        /// The list of supported formats - <see cref="IRestResponse{IStructureFormat}" />.
        /// </returns>
        public virtual IEnumerable<IRestResponse<IStructureFormat>> GetStructureFormats(WebHeaderCollection headers, ISortedAcceptHeaders sortedAcceptHeaders)
        {
            var encoding = this.BuildEncoding(sortedAcceptHeaders);

            return
                this._supportedMediaTypes.Select(
                    pair =>
                    new RestResponse<IStructureFormat>(
                        new Lazy<IStructureFormat>(() => this.BuildFormat(pair, headers, encoding, sortedAcceptHeaders)),
                        this.BuildResponseContentHeader(headers, sortedAcceptHeaders, pair, encoding)));
        }

        /// <inheritdoc cref="IStructureWriterPluginContract.GetAvailableStructureFormats"/>
        public IEnumerable<string> GetAvailableStructureFormats()
        {
            return this._supportedMediaTypes;
        }

        /// <summary>
        /// Gets the structure format.
        /// </summary>
        /// <param name="soapRequest">
        /// Type of the structure.
        /// </param>
        /// <param name="xmlWriter">
        /// The XML writer.
        /// </param>
        /// <returns>
        /// The <see cref="IStructureFormat"/>.
        /// </returns>
        public virtual IStructureFormat GetStructureFormat(
            ISoapRequest<StructureOutputFormat> soapRequest,
            XmlWriter xmlWriter)
        {
            return null;
        }

        /// <summary>
        /// Builds the encoding. Override this when you need to override the way the <see cref="Encoding"/> is selected.
        /// </summary>
        /// <param name="sortedAcceptHeaders">
        /// The sorted accept headers.
        /// </param>
        /// <returns>
        /// The <see cref="Encoding"/>.
        /// </returns>
        protected virtual Encoding BuildEncoding(ISortedAcceptHeaders sortedAcceptHeaders)
        {
            return sortedAcceptHeaders.CharSets.Select(quality => quality.Value).FirstOrDefault()
                   ?? new UTF8Encoding(false);
        }

        /// <summary>
        /// Gets the language. Override this to return a different language 
        /// </summary>
        /// <param name="headers">
        /// The headers.
        /// </param>
        /// <param name="acceptHeaders">
        /// The accept headers.
        /// </param>
        /// <returns>
        /// The language to return in Content-Language HTTP Header
        /// </returns>
        protected virtual string GetLanguage(WebHeaderCollection headers, ISortedAcceptHeaders acceptHeaders)
        {
            return null;
        }

        /// <summary>
        /// Builds the format. This is used only for REST. This needs to be overridden. The <paramref name="mediaType" /> and the <paramref name="encoding" /> are the most important.
        /// </summary>
        /// <param name="mediaType">Type of the media.</param>
        /// <param name="header">The header.</param>
        /// <param name="encoding">The encoding.</param>
        /// <param name="sortedAcceptHeaders">The sorted accept headers.</param>
        /// <param name="query">The query.</param>
        /// <returns>
        /// The <see cref="IStructureFormat" />.
        /// </returns>
        protected abstract IStructureFormat BuildFormat(string mediaType, WebHeaderCollection header, Encoding encoding, ISortedAcceptHeaders sortedAcceptHeaders);

        /// <summary>
        /// Builds the response content header.
        /// </summary>
        /// <param name="headers">
        /// The headers.
        /// </param>
        /// <param name="acceptHeaders">
        /// The accept headers.
        /// </param>
        /// <param name="mediaType">
        /// Type of the media.
        /// </param>
        /// <param name="encoding">
        /// The encoding.
        /// </param>
        /// <returns>
        /// The <see cref="IResponseContentHeader"/>.
        /// </returns>
        private IResponseContentHeader BuildResponseContentHeader(
            WebHeaderCollection headers,
            ISortedAcceptHeaders acceptHeaders,
            string mediaType,
            Encoding encoding)
        {
            return new ResponseContentHeader(
                new ContentType(mediaType) { CharSet = encoding.WebName },
                new Lazy<string>(() => this.GetLanguage(headers, acceptHeaders)));
        }
    }
}