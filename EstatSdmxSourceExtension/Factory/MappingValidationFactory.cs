﻿// -----------------------------------------------------------------------
// <copyright file="MappingValidationFactory.cs" company="EUROSTAT">
//   Date Created : 2018-01-12
//   Copyright (c) 2009, 2018 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sdmxsource.Extension.Factory
{
    using System.IO;
    using System.Text;

    using Engine;

    using Model;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Engine;
    using Org.Sdmxsource.Sdmx.Api.Factory;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
    using Org.Sdmxsource.Sdmx.Api.Model.Data;

    /// <inheritdoc />
    /// <summary>
    /// MappingValidationFactory
    /// </summary>
    /// <seealso cref="T:Org.Sdmxsource.Sdmx.Api.Factory.IDataWriterFactory" />
    public class MappingValidationFactory : IDataWriterFactory
    {
        private readonly ISdmxObjectRetrievalManager _sdmxObjectRetrievalManager;
        private readonly SdmxSchema _sdmxSchema;

        /// <summary>
        /// MappingValidationFactory
        /// </summary>
        /// <param name="sdmxSchema"></param>
        /// <param name="sdmxObjectRetrievalManager"></param>
        public MappingValidationFactory(SdmxSchema sdmxSchema, ISdmxObjectRetrievalManager sdmxObjectRetrievalManager)
        {
            this._sdmxSchema = sdmxSchema;
            this._sdmxObjectRetrievalManager = sdmxObjectRetrievalManager;
        }

        /// <inheritdoc />
        /// <summary>
        /// Gets the data writer engine.
        /// </summary>
        /// <param name="dataFormat">The data format.</param>
        /// <param name="outStream">The output stream.</param>
        /// <returns>
        /// The <see cref="T:Org.Sdmxsource.Sdmx.Api.Engine.IDataWriterEngine" />.
        /// </returns>
        public IDataWriterEngine GetDataWriterEngine(IDataFormat dataFormat, Stream outStream)
        {
            var mappingValidationFormat = dataFormat as MappingValidationFormat;
            if (mappingValidationFormat == null)
            {
                return null;
            }
            return new MappingValidationWriterEngine(outStream,this._sdmxObjectRetrievalManager,this._sdmxSchema);
        }
    }
}
