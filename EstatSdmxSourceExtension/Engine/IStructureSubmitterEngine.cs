﻿// -----------------------------------------------------------------------
// <copyright file="IStructureSubmitterEngine.cs" company="EUROSTAT">
//   Date Created : 2017-11-16
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using Estat.Sdmxsource.Extension.Model.Error;
using Org.Sdmxsource.Sdmx.Api.Model.Objects;
using System.Collections.Generic;

namespace Estat.Sdmxsource.Extension.Engine
{
    /// <summary>
    /// This interface handles requests for Appending/Replacing/Updating/Delete structural metadat
    /// </summary>
    public interface IStructureSubmitterEngine
    {
        /// <summary>
        /// Submits the <paramref name="sdmxObjects" /> to a store specific to the implementation. The action is determined by the <paramref name="sdmxObjects" /> <see cref="ISdmxObjects.Action" />
        /// </summary>
        /// <param name="sdmxObjects">The SDMX objects.</param>
        /// <returns>
        /// The list of <see cref="IResponseWithStatusObject" />
        /// </returns>
        IList<IResponseWithStatusObject> SubmitStructures(ISdmxObjects sdmxObjects);
    }
}
