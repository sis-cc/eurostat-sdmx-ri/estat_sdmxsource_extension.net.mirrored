// -----------------------------------------------------------------------
// <copyright file="MappingValidationWriterEngine.cs" company="EUROSTAT">
//   Date Created : 2018-01-03
//   Copyright (c) 2009, 2018 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using Estat.Sdmxsource.Extension.Constant;
using Estat.Sdmxsource.Extension.Model;
using Estat.Sdmxsource.Extension.Util;

namespace Estat.Sdmxsource.Extension.Engine
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;

    using Newtonsoft.Json;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Constants.InterfaceConstant;
    using Org.Sdmxsource.Sdmx.Api.Engine;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
    using Org.Sdmxsource.Sdmx.Api.Model;
    using Org.Sdmxsource.Sdmx.Api.Model.Data;
    using Org.Sdmxsource.Sdmx.Api.Model.Header;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Util.Date;
    using Org.Sdmxsource.Sdmx.Util.Objects;
    using Org.Sdmxsource.Util;

    /// <summary>
    /// A Mapping Validation engine
    /// </summary>
    /// <seealso cref="Org.Sdmxsource.Sdmx.Api.Engine.IDataWriterEngine" />
    public class MappingValidationWriterEngine : IDataWriterEngine
    {
        private readonly List<ICodelistObject> _codelists = new List<ICodelistObject>();
        private readonly Stream _outStream;
        private readonly ISdmxObjectRetrievalManager _sdmxObjectRetrievalManager;
        private readonly SdmxSchema _sdmxSchema;
        private IDataStructureObject _dsd;
        private readonly ValidationErrors _validationErrors = new ValidationErrors();
        private string _obsConcept;
        private readonly Dictionary<string, WrittenObservations> _writtenObservations = new Dictionary<string, WrittenObservations>(StringComparer.Ordinal);
        private WrittenObservations _seriesObservations;
        private readonly ReportingTimePeriod _timeReporting;


        /// <summary>
        /// Initializes a new instance of the <see cref="MappingValidationWriterEngine"/> class.
        /// </summary>
        /// <param name="outStream">The out stream.</param>
        /// <param name="sdmxObjectRetrievalManager">The SDMX object retrieval manager.</param>
        /// <param name="sdmxSchema">The SDMX schema.</param>
        public MappingValidationWriterEngine(Stream outStream, ISdmxObjectRetrievalManager sdmxObjectRetrievalManager,SdmxSchema sdmxSchema)
        {
            this._outStream = outStream;
            this._sdmxObjectRetrievalManager = sdmxObjectRetrievalManager;
            this._sdmxSchema = sdmxSchema;
            this._timeReporting = new ReportingTimePeriod();
        }

        /// <summary>
        /// write the validation errors to the provided stream
        /// </summary>
        /// <param name="footer">The footer.</param>
        public void Close(params IFooterMessage[] footer)
        {
            string message;

            if (ObjectUtil.ValidArray(footer))
            {
                StringBuilder sb = new StringBuilder();
                foreach(IFooterMessage footerMessage in footer)
                {
                    sb.Append(footerMessage.Code + " - ");
                    sb.AppendLine(string.Join(";", footerMessage.FooterText.Select(t => t.Value)));
                }
                message = sb.ToString();
            }
            else
            {
                message = JsonConvert.SerializeObject(this._validationErrors);
            }
            this._outStream.Write(new UTF8Encoding().GetBytes(message), 0, message.Length);
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }

        public void StartDataset(IDataflowObject dataflow, IDataStructureObject dsd, IDatasetHeader header, params IAnnotation[] annotations)
        {
            this._dsd = dsd;
            this._obsConcept = this._sdmxSchema == SdmxSchema.GetFromEnum(SdmxSchemaEnumType.VersionTwoPointOne)
                ? this.GetDimensionAtObservation(header)
                : ConceptRefUtil.GetConceptId(this._dsd.TimeDimension?.ConceptRef ?? this._dsd.GetDimensions().Last().ConceptRef);
        }


        protected virtual string GetDimensionAtObservation(IDatasetHeader header)
        {
            string dimensionAtObservation = DimensionObject.TimeDimensionFixedId;
            if (header != null && this._sdmxSchema.EnumType == SdmxSchemaEnumType.VersionTwoPointOne && !string.IsNullOrWhiteSpace(header.DataStructureReference?.DimensionAtObservation))
            {
                dimensionAtObservation = header.DataStructureReference.DimensionAtObservation;
            }

            return dimensionAtObservation;
        }


        public void StartGroup(string groupId, params IAnnotation[] annotations)
        {
        }

        public void StartSeries(params IAnnotation[] annotations)
        {
            this._seriesObservations = new WrittenObservations();
        }

        public void WriteAttributeValue(string id, string valueRen)
        {
            this.CheckValueValidity(id, valueRen);
            this.CheckTimeValue(id, valueRen);
        }

        public void WriteGroupKeyValue(string id, string valueRen)
        {
            this.CheckValueValidity(id, valueRen);
            this.CheckTimeValue(id, valueRen);
        }

        public void WriteHeader(IHeader header)
        {
        }

        public void WriteObservation(string obsConceptValue, string obsValue, params IAnnotation[] annotations)
        {
            this.WriteObservation(this._obsConcept, obsConceptValue, obsValue, annotations);
        }

        public void WriteObservation(string observationConceptId, string obsConceptValue, string observationValue, params IAnnotation[] annotations)
        {

            var currentSeriesKey = _seriesObservations.GenerateUniqueSeriesId();
            WrittenObservations existingObservations;
            if (_writtenObservations.TryGetValue(currentSeriesKey, out existingObservations))
            {
                if (!existingObservations.ObservationConceptValues.Add(obsConceptValue))
                {
                    this._validationErrors.DuplicateObservations.Add(
                        $"{this._seriesObservations.SeriesKey.Values.Aggregate((i, j) => i + ":" + j) + ":" + obsConceptValue}");
                }
            }
            else
            {
                this._seriesObservations.ObservationConceptValues.Add(obsConceptValue);
                this._writtenObservations.Add(currentSeriesKey, this._seriesObservations);
            }

            this.CheckTimeValue(observationConceptId, obsConceptValue);
        }

        public void WriteObservation(DateTime obsTime, string obsValue, TimeFormat sdmxSwTimeFormat, params IAnnotation[] annotations)
        {
            this.WriteObservation(DimensionObject.TimeDimensionFixedId, DateUtil.FormatDate(obsTime, sdmxSwTimeFormat), obsValue, annotations);
        }

        public void WriteSeriesKeyValue(string id, string valueRen)
        {
            this.CheckValueValidity(id, valueRen);
            this.CheckTimeValue(id,valueRen);
            this._seriesObservations.SeriesKey.Add(id,valueRen);
        }

        private void CheckValueValidity(string id, string valueRen)
        {
            var component = this._dsd.GetComponent(id);
            if (component != null)
            {
                if (component.HasCodedRepresentation())
                {
                    var representation = component.Representation.Representation;
                    var codelistObject = this.GetCodelist(representation);
                    if (codelistObject != null)
                    {
                        var codeById = codelistObject.GetCodeById(valueRen);
                        if (codeById == null)
                        {
                            if (this._validationErrors.CodelistErrors.ContainsKey(component.Id))
                            {
                                if (!this._validationErrors.CodelistErrors[component.Id].Contains(valueRen))
                                {
                                    this._validationErrors.CodelistErrors[component.Id].Add(valueRen);
                                }
                            }
                            else
                            {
                                this._validationErrors.CodelistErrors.Add(component.Id,new List<string> {valueRen});
                            }
                        }
                    }
                }
                if (component.Representation?.TextFormat != null)
                {
                    var textFormat = component.Representation.TextFormat;

                    var errors = TextFormatValidator.ValidateTextFormat(textFormat, valueRen, SdmxSchema.GetFromEnum(SdmxSchemaEnumType.Null), DataVersion.NULL, null);
                    if (errors != null && errors.Any())
                    {
                        var valueErrors = new ValueErrors
                        {
                            Value = valueRen,
                            Errors = errors
                        };
                        this._validationErrors.TextFormatErrors.Add(new ComponentValuesErrors
                            {ComponentId = component.Id,
                             ValuesErrors = new List<ValueErrors>()
                             {
                                 valueErrors
                             }});
                    }
                }
            }
        }

        private void CheckTimeValue(string id, string value)
        {
            if (id == DimensionObject.TimeDimensionFixedId)
            {
                try
                {
                    DateUtil.GetTimeFormatOfDate(value);
                }
                catch (Exception e)
                {
                    if (!this._timeReporting.CheckReportingPeriod(value, null))
                    {
                        this._validationErrors.TimePeriodErrors.Add($"{value}");
                    }
                }
            }

            
        }

        private ICodelistObject GetCodelist(IStructureReference representation)
        {
            var codelistToSearch = this._codelists.Find(item => item.Urn == representation.TargetUrn);
            if (codelistToSearch != null)
            {
                return codelistToSearch;
            }
            var codelist = this._sdmxObjectRetrievalManager.GetMaintainableObject<ICodelistObject>(representation);
            this._codelists.Add(codelist);
            return codelist;
        }

        public void WriteComplexAttributeValue(IKeyValue keyValue)
        {
           
        }

        public void WriteComplexMeasureValue(IKeyValue keyValue)
        {
            
        }

        public void WriteMeasureValue(string id, string value)
        {
        }

        public void WriteObservation(string obsConceptValue, params IAnnotation[] annotations)
        {
        }
    }
}