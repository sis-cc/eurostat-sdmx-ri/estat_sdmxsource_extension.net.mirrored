// -----------------------------------------------------------------------
// <copyright file="CsvDataWriterEngine.cs" company="EUROSTAT">
//   Date Created : 2016-09-26
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sdmxsource.Extension.Engine
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using CsvHelper;
    using Model;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Constants.InterfaceConstant;
    using Org.Sdmxsource.Sdmx.Api.Engine;
    using Org.Sdmxsource.Sdmx.Api.Model;
    using Org.Sdmxsource.Sdmx.Api.Model.Data;
    using Org.Sdmxsource.Sdmx.Api.Model.Header;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.Util.Objects;
    using Org.Sdmxsource.Util.Extensions;

    /// <summary>
    /// An implementation of <see cref="IDataWriterEngine"/> which writes to CSV
    /// </summary>
    public class CsvDataWriterEngine : IDataWriterEngine
    {
        /// <summary>
        /// The _final row
        /// </summary>
        private readonly List<string> _finalRow = new List<string>();

        /// <summary>
        /// The _header
        /// </summary>
        private readonly List<string> _header = new List<string>();

        /// <summary>
        /// The _obs
        /// </summary>
        private readonly List<string> _obs = new List<string>();

        /// <summary>
        /// The _row
        /// </summary>
        private readonly List<string> _row = new List<string>();

        /// <summary>
        /// The _writer
        /// </summary>
        private readonly CsvWriter _writer;

        /// <summary>
        /// The _CSV data format
        /// </summary>
        private readonly CsvDataFormat _csvDataFormat;

        /// <summary>
        /// The _has header
        /// </summary>
        private bool _hasHeader;

        /// <summary>
        /// The _disposed
        /// </summary>
        private bool _disposed;

        /// <summary>
        /// The _counter
        /// </summary>
        private int _counter;

        /// <summary>
        /// The _current position
        /// </summary>
        private Position _currentPosition;

        /// <summary>
        /// The _dimension at observation
        /// </summary>
        private string _dimensionAtObservation;

        /// <summary>
        /// Initializes a new instance of the <see cref="CsvDataWriterEngine"/> class.
        /// </summary>
        /// <param name="csvDataFormat">the data format</param>
        /// <param name="textWriter">the text writer</param>
        public CsvDataWriterEngine(CsvDataFormat csvDataFormat, TextWriter textWriter)
        {
            this._csvDataFormat = csvDataFormat;
            // set the CsvConfiguration
            var config = new CsvHelper.Configuration.CsvConfiguration(System.Globalization.CultureInfo.InvariantCulture)
            {
                LineBreakInQuotedFieldIsBadData = !this._csvDataFormat.CsvWriterOptions.FieldsCanBeQuoted,
            };

            if (!string.IsNullOrEmpty(this._csvDataFormat.CsvWriterOptions.FieldSeparator))
            {
                config.Delimiter = this._csvDataFormat.CsvWriterOptions.FieldSeparator;
            }

            this._writer = new CsvWriter(textWriter, config);
        }

        /// <summary>
        /// The position
        /// </summary>
        private enum Position
        {
            /// <summary>
            /// The dataset
            /// </summary>
            Dataset,
            
            /// <summary>
            /// The dataset attribute
            /// </summary>
            DatasetAttribute,
            
            /// <summary>
            /// The series key
            /// </summary>
            SeriesKey,
            
            /// <summary>
            /// The series key attribute
            /// </summary>
            SeriesKeyAttribute,
            
            /// <summary>
            /// The group
            /// </summary>
            Group,
            
            /// <summary>
            /// The group key
            /// </summary>
            GroupKey,
            
            /// <summary>
            /// The group key attribute
            /// </summary>
            GroupKeyAttribute,
            
            /// <summary>
            /// The observation
            /// </summary>
            Observation,
            
            /// <summary>
            /// The observation attribute
            /// </summary>
            ObservationAttribute
        }

        /// <summary>
        /// Writes the footer message (if supported by the writer implementation), and then completes the XML document, closes
        /// off all the tags, closes any resources.
        /// <b>NOTE</b> It is very important to close off a completed DataWriterEngine,
        /// as this ensures any output is written to the given location, and any resources are closed.  If this call
        /// is not made, the output document may be incomplete.
        /// <p><b> NOTE: </b> If writing a footer is not supported, then the footer will be silently ignored
        /// </p>
        /// Close also validates that the last series key or group key has been completed.
        /// </summary>
        /// <param name="footer">The footer.</param>
        public void Close(params IFooterMessage[] footer)
        {
            if (this._finalRow != null)
            {
                var rowArr = this._finalRow.ToArray();

                if (rowArr.Length > 0)
                {
                    this._writer.WriteList(rowArr);
                }

                this._finalRow.Clear();
                this._obs.Clear();
            }
        }

        /// <summary>
        /// Starts a dataset with the data conforming to the DSD
        /// </summary>
        /// <param name="dataflow">Optional. The dataflow can be provided to give extra information about the dataset.</param>
        /// <param name="dsd">The data structure is used to know the dimensionality of the data</param>
        /// <param name="header">Dataset header containing, amongst others, the dataset action, reporting dates,
        /// dimension at observation if null then the dimension at observation is assumed to be TIME_PERIOD and the dataset
        /// action is assumed to be INFORMATION</param>
        /// <param name="annotations">Any additional annotations that are attached to the dataset, can be null
        /// if no annotations exist</param>
        public void StartDataset(
            IDataflowObject dataflow,
            IDataStructureObject dsd,
            IDatasetHeader header,
            params IAnnotation[] annotations)
        {
            this.GetComponentId(dsd.PrimaryMeasure);
            this.GetComponentId(dsd.TimeDimension);
            this._dimensionAtObservation = header.DataStructureReference.DimensionAtObservation;

            this._currentPosition = Position.Dataset;
        }

        /// <summary>
        /// Starts a group with the given id, the subsequent calls to <c>writeGroupKeyValue</c> will write the id/values to
        /// this group.  After
        /// the group key is complete <c>writeAttributeValue</c> may be called to add attributes to this group.
        /// <p /><b>Example Usage</b>
        /// A group 'demo' is made up of 3 concepts (Country/Sex/Status), and has an attribute (Collection).
        /// <code>
        /// DataWriterEngine dre = //Create instance
        /// dre.startGroup("demo");
        /// dre.writeGroupKeyValue("Country", "FR");
        /// dre.writeGroupKeyValue("Sex", "M");
        /// dre.writeGroupKeyValue("Status", "U");
        /// dre.writeAttributeValue("Collection", "A");
        /// </code>
        /// Any subsequent calls, for example to start a series, or to start a new group, will close off this exiting group.
        /// </summary>
        /// <param name="groupId">Group Id</param>
        /// <param name="annotations">Annotations any additional annotations that are attached to the group, can be null if no
        /// annotations exist</param>
        public void StartGroup(string groupId, params IAnnotation[] annotations)
        {
            this._currentPosition = Position.Group;
        }

        /// <summary>
        /// Starts a new series, closes off any existing series keys or attribute/observations.
        /// </summary>
        /// <param name="annotations">Any additional annotations that are attached to the series, can be null if no annotations
        /// exist</param>
        public void StartSeries(params IAnnotation[] annotations)
        {
            this._currentPosition = Position.SeriesKey;
            this._row.Clear();

            this._counter++;

            if (this._counter == 2 && !this._hasHeader)
            {
                this.WriteCsvHeader();
                this._hasHeader = true;
            }
        }

        /// <summary>
        /// Writes the attribute value.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="value">The value.</param>
        public void WriteAttributeValue(string id, string value)
        {
            switch (this._currentPosition)
            {
                case Position.SeriesKey:
                    this._currentPosition = Position.SeriesKeyAttribute;
                    break;
                case Position.Observation:
                    this._currentPosition = Position.ObservationAttribute;
                    break;
            }

            if (this._counter == 1)
            {
                this._header.Add(id);
            }

            if (this._currentPosition == Position.ObservationAttribute)
            {
                this._finalRow.Add(value);
            }
            else
            {
                this._row.Add(value);
            }
        }

        /// <summary>
        /// Writes the group key value.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="value">The value.</param>
        public void WriteGroupKeyValue(string id, string value)
        {
        }

        /// <summary>
        /// (Optional) Writes a header to the message, any missing mandatory attributes are defaulted.  If a header is required
        /// for a message, then this
        /// call should be the first call on a WriterEngine
        /// <p />
        /// If this method is not called, and a message requires a header, a default header will be generated.
        /// </summary>
        /// <param name="header">The header.</param>
        public void WriteHeader(IHeader header)
        {
        }

        /// <summary>
        /// Writes an observation, the observation concept is assumed to be that which has been defined to be at the
        /// observation level (as declared in the start dataset method DatasetHeaderObject).
        /// </summary>
        /// <param name="obsConceptValue">May be the observation time, or the cross section value</param>
        /// <param name="obsValue">The observation value - can be numerical</param>
        /// <param name="annotations">Any additional annotations that are attached to the observation, can be null if no
        /// annotations exist</param>
        public void WriteObservation(
            string obsConceptValue,
            string obsValue,
            params IAnnotation[] annotations)
        {
            this.WriteObservation(this._dimensionAtObservation ?? DimensionObject.TimeDimensionFixedId, obsConceptValue, obsValue, annotations);
        }

        /// <summary>
        /// Writes an Observation value against the current series.
        /// <p />
        /// The current series is determined by the latest writeKeyValue call,
        /// If this is a cross sectional dataset, then the observation Concept is expected to be the cross sectional concept
        /// value - for
        /// example if this is cross sectional on Country the id may be "FR"
        /// If this is a time series dataset then the observation Concept is expected to be the observation time - for example
        /// 2006-12-12
        /// <p />
        /// Validates the following:
        /// <ul><li>The observation Time string format is one of an allowed SDMX time format</li><li>The observation Time does not replicate a previously reported observation Time for the current series</li></ul>
        /// </summary>
        /// <param name="observationConceptId">the concept id for the observation, for example 'COUNTRY'.  If this is a Time
        /// Series, then the id will be DimensionBean.TIME_DIMENSION_FIXED_ID.</param>
        /// <param name="obsConceptValue">may be the observation time, or the cross section value</param>
        /// <param name="obsValue">The observation value - can be numerical</param>
        /// <param name="annotations">Any additional annotations that are attached to the observation, can be null if no
        /// annotations exist</param>
        public void WriteObservation(
            string observationConceptId,
            string obsConceptValue,
            string obsValue,
            params IAnnotation[] annotations)
        {
            switch (this._currentPosition)
            {
                case Position.ObservationAttribute:
                    if (!this._hasHeader)
                    {
                        this.WriteCsvHeader();
                        this._hasHeader = true;
                    }

                    break;
            }

            if (this._finalRow != null)
            {
                var rowArr = this._finalRow.ToArray();

                if (rowArr.Length > 0)
                {
                    this._writer.WriteList(rowArr);
                }

                this._finalRow.Clear();
                this._obs.Clear();
            }

            this._currentPosition = Position.Observation;

            if (this._counter == 1)
            {
                this._header.Add(observationConceptId);
                this._header.Add(PrimaryMeasure.FixedId);
            }

            this._obs.Add(obsConceptValue);
            this._obs.Add(obsValue);
            this._finalRow.AddAll(this._row);
            this._finalRow.AddAll(this._obs);
        }

        /// <summary>
        /// Writes the observation.
        /// </summary>
        /// <param name="obsTime">The obs time.</param>
        /// <param name="obsValue">The obs value.</param>
        /// <param name="sdmxTimeFormat">The SDMX time format.</param>
        /// <param name="annotations">The annotations.</param>
        public void WriteObservation(
            DateTime obsTime,
            string obsValue,
            TimeFormat sdmxTimeFormat, 
            params IAnnotation[] annotations)
        {
        }

        /// <summary>
        /// Writes the series key value.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="value">The value.</param>
        public void WriteSeriesKeyValue(string id, string value)
        {
            this._currentPosition = Position.SeriesKeyAttribute;

            if (this._counter == 1)
            {
                this._header.Add(id);
            }

            this._row.Add(value);
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        ///     Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposable">
        ///     <c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only
        ///     unmanaged resources.
        /// </param>
        private void Dispose(bool disposable)
        {
            if (!this._disposed)
            {
                if (disposable)
                {
                    this._writer.Dispose();
                }

                this._disposed = true;
            }
        }

        /// <summary>
        /// Gets the component identifier.
        /// </summary>
        /// <param name="component">The component.</param>
        /// <returns>the component id</returns>
        private string GetComponentId(IComponent component)
        {
            if (component == null)
            {
                return null;
            }

            if (this.IsTwoPointOne())
            {
                return component.Id;
            }

            return ConceptRefUtil.GetConceptId(component.ConceptRef);
        }

        /// <summary>
        /// Determines whether [is two point one].
        /// </summary>
        /// <returns>true if it is 2.1</returns>
        private bool IsTwoPointOne()
        {
            return this._csvDataFormat.SdmxDataFormat.SchemaVersion == SdmxSchemaEnumType.VersionTwoPointOne;
        }

        /// <summary>
        /// Writes the CSV header.
        /// </summary>
        private void WriteCsvHeader()
        {
            var headerArr = this._header.ToArray();
            this._writer.WriteList(headerArr);
        }

        public void WriteComplexAttributeValue(IKeyValue keyValue)
        {
            throw new NotImplementedException();
        }

        public void WriteComplexMeasureValue(IKeyValue keyValue)
        {
            throw new NotImplementedException();
        }

        public void WriteMeasureValue(string id, string value)
        {
            throw new NotImplementedException();
        }

        public void WriteObservation(string obsConceptValue, params IAnnotation[] annotations)
        {
            throw new NotImplementedException();
        }
    }
}