// -----------------------------------------------------------------------
// <copyright file="SoapMessageSubmitter.cs" company="EUROSTAT">
//   Date Created : 2017-12-4
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sdmxsource.Extension.Engine.WebService
{
    using System;
    using System.Net;
    using System.Net.Mime;
    using System.Xml;
    using System.Xml.Linq;

    using Estat.Sdmxsource.Extension.Constant;
    using Estat.Sdmxsource.Extension.Model;
    using Estat.Sdmxsource.Extension.Util;

    /// <summary>
    /// A SOAP v1.1 message writer. 
    /// </summary>
    public class SoapMessageSubmitter : ISoapMessageSubmitterEngine
    {
        /// <summary>
        /// The WSDL configuration
        /// </summary>
        private readonly WSDLSettings _wsdlConfig;
        
        /// <summary>
        /// The configuration
        /// </summary>
        private readonly WsInfo _config;

        /// <summary>
        /// The SOAP message writer
        /// </summary>
        private readonly ISoapMessageWriter _soapMessageWriter;

        /// <summary>
        /// Initializes a new instance of the <see cref="SoapMessageSubmitter" /> class.
        /// </summary>
        /// <param name="config">The configuration.</param>
        /// <param name="soapMessageWriter">The SOAP message writer.</param>
        /// <exception cref="ArgumentNullException">
        /// config
        /// or
        /// soapMessageWriter
        /// </exception>
        public SoapMessageSubmitter(WsInfo config, ISoapMessageWriter soapMessageWriter)
        {
            if (config == null)
            {
                throw new ArgumentNullException(nameof(config));
            }

            if (soapMessageWriter == null)
            {
                throw new ArgumentNullException(nameof(soapMessageWriter));
            }

            this._wsdlConfig = new WSDLSettings(config);
            this._config = config;
            this._soapMessageWriter = soapMessageWriter;
        }

        /// <summary>
        /// Writes the SOAP message with the specified body writer.
        /// </summary>
        /// <param name="bodyWriter">The body writer.</param>
        /// <param name="operation">The operation.</param>
        /// <returns>The Web Response</returns>
        public WebResponse Submit(Action<XmlWriter> bodyWriter, string operation)
        {
            bool writeOperation = this._wsdlConfig.CheckOperationExists(operation);
            string parameterName = this._wsdlConfig.GetParameterName(operation);
            var targetNamespace = this._wsdlConfig.GetTargetNamespace();
            var request = this.GetWebRequest(operation);
            SoapRequestInfo soapRequestInfo = new SoapRequestInfo()
            {
                Operation = operation,
                WriteOperation = writeOperation,
                ParameterName = parameterName,
                TargetNamespace = targetNamespace,
                Prefix = this._config.Prefix
            };

            using (var stream = request.GetRequestStream())
            {
                this._soapMessageWriter.Write(bodyWriter, stream, soapRequestInfo);
                stream.Flush();
            }

            return request.GetResponse();
        }

        /// <summary>
        /// Gets the web request.
        /// </summary>
        /// <param name="webServiceOperation">The web service operation.</param>
        /// <returns>The <see cref="HttpWebRequest"/></returns>
        private HttpWebRequest GetWebRequest(string webServiceOperation)
        {
            var endpointUri = new Uri(this._config.EndPoint);
            var webRequest = (HttpWebRequest)WebRequest.Create(endpointUri);
            // We normally don't need this.
//            webRequest.Headers.Add("Accept-Encoding", "gzip, deflate");
            webRequest.AutomaticDecompression = DecompressionMethods.Deflate | DecompressionMethods.GZip;
            webRequest.Accept = MediaTypeNames.Text.Xml;

            // Workaround for Eurostat Registry (The reverse proxy at European Comission seems to need that)
            webRequest.UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.17 (KHTML, like Gecko) Chrome/24.0.1312.57 Safari/537.17";
            var soapAction = this._wsdlConfig.GetSoapAction(webServiceOperation);
            if (soapAction != null)
            {
                webRequest.Headers.Add(SoapConstants.SoapAction, soapAction);
            }

            webRequest.ContentType = HttpConstants.Content;

            webRequest.Method = HttpConstants.Post;
            // Authentication
            WebAuthenticationHelper.SetupWebRequestAuth(this._config, webRequest);

            // TODO Configuration option
            webRequest.Timeout = 1800 * 1000;
            return webRequest;
        }
    }
}
