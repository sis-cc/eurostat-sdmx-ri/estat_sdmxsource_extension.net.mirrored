// -----------------------------------------------------------------------
// <copyright file="SoapMessageWriterEngine.cs" company="EUROSTAT">
//   Date Created : 2017-12-4
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sdmxsource.Extension.Engine.WebService
{
    using System;
    using System.Text;
    using System.Xml;

    using Estat.Sdmxsource.Extension.Constant;
    using Estat.Sdmxsource.Extension.Model;

    /// <summary>
    ///  Writes a SOAP message to the stream
    /// </summary>
    /// <seealso cref="ISoapMessageWriter" />
    public class SoapMessageWriterEngine : ISoapMessageWriter
    {
        /// <summary>
        /// Writes the specified body writer.
        /// </summary>
        /// <param name="bodyWriter">The body writer.</param>
        /// <param name="stream">The stream.</param>
        /// <param name="soapRequestInfo">The SOAP request information.</param>
        public void Write(Action<XmlWriter> bodyWriter, System.IO.Stream stream, SoapRequestInfo soapRequestInfo)
        {
            var parameterName = soapRequestInfo.ParameterName;
            var operation = soapRequestInfo.Operation;
            var writeOperation = soapRequestInfo.WriteOperation;
            var targetNamespace = soapRequestInfo.TargetNamespace;
            string prefix = soapRequestInfo.Prefix;
            
            XmlWriterSettings settings = new XmlWriterSettings { NamespaceHandling = NamespaceHandling.OmitDuplicates, Indent = true, Encoding = new UTF8Encoding(false) };
            var hasParameterName = !string.IsNullOrWhiteSpace(parameterName);
            using (XmlWriter writer = XmlWriter.Create(stream, settings))
            {
                writer.WriteStartDocument();
                writer.WriteStartElement(SoapConstants.SoapPrefix, SoapConstants.Envelope, SoapConstants.Soap11Ns);
                writer.WriteStartElement(SoapConstants.SoapPrefix, SoapConstants.Header, SoapConstants.Soap11Ns);
                writer.WriteEndElement(); // Header

                writer.WriteStartElement(SoapConstants.SoapPrefix, SoapConstants.Body, SoapConstants.Soap11Ns);

                // Operation it should be present except in Eurostat Registry SDMX v2.0 endpoints
                if (writeOperation)
                {
                    writer.WriteStartElement(prefix, operation, targetNamespace);
                }

                // parameter name (used in ASMX based .NET web services
                if (hasParameterName)
                {
                    writer.WriteStartElement(prefix, parameterName, targetNamespace);
                }

                bodyWriter(writer);

                if (hasParameterName)
                {
                    writer.WriteEndElement(); // parameter name
                }

                if (writeOperation)
                {
                    writer.WriteEndElement(); // operation name 
                }

                writer.WriteEndElement(); // Body

                writer.WriteEndElement(); // Envelope
                writer.WriteEndDocument();
                writer.Flush();
            }
        }
    }
}
