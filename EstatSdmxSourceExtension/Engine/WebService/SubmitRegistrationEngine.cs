// -----------------------------------------------------------------------
// <copyright file="SubmitRegistrationEngine.cs" company="EUROSTAT">
//   Date Created : 2017-11-28
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sdmxsource.Extension.Engine.WebService
{
    using System.Collections.Generic;
    using System.IO;

    using Estat.Sdmxsource.Extension.Extension;

    using log4net;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Registry;
    using Org.Sdmxsource.Sdmx.Api.Model.SubmissionResponse;
    using Org.Sdmxsource.Sdmx.Structureparser.Builder.XmlSerialization;
    using Org.Sdmxsource.Sdmx.Structureparser.Builder.XmlSerialization.V21.RegistryRequest;
    using Org.Sdmxsource.Sdmx.Util.Sdmx;

    /// <summary>
    /// This class is responsible for sending a SDMX v2.1 Data Registration to a Registry via SOAP
    /// </summary>
    public class SubmitRegistrationEngine
    {
        /// <summary>
        /// The log
        /// </summary>
        private static readonly ILog _log = LogManager.GetLogger(typeof(SubmitRegistrationEngine));

        /// <summary>
        /// The builder
        /// </summary>
        private readonly SubmitRegistrationBuilder _builder = new SubmitRegistrationBuilder();
        /// <summary>
        /// The submit registration response builder
        /// </summary>
        private readonly SubmitRegistrationResponseBuilder _submitRegistrationResponseBuilder = new SubmitRegistrationResponseBuilder();

        /// <summary>
        /// Registers the specified writer engine.
        /// </summary>
        /// <param name="writerEngine">The writer engine.</param>
        /// <param name="registration">The registration.</param>
        /// <param name="datasetAction">The dataset action.</param>
        /// <returns>The list of  <see cref="ISubmitRegistrationResponse"/></returns>
        public IList<ISubmitRegistrationResponse> Register(IMessageSubmitterEngine writerEngine, IList<IRegistrationObject> registration, DatasetAction datasetAction)
        {
            var registryInterface = this._builder.BuildRegistryInterfaceDocument(registration, datasetAction);

            using (var dataLocation = writerEngine.Submit(registryInterface.Untyped.GetWriterAction()))
            {
                var sdmxMessageType = SdmxMessageUtil.GetMessageType(dataLocation);
                if (sdmxMessageType == MessageEnumType.Error)
                {
                    if (_log.IsDebugEnabled)
                    {
                        _log.Debug("Got Error Response");
                        using (var reader = new StreamReader(dataLocation.InputStream))
                        {
                            var readToEnd = reader.ReadToEnd();
                            _log.Debug(readToEnd);
                        }
                    }

                    SdmxMessageUtil.ParseSdmxErrorMessage(dataLocation);
                }

                return this._submitRegistrationResponseBuilder.Build(dataLocation);
            }
        }
    }
}
