// -----------------------------------------------------------------------
// <copyright file="IMessageSubmitterEngine.cs" company="EUROSTAT">
//   Date Created : 2018-1-18
//   Copyright (c) 2009, 2018 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sdmxsource.Extension.Engine.WebService
{
    using System;
    using System.Xml;

    using Org.Sdmxsource.Sdmx.Api.Util;

    /// <summary>
    /// A XML message writer engine interface
    /// </summary>
    public interface IMessageSubmitterEngine
    {
        /// <summary>
        /// Writes the specified body writer.
        /// </summary>
        /// <param name="bodyWriter">The body writer.</param>
        /// <returns>The WebResponse.</returns>
        IReadableDataLocation Submit(Action<XmlWriter> bodyWriter);
    }
}