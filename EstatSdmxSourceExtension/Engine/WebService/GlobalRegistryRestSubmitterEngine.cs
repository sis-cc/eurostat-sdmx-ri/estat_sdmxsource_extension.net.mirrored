// -----------------------------------------------------------------------
// <copyright file="GlobalRegistryRestSubmitterEngine.cs" company="EUROSTAT">
//   Date Created : 2018-1-18
//   Copyright (c) 2009, 2018 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sdmxsource.Extension.Engine.WebService
{
    using System;
    using System.Net;
    using System.Net.Mime;
    using System.Text;
    using System.Xml;

    using Estat.Sdmxsource.Extension.Constant;
    using Estat.Sdmxsource.Extension.Model;
    using Estat.Sdmxsource.Extension.Util;
    using Estat.Sri.SdmxXmlConstants;

    using Org.Sdmxsource.Sdmx.Api.Util;

    /// <summary>
    /// A Registry interface submitter for Global Registry
    /// </summary>
    /// <seealso cref="IMessageSubmitterEngine" />
    public class GlobalRegistryRestSubmitterEngine : IMessageSubmitterEngine
    {
        /// <summary>
        /// The configuration
        /// </summary>
        private readonly WsInfo _config;

        /// <summary>
        /// Initializes a new instance of the <see cref="GlobalRegistryRestSubmitterEngine"/> class.
        /// </summary>
        /// <param name="config">The configuration.</param>
        public GlobalRegistryRestSubmitterEngine(WsInfo config)
        {
            this._config = config;
        }

        /// <summary>
        /// Writes the specified body writer.
        /// </summary>
        /// <param name="bodyWriter">The body writer.</param>
        /// <returns>
        /// The WebResponse.
        /// </returns>
        public IReadableDataLocation Submit(Action<XmlWriter> bodyWriter)
        {
            var request = this.GetWebRequest();
            XmlWriterSettings settings = new XmlWriterSettings { NamespaceHandling = NamespaceHandling.OmitDuplicates, Indent = true, Encoding = new UTF8Encoding(false) };
            using (var stream = request.GetRequestStream())
            {
                using (var writer = XmlWriter.Create(stream, settings))
                {
                    writer.WriteStartDocument();
                    bodyWriter(writer);
                    writer.WriteEndDocument();
                    writer.Flush();
                }

                stream.Flush();
            }

            return NsiClientHelper.GetResponseDataLocationFromRest(request);
        }

        /// <summary>
        /// Gets the web request.
        /// </summary>
        /// <returns>The <see cref="HttpWebRequest"/></returns>
        private HttpWebRequest GetWebRequest()
        {
            var endpointUri = new Uri(this._config.EndPoint);
            var webRequest = (HttpWebRequest)WebRequest.Create(endpointUri);
            // We normally don't need this.
            //            webRequest.Headers.Add("Accept-Encoding", "gzip, deflate");
            webRequest.AutomaticDecompression = DecompressionMethods.Deflate | DecompressionMethods.GZip;
            webRequest.Accept = "application/xml";

            // Workaround for Eurostat Registry (The reverse proxy at European Comission seems to need that)
            webRequest.UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.17 (KHTML, like Gecko) Chrome/24.0.1312.57 Safari/537.17";

            webRequest.ContentType = HttpConstants.ApplicationXml;

            webRequest.Method = HttpConstants.Post;
            // Authentication
            WebAuthenticationHelper.SetupWebRequestAuth(this._config, webRequest);

            // TODO Configuration option
            webRequest.Timeout = 1800 * 1000;
            return webRequest;
        }
    }
}