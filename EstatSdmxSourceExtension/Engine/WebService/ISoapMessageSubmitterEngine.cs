// -----------------------------------------------------------------------
// <copyright file="ISoapMessageSubmitterEngine.cs" company="EUROSTAT">
//   Date Created : 2017-12-4
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sdmxsource.Extension.Engine.WebService
{
    using System;
    using System.Net;
    using System.Xml;
    using System.Xml.Linq;

    /// <summary>
    /// This interface is responsible for submitting SOAP messages
    /// </summary>
    public interface ISoapMessageSubmitterEngine
    {
        /// <summary>
        /// Writes the specified body writer.
        /// </summary>
        /// <param name="bodyWriter">The body writer.</param>
        /// <param name="operation">The operation.</param>
        /// <returns>The WebResponse.</returns>
        WebResponse Submit(Action<XmlWriter> bodyWriter, string operation);
    }
}