using System;
using System.Linq;

namespace Estat.Sdmxsource.Extension.Engine
{
    using System.Collections.Generic;

    internal class WrittenObservations   
    {
        public Dictionary<string,string> SeriesKey { get; set; } = new Dictionary<string, string>(StringComparer.Ordinal);
        public ISet<string> ObservationConceptValues { get; set; } = new HashSet<string>(StringComparer.Ordinal);

        public string GenerateUniqueSeriesId()
        {
            return string.Join(
                ":",
                SeriesKey.OrderBy(pair => pair.Key, StringComparer.Ordinal).Select(pair => pair.Value));
        }
    }
}