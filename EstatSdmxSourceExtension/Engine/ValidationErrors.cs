using Estat.Sdmxsource.Extension.Model;

namespace Estat.Sdmxsource.Extension.Engine
{
    using System.Collections.Generic;

    internal class ValidationErrors
    {
        public Dictionary<string, List<string>> CodelistErrors { get; set; } = new Dictionary<string, List<string>>();
        public List<string> DuplicateObservations { get; set; } = new List<string>();
        public List<string> TimePeriodErrors { get; set; } = new List<string>();
        public List<ComponentValuesErrors> TextFormatErrors { get; set; } = new List<ComponentValuesErrors>();
    }
}