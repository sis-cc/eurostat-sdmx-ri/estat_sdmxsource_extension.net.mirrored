// -----------------------------------------------------------------------
// <copyright file="IHeaderRetrieverEngine.cs" company="EUROSTAT">
//   Date Created : 2013-04-10
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or � as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sdmxsource.Extension.Engine
{
    using System;

    using Org.Sdmxsource.Sdmx.Api.Model.Data.Query;
    using Org.Sdmxsource.Sdmx.Api.Model.Header;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;

    /// <summary>
    ///     An interface that retrieves SDMX Header information.
    /// </summary>
    public interface IHeaderRetrieverEngine
    {
        /// <summary>
        ///     This method queries the mapping store for header information for a specific dataflow
        /// </summary>
        /// <param name="dataQuery">
        ///     The <see cref="IDataQuery" /> object
        /// </param>
        /// <param name="beginDate">
        ///     For ReportingBegin element
        /// </param>
        /// <param name="endDate">
        ///     For ReportingEnd element
        /// </param>
        /// <returns>
        ///     A <see cref="IHeader" /> object. Otherwise null
        /// </returns>
        IHeader GetHeader(IBaseDataQuery dataQuery, DateTime? beginDate, DateTime? endDate);

        /// <summary>
        ///     This method queries the mapping store for header information for a specific dataflow
        /// </summary>
        /// <param name="dataflow">
        ///     The <see cref="IDataflowObject" /> object
        /// </param>
        /// <param name="beginDate">
        ///     For ReportingBegin element
        /// </param>
        /// <param name="endDate">
        ///     For ReportingEnd element
        /// </param>
        /// <returns>
        ///     A <see cref="IHeader" /> object. Otherwise null
        /// </returns>
        IHeader GetHeader(IMaintainableObject dataflow, DateTime? beginDate, DateTime? endDate);
    }
}