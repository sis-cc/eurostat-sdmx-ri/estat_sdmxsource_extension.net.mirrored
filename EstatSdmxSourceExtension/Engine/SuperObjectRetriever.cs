namespace Estat.Sdmxsource.Extension.Engine
{
    using Factory;

    using log4net;
    
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
    using Org.Sdmxsource.Sdmx.Api.Model.Data.Query;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects;
    using Org.Sdmxsource.Sdmx.Structureparser.Builder.SuperObjects;
    using Org.Sdmxsource.Sdmx.StructureRetrieval.Manager;
    using Org.Sdmxsource.Sdmx.Util.Objects.Container;
    using Org.Sdmxsource.Translator;

    /// <summary>
    /// SuperObjectRetriever
    /// </summary>
    public class SuperObjectRetriever
    {
        /// <summary>
        /// The log
        /// </summary>
        private static readonly ILog _log = LogManager.GetLogger(typeof(SuperObjectRetriever));

        /// <summary>
        /// The _data request
        /// </summary>
        private readonly IRestDataRequest _dataRequest;

        /// <summary>
        /// The _super objects builder
        /// </summary>
        private readonly SuperObjectsBuilder _superObjectsBuilder;

        /// <summary>
        /// The translator factory
        /// </summary>
        private readonly ITranslatorFactory _translatorFactory;

        /// <summary>
        /// The retrieval manager
        /// </summary>
        private ISdmxSuperObjectRetrievalManager _retrievalManager;

        /// <summary>
        /// The _SDMX objects
        /// </summary>
        private ISuperObjects _superObjects;

        /// <summary>
        /// The _translator
        /// </summary>
        private ITranslator _translator;

        /// <summary>
        /// Initializes a new instance of the <see cref="SuperObjectRetriever"/> class.
        /// </summary>
        /// <param name="dataRequest">The data request.</param>
        /// <param name="superObjectsBuilder">The super objects builder.</param>
        /// <param name="translatorFactory"></param>
        public SuperObjectRetriever(IRestDataRequest dataRequest, SuperObjectsBuilder superObjectsBuilder, ITranslatorFactory translatorFactory)
        {
            this._dataRequest = dataRequest;
            this._superObjectsBuilder = superObjectsBuilder;
            this._translatorFactory = translatorFactory;
        }

        /// <summary>
        /// Gets the retrieval manager
        /// </summary>
        /// <value>
        /// The retrieval manager.
        /// </value>
        public ISdmxSuperObjectRetrievalManager RetrievalManager
        {
            get
            {
                if (this._retrievalManager == null)
                {
                    _log.Debug("plugin requested ISdmxSuperObjectRetrievalManager");
                    this._retrievalManager = new InMemorySdmxSuperObjectRetrievalManager(this.SuperObjects);
                    _log.Debug("plugin returned ISdmxSuperObjectRetrievalManager");
                }

                return this._retrievalManager;
            }
        }

        /// <summary>
        /// Gets the translator.
        /// </summary>
        /// <value>
        /// The translator.
        /// </value>
        public ITranslator Translator
        {
            get
            {
                if (this._translator == null)
                {
                    _log.Debug("plugin requested Translator");
                    this._translator = this._translatorFactory.GetTranslator(this.SuperObjects, this._dataRequest.SortedAcceptHeaders.Languages);
                    _log.Debug("plugin returned Translator");
                }

                return this._translator;
            }
        }

        /// <summary>
        /// Gets the super objects.
        /// </summary>
        /// <value>
        /// The super objects.
        /// </value>
        private ISuperObjects SuperObjects
        {
            get
            {
                if (this._superObjects == null)
                {
                    _log.Debug("plugin requested SuperObjects");
                    this._superObjects = this.GetSuperObjects();
                    _log.Debug("plugin returned SuperObjects");
                }

                return this._superObjects;
            }
        }

        /// <summary>
        /// Gets the super objects.
        /// </summary>
        /// <returns>
        /// The <see cref="ISuperObjects"/>.
        /// </returns>
        private ISuperObjects GetSuperObjects()
        {
            ISdmxObjects existingObjects = new SdmxObjectsImpl();
            IDataQueryV2 dataQueryV2 = this._dataRequest.DataQuery as IDataQueryV2;
            if (dataQueryV2 != null)
            {
                var iterator = dataQueryV2.StartIterator();
                while (iterator.MoveNext())
                {
                    existingObjects.AddDataStructure(this._dataRequest.DataQuery.DataStructure);
                    existingObjects.AddDataflow(this._dataRequest.DataQuery.Dataflow);
                }
            }
            else
            {
                existingObjects.AddDataStructure(this._dataRequest.DataQuery.DataStructure);
                existingObjects.AddDataflow(this._dataRequest.DataQuery.Dataflow);
            }

            _log.Debug("Building Super Objects");
            _log.Debug("And ... Requesting structural metadata");
            var superObjects = this._superObjectsBuilder.Build(existingObjects, null, this._dataRequest.RetrievalManager);
            _log.Debug("Building Super Objects - Done");
            return superObjects;
        }
    }
}