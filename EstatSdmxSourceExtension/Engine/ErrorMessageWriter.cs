// -----------------------------------------------------------------------
// <copyright file="ErrorMessageWriter.cs" company="EUROSTAT">
//   Date Created : 2017-11-15
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sdmxsource.Extension.Engine
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Xml;

    using Estat.Sdmxsource.Extension.Model.Error;
    using Estat.Sri.SdmxXmlConstants;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;

    /// <summary>
    /// The error message writer.
    /// </summary>
    class ErrorMessageWriter : IResponseMessageWriter
    {
        /// <summary>
        /// The XML writer
        /// </summary>
        private readonly XmlWriter _xmlWriter;

        /// <summary>
        /// The should close writer
        /// </summary>
        private readonly bool _shouldCloseWriter;

        /// <summary>
        /// The text type writer
        /// </summary>
        private readonly TextTypeWriter _textTypeWriter = new TextTypeWriter(nameof(ElementNameTable.Text), PrefixConstants.Common, SdmxConstants.CommonNs21);

        /// <summary>
        /// The disposed
        /// </summary>
        private bool _disposed;

        /// <summary>
        /// Initializes a new instance of the <see cref="ErrorMessageWriter"/> class.
        /// </summary>
        /// <param name="xmlWriter">The XML writer.</param>
        /// <exception cref="ArgumentNullException">xmlWriter is <c>null</c></exception>
        public ErrorMessageWriter(XmlWriter xmlWriter)
        {
            if (xmlWriter == null)
            {
                throw new ArgumentNullException(nameof(xmlWriter));
            }

            _xmlWriter = xmlWriter;
            _shouldCloseWriter = false;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ErrorMessageWriter"/> class.
        /// </summary>
        /// <param name="outputStream">The output stream.</param>
        /// <param name="encoding">The encoding.</param>
        /// <exception cref="ArgumentNullException">outputStream is <c>null</c></exception>
        public ErrorMessageWriter(Stream outputStream, Encoding encoding)
        {
            if (outputStream == null)
            {
                throw new ArgumentNullException(nameof(outputStream));
            }

            encoding = encoding ?? Encoding.UTF8;

            var settings = new XmlWriterSettings()
            {
                Encoding = encoding,
                Indent = true
            };

            _shouldCloseWriter = true;
            _xmlWriter = XmlWriter.Create(outputStream, settings);
        }

        /// <summary>
        /// Writes the specified status objects.
        /// </summary>
        /// <param name="statusObjects">The status objects.</param>
        /// <exception cref="ObjectDisposedException">this object is disposed</exception>
        /// <exception cref="ArgumentNullException">statusObjects is <c>null</c></exception>
        /// <exception cref="ArgumentException">Cannot write empty response</exception>
        public void Write(IList<IResponseWithStatusObject> statusObjects)
        {
            if (_disposed)
            {
                throw new ObjectDisposedException(this.GetType().Name);
            }

            if (statusObjects == null)
            {
                throw new ArgumentNullException(nameof(statusObjects));
            }

            if (statusObjects.Count == 0)
            {
                throw new ArgumentException("Cannot write empty response");
            }


            if (_xmlWriter.WriteState == WriteState.Start)
            {
                _xmlWriter.WriteStartDocument();
            }

            _xmlWriter.WriteStartElement(PrefixConstants.Message, nameof(ElementNameTable.Error),
                SdmxConstants.MessageNs21);
            _xmlWriter.WriteAttributeString(XmlConstants.Xmlns, PrefixConstants.Common, null,
                SdmxConstants.CommonNs21);

            var groupsByErrorCode = statusObjects.SelectMany(x => x.Messages).GroupBy(GroupFunc);

            foreach (var group in groupsByErrorCode)
            {
                foreach (var item in group)
                {
                    _xmlWriter.WriteStartElement(PrefixConstants.Message, nameof(ElementNameTable.ErrorMessage), SdmxConstants.MessageNs21);

                    _xmlWriter.WriteAttributeString(nameof(AttributeNameTable.code), group.Key);

                    if (item.Text.Count != 0)
                    {
                        _textTypeWriter.Write(_xmlWriter, item.Text.ToArray());
                    }

                    _xmlWriter.WriteEndElement();
                }
            }

            _xmlWriter.WriteEndElement();

            _xmlWriter.Flush();
        }

        private string GroupFunc(IMessageObject item)
        {
            if (item.ErrorCode != null)
            {
                return item.ErrorCode.ClientErrorCode.ToString(CultureInfo.InvariantCulture);
            }
            else
            {
                return item.HttpCode.ToString("D");
            }
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="managed"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        protected virtual void Dispose(bool managed)
        {
            if (managed)
            {
                if (_shouldCloseWriter && !_disposed)
                {
                    if (_xmlWriter.WriteState != WriteState.Closed)
                    {
                        if (_xmlWriter.WriteState != WriteState.Start)
                        {
                            _xmlWriter.WriteEndDocument();
                        }

                        _xmlWriter.Close();
                    }

                    _disposed = true;
                }
            }
        }
    }
}
