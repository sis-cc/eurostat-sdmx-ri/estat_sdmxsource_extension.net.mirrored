// -----------------------------------------------------------------------
// <copyright file="WritingEngineDecoratorFactory.cs" company="EUROSTAT">
//   Date Created : 2016-08-18
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or � as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sdmxsource.Extension.Engine
{
    using System.IO;
    using System.Xml;

    using Estat.Sri.SdmxStructureMutableParser.Engine.V2;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Engine;
    using Org.Sdmxsource.Sdmx.Api.Factory;
    using Org.Sdmxsource.Sdmx.Api.Model.Format;
    using Org.Sdmxsource.Sdmx.Structureparser.Engine.Writing;
    using Org.Sdmxsource.Sdmx.Structureparser.Model;

    /// <summary>
    /// Writing Engine DecoratorFactory
    /// </summary>
    public class WritingEngineDecoratorFactory : IStructureWriterFactory
    {
        /// <summary>
        /// The _decorated factory.
        /// </summary>
        private readonly IStructureWriterFactory _decoratedFactory;

        /// <summary>
        /// The _indicator.
        /// </summary>
        private readonly IToolIndicator _indicator;

        /// <summary>
        /// The _writer.
        /// </summary>
        private readonly XmlWriter _writer;

        /// <summary>
        /// Initializes a new instance of the <see cref="WritingEngineDecoratorFactory"/> class.
        /// </summary>
        /// <param name="indicator">
        /// The indicator.
        /// </param>
        /// <param name="writer">
        /// The writer.
        /// </param>
        /// <param name="decoratedFactory">
        /// The decorated factory.
        /// </param>
        public WritingEngineDecoratorFactory(
            IToolIndicator indicator,
            XmlWriter writer,
            IStructureWriterFactory decoratedFactory)
        {
            this._indicator = indicator;
            this._writer = writer;
            this._decoratedFactory = decoratedFactory;
        }

        /// <summary>
        /// Obtains a StructureWritingEngine engine for the given output format
        /// </summary>
        /// <param name="structureFormat">
        /// An implementation of the StructureFormat to describe the output format for the structures
        ///                 (required)
        ///             </param>
        /// <param name="streamWriter">
        /// The output stream to write to (can be null if it is not required)
        /// </param>
        /// <returns>
        /// Null if this factory is not capable of creating a data writer engine in the requested format
        /// </returns>
        public IStructureWriterEngine GetStructureWriterEngine(IStructureFormat structureFormat, Stream streamWriter)
        {
            var structureWriterEngine = this._decoratedFactory.GetStructureWriterEngine(structureFormat, streamWriter);
            var abstractWritingEngine = structureWriterEngine as AbstractWritingEngine;
            if (abstractWritingEngine == null)
            {
                var structureWriterBaseV2 = structureWriterEngine as StructureWriterBaseV2;
                if (structureWriterBaseV2 != null)
                {
                    structureWriterBaseV2.ToolIndicator = this._indicator;
                }

                return structureWriterEngine;
            }

            if (this._writer == null)
            {
                return new WritingEngineDecorator(
                    this._indicator,
                    abstractWritingEngine,
                    SdmxSchemaEnumType.VersionTwoPointOne,
                    streamWriter,
                    true);
            }

            return new WritingEngineDecorator(this._indicator, abstractWritingEngine, this._writer);
        }
    }
}