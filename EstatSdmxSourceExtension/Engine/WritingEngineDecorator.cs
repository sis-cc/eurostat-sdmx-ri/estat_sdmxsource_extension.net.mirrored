﻿// -----------------------------------------------------------------------
// <copyright file="WritingEngineDecorator.cs" company="EUROSTAT">
//   Date Created : 2016-08-18
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sdmxsource.Extension.Engine
{
    using System.IO;
    using System.Xml;
    using System.Xml.Linq;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Sdmx.Structureparser.Engine.Writing;
    using Org.Sdmxsource.Sdmx.Structureparser.Model;

    /// <summary>
    /// A decorator class for classes derived from AbstractWritingEngine
    /// </summary>
    public class WritingEngineDecorator : AbstractWritingEngine
    {
        /// <summary>
        /// The _decorated.
        /// </summary>
        private readonly AbstractWritingEngine _decorated;

        /// <summary>
        /// The _indicator.
        /// </summary>
        private readonly IToolIndicator _indicator;

        /// <summary>
        /// Initializes a new instance of the <see cref="WritingEngineDecorator" /> class.
        /// Initializes a new instance of the <see cref="T:Org.Sdmxsource.Sdmx.Structureparser.Engine.Writing.AbstractWritingEngine" /> class.
        /// </summary>
        /// <param name="indicator">The indicator.</param>
        /// <param name="decorated">The decorated.</param>
        /// <param name="schemaVersion">The schema version.</param>
        /// <param name="outputStream">The output stream.</param>
        /// <param name="prettyfy">controls if output should be pretty (indented and no duplicate namespaces)</param>
        /// <exception cref="T:System.ArgumentNullException"><paramref name="outputStream" /> is null</exception>
        public WritingEngineDecorator(
            IToolIndicator indicator,
            AbstractWritingEngine decorated,
            SdmxSchemaEnumType schemaVersion,
            Stream outputStream,
            bool prettyfy)
            : base(schemaVersion, outputStream, prettyfy)
        {
            this._indicator = indicator;
            this._decorated = decorated;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="WritingEngineDecorator" /> class.
        /// </summary>
        /// <param name="indicator">The indicator.</param>
        /// <param name="decorated">The decorated.</param>
        /// <param name="writer">The writer.</param>
        /// <exception cref="T:System.ArgumentNullException"><paramref name="writer" /> is null</exception>
        public WritingEngineDecorator(IToolIndicator indicator, AbstractWritingEngine decorated, XmlWriter writer)
            : this(writer)
        {
            this._indicator = indicator;
            this._decorated = decorated;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="WritingEngineDecorator"/> class. 
        /// Initializes a new instance of the <see cref="T:Org.Sdmxsource.Sdmx.Structureparser.Engine.Writing.AbstractWritingEngine"/> class.
        /// </summary>
        /// <param name="writer">
        /// The writer.
        ///             </param>
        /// <exception cref="T:System.ArgumentNullException">
        /// <paramref name="writer"/> is null
        /// </exception>
        private WritingEngineDecorator(XmlWriter writer)
            : base(writer)
        {
        }

        /// <summary>
        /// Build the XSD generated class objects from the specified <paramref name="beans"/>
        /// </summary>
        /// <param name="beans">
        /// The beans.
        ///             </param>
        /// <returns>
        /// the XSD generated class objects from the specified <paramref name="beans"/>
        /// </returns>
        public override XNode Build(ISdmxObjects beans)
        {
            var xmlObject = this._decorated.Build(beans);
            if (xmlObject != null)
            {
                return new XDocument(new XComment(this._indicator.GetComment()), xmlObject);
            }

            return null;
        }
    }
}