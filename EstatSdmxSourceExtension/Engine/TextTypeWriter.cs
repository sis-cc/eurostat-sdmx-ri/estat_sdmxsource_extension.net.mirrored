﻿// -----------------------------------------------------------------------
// <copyright file="TextTypeWriter.cs" company="EUROSTAT">
//   Date Created : 2017-11-15
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using Estat.Sri.SdmxXmlConstants;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace Estat.Sdmxsource.Extension.Engine
{
    /// <summary>
    /// A SDMX TextType writer
    /// </summary>
    class TextTypeWriter
    {
        /// <summary>
        /// The element name
        /// </summary>
        private readonly string _elementName;

        /// <summary>
        /// The prefix
        /// </summary>
        private readonly string _prefix;

        /// <summary>
        /// The namespace
        /// </summary>
        private readonly string _ns;

        /// <summary>
        /// Initializes a new instance of the <see cref="TextTypeWriter"/> class.
        /// </summary>
        /// <param name="elementName">Name of the element.</param>
        /// <param name="prefix">The prefix.</param>
        /// <param name="ns">The namespace.</param>
        public TextTypeWriter(string elementName, string prefix, string ns)
        {
            _elementName = elementName;
            _prefix = prefix;
            _ns = ns;
        }

        /// <summary>
        /// Writes the specified writer.
        /// </summary>
        /// <param name="writer">The writer.</param>
        /// <param name="text">The text.</param>
        /// <exception cref="ArgumentNullException">
        /// writer
        /// or
        /// text
        /// </exception>
        public void Write(XmlWriter writer, params ITextTypeWrapper[] text)
        {
            if (writer == null)
            {
                throw new ArgumentNullException(nameof(writer));
            }

            if (text == null)
            {
                throw new ArgumentNullException(nameof(text));
            }

            foreach (var item in text)
            {
                writer.WriteStartElement(_prefix, _elementName, _ns);
                writer.WriteAttributeString(XmlConstants.XmlPrefix, XmlConstants.LangAttribute, null, item.Locale);
                writer.WriteString(item.Value);
                writer.WriteEndElement();
            }
        }
    }
}
