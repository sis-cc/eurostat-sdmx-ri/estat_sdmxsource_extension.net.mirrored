﻿// -----------------------------------------------------------------------
// <copyright file="SubmitStructureResponseWriter.cs" company="EUROSTAT">
//   Date Created : 2017-11-15
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Estat.Sdmxsource.Extension.Model.Error;
using System.Xml;
using System.IO;
using Estat.Sri.SdmxXmlConstants;
using Org.Sdmxsource.Sdmx.Api.Constants;
using System.Globalization;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;
using Estat.Sri.SdmxParseBase.Engine;
using Org.Sdmxsource.Sdmx.Api.Model.Header;

namespace Estat.Sdmxsource.Extension.Engine
{
    class SubmitStructureResponseWriter : IResponseMessageWriter
    {
        /// <summary>
        /// The XML writer
        /// </summary>
        private readonly XmlWriter _xmlWriter;

        /// <summary>
        /// The should close writer
        /// </summary>
        private readonly bool _shouldCloseWriter;

        /// <summary>
        /// The text type writer
        /// </summary>
        private readonly TextTypeWriter _textTypeWriter = new TextTypeWriter(nameof(ElementNameTable.Text), PrefixConstants.Common, SdmxConstants.CommonNs21);

        /// <summary>
        /// The header writer
        /// </summary>
        private readonly HeaderWriter _headerWriter;

        /// <summary>
        /// The header
        /// </summary>
        private readonly IHeader _header;

        /// <summary>
        /// The root element name
        /// </summary>
        private readonly string _rootElementName;

        /// <summary>
        /// The disposed
        /// </summary>
        private bool _disposed;

        /// <summary>
        /// Initializes a new instance of the <see cref="SubmitStructureResponseWriter" /> class.
        /// </summary>
        /// <param name="xmlWriter">The XML writer.</param>
        /// <param name="header">The header.</param>
        /// <param name="registryInterfaceIsRootElement">if set to <c>true</c> [registry interface is root element].</param>
        /// <exception cref="ArgumentNullException">xmlWriter is <c>null</c></exception>
        public SubmitStructureResponseWriter(XmlWriter xmlWriter, IHeader header, bool registryInterfaceIsRootElement)
        {
            if (xmlWriter == null)
            {
                throw new ArgumentNullException(nameof(xmlWriter));
            }

            if (header == null)
            {
                throw new ArgumentNullException(nameof(header));
            }

            _xmlWriter = xmlWriter;
            _shouldCloseWriter = false;
            _headerWriter = new HeaderWriter(_xmlWriter, null, SdmxSchema.GetFromEnum(SdmxSchemaEnumType.VersionTwoPointOne));
            _header = header;
            _rootElementName = registryInterfaceIsRootElement ? nameof(ElementNameTable.RegistryInterface) : nameof(ElementNameTable.SubmitStructureResponse);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SubmitStructureResponseWriter" /> class.
        /// </summary>
        /// <param name="outputStream">The output stream.</param>
        /// <param name="encoding">The encoding.</param>
        /// <param name="header">The header.</param>
        /// <exception cref="ArgumentNullException">outputStream is <c>null</c></exception>
        public SubmitStructureResponseWriter(Stream outputStream, Encoding encoding, IHeader header)
        {
            if (outputStream == null)
            {
                throw new ArgumentNullException(nameof(outputStream));
            }

            if (header == null)
            {
                throw new ArgumentNullException(nameof(header));
            }

            encoding = encoding ?? Encoding.UTF8;

            var settings = new XmlWriterSettings()
            {
                Encoding = encoding,
                Indent = true
            };

            _shouldCloseWriter = true;
            _xmlWriter = XmlWriter.Create(outputStream, settings);
            _headerWriter = new HeaderWriter(_xmlWriter, null, SdmxSchema.GetFromEnum(SdmxSchemaEnumType.VersionTwoPointOne));
            _header = header;
        }

        /// <summary>
        /// Writes the specified status objects.
        /// </summary>
        /// <param name="statusObjects">The status objects.</param>
        /// <exception cref="ObjectDisposedException">this object is disposed</exception>
        /// <exception cref="ArgumentNullException">statusObjects is <c>null</c></exception>
        /// <exception cref="ArgumentException">Cannot write empty response</exception>
        public void Write(IList<IResponseWithStatusObject> statusObjects)
        {
            if (_disposed)
            {
                throw new ObjectDisposedException(this.GetType().Name);
            }

            if (statusObjects == null)
            {
                throw new ArgumentNullException(nameof(statusObjects));
            }

            if (statusObjects.Count == 0)
            {
                throw new ArgumentException("Cannot write empty response");
            }

            if (_xmlWriter.WriteState == WriteState.Start)
            {
                _xmlWriter.WriteStartDocument();
            }
           
            _xmlWriter.WriteStartElement(PrefixConstants.Message, _rootElementName, SdmxConstants.MessageNs21);
            _xmlWriter.WriteAttributeString(XmlConstants.Xmlns, PrefixConstants.Common, null, SdmxConstants.CommonNs21);
            _xmlWriter.WriteAttributeString(XmlConstants.Xmlns, PrefixConstants.Registry, null, SdmxConstants.RegistryNs21);
            _xmlWriter.WriteAttributeString(XmlConstants.Xmlns, PrefixConstants.Structure, null, SdmxConstants.StructureNs21);

            _headerWriter.WriteHeader(_header);
            _xmlWriter.WriteStartElement(PrefixConstants.Message, nameof(ElementNameTable.SubmitStructureResponse), SdmxConstants.MessageNs21);

            foreach (var item in statusObjects)
            {
                _xmlWriter.WriteStartElement(PrefixConstants.Registry, nameof(ElementNameTable.SubmissionResult), SdmxConstants.RegistryNs21);
                _xmlWriter.WriteStartElement(PrefixConstants.Registry, nameof(ElementNameTable.SubmittedStructure), SdmxConstants.RegistryNs21);

                // The action attribute will indicate the action to be taken on the referenced structural object. This should be provided when this structure is used in a submit structure response.
                _xmlWriter.WriteAttributeString(nameof(AttributeNameTable.action), item.Action.Action);

                _xmlWriter.WriteStartElement(PrefixConstants.Registry, nameof(ElementNameTable.MaintainableObject), SdmxConstants.RegistryNs21);
                _xmlWriter.WriteElementString(nameof(ElementNameTable.URN), item.StructureReference.TargetUrn.ToString());
                _xmlWriter.WriteEndElement(); // MaintainableObject
                _xmlWriter.WriteEndElement(); // SubmittedStructure


                _xmlWriter.WriteStartElement(PrefixConstants.Registry, nameof(ElementNameTable.StatusMessage), SdmxConstants.RegistryNs21);
                _xmlWriter.WriteAttributeString("status", item.Status.ToString());
                foreach (var message in item.Messages)
                {
                    _xmlWriter.WriteStartElement(PrefixConstants.Registry, nameof(ElementNameTable.MessageText), SdmxConstants.RegistryNs21);
                    if (message.ErrorCode != null)
                    {
                        _xmlWriter.WriteAttributeString(nameof(AttributeNameTable.code), message.ErrorCode.ClientErrorCode.ToString(CultureInfo.InvariantCulture));
                        _xmlWriter.WriteComment(message.ErrorCode.ErrorString);
                        if (message.Text.Count == 0)
                        {
                            _textTypeWriter.Write(_xmlWriter, new TextTypeWrapperImpl(null, message.ErrorCode.ErrorString, null));
                        }
                        else
                        {
                            _textTypeWriter.Write(_xmlWriter, message.Text.ToArray());
                        }
                    }
                    else
                    {
                        if (message.Text.Count == 0)
                        {
                            _textTypeWriter.Write(_xmlWriter, new TextTypeWrapperImpl(null, "Success", null));
                        }
                        else
                        {
                            _textTypeWriter.Write(_xmlWriter, message.Text.ToArray());
                        }
                    }

                    _xmlWriter.WriteEndElement(); // MessageText
                }

                _xmlWriter.WriteEndElement(); // StatusMessage

                _xmlWriter.WriteEndElement(); // SubmissionResult
            }

            _xmlWriter.WriteEndElement(); // SubmitStructureResponse
            _xmlWriter.WriteEndElement(); // RegistryInterface
            _xmlWriter.Flush();
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="managed"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        protected virtual void Dispose(bool managed)
        {
            if (managed)
            {
                if (_shouldCloseWriter && !_disposed)
                {
                    if (_xmlWriter.WriteState != WriteState.Closed)
                    {
                        if (_xmlWriter.WriteState != WriteState.Start)
                        {
                            _xmlWriter.WriteEndDocument();
                        }

                        _xmlWriter.Close();
                    }

                    _disposed = true;
                }
            }
        }
    }
}
