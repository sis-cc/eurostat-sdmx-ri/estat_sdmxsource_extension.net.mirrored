// -----------------------------------------------------------------------
// <copyright file="ReferenceTransform.cs" company="EUROSTAT">
//   Date Created : 2016-03-10
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or � as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.CustomRequests.Builder
{
    using System;
    using System.Collections.Generic;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V20.Registry;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;

    /// <summary>
    /// ReferenceTransform class
    /// </summary>
    internal class ReferenceTransform
    {
        /// <summary>
        /// Adds the codelist.
        /// </summary>
        /// <param name="structureQuery">The structure query.</param>
        /// <param name="codelistRefTypes">The codelist reference types.</param>
        /// <returns>
        /// The reference type
        /// </returns>
        public CodelistRefType AddCodelist(IStructureReference structureQuery, IList<CodelistRefType> codelistRefTypes)
        {
            return AddReference(
                codelistRefTypes, 
                structureQuery, 
                (type, s) => type.AgencyID = s, 
                (type, s) => type.CodelistID = s, 
                (type, s) => type.Version = s, 
                (type, uri) => type.URN = uri);
        }

        /// <summary>
        /// Adds the hierarchical codelist.
        /// </summary>
        /// <param name="structureQuery">The structure query.</param>
        /// <param name="hierarchicalCodelistRef">The hierarchical codelist reference.</param>
        /// <returns>The reference type</returns>
        public HierarchicalCodelistRefType AddHierarchicalCodelist(
            IStructureReference structureQuery, 
            IList<HierarchicalCodelistRefType> hierarchicalCodelistRef)
        {
            return AddReference(
                hierarchicalCodelistRef, 
                structureQuery, 
                (type, s) => type.AgencyID = s, 
                (type, s) => type.HierarchicalCodelistID = s, 
                (type, s) => type.Version = s, 
                (type, uri) => type.URN = uri);
        }

        /// <summary>
        /// Adds the category scheme.
        /// </summary>
        /// <param name="structureQuery">The structure query.</param>
        /// <param name="categorySchemeRef">The category scheme reference.</param>
        /// <returns>The reference type</returns>
        public CategorySchemeRefType AddCategoryScheme(
            IStructureReference structureQuery, 
            IList<CategorySchemeRefType> categorySchemeRef)
        {
            return AddReference(
                categorySchemeRef, 
                structureQuery, 
                (type, s) => type.AgencyID = s, 
                (type, s) => type.CategorySchemeID = s, 
                (type, s) => type.Version = s, 
                (type, uri) => type.URN = uri);
        }

        /// <summary>
        /// Adds the concept scheme.
        /// </summary>
        /// <param name="structureQuery">The structure query.</param>
        /// <param name="conceptSchemeRef">The concept scheme reference.</param>
        /// <returns>The reference type</returns>
        public ConceptSchemeRefType AddConceptScheme(
            IStructureReference structureQuery, 
            IList<ConceptSchemeRefType> conceptSchemeRef)
        {
            return AddReference(
                conceptSchemeRef, 
                structureQuery, 
                (type, s) => type.AgencyID = s, 
                (type, s) => type.ConceptSchemeID = s, 
                (type, s) => type.Version = s, 
                (type, uri) => type.URN = uri);
        }

        /// <summary>
        /// Adds the key family.
        /// </summary>
        /// <param name="structureQuery">The structure query.</param>
        /// <param name="keyFamilyRef">The key family reference.</param>
        /// <returns>The reference type</returns>
        public KeyFamilyRefType AddKeyFamily(IStructureReference structureQuery, IList<KeyFamilyRefType> keyFamilyRef)
        {
            return AddReference(
                keyFamilyRef, 
                structureQuery, 
                (type, s) => type.AgencyID = s, 
                (type, s) => type.KeyFamilyID = s, 
                (type, s) => type.Version = s, 
                (type, uri) => type.URN = uri);
        }

        /// <summary>
        /// Adds the dataflow.
        /// </summary>
        /// <param name="structureQuery">The structure query.</param>
        /// <param name="dataflowRef">The dataflow reference.</param>
        /// <returns>The reference type</returns>
        public DataflowRefType AddDataflow(IStructureReference structureQuery, IList<DataflowRefType> dataflowRef)
        {
            return AddReference(
                dataflowRef, 
                structureQuery, 
                (type, s) => type.AgencyID = s, 
                (type, s) => type.DataflowID = s, 
                (type, s) => type.Version = s, 
                (type, uri) => type.URN = uri);
        }

        /// <summary>
        /// Adds the metadata structure.
        /// </summary>
        /// <param name="structureQuery">The structure query.</param>
        /// <param name="metadataStructureRef">The metadata structure reference.</param>
        /// <returns>The reference type</returns>
        public MetadataStructureRefType AddMetadataStructure(
            IStructureReference structureQuery, 
            IList<MetadataStructureRefType> metadataStructureRef)
        {
            return AddReference(
                metadataStructureRef, 
                structureQuery, 
                (type, s) => type.AgencyID = s, 
                (type, s) => type.MetadataStructureID = s, 
                (type, s) => type.Version = s, 
                (type, uri) => type.URN = uri);
        }

        /// <summary>
        /// Adds the metadataflow.
        /// </summary>
        /// <param name="structureQuery">The structure query.</param>
        /// <param name="metadataflowRef">The metadataflow reference.</param>
        /// <returns>The reference type</returns>
        public MetadataflowRefType AddMetadataflow(
            IStructureReference structureQuery, 
            IList<MetadataflowRefType> metadataflowRef)
        {
            return AddReference(
                metadataflowRef, 
                structureQuery, 
                (type, s) => type.AgencyID = s, 
                (type, s) => type.MetadataflowID = s, 
                (type, s) => type.Version = s, 
                (type, uri) => type.URN = uri);
        }

        /// <summary>
        /// Adds the process.
        /// </summary>
        /// <param name="structureQuery">The structure query.</param>
        /// <param name="processRef">The process reference.</param>
        /// <returns>The reference type</returns>
        public ProcessRefType AddProcess(IStructureReference structureQuery, IList<ProcessRefType> processRef)
        {
            return AddReference(
                processRef, 
                structureQuery, 
                (type, s) => type.AgencyID = s, 
                (type, s) => type.ProcessID = s, 
                (type, s) => type.Version = s, 
                (type, uri) => type.URN = uri);
        }

        /// <summary>
        /// Adds the structure set.
        /// </summary>
        /// <param name="structureQuery">The structure query.</param>
        /// <param name="structureSetRef">The structure set reference.</param>
        /// <returns>The reference type</returns>
        public StructureSetRefType AddStructureSet(
            IStructureReference structureQuery, 
            IList<StructureSetRefType> structureSetRef)
        {
            return AddReference(
                structureSetRef, 
                structureQuery, 
                (type, s) => type.AgencyID = s, 
                (type, s) => type.StructureSetID = s, 
                (type, s) => type.Version = s, 
                (type, uri) => type.URN = uri);
        }

        /// <summary>
        /// Adds the reference.
        /// </summary>
        /// <typeparam name="T">The generic type</typeparam>
        /// <param name="output">The output.</param>
        /// <param name="input">The input.</param>
        /// <param name="setAgency">The set agency.</param>
        /// <param name="setId">The set identifier.</param>
        /// <param name="setVersion">The set version.</param>
        /// <param name="setUrn">The set urn.</param>
        /// <returns>The reference type</returns>
        private static T AddReference<T>(
            ICollection<T> output, 
            IStructureReference input, 
            Action<T, string> setAgency, 
            Action<T, string> setId, 
            Action<T, string> setVersion, 
            Action<T, Uri> setUrn) where T : new()
        {
            var xsdClass = new T();
            var reference = input.MaintainableReference;
            bool identified = false;
            if (reference.HasAgencyId())
            {
                setAgency(xsdClass, reference.AgencyId);
                identified = true;
            }

            if (reference.HasMaintainableId())
            {
                setId(xsdClass, reference.MaintainableId);
                identified = true;
            }

            if (reference.HasVersion())
            {
                setVersion(xsdClass, reference.Version);
                identified = true;
            }

            if (!identified && input.HasMaintainableUrn())
            {
                setUrn(xsdClass, input.MaintainableUrn);
            }

            output.Add(xsdClass);

            return xsdClass;
        }
    }
}