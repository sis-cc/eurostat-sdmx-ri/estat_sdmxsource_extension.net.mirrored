﻿// -----------------------------------------------------------------------
// <copyright file="ContentTypeBuilder.cs" company="EUROSTAT">
//   Date Created : 2016-06-30
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sdmxsource.Extension.Builder
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Net.Mime;

    using Org.Sdmxsource.Sdmx.Api.Builder;

    /// <summary>
    /// The content type builder.
    /// </summary>
    public class ContentTypeBuilder : GenericFromAcceptBuilder<ContentType>, IBuilder<IEnumerable<ContentType>, WebHeaderCollection>
    {
        /// <summary>
        /// The default media type
        /// </summary>
        private const string DefaultMediaType = "*/*";

        /// <summary>
        /// Initializes a new instance of the <see cref="ContentTypeBuilder"/> class.
        /// </summary>
        public ContentTypeBuilder() : this(new ContentType(DefaultMediaType))
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ContentTypeBuilder"/> class.
        /// </summary>
        /// <param name="defaultValue">The default value.</param>
        public ContentTypeBuilder(ContentType defaultValue)
            : base(defaultValue, s => new ContentType(s.ToString()))
        {
        }

        /// <summary>
        /// Builds an object of type <see cref="ContentType"/> from the specified <see cref="WebHeaderCollection"/>
        /// </summary>
        /// <param name="headers">An Object to build the output object from</param>
        /// <returns>
        /// The content type
        /// </returns>
        public IEnumerable<ContentType> Build(WebHeaderCollection headers)
        {
            if (headers == null)
            {
                return new ContentType[0];
            }

            var acceptRaw = headers.Get("Accept");
            if (string.IsNullOrWhiteSpace(acceptRaw))
            {
                acceptRaw = DefaultMediaType;
            }

            var acceptValues = acceptRaw.Split(',');
            return acceptValues.Select(s => new ContentType(s.Trim()));
        }
    }
}