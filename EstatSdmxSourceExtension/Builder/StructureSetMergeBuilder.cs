﻿// -----------------------------------------------------------------------
// <copyright file="StructureSetMergeBuilder.cs" company="EUROSTAT">
//   Date Created : 2017-07-11
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sdmxsource.Extension.Builder
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;

    using Estat.Sdmxsource.Extension.Extension;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Mapping;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Mapping;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Mapping;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;

    /// <summary>
    /// The structure set merge builder.
    /// </summary>
    public class StructureSetMergeBuilder
    {
        /// <summary>
        /// Builds the specified structure map path.
        /// </summary>
        /// <param name="structureMapPath">
        /// The structure map path.
        /// </param>
        /// <param name="retrievalManager">
        /// The retrieval manager.
        /// </param>
        /// <returns>
        /// The <see cref="IStructureSetObject"/>.
        /// </returns>
        public IStructureSetObject Build(IEnumerable<IStructureMapObject> structureMapPath, ISdmxObjectRetrievalManager retrievalManager)
        {
            var structureMapObject = structureMapPath.Aggregate((left, right) => Accumulate(left, right, retrievalManager));
            return (IStructureSetObject)structureMapObject.MaintainableParent;
        }

        /// <summary>
        /// Accumulates the specified structure maps.
        /// </summary>
        /// <param name="leftStructureMap">
        /// The left structure map.
        /// </param>
        /// <param name="rightStructureMap">
        /// The right structure map.
        /// </param>
        /// <param name="retrievalManager">
        /// The retrieval manager.
        /// </param>
        /// <returns>
        /// The <see cref="IStructureMapObject"/>.
        /// </returns>
        private static IStructureMapObject Accumulate(IStructureMapObject leftStructureMap, IStructureMapObject rightStructureMap, ISdmxObjectRetrievalManager retrievalManager)
        {
            Validate(leftStructureMap, rightStructureMap);
            var dataStructureObject = retrievalManager.GetMaintainableObject<IDataStructureObject>(rightStructureMap.TargetRef);
            var mergedStructureSet = Merge(leftStructureMap, rightStructureMap, dataStructureObject);
            return mergedStructureSet.StructureMapList.First();
        }

        /// <summary>
        /// Merges the specified left structure map with the right structure map and produces and a new <see cref="IStructureSetObject" />.
        /// </summary>
        /// <param name="leftStructureMap">The left structure map.</param>
        /// <param name="rightStructureMap">The right structure map.</param>
        /// <param name="rightTargetDsd">The right target DSD.</param>
        /// <returns>
        /// The <see cref="IStructureSetObject" />.
        /// </returns>
        private static IStructureSetObject Merge(IStructureMapObject leftStructureMap, IStructureMapObject rightStructureMap, IDataStructureObject rightTargetDsd)
        {
            IStructureSetMutableObject mutable = new StructureSetMutableCore();
            SetMaintainable(mutable, leftStructureMap, rightStructureMap);

            IStructureMapMutableObject result = new StructureMapMutableCore();
            SetNameable(result, leftStructureMap, rightStructureMap);
            mutable.StructureMapList.Add(result);
            result.SourceRef = leftStructureMap.SourceRef;
            result.TargetRef = rightStructureMap.TargetRef;

            foreach (var linkedMap in LinkComponentMaps(leftStructureMap.Components, rightStructureMap.Components, rightTargetDsd))
            {
                IComponentMapObject rightMap = linkedMap.Item2;
                IComponentMapObject leftMap = linkedMap.Item1;
                var outputMap = new ComponentMapMutableCore();
                outputMap.MapConceptRef = leftMap.MapConceptRef;
                outputMap.MapTargetConceptRef = rightMap.MapTargetConceptRef;
                var rightCodelistMap = rightMap.GetCodelistMap();
                var leftCodelistMap = leftMap.GetCodelistMap();

                var outputCodelistMap = MergeCodelistMapMutableObject(rightCodelistMap, leftCodelistMap);
                
                if (outputCodelistMap != null)
                {
                    outputMap.RepMapRef = new RepresentationMapRefMutableCore();
                    outputMap.RepMapRef.CodelistMap = BuildCodelistMapReference(mutable, outputCodelistMap);
                    mutable.CodelistMapList.Add(outputCodelistMap);
                }

                result.AddComponent(outputMap);
            }

            return mutable.ImmutableInstance;
        }

        /// <summary>
        /// Builds the codelist map reference.
        /// </summary>
        /// <param name="mutable">
        /// The mutable.
        /// </param>
        /// <param name="outputCodelistMap">
        /// The output codelist map.
        /// </param>
        /// <returns>
        /// The <see cref="StructureReferenceImpl"/>.
        /// </returns>
        private static StructureReferenceImpl BuildCodelistMapReference(IStructureSetMutableObject mutable, ICodelistMapMutableObject outputCodelistMap)
        {
            return new StructureReferenceImpl(mutable.AgencyId, mutable.Id, mutable.Version, SdmxStructureEnumType.CodeListMap, outputCodelistMap.Id);
        }

        /// <summary>
        /// Merges the codelist map mutable object.
        /// </summary>
        /// <param name="rightCodelistMap">
        /// The right codelist map.
        /// </param>
        /// <param name="leftCodelistMap">
        /// The left codelist map.
        /// </param>
        /// <returns>
        /// The <see cref="ICodelistMapMutableObject"/>.
        /// </returns>
        private static ICodelistMapMutableObject MergeCodelistMapMutableObject(ICodelistMapObject rightCodelistMap, ICodelistMapObject leftCodelistMap)
        {
            ICodelistMapMutableObject outputCodelistMap = null;
            if (rightCodelistMap != null)
            {
                if (ReferenceEquals(rightCodelistMap, leftCodelistMap))
                {
                    outputCodelistMap = new CodelistMapMutableCore(rightCodelistMap);
                }
                else if (leftCodelistMap != null)
                {
                    outputCodelistMap = MergeTwoCodelistMap(rightCodelistMap, leftCodelistMap);
                }
                else
                {
                    outputCodelistMap = new CodelistMapMutableCore(rightCodelistMap);
                }
            }
            else if (leftCodelistMap != null)
            {
                outputCodelistMap = new CodelistMapMutableCore(leftCodelistMap);
            }

            return outputCodelistMap;
        }

        /// <summary>
        /// Merges the two codelist map.
        /// </summary>
        /// <param name="rightCodelistMap">
        /// The right codelist map.
        /// </param>
        /// <param name="leftCodelistMap">
        /// The left codelist map.
        /// </param>
        /// <returns>
        /// The <see cref="ICodelistMapMutableObject"/>.
        /// </returns>
        private static ICodelistMapMutableObject MergeTwoCodelistMap(ICodelistMapObject rightCodelistMap, ICodelistMapObject leftCodelistMap)
        {
            ICodelistMapMutableObject outputCodelistMap = new CodelistMapMutableCore();

            SetNameable(outputCodelistMap, leftCodelistMap, rightCodelistMap);
            outputCodelistMap.SourceRef = leftCodelistMap.SourceRef;
            outputCodelistMap.TargetRef = rightCodelistMap.TargetRef;

            foreach (var linkedMap in LinkItemMaps(leftCodelistMap.Items, rightCodelistMap.Items))
            {
                IItemMap leftCodeMap = linkedMap.Item1;
                IItemMap rightCodeMap = linkedMap.Item2;
                var outputCodeMap = new ItemMapMutableCore();
                outputCodeMap.SourceId = leftCodeMap.SourceId;
                outputCodeMap.TargetId = rightCodeMap.TargetId;
                outputCodelistMap.AddItem(outputCodeMap);
            }

            return outputCodelistMap;
        }

        /// <summary>
        /// Sets the identifier.
        /// </summary>
        /// <param name="mutable">
        /// The mutable.
        /// </param>
        /// <param name="leftIdentifiableObject">
        /// The left identifiable object.
        /// </param>
        /// <param name="rightIdentifiableObject">
        /// The right identifiable object.
        /// </param>
        private static void SetId(IIdentifiableMutableObject mutable, IIdentifiableObject leftIdentifiableObject, IIdentifiableObject rightIdentifiableObject)
        {
            mutable.Id = string.Format(CultureInfo.InvariantCulture, "{0}_{1}", leftIdentifiableObject.Id, rightIdentifiableObject.Id);
        }

        /// <summary>
        /// Sets the maintainable.
        /// </summary>
        /// <param name="mutable">
        /// The mutable.
        /// </param>
        /// <param name="leftIdentifiableObject">
        /// The left identifiable object.
        /// </param>
        /// <param name="rightIdentifiableObject">
        /// The right identifiable object.
        /// </param>
        private static void SetMaintainable(IMaintainableMutableObject mutable, IIdentifiableObject leftIdentifiableObject, IIdentifiableObject rightIdentifiableObject)
        {
            SetNameable(mutable, leftIdentifiableObject.MaintainableParent, rightIdentifiableObject.MaintainableParent);
            mutable.AgencyId = leftIdentifiableObject.MaintainableParent.AgencyId;
            mutable.Version = "1.0";
        }

        /// <summary>
        /// Sets the nameable.
        /// </summary>
        /// <param name="mutable">
        /// The mutable.
        /// </param>
        /// <param name="leftIdentifiableObject">
        /// The left identifiable object.
        /// </param>
        /// <param name="rightIdentifiableObject">
        /// The right identifiable object.
        /// </param>
        private static void SetNameable(INameableMutableObject mutable, INameableObject leftIdentifiableObject, INameableObject rightIdentifiableObject)
        {
            SetId(mutable, leftIdentifiableObject, rightIdentifiableObject);
            mutable.AddName("en", string.Format(CultureInfo.InvariantCulture, "Merged '{0}' and '{1}'", leftIdentifiableObject.Name, rightIdentifiableObject.Name));
        }

        /// <summary>
        /// Validates the specified left map object.
        /// </summary>
        /// <param name="leftMapObject">The left map object.</param>
        /// <param name="rightMapObject">The right map object.</param>
        /// <exception cref="ArgumentException">Left Structure Map Target and Right Structure Map Source do no match.</exception>
        private static void Validate(IStructureMapObject leftMapObject, IStructureMapObject rightMapObject)
        {
            if (!leftMapObject.TargetRef.Equals(rightMapObject.SourceRef))
            {
                throw new ArgumentException(string.Format(CultureInfo.InvariantCulture, "Left Structure Map Target and Right Structure Map Source do no match. '{0}' != '{1}'", leftMapObject.TargetRef.TargetUrn, rightMapObject.SourceRef.TargetUrn));
            }
        }

        /// <summary>
        /// Links the component maps.
        /// </summary>
        /// <param name="leftComponentMaps">The left component maps.</param>
        /// <param name="rightCompnentMaps">The right component maps.</param>
        /// <param name="rightTargetDsd">The right target DSD.</param>
        /// <returns>The tuples of source and target component maps</returns>
        private static IEnumerable<Tuple<IComponentMapObject, IComponentMapObject>> LinkComponentMaps(IList<IComponentMapObject> leftComponentMaps, IList<IComponentMapObject> rightCompnentMaps, IDataStructureObject rightTargetDsd)
        {
            var rightComponentMapSourceToTarget = rightCompnentMaps.ToDictionary(o => o.MapConceptRef, StringComparer.Ordinal);
            foreach (var leftComponentMap in leftComponentMaps)
            {
                IComponentMapObject rightComponentMap;
                if (rightComponentMapSourceToTarget.TryGetValue(leftComponentMap.MapTargetConceptRef, out rightComponentMap))
                {
                    yield return Tuple.Create(leftComponentMap, rightComponentMap);
                }
                else if (rightTargetDsd.GetComponent(leftComponentMap.MapTargetConceptRef) != null)
                {
                    yield return Tuple.Create(leftComponentMap, leftComponentMap);
                }
            }

            var leftComponentMapTarget = leftComponentMaps.ToDictionary(o => o.MapTargetConceptRef, StringComparer.Ordinal);
            foreach (var rightCompnentMap in rightCompnentMaps)
            {
                if (!leftComponentMapTarget.ContainsKey(rightCompnentMap.MapConceptRef))
                {
                    yield return Tuple.Create(rightCompnentMap, rightCompnentMap);
                }
            }
        }

        /// <summary>
        /// Links the item maps.
        /// </summary>
        /// <param name="leftItemMaps">The left item maps.</param>
        /// <param name="rightItemMaps">The right item maps.</param>
        /// <returns>The tuples of source and target item maps</returns>
        private static IEnumerable<Tuple<IItemMap, IItemMap>> LinkItemMaps(IList<IItemMap> leftItemMaps, IList<IItemMap> rightItemMaps)
        {
            var sourceToTarget = rightItemMaps.ToDictionary(o => o.SourceId, StringComparer.Ordinal);
            foreach (var leftItemMap in leftItemMaps)
            {
                IItemMap rightItemMap;
                if (sourceToTarget.TryGetValue(leftItemMap.TargetId, out rightItemMap))
                {
                    yield return Tuple.Create(leftItemMap, rightItemMap);
                }
                else
                {
                    // TODO Should we check if the code still exists
                    yield return Tuple.Create(leftItemMap, leftItemMap);
                }
            }

            var leftItemMapTarget = leftItemMaps.ToDictionary(o => o.TargetId, StringComparer.Ordinal);
            foreach (var rightItemMap in rightItemMaps)
            {
                if (!leftItemMapTarget.ContainsKey(rightItemMap.SourceId))
                {
                    yield return Tuple.Create(rightItemMap, rightItemMap);
                }
            }
        }
    }
}