// -----------------------------------------------------------------------
// <copyright file="DocumentBuilder.cs" company="EUROSTAT">
//   Date Created : 2016-03-10
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or � as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.CustomRequests.Builder.QueryBuilder
{
    using System;
    using System.Xml.Linq;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Common;
    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Query;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Complex;

    /// <summary>
    ///     DocumentBuilder class
    /// </summary>
    public abstract class DocumentBuilder
    {
        /// <summary>
        /// For the specified complex structure query metadata.
        /// </summary>
        /// <param name="complexStructureQueryMetadata">The complex structure query metadata.</param>
        /// <param name="complexStructureRef">The complex structure reference.</param>
        /// <returns>The document</returns>
        public abstract XDocument ConstructFor(
            IComplexStructureQueryMetadata complexStructureQueryMetadata, 
            IComplexStructureReferenceObject complexStructureRef);

        /// <summary>
        ///     Fills the data query details.
        /// </summary>
        /// <param name="structureQueryMetadata">The structure query metadata.</param>
        /// <param name="returnDetailsType">Type of the return details.</param>
        /// <exception cref="System.ArgumentNullException">
        ///     returnDetailsType
        ///     or
        ///     structureQueryMetadata
        /// </exception>
        protected void FillDataQueryDetails(
            IComplexStructureQueryMetadata structureQueryMetadata, 
            StructureReturnDetailsType returnDetailsType)
        {
            if (returnDetailsType == null)
            {
                throw new ArgumentNullException("returnDetailsType");
            }

            if (structureQueryMetadata == null)
            {
                throw new ArgumentNullException("structureQueryMetadata");
            }

            var referencesType = new ReferencesType();
            returnDetailsType.References = referencesType;

            if (structureQueryMetadata.StructureQueryDetail != null)
            {
                switch (structureQueryMetadata.StructureQueryDetail.EnumType)
                {
                    case ComplexStructureQueryDetailEnumType.Stub:
                        returnDetailsType.detail = "Stub";
                        break;

                    default:
                        returnDetailsType.detail = "Full";
                        break;
                }
            }

            if (structureQueryMetadata.ReferencesQueryDetail != null)
            {
                switch (structureQueryMetadata.ReferencesQueryDetail.EnumType)
                {
                    case ComplexMaintainableQueryDetailEnumType.Stub:
                        referencesType.detail = "Stub";
                        break;

                    default:
                        referencesType.detail = "Full";
                        break;
                }
            }

            if (structureQueryMetadata.StructureReferenceDetail != null)
            {
                switch (structureQueryMetadata.StructureReferenceDetail.EnumType)
                {
                    case StructureReferenceDetailEnumType.All:
                        referencesType.All = new EmptyType();
                        break;
                    case StructureReferenceDetailEnumType.None:
                        referencesType.None = new EmptyType();
                        break;
                    case StructureReferenceDetailEnumType.Children:
                        referencesType.Children = new EmptyType();
                        break;
                    case StructureReferenceDetailEnumType.Descendants:
                        referencesType.Descendants = new EmptyType();
                        break;
                    case StructureReferenceDetailEnumType.Parents:
                        referencesType.Parents = new EmptyType();
                        break;
                    case StructureReferenceDetailEnumType.ParentsSiblings:
                        referencesType.ParentsAndSiblings = new EmptyType();
                        break;
                }
            }

            if (structureQueryMetadata.ReferenceSpecificStructures != null
                && structureQueryMetadata.ReferenceSpecificStructures.Count > 0)
            {
                var objectTypeListType = new MaintainableObjectTypeListType();
                referencesType.SpecificObjects = objectTypeListType;

                foreach (var sdmxType in structureQueryMetadata.ReferenceSpecificStructures)
                {
                    this.InitializeProperties(sdmxType, objectTypeListType);
                }
            }

            referencesType.processConstraints = false;
        }

        /// <summary>
        /// Builds the WhereType.
        /// </summary>
        /// <typeparam name="TWhereType">The type of the where type. Must be based on MaintainableWhereType.</typeparam>
        /// <param name="complexStructureRef">The complex structure preference.</param>
        /// <returns>
        /// THe where type
        /// </returns>
        /// <exception cref="ArgumentNullException">The exception</exception>
        protected TWhereType BuildWhereType<TWhereType>(IComplexStructureReferenceObject complexStructureRef)
            where TWhereType : MaintainableWhereType, new()
        {
            if (complexStructureRef == null)
            {
                throw new ArgumentNullException("complexStructureRef");
            }

            var agencyId = (complexStructureRef.AgencyId != null)
                ? complexStructureRef.AgencyId.SearchParameter
                : null;
            var artefactId = (complexStructureRef.Id != null) ? complexStructureRef.Id.SearchParameter : null;
            var version = (complexStructureRef.VersionReference != null)
                ? complexStructureRef.VersionReference.Version
                : null;
            var dataStructureWhereType = new TWhereType();

            if (!string.IsNullOrWhiteSpace(artefactId))
            {
                var queryIdType = new QueryIDType();
                dataStructureWhereType.ID = queryIdType;
                queryIdType.@operator = complexStructureRef.Id.Operator.Operator;
                queryIdType.TypedValue = artefactId;
            }

            if (!string.IsNullOrWhiteSpace(version))
            {
                dataStructureWhereType.Version = version;
            }

            if (!string.IsNullOrWhiteSpace(agencyId))
            {
                var queryAgencyIdType = new QueryNestedIDType();
                dataStructureWhereType.AgencyID = queryAgencyIdType;

                queryAgencyIdType.@operator = OrderedOperator.GetFromEnum(OrderedOperatorEnumType.Equal).OrdOperator;
                queryAgencyIdType.TypedValue = agencyId;
            }

            return dataStructureWhereType;
        }

        /// <summary>
        /// Initializes the properties.
        /// </summary>
        /// <param name="sdmxType">Type of the SDMX.</param>
        /// <param name="objectTypeListType">Type of the object type list.</param>
        private void InitializeProperties(SdmxStructureType sdmxType, MaintainableObjectTypeListType objectTypeListType)
        {
            switch (sdmxType.EnumType)
            {
                case SdmxStructureEnumType.AgencyScheme:
                    objectTypeListType.AgencyScheme = new AgencyScheme();
                    break;
                case SdmxStructureEnumType.AttachmentConstraint:
                    objectTypeListType.AttachmentConstraint = new AttachmentConstraint();
                    break;
                case SdmxStructureEnumType.Categorisation:
                    objectTypeListType.Categorisation = new Categorisation();
                    break;
                case SdmxStructureEnumType.CodeList:
                    objectTypeListType.Codelist = new Codelist();
                    break;
                case SdmxStructureEnumType.ConceptScheme:
                    objectTypeListType.ConceptScheme = new ConceptScheme();
                    break;
                case SdmxStructureEnumType.ContentConstraint:
                    objectTypeListType.ContentConstraint = new ContentConstraint();
                    break;
                case SdmxStructureEnumType.Dataflow:
                    objectTypeListType.Dataflow = new Dataflow();
                    break;
                case SdmxStructureEnumType.DataConsumerScheme:
                    objectTypeListType.DataConsumerScheme = new DataConsumerScheme();
                    break;
                case SdmxStructureEnumType.DataProviderScheme:
                    objectTypeListType.DataProviderScheme = new DataProviderScheme();
                    break;
                case SdmxStructureEnumType.Dsd:
                    objectTypeListType.DataStructure = new DataStructure();
                    break;
                case SdmxStructureEnumType.HierarchicalCodelist:
                    objectTypeListType.HierarchicalCodelist = new HierarchicalCodelist();
                    break;
                case SdmxStructureEnumType.MetadataFlow:
                    objectTypeListType.Metadataflow = new Metadataflow();
                    break;
                case SdmxStructureEnumType.Msd:
                    objectTypeListType.MetadataSet = new MetadataSet();
                    break;
                case SdmxStructureEnumType.OrganisationUnitScheme:
                    objectTypeListType.OrganisationUnitScheme = new OrganisationUnitScheme();
                    break;
                case SdmxStructureEnumType.Process:
                    objectTypeListType.Process = new Process();
                    break;
                case SdmxStructureEnumType.ProvisionAgreement:
                    objectTypeListType.ProvisionAgreement = new ProvisionAgreement();
                    break;
                case SdmxStructureEnumType.ReportingTaxonomy:
                    objectTypeListType.ReportingTaxonomy = new ReportingTaxonomy();
                    break;
                case SdmxStructureEnumType.StructureSet:
                    objectTypeListType.StructureSet = new StructureSet();
                    break;
            }
        }
    }
}