// -----------------------------------------------------------------------
// <copyright file="DSDDocumentBuilder.cs" company="EUROSTAT">
//   Date Created : 2016-03-10
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or � as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.CustomRequests.Builder.QueryBuilder
{
    using System.Xml.Linq;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Message;
    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Query;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Complex;
    using Org.Sdmxsource.Sdmx.Structureparser.Builder.XmlSerialization.Registry.Response.V21;

    using DataStructureQueryType = Org.Sdmx.Resources.SdmxMl.Schemas.V21.Message.DataStructureQueryType;

    /// <summary>
    ///     DSDDocumentBuilder class
    /// </summary>
    public class DSDDocumentBuilder : DocumentBuilder
    {
        /// <summary>
        /// For the specified complex structure query metadata.
        /// </summary>
        /// <param name="complexStructureQueryMetadata">The complex structure query metadata.</param>
        /// <param name="complexStructureRef">The complex structure reference.</param>
        /// <returns>
        /// The document
        /// </returns>
        public override XDocument ConstructFor(
            IComplexStructureQueryMetadata complexStructureQueryMetadata, 
            IComplexStructureReferenceObject complexStructureRef)
        {
            var dataStructureQuery = new DataStructureQuery(new DataStructureQueryType());
            dataStructureQuery.Content.Header = new BasicHeaderType();
            V21Helper.SetHeader(dataStructureQuery.Content.Header, null);

            var dataStructureQueryChild =
                new Org.Sdmx.Resources.SdmxMl.Schemas.V21.Query.DataStructureQueryType();
            dataStructureQuery.Content.SdmxQuery = dataStructureQueryChild;
            var returnDetailsType = new StructureReturnDetailsType();
            dataStructureQueryChild.ReturnDetails = returnDetailsType;

            this.FillDataQueryDetails(complexStructureQueryMetadata, returnDetailsType);

            var dataStructureWhereType = this.BuildWhereType<DataStructureWhereType>(complexStructureRef);
            dataStructureQueryChild.StructuralMetadataWhere = new DataStructureWhere(dataStructureWhereType);
            return new XDocument(dataStructureQuery.Untyped);
        }
    }
}