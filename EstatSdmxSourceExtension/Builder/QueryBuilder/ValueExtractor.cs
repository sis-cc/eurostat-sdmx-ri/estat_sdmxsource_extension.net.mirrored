// -----------------------------------------------------------------------
// <copyright file="ValueExtractor.cs" company="EUROSTAT">
//   Date Created : 2016-03-10
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or � as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.CustomRequests.Builder.QueryBuilder
{
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Data.Query.Complex;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;

    /// <summary>
    /// valueExtractor class
    /// </summary>
    internal class ValueExtractor
    {
        /// <summary>
        /// Gets for.
        /// </summary>
        /// <param name="component">The component.</param>
        /// <param name="complexComponentValue">The complex component value.</param>
        /// <returns>The document</returns>
        public ValueType GetFor(IComponent component, IComplexComponentValue complexComponentValue)
        {
            if (component.HasCodedRepresentation())
            {
                return ValueType.Value;
            }

            var defaultValueType = ValueType.TextValue;
            if (complexComponentValue.TextSearchOperator != null)
            {
                return ValueType.TextValue;
            }

            if (complexComponentValue.OrderedOperator != null)
            {
                switch (complexComponentValue.OrderedOperator.EnumType)
                {
                    case OrderedOperatorEnumType.GreaterThanOrEqual:
                    case OrderedOperatorEnumType.LessThanOrEqual:
                    case OrderedOperatorEnumType.LessThan:
                    case OrderedOperatorEnumType.GreaterThan:
                        defaultValueType = ValueType.NumericValue;
                        break;
                    default:
                        defaultValueType = ValueType.Value;
                        break;
                }
            }

            if (component.Representation != null && component.Representation.TextFormat != null)
            {
                if (component.Representation.TextFormat.TextType != null)
                {
                    switch (component.Representation.TextFormat.TextType.EnumType)
                    {
                        case TextEnumType.BasicTimePeriod:
                        case TextEnumType.DateTime:
                        case TextEnumType.Date:
                        case TextEnumType.Time:
                        case TextEnumType.Year:
                        case TextEnumType.Month:
                        case TextEnumType.Day:
                        case TextEnumType.MonthDay:
                        case TextEnumType.YearMonth:
                        case TextEnumType.Duration:
                        case TextEnumType.Timespan:
                        case TextEnumType.TimePeriod:
                        case TextEnumType.ObservationalTimePeriod:
                        case TextEnumType.GregorianDay:
                        case TextEnumType.GregorianTimePeriod:
                        case TextEnumType.GregorianYear:
                        case TextEnumType.GregorianYearMonth:
                        case TextEnumType.ReportingDay:
                        case TextEnumType.ReportingMonth:
                        case TextEnumType.ReportingQuarter:
                        case TextEnumType.ReportingSemester:
                        case TextEnumType.ReportingTimePeriod:
                        case TextEnumType.ReportingTrimester:
                        case TextEnumType.ReportingWeek:
                        case TextEnumType.ReportingYear:
                        case TextEnumType.StandardTimePeriod:
                        case TextEnumType.TimesRange:
                            return ValueType.TimeValue;
                        case TextEnumType.BigInteger:
                        case TextEnumType.Integer:
                        case TextEnumType.Long:
                        case TextEnumType.Short:
                        case TextEnumType.Decimal:
                        case TextEnumType.Float:
                        case TextEnumType.Double:
                        case TextEnumType.Count:
                        case TextEnumType.Numeric:
                        case TextEnumType.InclusiveValueRange:
                        case TextEnumType.ExclusiveValueRange:
                            return ValueType.NumericValue;
                        case TextEnumType.Boolean:
                            return ValueType.Value;
                    }
                }

                return defaultValueType;
            }

            // try guessing type from component type.
            switch (component.StructureType.EnumType)
            {
                case SdmxStructureEnumType.Dimension:
                    return ValueType.Value;
                case SdmxStructureEnumType.PrimaryMeasure:
                    return ValueType.NumericValue;
                case SdmxStructureEnumType.TimeDimension:
                    return ValueType.TimeValue;
            }

            return defaultValueType;
        }
    }
}