﻿// -----------------------------------------------------------------------
// <copyright file="ComplexStructureQueryBuilderV21.cs" company="EUROSTAT">
//   Date Created : 2013-08-01
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.CustomRequests.Builder.QueryBuilder
{
    using System;
    using System.Xml.Linq;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Complex;

    /// <summary>
    ///     Builder for StructureQueryRequest <code>org.w3c.dom.Document</code> in SDMX 2.1
    ///     rom <code>ComplexStructureQuery</code> object.
    /// </summary>
    public class ComplexStructureQueryBuilderV21 : IComplexStructureQueryBuilder<XDocument>
    {
        /// <summary>
        ///     Builds a ComplexStructureQuery that matches the passed in format
        /// </summary>
        /// <param name="query">The query</param>
        /// <returns>The document</returns>
        /// <exception cref="SdmxException">At least type expected in the reference</exception>
        public XDocument BuildComplexStructureQuery(IComplexStructureQuery query)
        {
            if (query == null)
            {
                throw new ArgumentNullException("query");
            }

            var complexStructureRef = query.StructureReference;
            if (complexStructureRef == null)
            {
                throw new SdmxException("At least type expected in the reference", SdmxErrorCode.GetFromEnum(SdmxErrorCodeEnumType.SemanticError));
            }

            var complexStructureQueryMetadata = query.StructureQueryMetadata;

            // common tags
            XDocument doc = null;

            switch (complexStructureRef.ReferencedStructureType.EnumType)
            {
                case SdmxStructureEnumType.Dsd:
                    doc = new DSDDocumentBuilder().ConstructFor(complexStructureQueryMetadata, complexStructureRef);
                    break;
                case SdmxStructureEnumType.Dataflow:
                    doc = new DataflowDocumentBuilder().ConstructFor(complexStructureQueryMetadata, complexStructureRef);
                    break;
                case SdmxStructureEnumType.CategoryScheme:
                    doc = new CategorySchemeDocumentBuilder().ConstructFor(complexStructureQueryMetadata, complexStructureRef);
                    break;
                case SdmxStructureEnumType.CodeList:
                    doc = new CodeListDocumentBuilder().ConstructFor(complexStructureQueryMetadata, complexStructureRef);
                    break;
                case SdmxStructureEnumType.Categorisation:
                    doc = new CategorySchemeDocumentBuilder().ConstructFor(complexStructureQueryMetadata, complexStructureRef);
                    break;

                case SdmxStructureEnumType.ConceptScheme:
                    doc = new ConceptSchemeDocumentBuilder().ConstructFor(complexStructureQueryMetadata, complexStructureRef);
                    break;
            }

            return doc;
        }
    }
}