// -----------------------------------------------------------------------
// <copyright file="DataflowDocumentBuilder.cs" company="EUROSTAT">
//   Date Created : 2016-03-10
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or � as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.CustomRequests.Builder.QueryBuilder
{
    using System.Xml.Linq;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Message;
    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Query;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Complex;
    using Org.Sdmxsource.Sdmx.Structureparser.Builder.XmlSerialization.Registry.Response.V21;

    using DataflowQueryType = Org.Sdmx.Resources.SdmxMl.Schemas.V21.Message.DataflowQueryType;

    /// <summary>
    ///     DataflowDocumentBuilder class
    /// </summary>
    public class DataflowDocumentBuilder : DocumentBuilder
    {
        /// <summary>
        ///     For the specified complex structure query metadata.
        /// </summary>
        /// <param name="complexStructureQueryMetadata">The complex structure query metadata.</param>
        /// <param name="complexStructureRef">The complex structure reference.</param>
        /// <returns>the document</returns>
        public override XDocument ConstructFor(
            IComplexStructureQueryMetadata complexStructureQueryMetadata, 
            IComplexStructureReferenceObject complexStructureRef)
        {
            var dataflowQuery = new DataflowQuery(new DataflowQueryType());
            dataflowQuery.Content.Header = new BasicHeaderType();
            V21Helper.SetHeader(dataflowQuery.Content.Header, null);

            var dataflowQueryTypeChild = new Org.Sdmx.Resources.SdmxMl.Schemas.V21.Query.DataflowQueryType();
            dataflowQuery.Content.SdmxQuery = dataflowQueryTypeChild;
            var returnDetailsType = new StructureReturnDetailsType();
            dataflowQueryTypeChild.ReturnDetails = returnDetailsType;

            this.FillDataQueryDetails(complexStructureQueryMetadata, returnDetailsType);

            var dataflowWhereType = this.BuildWhereType<DataflowWhereType>(complexStructureRef);
            dataflowQueryTypeChild.StructuralMetadataWhere = new DataflowWhere(dataflowWhereType);

            return new XDocument(dataflowQuery.Untyped);
        }
    }
}