﻿// -----------------------------------------------------------------------
// <copyright file="GenericFromAcceptBuilder.cs" company="EUROSTAT">
//   Date Created : 2016-07-22
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sdmxsource.Extension.Builder
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;

    using Estat.Sdmxsource.Extension.Extension;
    using Estat.Sdmxsource.Extension.Model;

    using log4net;

    using Org.Sdmxsource.Sdmx.Api.Builder;

    /// <summary>
    /// An abstract builder for Accept-Language and Accept-CharSet 
    /// </summary>
    /// <typeparam name="TOutputType">The type of the output type.</typeparam>
    public abstract class GenericFromAcceptBuilder<TOutputType> : IBuilder<IList<HeaderValueWithQuality<TOutputType>>, IList<HeaderValueWithQuality<string>>>, IBuilder<HeaderValueWithQuality<TOutputType>, HeaderValueWithQuality<string>>
    {
        /// <summary>
        /// The _log
        /// </summary>
        private readonly ILog _log;

        /// <summary>
        /// The _default value
        /// </summary>
        private readonly TOutputType _defaultValue;

        /// <summary>
        /// The _build value
        /// </summary>
        private readonly Func<HeaderValueWithQuality<string>, TOutputType> _buildValue;

        /// <summary>
        /// Initializes a new instance of the <see cref="GenericFromAcceptBuilder{TOutputType}" /> class.
        /// </summary>
        /// <param name="defaultValue">The default value.</param>
        /// <param name="buildValue">The build value.</param>
        protected GenericFromAcceptBuilder(TOutputType defaultValue, Func<HeaderValueWithQuality<string>, TOutputType> buildValue)
        {
            this._defaultValue = defaultValue;
            this._buildValue = buildValue;
            this._log = LogManager.GetLogger(this.GetType());
        }

        /// <summary>
        /// Builds a list of the output type
        /// </summary>
        /// <param name="buildFrom">An Object to build the output object from</param>
        /// <returns>
        /// A list of output type
        /// </returns>
        public IList<HeaderValueWithQuality<TOutputType>> Build(IList<HeaderValueWithQuality<string>> buildFrom)
        {
            return buildFrom.Select(this.Build).Where(info => !info.IsDefaultValue()).Distinct().ToArray();
        }

        /// <summary>
        /// Builds an object of the output type 
        /// </summary>
        /// <param name="buildFrom">An Object to build the output object from</param>
        /// <returns>
        /// Object of the output type; otherwise null
        /// </returns>
        public HeaderValueWithQuality<TOutputType> Build(HeaderValueWithQuality<string> buildFrom)
        {
            return this.SafeValue(buildFrom);
        }

        /// <summary>
        /// Wraps the <see cref="CultureInfo.GetCultureInfo(string)"/> so it returns null instead of throwing an exception
        /// </summary>
        /// <param name="s">
        /// The Accept-Language value without any quality values .
        /// </param>
        /// <returns>
        /// The <see cref="CultureInfo"/>.
        /// </returns>
        private HeaderValueWithQuality<TOutputType> SafeValue(HeaderValueWithQuality<string> s)
        {
            if (string.IsNullOrWhiteSpace(s.Value))
            {
                return null;
            }

            if (s.Value.Equals("*") || s.Value.Equals("*/*"))
            {
                return new HeaderValueWithQuality<TOutputType>(this._defaultValue, s.Quality);
            }

            try
            {
                return new HeaderValueWithQuality<TOutputType>(this._buildValue(s), s.Quality);
            }
            catch (CultureNotFoundException e)
            {
                this._log.Warn("Culture Not found: " + s, e);
            }
            catch (ArgumentException e)
            {
                this._log.Warn("Encoding Not found: " + s, e);
            }

            return null;
        }
    }
}