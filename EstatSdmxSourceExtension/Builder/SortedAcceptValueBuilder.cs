﻿// -----------------------------------------------------------------------
// <copyright file="SortedAcceptValueBuilder.cs" company="EUROSTAT">
//   Date Created : 2016-07-22
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sdmxsource.Extension.Builder
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Globalization;
    using System.Linq;

    using Estat.Sdmxsource.Extension.Model;

    using Org.Sdmxsource.Sdmx.Api.Builder;

    /// <summary>
    /// The sorted accept value builder.
    /// </summary>
    public class SortedAcceptValueBuilder : IBuilder<IList<HeaderValueWithQuality<string>>, string>
    {
        /// <summary>
        /// The quality key
        /// </summary>
        private const string QualityKey = "q";

        /// <summary>
        /// The _empty list
        /// </summary>
        private readonly ReadOnlyCollection<HeaderValueWithQuality<string>> _empty = new ReadOnlyCollection<HeaderValueWithQuality<string>>(new HeaderValueWithQuality<string>[0]);

        /// <summary>
        /// Gets the sorted accept by quality. 
        /// </summary>
        /// <param name="buildFrom">The accept raw value.</param>
        /// <returns>The sorted list</returns>
        public IList<HeaderValueWithQuality<string>> Build(string buildFrom)
        {
            // TODO replace
            if (!string.IsNullOrWhiteSpace(buildFrom))
            {
                var tokens = buildFrom.Split(',');
                var encodingList = new List<HeaderValueWithQuality<string>>();
                foreach (var token in tokens)
                {
                    var split = token.Trim().Split(';').ToArray();
                    
                    IDictionary<string, string> parameters = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);
                    for (int i = 1; i < split.Length; i++)
                    {
                        var variableValue = split[i].Split('=');
                        if (variableValue.Length == 2)
                        {
                            parameters.Add(variableValue[0].Trim(), variableValue[1].Trim());
                        }
                    }

                    decimal qualityValue;
                    string qualityString;
                    if (!parameters.TryGetValue(QualityKey, out qualityString) || !decimal.TryParse(qualityString, NumberStyles.Float, CultureInfo.InvariantCulture, out qualityValue))
                    {
                        qualityValue = 1.0m;
                    }

                    var value = split[0].Trim();
                    parameters.Remove(QualityKey);
                    encodingList.Add(new HeaderValueWithQuality<string>(value, qualityValue, parameters));
                }

                encodingList.Sort((pair, valuePair) => -pair.Quality.CompareTo(valuePair.Quality));
                return new ReadOnlyCollection<HeaderValueWithQuality<string>>(encodingList.ToArray());
            }

            return this._empty;
        }
    }
}