﻿// -----------------------------------------------------------------------
// <copyright file="DataQuery2ComplexQueryBuilder.cs" company="EUROSTAT">
//   Date Created : 2013-08-20
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sdmxsource.Extension.Builder
{
    using System;
    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Builder;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Data.Query;
    using Org.Sdmxsource.Sdmx.Api.Model.Data.Query.Complex;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Data.Query.Complex;

    /// <summary>
    ///     Builder class of IComplexDataQuery object from IDataQuery object.
    ///     Used for transforming messages in SDMX 2.0 version to SDMX 2.1 Messages.
    /// </summary>
    public class DataQuery2ComplexQueryBuilder : IBuilder<IComplexDataQuery, IDataQuery>
    {
        /// <summary>
        ///     The SOAP V20
        /// </summary>
        private readonly bool _soapV20;

        /// <summary>
        ///     Initializes a new instance of the <see cref="DataQuery2ComplexQueryBuilder" /> class.
        /// </summary>
        /// <param name="soapV20">if set to <c>true</c> [SOAP V20].</param>
        public DataQuery2ComplexQueryBuilder(bool soapV20)
        {
            this._soapV20 = soapV20;
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="DataQuery2ComplexQueryBuilder" /> class.
        /// </summary>
        public DataQuery2ComplexQueryBuilder()
        {
            this._soapV20 = false;
        }

        /// <summary>
        ///     Builds the specified data query.
        /// </summary>
        /// <param name="buildFrom">The data query.</param>
        /// <returns>The complex data query</returns>
        public virtual IComplexDataQuery Build(IDataQuery buildFrom)
        {
            if (buildFrom == null)
            {
                throw new ArgumentNullException("buildFrom");
            }

            TextSearch datasetIdOperator = TextSearch.GetFromEnum(TextSearchEnumType.Equal);

            int? defaultLimit = null;
            int? firstNObs = null;
            if (this._soapV20)
            {
                defaultLimit = buildFrom.FirstNObservations;
            }
            else
            {
                firstNObs = buildFrom.FirstNObservations;
            }

            int? lastNObs = buildFrom.LastNObservations;

            ObservationAction obsAction = ObservationAction.GetFromEnum(ObservationActionEnumType.Active);
            string dimensionAtObservation = buildFrom.DimensionAtObservation;
            DataQueryDetail queryDetail = buildFrom.DataQueryDetail;

            // map all DataQuerySelectionGroups on complexDataQuerySelectionGroups
            ISet<IComplexDataQuerySelectionGroup> complexSelectionGroups = null;
            if (buildFrom.SelectionGroups != null)
            {
                complexSelectionGroups = new HashSet<IComplexDataQuerySelectionGroup>();

                foreach (IDataQuerySelectionGroup selectionGroup in buildFrom.SelectionGroups)
                {
                    var complexSelectionGroup = Build(selectionGroup);

                    complexSelectionGroups.Add(complexSelectionGroup);
                }
            }

            IComplexDataQuery complexDataQuery = new ComplexDataQueryImpl(
                null,  // DataQuery does not support DatasetID
                datasetIdOperator, 
                buildFrom.DataProvider, 
                buildFrom.DataStructure, 
                buildFrom.Dataflow, 
                null, // DataQuery does not support PA
                null,  // DataQuery does not support Last Updated
                firstNObs, 
                lastNObs, 
                defaultLimit, 
                obsAction, 
                dimensionAtObservation,
                false, // DataQuery does not support explicit measures 
                queryDetail, 
                complexSelectionGroups);
            return complexDataQuery;
        }

        /// <summary>
        /// Builds the specified selection group.
        /// </summary>
        /// <param name="selectionGroup">
        /// The selection group.
        /// </param>
        /// <returns>
        /// The <see cref="IComplexDataQuerySelectionGroup"/>.
        /// </returns>
        private static IComplexDataQuerySelectionGroup Build(IDataQuerySelectionGroup selectionGroup)
        {
            // mapping selections
            ISet<IComplexDataQuerySelection> complexSelections = null;
            if (selectionGroup.Selections != null)
            {
                complexSelections = new HashSet<IComplexDataQuerySelection>();
                foreach (IDataQuerySelection querySelection in selectionGroup.Selections)
                {
                    var complexDataQuerySelection = Build(querySelection);
                    complexSelections.Add(complexDataQuerySelection);
                }
            }

            // Time dimension Value
            ISdmxDate dateFrom = (selectionGroup.DateFrom != null) ? selectionGroup.DateFrom : null;
            OrderedOperator orderedOperatorFrom = OrderedOperator.GetFromEnum(OrderedOperatorEnumType.GreaterThanOrEqual);

            ISdmxDate dateTo = (selectionGroup.DateTo != null) ? selectionGroup.DateTo : null;
            OrderedOperator orderedOperatorTo = OrderedOperator.GetFromEnum(OrderedOperatorEnumType.LessThanOrEqual);

            IComplexDataQuerySelectionGroup complexSelectionGroup = new ComplexDataQuerySelectionGroupImpl(
                complexSelections,
                dateFrom,
                orderedOperatorFrom,
                dateTo,
                orderedOperatorTo,
                null);
            return complexSelectionGroup;
        }

        /// <summary>
        /// Builds the specified query selection.
        /// </summary>
        /// <param name="querySelection">
        /// The query selection.
        /// </param>
        /// <returns>
        /// The <see cref="IComplexDataQuerySelection"/>.
        /// </returns>
        private static IComplexDataQuerySelection Build(IDataQuerySelection querySelection)
        {
            string componentId = querySelection.ComponentId;
            ISet<IComplexComponentValue> complexComponentValues = new HashSet<IComplexComponentValue>();

            if (querySelection.Values != null)
            {
                foreach (string value in querySelection.Values)
                {
                    IComplexComponentValue complexComponentValue = new ComplexComponentValueImpl(
                        value,
                        OrderedOperator.GetFromEnum(OrderedOperatorEnumType.Equal),
                        SdmxStructureEnumType.Dimension);
                    complexComponentValues.Add(complexComponentValue);
                }
            }

            IComplexDataQuerySelection complexDataQuerySelection = new ComplexDataQuerySelectionImpl(
                componentId,
                complexComponentValues);
            return complexDataQuerySelection;
        }
    }
}