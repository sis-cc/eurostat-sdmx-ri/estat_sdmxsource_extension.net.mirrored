// -----------------------------------------------------------------------
// <copyright file="ConstrainQueryBuilderV2.cs" company="EUROSTAT">
//   Date Created : 2013-03-22
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or � as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.CustomRequests.Builder
{
    using System;

    using Constants;

    using Model;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V20.Common;
    using Org.Sdmx.Resources.SdmxMl.Schemas.V20.Registry;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Registry;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Registry;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Registry;
    using Org.Sdmxsource.Sdmx.Structureparser.Builder.Query;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;
    using Org.Sdmxsource.Util;

    /// <summary>
    ///     Singleton factory pattern to build Version 2 reference beans from query types
    /// </summary>
    public class ConstrainQueryBuilderV2 : QueryBuilderV2
    {
        /// <summary>
        ///     Build dataflow query. Override to alter the default behavior
        /// </summary>
        /// <param name="refType">
        ///     The Dataflow reference (SDMX v2.0
        /// </param>
        /// <returns>
        ///     The <see cref="IStructureReference" />.
        /// </returns>
        protected override IStructureReference BuildDataflowQuery(DataflowRefType refType)
        {
            if (refType == null)
            {
                throw new ArgumentNullException("refType");
            }

            var urn = refType.URN;
            var immutableInstance = BuildConstraintObject(refType);

            StructureReferenceImpl structureReferenceImpl;
            if (ObjectUtil.ValidString(urn))
            {
                structureReferenceImpl = new ConstrainableStructureReference(urn);
            }
            else
            {
                string agencyId16 = refType.AgencyID;
                string maintId17 = refType.DataflowID;
                string version18 = refType.Version;

                structureReferenceImpl = new ConstrainableStructureReference(
                    agencyId16, 
                    maintId17, 
                    version18, 
                    SdmxStructureEnumType.Dataflow, 
                    immutableInstance);
            }

            return structureReferenceImpl;
        }

        /// <summary>
        ///     Add dummy values for validity.
        /// </summary>
        /// <param name="dataflowRefType">
        ///     The dataflow ref type.
        /// </param>
        /// <param name="mutable">
        ///     The mutable.
        /// </param>
        private static void AddDummyValuesForValidity(
            DataflowRefType dataflowRefType, 
            IMaintainableMutableObject mutable)
        {
            mutable.AddName("en", "Content Constraint for dataflow"); // TODO: Name is hardcoded.
            mutable.AgencyId = dataflowRefType.AgencyID;
        }

        /// <summary>
        ///     The build constraint object.
        /// </summary>
        /// <param name="dataflowRefType">
        ///     The dataflow ref type.
        /// </param>
        /// <returns>
        ///     The <see cref="IContentConstraintObject" />.
        /// </returns>
        private static IContentConstraintObject BuildConstraintObject(DataflowRefType dataflowRefType)
        {
            IContentConstraintObject immutableInstance = null;
            if (dataflowRefType.Constraint != null)
            {
                string id = dataflowRefType.Constraint.ConstraintID;
                IContentConstraintMutableObject mutable = new ContentConstraintMutableCore();
                AddDummyValuesForValidity(dataflowRefType, mutable);
                mutable.Id = id;
                foreach (CubeRegionType cubeRegionType in dataflowRefType.Constraint.CubeRegion)
                {
                    ICubeRegionMutableObject cubeRegion = BuildCubeRegion(cubeRegionType);

                    if (cubeRegionType.isIncluded && mutable.IncludedCubeRegion == null)
                    {
                        mutable.IncludedCubeRegion = cubeRegion;
                    }
                    else if (mutable.ExcludedCubeRegion == null)
                    {
                        mutable.ExcludedCubeRegion = cubeRegion;
                    }
                }

                if (dataflowRefType.Constraint.ReferencePeriod != null)
                {
                    mutable.ReferencePeriod = new ReferencePeriodMutableCore
                    {
                        StartTime =
                            dataflowRefType.Constraint
                                .ReferencePeriod.startTime, 
                        EndTime =
                            dataflowRefType.Constraint
                                .ReferencePeriod.endTime
                    };
                }

                immutableInstance = mutable.ImmutableInstance;
            }

            return immutableInstance;
        }

        /// <summary>
        ///     The build cube region.
        /// </summary>
        /// <param name="cubeRegionType">
        ///     The cube region type.
        /// </param>
        /// <returns>
        ///     The <see cref="ICubeRegionMutableObject" />.
        /// </returns>
        private static ICubeRegionMutableObject BuildCubeRegion(CubeRegionType cubeRegionType)
        {
            ICubeRegionMutableObject cubeRegion = new CubeRegionMutableCore();
            foreach (MemberType memberType in cubeRegionType.Member)
            {
                IKeyValuesMutable keyValues = new KeyValuesMutableImpl();
                keyValues.Id = memberType.ComponentRef;
                cubeRegion.AddKeyValue(keyValues);
                if (memberType.MemberValue == null || memberType.MemberValue.Count == 0)
                {
                    keyValues.AddValue(SpecialValues.DummyMemberValue);
                }
                else
                {
                    foreach (MemberValueType memberValueType in memberType.MemberValue)
                    {
                        keyValues.AddValue(memberValueType.Value);
                    }
                }
            }

            return cubeRegion;
        }
    }
}