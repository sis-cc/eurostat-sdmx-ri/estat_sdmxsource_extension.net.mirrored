﻿// -----------------------------------------------------------------------
// <copyright file="StructureMapPathBuilder.cs" company="EUROSTAT">
//   Date Created : 2017-07-10
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sdmxsource.Extension.Builder
{
    using System.Collections.Generic;
    using System.Linq;

    using Estat.Sdmxsource.Extension.Extension;

    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Mapping;

    /// <summary>
    /// Builds the upgrade map between two dataflows given a structure set
    /// </summary>
    public class StructureMapPathBuilder
    {
        /// <summary>
        /// Builds the structure map path from only one structure set.
        /// </summary>
        /// <param name="structureSets">The structure sets.</param>
        /// <param name="sourceDataflow">The source dataflow.</param>
        /// <param name="destinationDataflow">The destination dataflow.</param>
        /// <returns>
        /// A list containing the StructureMap(s) starting from the source to the target dataflow
        /// </returns>
        /// <exception cref="SdmxSemmanticException">Cannot determine which StructureMap to use.</exception>
        public IList<IStructureMapObject> BuildStructureMapPath(IEnumerable<IStructureSetObject> structureSets, IDataflowObject sourceDataflow, IDataflowObject destinationDataflow)
        {
            foreach (var structureSetObject in structureSets)
            {
                var path = this.BuildStructureMapPath(structureSetObject, sourceDataflow, destinationDataflow);
                if (path.Count > 0)
                {
                    return path;
                }
            }

            return new IStructureMapObject[0]; 
        }

        /// <summary>
        /// Builds the structure map path.
        /// </summary>
        /// <param name="structureSet">The structure set.</param>
        /// <param name="sourceDataflow">The source dataflow.</param>
        /// <param name="destinationDataflow">The destination dataflow.</param>
        /// <returns>A list containing the StructureMap(s) starting from the source to the target dataflow</returns>
        public IList<IStructureMapObject> BuildStructureMapPath(
         IStructureSetObject structureSet,
         IDataflowObject sourceDataflow,
         IDataflowObject destinationDataflow)
        {
            var sourcemaps = structureSet.StructureMapList.Where(o => sourceDataflow.IsStructureOrUsageMatch(o.SourceRef)).ToArray();

            var structureMapUpgradePath = new LinkedList<IStructureMapObject>();
            foreach (var sourcemap in sourcemaps)
            {
                structureMapUpgradePath.Clear();
                var stack = new Stack<IStructureMapObject>();
                stack.Push(sourcemap);
                while (stack.Count > 0)
                {
                    var currentMap = stack.Pop();
                    if (structureMapUpgradePath.Count > 0
                        && !structureMapUpgradePath.Last.Value.TargetRef.Equals(currentMap.SourceRef))
                    {
                        structureMapUpgradePath.RemoveLast();
                    }

                    structureMapUpgradePath.AddLast(currentMap);
                    if (destinationDataflow.IsStructureOrUsageMatch(currentMap.TargetRef))
                    {
                        return structureMapUpgradePath.ToArray();
                    }

                    foreach (var otherMaps in structureSet.StructureMapList.Where(o => o.SourceRef.Equals(currentMap.TargetRef)))
                    {
                        stack.Push(otherMaps);
                    }
                }
            }

            return new IStructureMapObject[0];
        }

        /// <summary>
        /// Builds the structure map path crossing structure set boundaries.
        /// </summary>
        /// <param name="structureSets">The structure sets.</param>
        /// <param name="sourceDataflow">The source dataflow.</param>
        /// <param name="destinationDataflow">The destination dataflow.</param>
        /// <returns>
        /// A list containing the StructureMap(s) starting from the source to the target dataflow
        /// </returns>
        public IList<IStructureMapObject> BuildStructureMapPathCrossSet(
         IEnumerable<IStructureSetObject> structureSets,
         IDataflowObject sourceDataflow,
         IDataflowObject destinationDataflow)
        {
            var structureMapObjects = structureSets.SelectMany(o => o.StructureMapList).ToArray();
            var sourcemaps = structureMapObjects.Where(o => sourceDataflow.IsStructureOrUsageMatch(o.SourceRef)).ToArray();

            var structureMapUpgradePath = new LinkedList<IStructureMapObject>();
            foreach (var sourcemap in sourcemaps)
            {
                structureMapUpgradePath.Clear();
                var stack = new Stack<IStructureMapObject>();
                stack.Push(sourcemap);
                while (stack.Count > 0)
                {
                    var currentMap = stack.Pop();
                    if (structureMapUpgradePath.Count > 0
                        && !structureMapUpgradePath.Last.Value.TargetRef.Equals(currentMap.SourceRef))
                    {
                        structureMapUpgradePath.RemoveLast();
                    }

                    structureMapUpgradePath.AddLast(currentMap);
                    if (destinationDataflow.IsStructureOrUsageMatch(currentMap.TargetRef))
                    {
                        return structureMapUpgradePath.ToArray();
                    }

                    foreach (var otherMaps in structureMapObjects.Where(o => o.SourceRef.Equals(currentMap.TargetRef)))
                    {
                        stack.Push(otherMaps);
                    }
                }
            }

            return new IStructureMapObject[0];
        }

        /// <summary>
        /// Builds the component map path for the specified <paramref name="sourceComponentId"/>.
        /// </summary>
        /// <param name="structureMapPath">The structure map path. It must be ordered</param>
        /// <param name="sourceComponentId">The source component identifier.</param>
        /// <returns>the component map path for the specified <paramref name="sourceComponentId"/>.</returns>
        public IList<IComponentMapObject> BuildComponentMapPath(IList<IStructureMapObject> structureMapPath, string sourceComponentId)
        {
            List<IComponentMapObject> componentMapPath = new List<IComponentMapObject>();
            var currentComponentId = sourceComponentId;
            foreach (var structureMapObject in structureMapPath)
            {
                var componentMapObject = structureMapObject.Components.FirstOrDefault(o => o.MapConceptRef.Equals(currentComponentId));
                if (componentMapObject == null)
                {
                    return componentMapPath;
                }

                componentMapPath.Add(componentMapObject);
                currentComponentId = componentMapObject.MapTargetConceptRef;
            }

            return componentMapPath;
        }
    }
}