// -----------------------------------------------------------------------
// <copyright file="QueryStructureRequestBuilderV2.cs" company="EUROSTAT">
//   Date Created : 2013-03-22
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.CustomRequests.Builder
{
    using System;
    using System.Collections.Generic;
    using System.Xml.Linq;

    using Constants;

    using Model;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V20.Common;
    using Org.Sdmx.Resources.SdmxMl.Schemas.V20.Message;
    using Org.Sdmx.Resources.SdmxMl.Schemas.V20.Registry;
    using Org.Sdmxsource.Sdmx.Api.Builder;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Header;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Registry;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Header;
    using Org.Sdmxsource.Sdmx.Structureparser.Builder.XmlSerialization.V2;

    /// <summary>
    ///     The queries structure request builder v 2.
    /// </summary>
    public class QueryStructureRequestBuilderV2 : IQueryStructureRequestBuilder<XDocument>
    {
        /// <summary>
        ///     The unknown id.
        /// </summary>
        private const string UnknownId = "UNKNOWN";

        /// <summary>
        /// The _reference transform
        /// </summary>
        private static readonly ReferenceTransform _referenceTransform = new ReferenceTransform();

        /// <summary>
        ///     The _header xml builder.
        /// </summary>
        private readonly IBuilder<HeaderType, IHeader> _headerXmlBuilder;

        /// <summary>
        ///     The _header.
        /// </summary>
        private IHeader _header;

        /// <summary>
        ///     Initializes a new instance of the <see cref="QueryStructureRequestBuilderV2" /> class.
        /// </summary>
        /// <param name="header">
        ///     The header.
        /// </param>
        public QueryStructureRequestBuilderV2(IHeader header)
            : this(header, null)
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="QueryStructureRequestBuilderV2" /> class.
        /// </summary>
        /// <param name="header">
        ///     The header.
        /// </param>
        /// <param name="headerXmlBuilder">
        ///     The header xml builder.
        /// </param>
        public QueryStructureRequestBuilderV2(IHeader header, IBuilder<HeaderType, IHeader> headerXmlBuilder)
        {
            this._header = header;
            this._headerXmlBuilder = headerXmlBuilder ?? new StructureHeaderXmlBuilder();
        }

        /// <summary>
        ///     Builds a StructureQuery that matches the passed in format
        /// </summary>
        /// <param name="queries">
        ///     The list of Queries
        /// </param>
        /// <param name="resolveReferences">
        ///     Set to <c>True</c> to resolve references.
        /// </param>
        /// <returns>
        ///     The <see cref="QueryStructureRequestType" />.
        /// </returns>
        public XDocument BuildStructureQuery(IEnumerable<IStructureReference> queries, bool resolveReferences)
        {
            if (queries == null)
            {
                throw new ArgumentNullException("queries");
            }

            var requestType = new QueryStructureRequestType { resolveReferences = resolveReferences };
            foreach (var reference in queries)
            {
                BuildStructureQuery(reference, requestType);
            }

            return this.BuildRegistryInterface(requestType);
        }

        /// <summary>
        /// Builds the constrainable.
        /// </summary>
        /// <param name="structureQuery">The structure queries.</param>
        /// <param name="dataflowRefType">The dataflow ref type.</param>
        private static void BuildConstrainable(IStructureReference structureQuery, DataflowRefType dataflowRefType)
        {
            var constrainable = structureQuery as IConstrainableStructureReference;
            if (constrainable != null && constrainable.ConstraintObject != null)
            {
                dataflowRefType.Constraint = new ConstraintType
                {
                    ConstraintID = constrainable.ConstraintObject.Id, 
                    ConstraintType1 = "Content"
                };
                BuildCubeRegion(constrainable.ConstraintObject.IncludedCubeRegion, dataflowRefType);
                BuildCubeRegion(constrainable.ConstraintObject.ExcludedCubeRegion, dataflowRefType);

                if (constrainable.ConstraintObject.ReferencePeriod != null)
                {
                    var referencePeriod = new ReferencePeriodType();
                    if (constrainable.ConstraintObject.ReferencePeriod.StartTime != null
                        && constrainable.ConstraintObject.ReferencePeriod.StartTime.Date != default(DateTime))
                    {
                        referencePeriod.startTime = constrainable.ConstraintObject.ReferencePeriod.StartTime.Date;
                        dataflowRefType.Constraint.ReferencePeriod = referencePeriod;

                        if (constrainable.ConstraintObject.ReferencePeriod.EndTime != null
                            && constrainable.ConstraintObject.ReferencePeriod.EndTime.Date != default(DateTime))
                        {
                            referencePeriod.endTime = constrainable.ConstraintObject.ReferencePeriod.EndTime.Date;
                        }
                    }
                }
            }
        }

        /// <summary>
        ///     Build cube region from <paramref name="cubeRegionObject" /> and add it to <paramref name="dataflowRefType" />
        /// </summary>
        /// <param name="cubeRegionObject">
        ///     The cube Region Object.
        /// </param>
        /// <param name="dataflowRefType">
        ///     The dataflow ref type.
        /// </param>
        private static void BuildCubeRegion(ICubeRegion cubeRegionObject, DataflowRefType dataflowRefType)
        {
            if (cubeRegionObject != null)
            {
                var cubeRegion = new CubeRegionType { isIncluded = true };
                dataflowRefType.Constraint.CubeRegion.Add(cubeRegion);
                foreach (var keyValuese in cubeRegionObject.KeyValues)
                {
                    BuildMember(keyValuese, cubeRegion);
                }
            }
        }

        /// <summary>
        ///     Build member from <paramref name="keyValuese" /> and add it to <paramref name="cubeRegion" />
        /// </summary>
        /// <param name="keyValuese">
        ///     The key values.
        /// </param>
        /// <param name="cubeRegion">
        ///     The cube region.
        /// </param>
        private static void BuildMember(IKeyValues keyValuese, CubeRegionType cubeRegion)
        {
            var member = new MemberType { ComponentRef = keyValuese.Id, isIncluded = true };
            cubeRegion.Member.Add(member);
            foreach (var value in keyValuese.Values)
            {
                switch (value)
                {
                    case SpecialValues.DummyMemberValue:
                        break;
                    default:
                        member.MemberValue.Add(new MemberValueType { Value = value });
                        break;
                }
            }
        }

        /// <summary>
        ///     The build structure queries.
        /// </summary>
        /// <param name="structureQuery">
        ///     The structure queries.
        /// </param>
        /// <param name="requestType">
        ///     The request type.
        /// </param>
        private static void BuildStructureQuery(
            IStructureReference structureQuery, 
            QueryStructureRequestType requestType)
        {
            switch (structureQuery.MaintainableStructureEnumType.EnumType)
            {
                case SdmxStructureEnumType.CodeList:
                    _referenceTransform.AddCodelist(structureQuery, requestType.CodelistRef);
                    break;
                case SdmxStructureEnumType.HierarchicalCodelist:
                    _referenceTransform.AddHierarchicalCodelist(structureQuery, requestType.HierarchicalCodelistRef);
                    break;
                case SdmxStructureEnumType.CategoryScheme:
                    _referenceTransform.AddCategoryScheme(structureQuery, requestType.CategorySchemeRef);
                    break;
                case SdmxStructureEnumType.ConceptScheme:
                    _referenceTransform.AddConceptScheme(structureQuery, requestType.ConceptSchemeRef);
                    break;
                case SdmxStructureEnumType.Dsd:
                    _referenceTransform.AddKeyFamily(structureQuery, requestType.KeyFamilyRef);
                    break;
                case SdmxStructureEnumType.Dataflow:
                {
                    var dataflowRefType = _referenceTransform.AddDataflow(structureQuery, requestType.DataflowRef);
                    BuildConstrainable(structureQuery, dataflowRefType);
                }

                    break;
                case SdmxStructureEnumType.Msd:
                    _referenceTransform.AddMetadataStructure(structureQuery, requestType.MetadataStructureRef);
                    break;

                case SdmxStructureEnumType.MetadataFlow:
                    _referenceTransform.AddMetadataflow(structureQuery, requestType.MetadataflowRef);
                    break;
                case SdmxStructureEnumType.Process:
                    _referenceTransform.AddProcess(structureQuery, requestType.ProcessRef);
                    break;
                case SdmxStructureEnumType.StructureSet:
                    _referenceTransform.AddStructureSet(structureQuery, requestType.StructureSetRef);
                    break;
            }
        }

        /// <summary>
        ///     The build registry interface.
        /// </summary>
        /// <param name="requestType">
        ///     The request type.
        /// </param>
        /// <returns>
        ///     The <see cref="XDocument" />.
        /// </returns>
        private XDocument BuildRegistryInterface(QueryStructureRequestType requestType)
        {
            var registry = new RegistryInterface();

            if (this._header == null)
            {
                this._header = new HeaderImpl(UnknownId, UnknownId);
            }

            registry.Content.Header = this._headerXmlBuilder.Build(this._header);

            registry.Content.QueryStructureRequest = requestType;

            return new XDocument(registry.Untyped);
        }
    }
}