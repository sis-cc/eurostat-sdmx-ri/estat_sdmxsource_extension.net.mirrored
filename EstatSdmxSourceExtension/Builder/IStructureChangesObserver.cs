using System.Collections.Generic;
using System.Security.Principal;
using System.Threading.Tasks;
using Estat.Sdmxsource.Extension.Model.Error;

namespace Estat.Sdmxsource.Extension.Builder
{
    public interface IStructureChangesObserver
    {
        Task Notify(IList<IResponseWithStatusObject> summary, IPrincipal principal);
    }
}