﻿// -----------------------------------------------------------------------
// <copyright file="EncodingBuilder.cs" company="EUROSTAT">
//   Date Created : 2016-07-22
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sdmxsource.Extension.Builder
{
    using System.Text;

    /// <summary>
    /// The culture info builder.
    /// </summary>
    public class EncodingBuilder : GenericFromAcceptBuilder<Encoding>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EncodingBuilder"/> class.
        /// </summary>
        public EncodingBuilder() : this(new UTF8Encoding(false))
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="EncodingBuilder"/> class.
        /// </summary>
        /// <param name="defaultValue">The default culture.</param>
        public EncodingBuilder(Encoding defaultValue) : base(defaultValue, s => Encoding.GetEncoding(s.Value))
        {
        }
    }
}