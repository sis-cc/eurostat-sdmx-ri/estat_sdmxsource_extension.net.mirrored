﻿// -----------------------------------------------------------------------
// <copyright file="SortedAcceptHeadersBuilder.cs" company="EUROSTAT">
//   Date Created : 2016-07-22
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sdmxsource.Extension.Builder
{
    using System.Collections.Generic;
    using System.Globalization;
    using System.Net;
    using System.Text;

    using Estat.Sdmxsource.Extension.Model;

    using Org.Sdmxsource.Sdmx.Api.Builder;

    /// <summary>
    /// The sorted accept headers builder.
    /// </summary>
    public class SortedAcceptHeadersBuilder : IBuilder<ISortedAcceptHeaders, WebHeaderCollection>
    {
        /// <summary>
        /// The _culture information builder
        /// </summary>
        private readonly CultureInfoBuilder _cultureInfoBuilder = new CultureInfoBuilder();

        /// <summary>
        /// The _char set builder
        /// </summary>
        private readonly EncodingBuilder _charSetBuilder = new EncodingBuilder();

        /// <summary>
        /// The _sorted accept value builder
        /// </summary>
        private readonly SortedAcceptValueBuilder _sortedAcceptValueBuilder = new SortedAcceptValueBuilder();

        /// <summary>
        /// The _content type builder
        /// </summary>
        private readonly ContentTypeBuilder _contentTypeBuilder = new ContentTypeBuilder();

        /// <summary>
        /// Builds an object of type ISortedAcceptHeaders
        /// </summary>
        /// <param name="buildFrom">An Object to build the output object from</param>
        /// <returns>
        /// Object of type ISortedAcceptHeaders
        /// </returns>
        public ISortedAcceptHeaders Build(WebHeaderCollection buildFrom)
        {
            var acceptContentTokens = this._sortedAcceptValueBuilder.Build(buildFrom[HttpRequestHeader.Accept]);
            if (acceptContentTokens.Count == 0)
            {
                acceptContentTokens = new[] { new HeaderValueWithQuality<string>("*/*", 1.0m) };
            }

            var acceptContentTypes =
                this._contentTypeBuilder.Build(
                    acceptContentTokens);
            var acceptLanguages =
                this._cultureInfoBuilder.Build(
                    this._sortedAcceptValueBuilder.Build(buildFrom[HttpRequestHeader.AcceptLanguage]));
            var acceptCharSets =
               this._charSetBuilder.Build(
                   this._sortedAcceptValueBuilder.Build(buildFrom[HttpRequestHeader.AcceptCharset]));

            return new SortedAcceptHeaders(acceptContentTypes, acceptLanguages, acceptCharSets);
        }
    }
}