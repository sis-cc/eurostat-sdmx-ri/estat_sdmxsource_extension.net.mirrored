// -----------------------------------------------------------------------
// <copyright file="IStoreIdBuilder.cs" company="EUROSTAT">
//   Date Created : 2018-6-5
//   Copyright (c) 2009, 2018 by the European Commission, represented by Eurostat.   All rights reserved.
//
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sdmxsource.Extension.Builder
{
    using System.Security.Principal;

    /// <summary>
    /// Interface for building the store id. In SDMX RI the Store ID is the
    /// </summary>
    public interface IStoreIdBuilder
    {
        /// <summary>
        /// Builds the store id using the default principal or implementation specific method.
        /// </summary>
        /// <returns>The store ID or null</returns>
        string Build();

        /// <summary>
        /// Builds the store id using the specified principal.
        /// </summary>
        /// <param name="principal">The principal.</param>
        /// <returns>The store ID or null</returns>
        string Build(IPrincipal principal);
    }
}