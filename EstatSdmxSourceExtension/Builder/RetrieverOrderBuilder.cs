﻿// -----------------------------------------------------------------------
// <copyright file="RetrieverOrderBuilder.cs" company="EUROSTAT">
//   Date Created : 2015-12-16
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sdmxsource.Extension.Builder
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;

    using Estat.Sdmxsource.Extension.Config;
    using Estat.Sdmxsource.Extension.Factory;

    using log4net;

    using Org.Sdmxsource.Sdmx.Api.Builder;

    /// <summary>
    ///     Takes a unordered list of <see cref="IRetrieverFactory" /> and returns a ordered list
    /// </summary>
    /// <remarks>
    ///     In case an implementation’s Id is not specified then it will be considered to be the last.
    ///     If more than one Id is not specified in the configuration then they will be ordered according to the
    ///     alphanumeric value of the Id values in ascending order, for example implementations that start with
    ///     A will be first, B next etc. If the configuration is empty then by default the SDMX RI implementation
    ///     will be the first, all other will be sorted according the their alphanumeric value in their Id in ascending order.
    ///     A warning should be logged so administrators are aware. On the other hand if an Id is specified but it cannot be
    ///     resolved to an implementation, then it should be ignored and a warning should be logged.
    /// </remarks>
    public class RetrieverOrderBuilder : IBuilder<IList<IRetrieverFactory>, IEnumerable<IRetrieverFactory>>
    {
        /// <summary>
        ///     The default implementation identifier (SDMX RI Mapping Store)
        /// </summary>
        private const string DefaultImplId = "MappingStoreRetrieversFactory";

        /// <summary>
        ///     The _log
        /// </summary>
        private static readonly ILog _log = LogManager.GetLogger(typeof(RetrieverOrderBuilder));

        /// <summary>
        ///     The _comparer
        /// </summary>
        private readonly RetrieverFactoryComparer _comparer;

        /// <summary>
        ///     The _configured order
        /// </summary>
        private readonly List<string> _configuredOrder;

        /// <summary>
        ///     Initializes a new instance of the <see cref="RetrieverOrderBuilder" /> class.
        /// </summary>
        /// <param name="configuredOrder">The configured order.</param>
        public RetrieverOrderBuilder(IList<string> configuredOrder)
        {
            this._configuredOrder = new List<string>(configuredOrder);

            this._comparer = this.BuildRetrieverFactoryComparer();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="RetrieverOrderBuilder" /> class.
        /// </summary>
        /// <remarks>This constructor will look in the .Config file for retrievers at the <see cref="ModuleConfigSection"/> section.</remarks>
        public RetrieverOrderBuilder()
        {
            var moduleConfig = ModuleConfigSection.GetSection();
            this._configuredOrder = new List<string>();
            if (moduleConfig != null)
            {
                foreach (RetrieverConfig retriever in moduleConfig.Retrievers)
                {
                    this._configuredOrder.Add(retriever.Name);
                }
            }

            this._comparer = this.BuildRetrieverFactoryComparer();
        }

        /// <summary>
        ///     Builds the specified retriever factories.
        /// </summary>
        /// <param name="retrieverFactories">The retriever factories.</param>
        /// <returns>The <see cref="IList{RetrieverFactory}" />.</returns>
        public IList<IRetrieverFactory> Build(IEnumerable<IRetrieverFactory> retrieverFactories)
        {
            var factories = new List<IRetrieverFactory>(retrieverFactories);
            factories.Sort(this._comparer);

            this.CheckConfiguration(factories);
            return factories;
        }

        /// <summary>
        ///     Builds the retriever factory comparer.
        /// </summary>
        /// <returns>The <see cref="RetrieverFactoryComparer" />.</returns>
        private RetrieverFactoryComparer BuildRetrieverFactoryComparer()
        {
            // The SDMX RI should be the default if nothing is configured.
            if (this._configuredOrder.Count == 0)
            {
                _log.WarnFormat(CultureInfo.InvariantCulture, "No configuration found for modules. Adding default: " + DefaultImplId);
                this._configuredOrder.Add(DefaultImplId);
            }

            var retrieverFactoryComparer = new RetrieverFactoryComparer(this._configuredOrder);
            return retrieverFactoryComparer;
        }

        /// <summary>
        ///     Checks the configuration.
        /// </summary>
        /// <param name="factories">The factories.</param>
        private void CheckConfiguration(IList<IRetrieverFactory> factories)
        {
            // report any configured ids not in the specified list.
            var enumerable = new HashSet<string>(factories.Select(factory => factory.Id), StringComparer.Ordinal);
            foreach (var source in this._configuredOrder.Except(enumerable, StringComparer.Ordinal))
            {
                _log.WarnFormat("Retriever factory with ID {0} configured but was not found.", source);
            }
        }
    }
}