﻿// -----------------------------------------------------------------------
// <copyright file="StructureSetBuilder.cs" company="EUROSTAT">
//   Date Created : 2017-10-04
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sdmxsource.Extension.Builder
{
    using System;
    using System.Globalization;
    using System.Linq;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
    using Org.Sdmxsource.Sdmx.Api.Model;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Mapping;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Mapping;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Mapping;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;

    /// <summary>
    /// The Structure Set from DSD/Dataflow builder
    /// </summary>
    public class StructureSetBuilder
    {
        /// <summary>
        /// Builds a <see cref="IStructureSetObject"/> between <paramref name="source"/> and <paramref name="target"/>
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="target">The target.</param>
        /// <param name="retrievalManager">The retrieval manager.</param>
        /// <returns>A <see cref="IStructureSetObject"/> if mapping is possible; otherwise <c>null</c></returns>
        /// <exception cref="System.ArgumentNullException">
        /// source
        /// or
        /// target
        /// or
        /// retrievalManager
        /// </exception>
        /// <exception cref="SdmxSemmanticException">
        /// Cannot create StructureSet between the same data structure and/or dataflow
        /// or
        /// Cannot create Structure set between the same data structure
        /// </exception>
        /// <exception cref="SdmxNoResultsException">
        /// Could not find the source Data Structure (DSD)
        /// or
        /// Could not find the target Data Structure (DSD)
        /// </exception>
        public IStructureSetObject Build(IStructureReference source, IStructureReference target, ISdmxObjectRetrievalManager retrievalManager)
        {
            if (source == null)
            {
                throw new ArgumentNullException(nameof(source));
            }

            if (target == null)
            {
                throw new ArgumentNullException(nameof(target));
            }

            if (retrievalManager == null)
            {
                throw new ArgumentNullException(nameof(retrievalManager));
            }

            Validate(source);
            Validate(target);

            if (source.Equals(target))
            {
                throw new SdmxSemmanticException("Cannot create StructureSet between the same data structure and/or dataflow");
            }

            var sourceDsd = GetDsd(source, retrievalManager);

            if (sourceDsd == null)
            {
                throw new SdmxNoResultsException("Could not find the source Data Structure (DSD)");
            }

            var targetDsd = GetDsd(target, retrievalManager);
            if (targetDsd == null)
            {
                throw new SdmxNoResultsException("Could not find the target Data Structure (DSD)");
            }

            return BuildStructureSetObject(source, target, retrievalManager, sourceDsd, targetDsd);
        }

        /// <summary>
        /// Builds the structure set object.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="target">The target.</param>
        /// <param name="retrievalManager">The retrieval manager.</param>
        /// <param name="sourceDsd">The source DSD.</param>
        /// <param name="targetDsd">The target DSD.</param>
        /// <returns>
        /// The <see cref="IStructureSetObject" />.
        /// </returns>
        private static IStructureSetObject BuildStructureSetObject(IStructureReference source, IStructureReference target, ISdmxObjectRetrievalManager retrievalManager, IDataStructureObject sourceDsd, IDataStructureObject targetDsd)
        {
            var structureSet = new StructureSetMutableCore();
            structureSet.Id = "SS_" + Guid.NewGuid().ToString("N");
            structureSet.AddName("en", string.Format(CultureInfo.InvariantCulture, "Structure Set between {0} and {1}", source.TargetUrn, target.TargetUrn));
            structureSet.AgencyId = sourceDsd.AgencyId;
            structureSet.Version = "1.0";

            var structureMap = new StructureMapMutableCore();
            structureMap.Id = string.Format(CultureInfo.InvariantCulture, "SMAP_{0}_{1}", sourceDsd.Id, targetDsd.Id);
            structureMap.AddName("en", string.Format(CultureInfo.InvariantCulture, "Structure Map between {0} and {1}", source.TargetUrn, target.TargetUrn));
            structureMap.SourceRef = source;
            structureMap.TargetRef = target;
            foreach (var sourceComponent in sourceDsd.Components)
            {
                var targetComponent = targetDsd.GetComponent(sourceComponent.Id);
                if (targetComponent != null)
                {
                    var componentMap = new ComponentMapMutableCore();
                    componentMap.MapConceptRef = sourceComponent.Id;
                    componentMap.MapTargetConceptRef = targetComponent.Id;
                    if (sourceComponent.HasCodedRepresentation() && targetComponent.HasCodedRepresentation())
                    {
                        var sourceReference = sourceComponent.Representation.Representation;
                        var targetReference = targetComponent.Representation.Representation;
                        if (!sourceReference.Equals(targetReference))
                        {
                            // TODO support measure dimension concept scheme mapping
                            // if (sourceReference.TargetReference.EnumType == SdmxStructureEnumType.CodeList)
                            {
                                componentMap.RepMapRef = new RepresentationMapRefMutableCore();
                                componentMap.RepMapRef.CodelistMap = BuildCodelistMap(sourceReference, targetReference, retrievalManager, structureSet);
                            }
                        }
                    }

                    structureMap.AddComponent(componentMap);
                }
            }

            if (structureMap.Components.Count == 0)
            {
                return null;
            }

            structureSet.AddStructureMap(structureMap);
            return structureSet.ImmutableInstance;
        }

        /// <summary>
        /// Builds the reference.
        /// </summary>
        /// <param name="structureSet">The structure set.</param>
        /// <param name="codelistMap">The codelist map.</param>
        /// <returns>
        /// The <see cref="IStructureReference" />.
        /// </returns>
        private static IStructureReference BuildReference(IStructureSetMutableObject structureSet, ICodelistMapMutableObject codelistMap)
        {
            return new StructureReferenceImpl(structureSet.AgencyId, structureSet.Id, structureSet.Version, SdmxStructureEnumType.CodeListMap, codelistMap.Id);
        }

        /// <summary>
        /// Builds the codelist map.
        /// </summary>
        /// <param name="sourceReference">The source reference.</param>
        /// <param name="targetReference">The target reference.</param>
        /// <param name="retrievalManager">The retrieval manager.</param>
        /// <param name="structureSet">The structure set.</param>
        /// <returns>
        /// The <see cref="IStructureReference" />.
        /// </returns>
        /// <exception cref="SdmxNoResultsException">Could not find codelist used by source component. The codelist URN is " + sourceReference.TargetUrn
        /// or
        /// Could not find codelist used by target component. The codelist URN is " + targetReference.TargetUrn</exception>
        private static IStructureReference BuildCodelistMap(ICrossReference sourceReference, ICrossReference targetReference, ISdmxObjectRetrievalManager retrievalManager, StructureSetMutableCore structureSet)
        {
            var codelistMap = new CodelistMapMutableCore();
            codelistMap.Id = string.Format(CultureInfo.InvariantCulture, "CLM_{0}_{1}", sourceReference.MaintainableId, targetReference.MaintainableId);
            codelistMap.AddName("en", string.Format(CultureInfo.InvariantCulture, "Codelist map between '{0}' and '{1}'", sourceReference.TargetUrn, targetReference.TargetUrn));
            var sourceCodelist = retrievalManager.GetMaintainableObject<ICodelistObject>(sourceReference);
            if (sourceCodelist == null)
            {
                throw new SdmxNoResultsException("Could not find codelist used by source component. The codelist URN is " + sourceReference.TargetUrn);
            }

            var targetCodelist = retrievalManager.GetMaintainableObject<ICodelistObject>(targetReference);
            if (targetCodelist == null)
            {
                throw new SdmxNoResultsException("Could not find codelist used by target component. The codelist URN is " + targetReference.TargetUrn);
            }

            codelistMap.SourceRef = sourceCodelist.AsReference;
            codelistMap.TargetRef = targetCodelist.AsReference;

            foreach (var sourceCode in sourceCodelist.Items)
            {
                var targetCode = targetCodelist.GetCodeById(sourceCode.Id);
                if (targetCode != null)
                {
                    var itemMap = new ItemMapMutableCore();
                    itemMap.SourceId = sourceCode.Id;
                    itemMap.TargetId = targetCode.Id;
                    codelistMap.AddItem(itemMap);
                }
            }

            structureSet.AddCodelistMap(codelistMap);

            return BuildReference(structureSet, codelistMap);
        }

        /// <summary>
        /// Validates the specified structure reference.
        /// </summary>
        /// <param name="structureReference">The structure reference.</param>
        /// <exception cref="SdmxSemmanticException">The structure reference is missing an agency id and/or maintainable id</exception>
        private static void Validate(IStructureReference structureReference)
        {
            if (structureReference.HasAgencyId() && structureReference.HasMaintainableId())
            {
                return;
            }

            throw new SdmxSemmanticException("The structure reference is missing an agency id and/or maintainable id");
        }

        /// <summary>
        /// Gets the DSD.
        /// </summary>
        /// <param name="structureReference">The structure reference.</param>
        /// <param name="retrievalManager">The retrieval manager.</param>
        /// <returns>
        /// The <see cref="IDataStructureObject" />.
        /// </returns>
        /// <exception cref="SdmxNotImplementedException">StructureSet builder supports only DSD or Dataflow</exception>
        private static IDataStructureObject GetDsd(IStructureReference structureReference, ISdmxObjectRetrievalManager retrievalManager)
        {
            switch (structureReference.TargetReference.EnumType)
            {
                case SdmxStructureEnumType.Dataflow:
                    var sdmxObjects = retrievalManager.GetSdmxObjects(structureReference, ResolveCrossReferences.ResolveExcludeAgencies);
                    return sdmxObjects.DataStructures.FirstOrDefault();
                case SdmxStructureEnumType.Dsd:
                    return retrievalManager.GetMaintainableObject<IDataStructureObject>(structureReference);
            }

            throw new SdmxNotImplementedException("StructureSet builder supports only DSD or Dataflow");
        }
    }
}