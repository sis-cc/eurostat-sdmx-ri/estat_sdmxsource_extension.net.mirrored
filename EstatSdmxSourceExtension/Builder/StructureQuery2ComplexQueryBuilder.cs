// -----------------------------------------------------------------------
// <copyright file="StructureQuery2ComplexQueryBuilder.cs" company="EUROSTAT">
//   Date Created : 2013-08-20
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace Estat.Sdmxsource.Extension.Builder
{
    using System;

    using Extension;

    using Org.Sdmxsource.Sdmx.Api.Builder;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Complex;
    using Org.Sdmxsource.Sdmx.Api.Model.Query;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference.Complex;
    using Org.Sdmxsource.Sdmx.Util.Extension;

    /// <summary>
    ///     Builder class of IComplexStructureQuery object from IRestStructureQuery object.
    ///     Used for transforming messages in SDMX 2.0 version to SDMX 2.1 Messages.
    /// </summary>
    public class StructureQuery2ComplexQueryBuilder : IBuilder<IComplexStructureQuery, IRestStructureQuery>
    {
        /// <summary>
        ///     The build
        /// </summary>
        /// <param name="buildFrom">The structure query</param>
        /// <returns>The complex structure</returns>
        public virtual IComplexStructureQuery Build(IRestStructureQuery buildFrom)
        {
            if (buildFrom == null)
            {
                throw new ArgumentNullException("buildFrom");
            }

            var complexQueryReferenceBeans = new HashSet<IComplexStructureReferenceObject>();
            foreach (var structureRef in buildFrom.StructureReferences)
            {
                IStructureReference queryReferenceBean = structureRef.ChangeStarsToNull();
                IComplexStructureReferenceObject complexQueryReferenceBean = this.GetComplexReference(
                    queryReferenceBean,
                    buildFrom.StructureQueryMetadata.IsReturnLatest, buildFrom.StructureQueryMetadata.IsReturnStable, buildFrom.SpecificItems);
                complexQueryReferenceBeans.Add(complexQueryReferenceBean);
            }
            IComplexStructureQueryMetadata complexStructureQueryMetadata = this.GetComplexStructureQueryMetadata(buildFrom.StructureQueryMetadata);

            IComplexStructureQuery complexStructureQuery = new ComplexStructureQueryCore(
                complexQueryReferenceBeans, 
                complexStructureQueryMetadata);
            return complexStructureQuery;
        }

        /// <summary>
        ///     The get complex reference
        /// </summary>
        /// <param name="reference">The structure reference</param>
        /// <param name="isReturnLatest">if set to <c>true</c> [is return latest].</param>
        /// /// <param name="isReturnStable">if set to <c>true</c> [is return stable].</param>
        /// <param name="specificItems"></param>
        /// <returns>the complex structure ref object</returns>
        private IComplexStructureReferenceObject GetComplexReference(IStructureReference reference, bool isReturnLatest, bool isReturnStable, ISet<string> specificItems)
        {
            /*create an instance of ComplexStructureReferenceBean for the sRef provided */
            IMaintainableRefObject maintainableRefRef = reference.MaintainableReference;
            IComplexTextReference agencyId = null;
            if (maintainableRefRef.AgencyId != null)
            {
                agencyId = new ComplexTextReferenceCore(
                    "en", 
                    TextSearch.GetFromEnum(TextSearchEnumType.Equal), 
                    maintainableRefRef.AgencyId);
            }

            IComplexTextReference id = null;
            if (maintainableRefRef.MaintainableId != null)
            {
                id = new ComplexTextReferenceCore(
                    "en", 
                    TextSearch.GetFromEnum(TextSearchEnumType.Equal), 
                    maintainableRefRef.MaintainableId);
            }

            IComplexVersionReference versionRef = null;
            TertiaryBool returnLatest = TertiaryBool.ParseBoolean(isReturnLatest);
            TertiaryBool returnStable = TertiaryBool.ParseBoolean(isReturnStable);
            versionRef = new ComplexVersionReferenceCore(
                    returnLatest,
                    returnStable,
                    maintainableRefRef.Version,
                    null,
                    null);

            var childRefs = new List<IComplexIdentifiableReferenceObject>();
            if (specificItems != null)
            {
                foreach (var specificItem in specificItems)
                {
                    if (string.IsNullOrWhiteSpace(specificItem))
                    {
                        continue;
                    }

                    var complexTextReferenceCore = new ComplexTextReferenceCore(
                        "en",
                        TextSearch.GetFromEnum(TextSearchEnumType.Equal),
                        specificItem);
                    var complexIdentifiableReferenceCore = new ComplexIdentifiableReferenceCore(
                        complexTextReferenceCore,
                        reference.TargetReference,
                        null,
                        null,
                        null,
                        null);
                    childRefs.Add(complexIdentifiableReferenceCore);
                }
            }

            IComplexStructureReferenceObject complexStructureRef = new ComplexStructureReferenceCore(
                agencyId, 
                id, 
                versionRef, 
                reference.TargetReference, 
                null, 
                null, 
                null, 
                childRefs);
            return complexStructureRef;
        }

        /// <summary>
        ///     The get complex structure query metadata
        /// </summary>
        /// <param name="queryMetadata">The structure type.</param>
        /// <returns>The complex structure query data</returns>
        private IComplexStructureQueryMetadata GetComplexStructureQueryMetadata(IStructureQueryMetadata queryMetadata)
        {
            ComplexStructureQueryDetail queryDetail = queryMetadata.StructureQueryDetail.ToComplex();
            ComplexMaintainableQueryDetail complexMaintainableQuery =
                queryMetadata.StructureQueryDetail.ToComplexReference();

            /*create an instance of ComplexStructureQueryMetadata  */
            var referenceSpecificStructures = queryMetadata.SpecificStructureReference != null
                ? new[] { queryMetadata.SpecificStructureReference }
                : null;
            IComplexStructureQueryMetadata complexStructureQueryMetadata = new ComplexStructureQueryMetadataCore(
                false, 
                queryDetail, 
                complexMaintainableQuery, 
                queryMetadata.StructureReferenceDetail, 
                referenceSpecificStructures);

            return complexStructureQueryMetadata;
        }
    }
}
