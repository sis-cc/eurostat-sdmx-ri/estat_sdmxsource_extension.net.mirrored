﻿// -----------------------------------------------------------------------
// <copyright file="RetrieverFactoryComparer.cs" company="EUROSTAT">
//   Date Created : 2015-12-16
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sdmxsource.Extension.Builder
{
    using System;
    using System.Collections.Generic;

    using Estat.Sdmxsource.Extension.Factory;

    /// <summary>
    /// This class is responsible for ordering a list/collection of <see cref="IRetrieverFactory"/>
    /// </summary>
    /// <remarks>
    /// In case an implementation’s Id is not specified then it will be considered to be the last. 
    /// If more than one Id is not specified in the configuration then they will be ordered according to the 
    /// alphanumeric value of the Id values in ascending order, for example implementations that start with 
    /// A will be first, B next etc. If the configuration is empty then by default the SDMX RI implementation 
    /// will be the first, all other will be sorted according the their alphanumeric value in their Id in ascending order. 
    /// A warning should be logged so administrators are aware. On the other hand if an Id is specified but it cannot be 
    /// resolved to an implementation, then it should be ignored and a warning should be logged. 
    /// </remarks>
    public class RetrieverFactoryComparer : IComparer<IRetrieverFactory>
    {
        /// <summary>
        /// The _configured order
        /// </summary>
        private readonly IList<string> _configuredOrder;

        /// <summary>
        /// Initializes a new instance of the <see cref="RetrieverFactoryComparer" /> class.
        /// </summary>
        /// <param name="configuredOrder">The configured order.</param>
        public RetrieverFactoryComparer(IList<string> configuredOrder)
        {
            this._configuredOrder = configuredOrder;
        }

        /// <summary>
        /// Compares two objects and returns a value indicating whether one is less than, equal to, or greater than the other.
        /// </summary>
        /// <returns>
        /// A signed integer that indicates the relative values of <paramref name="x"/> and <paramref name="y"/>, as shown in the following table.Value Meaning Less than zero<paramref name="x"/> is less than <paramref name="y"/>.Zero<paramref name="x"/> equals <paramref name="y"/>.Greater than zero<paramref name="x"/> is greater than <paramref name="y"/>.
        /// </returns>
        /// <param name="x">The first object to compare.</param><param name="y">The second object to compare.</param>
        public int Compare(IRetrieverFactory x, IRetrieverFactory y)
        {
            if (x == null && y == null)
            {
                return 0;
            }

            if (x == null)
            {
                return 1;
            }

            if (y == null)
            {
                return -1;
            }

            int ix = this._configuredOrder.IndexOf(x.Id);
            int iy = this._configuredOrder.IndexOf(y.Id);
            if (ix == iy)
            {
                return string.Compare(x.Id, y.Id, StringComparison.Ordinal);
            }

            if (ix > -1 && iy > -1)
            {
                return ix.CompareTo(iy);
            }

            if (ix == -1)
            {
                return 1;
            }

            if (iy == -1)
            {
                return -1;
            }

            return string.Compare(x.Id, y.Id, StringComparison.Ordinal);
        }
    }
}