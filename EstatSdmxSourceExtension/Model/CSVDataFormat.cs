﻿// -----------------------------------------------------------------------
// <copyright file="CSVDataFormat.cs" company="EUROSTAT">
//   Date Created : 2016-09-28
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sdmxsource.Extension.Model
{
    using System.Globalization;
    using System.Text;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Data;

    /// <summary>
    /// Data format for CSV
    /// </summary>
    public class CsvDataFormat : AbstractRestFormat, IDataFormat
    {
        /// <summary>
        /// The _CSV writer options
        /// </summary>
        private readonly CsvWriterOptions _csvWriterOptions;
       
        /// <summary>
        /// The _format as string
        /// </summary>
        private readonly string _formatAsString;
       
        /// <summary>
        /// The _data type
        /// </summary>
        private readonly DataType _dataType;

        /// <summary>
        /// Initializes a new instance of the <see cref="CsvDataFormat"/> class.
        /// </summary>
        /// <param name="dataType">Type of the data.</param>
        /// <param name="csvWriterOptions">Options for the CSV writer</param>
        /// <param name="encoding">The encoding</param>
        public CsvDataFormat(DataType dataType, CsvWriterOptions csvWriterOptions, Encoding encoding)
            : base(encoding)
        {
            this._csvWriterOptions = csvWriterOptions;
            this._dataType = dataType;
            this._formatAsString = string.Format(CultureInfo.InvariantCulture, "SDMX RI Data Format {0};charSet={1}", dataType, this.Encoding);
        }

        /// <summary>
        /// Gets the CSV writer options.
        /// </summary>
        /// <value>
        /// The CSV writer options.
        /// </value>
        public CsvWriterOptions CsvWriterOptions
        {
            get { return this._csvWriterOptions; }
        }

        /// <summary>
        /// Gets a string representation of the format, that can be used for auditing and debugging purposes.
        ///                 <p/>
        ///                 This is expected to return a not null response.
        /// </summary>
        public string FormatAsString
        {
            get { return this._formatAsString; }
        }

        /// <summary>
        /// Gets the sdmx data format that this interface is describing.
        ///                 If this is not describing an SDMX message then this will return null and the implementation class will be expected
        ///                 to expose additional methods to understand the data
        /// </summary>
        public DataType SdmxDataFormat
        {
            get { return this._dataType; }
        }
    }
}