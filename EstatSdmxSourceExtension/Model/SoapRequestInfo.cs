﻿// -----------------------------------------------------------------------
// <copyright file="SoapRequestInfo.cs" company="EUROSTAT">
//   Date Created : 2017-12-4
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sdmxsource.Extension.Model
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// A model class that holds information for a SOAP request
    /// </summary>
    public class SoapRequestInfo
    {
        /// <summary>
        /// Gets or sets the operation.
        /// </summary>
        /// <value>
        /// The operation.
        /// </value>
        public string Operation { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether to write operation.
        /// </summary>
        /// <value>
        ///   <c>true</c> if [write operation]; otherwise, <c>false</c>.
        /// </value>
        public bool WriteOperation { get; set; }

        /// <summary>
        /// Gets or sets the name of the parameter. This is for old .NET based Web Services only which require an extra element around SDMX
        /// </summary>
        /// <value>
        /// The name of the parameter.
        /// </value>
        public string ParameterName { get; set; }

        /// <summary>
        /// Gets or sets the target WSDL namespace.
        /// </summary>
        /// <value>
        /// The target namespace.
        /// </value>
        public string TargetNamespace { get; set; }

        /// <summary>
        /// Gets or sets the prefix.
        /// </summary>
        /// <value>
        /// The prefix.
        /// </value>
        public string Prefix { get; set; }
    }
}
