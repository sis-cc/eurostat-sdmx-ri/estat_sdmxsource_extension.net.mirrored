// -----------------------------------------------------------------------
// <copyright file="WSDLSettings.cs" company="EUROSTAT">
//   Date Created : 2017-06-14
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System.Linq;
using System.Xml.Linq;

namespace Estat.Sdmxsource.Extension.Model
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Xml;
    using System.Xml.Schema;

    using Manager;
    using System.Net;
    using System.IO.Compression;
    //using System.Web.Services.Description;

    using Estat.Sdmxsource.Extension.Util;

    /// <summary>
    /// A class that holds the settings extracted from A WSDL
    /// </summary>
    //TODO to make it configurable so we don't curse ourselves so much in the future
    public class WSDLSettings
    {
        #region Constants and Fields

        /// <summary>
        ///     This field holds the a map between a web service operation name
        ///     and the parameter wrapper element name. This is used for connecting to .NET WS.
        /// </summary>
        private readonly Dictionary<string, string> _operationParameterName = new Dictionary<string, string>();

        /// <summary>
        ///     This field holds the a map between a web service operation name
        ///     and the soap action
        /// </summary>
        private readonly Dictionary<string, string> _soapAction = new Dictionary<string, string>();

        protected internal string targetNamespace;

        /// <summary>
        ///     Holds the service description of the WSDL
        /// </summary>
        //private ServiceDescription _wsdl;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="WSDLSettings" /> class.
        /// Initialize a new instance of the WSDLSettings class with NSIClientSettings configuration
        /// </summary>
        /// <param name="config">The <see cref="WsInfo" /> config used to retrieve the WSDL URL and other information such as proxy</param>
        public WSDLSettings(WsInfo config)
        {
            this.GetWSDL(config);
            this.BuildOperationParameterName();
            this.ΒuildSoapActionMap();
        }

        #endregion

        #region Public Methods

        /// <summary>
        ///     Get the parameter element name if one exists for the given operation
        /// </summary>
        /// <param name="operationName">
        ///     The name of the operation or web method or function
        /// </param>
        /// <returns>
        ///     null if there isn't a parameter element name e.g. in Java or the parameter element name (e.g. in .net)
        /// </returns>
        public string GetParameterName(string operationName)
        {
            return null;
        }

        /// <summary>
        ///     Get the soapAction URI if one exists for the given operation
        /// </summary>
        /// <param name="operationName">
        ///     The name of the operation or web method or function
        /// </param>
        /// <returns>
        ///     The soapAction value or null if there isn't a soapAction
        /// </returns>
        public string GetSoapAction(string operationName)
        {
            return "";
        }

        /// <summary>
        /// Checks the operation exists.
        /// </summary>
        /// <param name="operationName">Name of the operation.</param>
        /// <returns></returns>
        public bool CheckOperationExists(string operationName)
        {
            return !"http://ec.europa.eu/eurostat/sdmxregistry/service/2.0".Equals(this.targetNamespace) &&
                   !"http://ec.europa.eu/eurostat/sdmxregistry/service/2.0/extended".Equals(this.targetNamespace);
        }

        /// <summary>
        ///     Get the WSDL target namespace
        /// </summary>
        /// <returns>
        ///     The target namespace
        /// </returns>
        public string GetTargetNamespace()
        {
            return targetNamespace;
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Populate the <see cref="_operationParameterName" /> dictionary with operation.Name to Parameter name
        /// </summary>
        private void BuildOperationParameterName()
        {
            //// Tested with .net and java NSI WS WSDL
            //foreach (XmlSchema schema in this._wsdl.Types.Schemas)
            //{
            //    foreach (XmlSchemaElement element in schema.Elements.Values)
            //    {
            //        if (element.RefName.IsEmpty)
            //        {
            //            var complexType = element.SchemaType as XmlSchemaComplexType;
            //            if (complexType != null)
            //            {
            //                var seq = complexType.Particle as XmlSchemaSequence;
            //                if (seq != null && seq.Items.Count == 1)
            //                {
            //                    var body = seq.Items[0] as XmlSchemaElement;
            //                    if (body != null && !string.IsNullOrEmpty(body.Name))
            //                    {
            //                        if (!this._operationParameterName.ContainsKey(element.Name))
            //                        {
            //                            this._operationParameterName.Add(element.Name, body.Name);
            //                        }
            //                    }
            //                }
            //            }
            //        }
            //    }
            //}
        }

        /// <summary>
        ///     Gets the WSDL.
        /// </summary>
        /// <param name="config">
        ///     The config.
        /// </param>
        private void GetWSDL(WsInfo config)
        {
            var resolver = new XmlProxyUrlResolver();
            resolver.SetConfig(config);

            var settings = new XmlReaderSettings
            {
                IgnoreComments = true,
                IgnoreWhitespace = true,
                XmlResolver = resolver
            };
            string wsdlUrl = config.Wsdl;
            if (string.IsNullOrEmpty(wsdlUrl) && !config.EndPoint.EndsWith("?wsdl", StringComparison.OrdinalIgnoreCase))
            {
                wsdlUrl = string.Format(CultureInfo.InvariantCulture, "{0}?wsdl", config.EndPoint);
            }

            var request = (HttpWebRequest)WebRequest.Create(wsdlUrl);
            request.UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.17 (KHTML, like Gecko) Chrome/24.0.1312.57 Safari/537.17";
            request.Accept = "text/xml";
            WebAuthenticationHelper.SetupWebRequestAuth(config, request);
            using (var response = request.GetResponse())
            {
                using (XmlReader reader = XmlReader.Create(response.GetResponseStream(), settings))
                {
                    var doc = XDocument.Load(reader);
                    var targetNamespaceNode = doc.Descendants().FirstOrDefault(x =>
                        x.HasAttributes && x.Attributes().FirstOrDefault(y => y.Name == "targetNamespace") != null);

                    targetNamespace = targetNamespaceNode.Attributes().FirstOrDefault(y => y.Name == "targetNamespace").Value;
                }
            }
        }

        /// <summary>
        ///     Populate the <see cref="_soapAction" /> dictionary with operation.Name to soapAction URI
        /// </summary>
        private void ΒuildSoapActionMap()
        {
            //foreach (Binding binding in this._wsdl.Bindings)
            //{
            //    foreach (OperationBinding operation in binding.Operations)
            //    {
            //        foreach (object ext in operation.Extensions)
            //        {
            //            var operationBinding = ext as SoapOperationBinding;
            //            if (operationBinding != null)
            //            {
            //                if (!this._soapAction.ContainsKey(operation.Name))
            //                {
            //                    this._soapAction.Add(operation.Name, operationBinding.SoapAction);
            //                }
            //            }
            //        }
            //    }
            //}
        }

        #endregion
    }
}