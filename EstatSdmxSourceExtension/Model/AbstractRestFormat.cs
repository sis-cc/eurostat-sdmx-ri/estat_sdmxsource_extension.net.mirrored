﻿// -----------------------------------------------------------------------
// <copyright file="AbstractRestFormat.cs" company="EUROSTAT">
//   Date Created : 2016-08-22
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sdmxsource.Extension.Model
{
    using System.Text;

    using Org.Sdmxsource.Sdmx.Api.Factory;
    using Org.Sdmxsource.Sdmx.Api.Model.Data;
    using Org.Sdmxsource.Sdmx.Api.Model.Format;

    /// <summary>
    /// A base class for REST <see cref="IDataFormat" /> or <see cref="IStructureFormat" />
    /// </summary>
    /// <remarks>An abstract class which can be used for IData/StructureFormats implementations which will be used for REST. It contains an Encoding property which ideally will contain the requested (or default) encoding. This can be used together with the output Stream provided at the <see cref="IDataWriterFactory.GetDataWriterEngine"/> to build an encoding aware format specific writer. </remarks>
    public abstract class AbstractRestFormat
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AbstractRestFormat"/> class.
        /// </summary>
        /// <param name="encoding">The encoding.</param>
        protected AbstractRestFormat(Encoding encoding)
        {
            this.Encoding = encoding;
        }

        /// <summary>
        /// Gets the encoding.
        /// </summary>
        /// <value>
        /// The encoding.
        /// </value>
        public Encoding Encoding { get; private set; }
    }
}