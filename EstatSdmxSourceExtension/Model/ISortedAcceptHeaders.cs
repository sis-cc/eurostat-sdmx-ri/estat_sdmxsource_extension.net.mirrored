﻿// -----------------------------------------------------------------------
// <copyright file="ISortedAcceptHeaders.cs" company="EUROSTAT">
//   Date Created : 2016-07-22
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sdmxsource.Extension.Model
{
    using System.Collections.Generic;
    using System.Globalization;
    using System.Net.Mime;
    using System.Text;

    /// <summary>
    /// The Sorted Accept Headers interface.
    /// </summary>
    public interface ISortedAcceptHeaders
    {
        /// <summary>
        /// Gets the content types. It will be an empty list if not value was requested
        /// </summary>
        /// <value>
        /// The content types.
        /// </value>
        IList<HeaderValueWithQuality<ContentType>> ContentTypes { get; }

        /// <summary>
        /// Gets the languages. It will be an empty list if not value was requested
        /// </summary>
        /// <value>
        /// The languages.
        /// </value>
        IList<HeaderValueWithQuality<CultureInfo>> Languages { get; }

        /// <summary>
        /// Gets the character sets, It will be an empty list if not value was requested.
        /// </summary>
        /// <value>
        /// The character sets.
        /// </value>
        IList<HeaderValueWithQuality<Encoding>> CharSets { get; }
    }
}