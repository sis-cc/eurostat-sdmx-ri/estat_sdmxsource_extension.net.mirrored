// -----------------------------------------------------------------------
// <copyright file="AcceptHeaderToken.cs" company="EUROSTAT">
//   Date Created : 2016-07-01
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sdmxsource.Extension.Model
{
    using System;

    /// <summary>
    /// The accept header token.
    /// </summary>
    /// <typeparam name="T">The output format type</typeparam>
    public class AcceptHeaderToken<T> : IComparable<AcceptHeaderToken<T>>
    {
        /// <summary>
        /// The _match score
        /// </summary>
        private readonly int _matchScore;

        /// <summary>
        /// The _quality
        /// </summary>
        private readonly float _quality;

        /// <summary>
        /// The _content type
        /// </summary>
        private readonly IRestResponse<T> _restResponse;

        /// <summary>
        /// The _position
        /// </summary>
        private readonly int _position;

        /// <summary>
        /// Initializes a new instance of the <see cref="AcceptHeaderToken{T}"/> class.
        /// </summary>
        /// <param name="matchScore">
        /// The match score.
        /// </param>
        /// <param name="quality">
        /// The quality.
        /// </param>
        /// <param name="restResponse">
        /// Type of the content.
        /// </param>
        /// <param name="position">
        /// The position.
        /// </param>
        public AcceptHeaderToken(int matchScore, float quality, IRestResponse<T> restResponse, int position)
        {
            this._matchScore = matchScore;
            this._quality = quality;
            this._restResponse = restResponse;
            this._position = position;
        }

        /// <summary>
        /// Gets the match score.
        /// </summary>
        /// <value>
        /// The match score.
        /// </value>
        public int MatchScore
        {
            get
            {
                return this._matchScore;
            }
        }

        /// <summary>
        /// Gets the quality.
        /// </summary>
        /// <value>
        /// The quality.
        /// </value>
        public float Quality
        {
            get
            {
                return this._quality;
            }
        }

        /// <summary>
        /// Gets the Rest response which contains the Content Type, format and language
        /// </summary>
        /// <value>
        /// The type of the content.
        /// </value>
        public IRestResponse<T> RestResponse
        {
            get
            {
                return this._restResponse;
            }
        }

        /// <summary>
        /// Gets the position.
        /// </summary>
        /// <value>
        /// The position.
        /// </value>
        public int Position
        {
            get
            {
                return this._position;
            }
        }

        /// <summary>
        /// Implements the operator ==.
        /// </summary>
        /// <param name="left">The left.</param>
        /// <param name="right">The right.</param>
        /// <returns>
        /// The result of the operator.
        /// </returns>
        public static bool operator ==(AcceptHeaderToken<T> left, AcceptHeaderToken<T> right)
        {
            return Equals(left, right);
        }

        /// <summary>
        /// Implements the operator !=.
        /// </summary>
        /// <param name="left">The left.</param>
        /// <param name="right">The right.</param>
        /// <returns>
        /// The result of the operator.
        /// </returns>
        public static bool operator !=(AcceptHeaderToken<T> left, AcceptHeaderToken<T> right)
        {
            return !Equals(left, right);
        }

        /// <summary>
        /// Implements the operator &lt;.
        /// </summary>
        /// <param name="left">The left.</param>
        /// <param name="right">The right.</param>
        /// <returns>
        /// The result of the operator.
        /// </returns>
        public static bool operator <(AcceptHeaderToken<T> left, AcceptHeaderToken<T> right)
        {
            return Compare(left, right) < 0;
        }

        /// <summary>
        /// Implements the operator &gt;.
        /// </summary>
        /// <param name="left">The left.</param>
        /// <param name="right">The right.</param>
        /// <returns>
        /// The result of the operator.
        /// </returns>
        public static bool operator >(AcceptHeaderToken<T> left, AcceptHeaderToken<T> right)
        {
            return Compare(left, right) > 0;
        }

        /// <summary>
        /// Compares the specified left.
        /// </summary>
        /// <param name="left">The left.</param>
        /// <param name="right">The right.</param>
        /// <returns>
        /// A value that indicates the relative order of the objects being compared. The return value has the following meanings: Value Meaning Less than zero <paramref name="left"/> object is less than the <paramref name="right" /> parameter.Zero  <paramref name="left"/> object is equal to <paramref name="right" />. Greater than zero  <paramref name="left"/> object is greater than <paramref name="right" />.
        /// </returns>
        public static int Compare(AcceptHeaderToken<T> left, AcceptHeaderToken<T> right)
        {
            if (ReferenceEquals(left, right))
            {
                return 0;
            }

            if (ReferenceEquals(left, null))
            {
                return -1;
            }

            return left.CompareTo(right);
        }

        /// <summary>
        /// Compares the current object with another object of the same type.
        /// </summary>
        /// <param name="other">An object to compare with this object.</param>
        /// <returns>
        /// A value that indicates the relative order of the objects being compared. The return value has the following meanings: Value Meaning Less than zero This object is less than the <paramref name="other" /> parameter.Zero This object is equal to <paramref name="other" />. Greater than zero This object is greater than <paramref name="other" />.
        /// </returns>
        public int CompareTo(AcceptHeaderToken<T> other)
        {
            if (ReferenceEquals(this, other))
            {
                return 0;
            }

            if (ReferenceEquals(other, null))
            {
                return -1;
            }

            if (Math.Abs(this._quality - other._quality) < 0.000001f)
            {
                if (this._matchScore == other._matchScore)
                {
                    // reverse on propose.
                    return other._position.CompareTo(this._position);
                }

                return this._matchScore.CompareTo(other._matchScore);
            }

            return this._quality.CompareTo(other._quality);
        }

        /// <summary>
        /// Determines whether the specified <see cref="AcceptHeaderToken{T}" />, is equal to this instance.
        /// </summary>
        /// <param name="obj">The <see cref="System.Object" /> to compare with this instance.</param>
        /// <returns>
        ///   <c>true</c> if the specified <see cref="System.Object" /> is equal to this instance; otherwise, <c>false</c>.
        /// </returns>
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
            {
                return false;
            }

            if (ReferenceEquals(this, obj))
            {
                return true;
            }

            if (obj.GetType() != this.GetType())
            {
                return false;
            }

            return Equals((AcceptHeaderToken<T>)obj);
        }

        /// <summary>
        /// Returns a hash code for this instance.
        /// </summary>
        /// <returns>
        /// A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. 
        /// </returns>
        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = this._matchScore;
                hashCode = (hashCode * 397) ^ this._quality.GetHashCode();
                hashCode = (hashCode * 397) ^ (this._restResponse != null ? this._restResponse.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ this._position;
                return hashCode;
            }
        }

        /// <summary>
        /// Determines whether the specified <see cref="AcceptHeaderToken{T}" />, is equal to this instance.
        /// </summary>
        /// <param name="other">The <see cref="AcceptHeaderToken{T}" /> to compare with this instance.</param>
        /// <returns>
        ///   <c>true</c> if the specified <see cref="AcceptHeaderToken{T}" /> is equal to this instance; otherwise, <c>false</c>.
        /// </returns>
        protected bool Equals(AcceptHeaderToken<T> other)
        {
            return this._matchScore == other._matchScore && this._quality.Equals(other._quality) && Equals(this._restResponse, other._restResponse) && this._position == other._position;
        }
    }
}