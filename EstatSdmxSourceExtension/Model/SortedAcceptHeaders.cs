﻿// -----------------------------------------------------------------------
// <copyright file="SortedAcceptHeaders.cs" company="EUROSTAT">
//   Date Created : 2016-07-22
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sdmxsource.Extension.Model
{
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Globalization;
    using System.Linq;
    using System.Net.Mime;
    using System.Text;

    /// <summary>
    /// The sorted accept headers.
    /// </summary>
    public class SortedAcceptHeaders : ISortedAcceptHeaders
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SortedAcceptHeaders"/> class.
        /// </summary>
        /// <param name="contentTypes">
        /// The content types.
        /// </param>
        /// <param name="languages">
        /// The languages.
        /// </param>
        /// <param name="charSets">
        /// The character sets.
        /// </param>
        public SortedAcceptHeaders(
            IList<HeaderValueWithQuality<ContentType>> contentTypes,
            IList<HeaderValueWithQuality<CultureInfo>> languages,
            IList<HeaderValueWithQuality<Encoding>> charSets)
        {
            contentTypes = contentTypes ?? new HeaderValueWithQuality<ContentType>[0];
            languages = languages ?? new HeaderValueWithQuality<CultureInfo>[0];
            charSets = charSets ?? new HeaderValueWithQuality<Encoding>[0];
            this.ContentTypes = new ReadOnlyCollection<HeaderValueWithQuality<ContentType>>(contentTypes.ToArray());
            this.Languages = new ReadOnlyCollection<HeaderValueWithQuality<CultureInfo>>(languages.ToArray());
            this.CharSets = new ReadOnlyCollection<HeaderValueWithQuality<Encoding>>(charSets.ToArray());
        }

        /// <summary>
        /// Gets the character sets, It will be an empty list if not value was requested.
        /// </summary>
        /// <value>
        /// The character sets.
        /// </value>
        public IList<HeaderValueWithQuality<Encoding>> CharSets { get; private set; }

        /// <summary>
        /// Gets the content types. It will be an empty list if not value was requested
        /// </summary>
        /// <value>
        /// The content types.
        /// </value>
        public IList<HeaderValueWithQuality<ContentType>> ContentTypes { get; private set; }

        /// <summary>
        /// Gets the languages. It will be an empty list if not value was requested
        /// </summary>
        /// <value>
        /// The languages.
        /// </value>
        public IList<HeaderValueWithQuality<CultureInfo>> Languages { get; private set; }
    }
}