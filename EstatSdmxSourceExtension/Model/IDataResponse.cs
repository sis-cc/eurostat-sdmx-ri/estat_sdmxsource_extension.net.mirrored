// -----------------------------------------------------------------------
// <copyright file="IDataResponse.cs" company="EUROSTAT">
//   Date Created : 2015-12-09
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System.Threading.Tasks;

namespace Estat.Sdmxsource.Extension.Model
{
    using System;

    using Estat.Sdmxsource.Extension.Engine;

    /// <summary>
    ///     An interface for holding a data request
    /// </summary>
    /// <typeparam name="TWriter">The type of the t writer.</typeparam>
    public interface IDataResponse<in TWriter>
        where TWriter : IDisposable
    {
        /// <summary>
        ///     Accepts the specified visitor.
        /// </summary>
        /// <param name="visitor">The visitor.</param>
        void Accept(IDataQueryVisitor visitor);

        /// <summary>
        ///     Writes the response.
        /// </summary>
        /// <param name="writer">The writer.</param>
        void WriteResponse(TWriter writer);

        /// <summary>
        ///     Writes the response.
        /// </summary>
        /// <param name="writer">The writer.</param>
        Task WriteResponseAsync(TWriter writer);
    }
}