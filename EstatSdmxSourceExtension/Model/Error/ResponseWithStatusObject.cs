﻿// -----------------------------------------------------------------------
// <copyright file="ResponseWithStatusObject.cs" company="EUROSTAT">
//   Date Created : 2017-11-16
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using Estat.Sdmxsource.Extension.Constant;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Estat.Sdmxsource.Extension.Model.Error
{
    /// <summary>
    /// Response message with Status
    /// </summary>
    /// <seealso cref="Estat.Sdmxsource.Extension.Model.Error.ResponseObject" />
    /// <seealso cref="Estat.Sdmxsource.Extension.Model.Error.IResponseWithStatusObject" />
    public class ResponseWithStatusObject : ResponseObject, IResponseWithStatusObject
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ResponseWithStatusObject"/> class.
        /// </summary>
        /// <param name="messages">The messages.</param>
        /// <param name="status">The status.</param>
        /// <param name="structureReference">The structure reference.</param>
        /// <param name="action">The action.</param>
        public ResponseWithStatusObject(IList<IMessageObject> messages, ResponseStatus status, IStructureReference structureReference, DatasetAction action) : base(messages)
        {
            Status = status;
            StructureReference = structureReference;
            Action = action;
        }

        /// <summary>
        /// Gets the status.
        /// </summary>
        /// <value>
        /// The status.
        /// </value>
        public ResponseStatus Status { get; }

        /// <summary>
        /// Gets the structure reference.
        /// </summary>
        /// <value>
        /// The structure reference.
        /// </value>
        public IStructureReference StructureReference { get; }

        /// <summary>
        /// Gets the action.
        /// </summary>
        /// <value>
        /// The action.
        /// </value>
        public DatasetAction Action { get; }
    }
}
