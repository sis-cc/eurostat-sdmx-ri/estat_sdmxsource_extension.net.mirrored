// -----------------------------------------------------------------------
// <copyright file="IMessageObject.cs" company="EUROSTAT">
//   Date Created : 2017-11-16
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sdmxsource.Extension.Model.Error
{
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using System.Collections.Generic;
    using System.Net;

    /// <summary>
    /// The error message object
    /// </summary>
    public interface IMessageObject
    {
        /// <summary>
        /// Gets the text.
        /// </summary>
        /// <value>
        /// The text.
        /// </value>
        IList<ITextTypeWrapper> Text { get; }

        /// <summary>
        /// Gets the error code.
        /// </summary>
        /// <value>
        /// The error code.
        /// </value>
        SdmxErrorCode ErrorCode { get; }

        /// <summary>
        /// Gets the HTTP code.
        /// </summary>
        /// <value>The HTTP code.</value>
        HttpStatusCode HttpCode { get; }
    }
}