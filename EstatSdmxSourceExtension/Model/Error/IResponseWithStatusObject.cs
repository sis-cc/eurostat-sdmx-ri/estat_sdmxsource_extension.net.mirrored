﻿// -----------------------------------------------------------------------
// <copyright file="IResponseWithStatusObject.cs" company="EUROSTAT">
//   Date Created : 2017-11-15
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using Estat.Sdmxsource.Extension.Constant;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;

namespace Estat.Sdmxsource.Extension.Model.Error
{
    /// <summary>
    /// Represents a Registry Interface Sub,itResultType
    /// </summary>
    /// <seealso cref="Estat.Sdmxsource.Extension.Model.Error.IResponseObject" />
    public interface IResponseWithStatusObject : IResponseObject
    {
        /// <summary>
        /// Gets the status.
        /// </summary>
        /// <value>
        /// The status.
        /// </value>
        ResponseStatus Status { get; }

        /// <summary>
        /// Gets the structure reference.
        /// </summary>
        /// <value>
        /// The structure reference.
        /// </value>
        IStructureReference StructureReference { get; }

        /// <summary>
        /// Gets the action.
        /// </summary>
        /// <value>
        /// The action.
        /// </value>
        DatasetAction Action { get; }
    }
}
