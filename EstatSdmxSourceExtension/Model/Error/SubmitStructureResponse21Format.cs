﻿// -----------------------------------------------------------------------
// <copyright file="SubmitStructureResponse21Format.cs" company="EUROSTAT">
//   Date Created : 2017-11-15
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using Org.Sdmxsource.Sdmx.Api.Model.Header;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace Estat.Sdmxsource.Extension.Model.Error
{
    /// <summary>
    /// SDMX v2.1 SubmitStructureResponse Writer format
    /// </summary>
    /// <seealso cref="Estat.Sdmxsource.Extension.Model.Error.IResponseFormat" />
    public class SubmitStructureResponse21Format : IResponseFormat
    {
        /// <summary>
        /// The writer
        /// </summary>
        private readonly XmlWriter _writer;

        /// <summary>
        /// The header
        /// </summary>
        private readonly IHeader _header;

        /// <summary>
        /// The registry interface is root element
        /// </summary>
        private readonly bool _registryInterfaceIsRootElement;

        /// <summary>
        /// Initializes a new instance of the <see cref="SubmitStructureResponse21Format" /> class.
        /// </summary>
        /// <param name="writer">The writer.</param>
        /// <param name="header">The header.</param>
        /// <param name="registryInterfaceIsRootElement">if set to <c>true</c> [registry interface is root element].</param>
        /// <exception cref="ArgumentNullException">writer
        /// or
        /// header</exception>
        public SubmitStructureResponse21Format(XmlWriter writer, IHeader header, bool registryInterfaceIsRootElement)
        {
            if (header == null)
            {
                throw new ArgumentNullException(nameof(header));
            }

            if (writer == null)
            {
                throw new ArgumentNullException(nameof(writer));
            }

            _writer = writer;
            _header = header;
            _registryInterfaceIsRootElement = registryInterfaceIsRootElement;
        }

        /// <summary>
        /// Gets the writer.
        /// </summary>
        /// <value>
        /// The writer.
        /// </value>
        public XmlWriter Writer
        {
            get
            {
                return _writer;
            }
        }

        /// <summary>
        /// Gets the header.
        /// </summary>
        /// <value>
        /// The header.
        /// </value>
        public IHeader Header
        {
            get
            {
                return _header;
            }
        }

        /// <summary>
        /// Gets a value indicating whether [registry interface is root element].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [registry interface is root element]; otherwise, <c>false</c>.
        /// </value>
        public bool RegistryInterfaceIsRootElement
        {
            get
            {
                return _registryInterfaceIsRootElement;
            }
        }
    }
}
