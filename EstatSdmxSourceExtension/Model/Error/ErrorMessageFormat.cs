﻿// -----------------------------------------------------------------------
// <copyright file="ErrorMessageFormat.cs" company="EUROSTAT">
//   Date Created : 2017-11-15
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.IO;
using System.Text;

namespace Estat.Sdmxsource.Extension.Model.Error
{
    /// <summary>
    /// SDMX v2.1 Error Message Format
    /// </summary>
    /// <seealso cref="Estat.Sdmxsource.Extension.Model.Error.IResponseFormat" />
    public class ErrorMessageFormat : IResponseFormat
    {
        /// <summary>
        /// The stream
        /// </summary>
        private readonly Stream _stream;

        /// <summary>
        /// The encoding
        /// </summary>
        private readonly Encoding _encoding;

        /// <summary>
        /// Initializes a new instance of the <see cref="ErrorMessageFormat"/> class.
        /// </summary>
        /// <param name="stream">The stream.</param>
        /// <param name="encoding">The encoding.</param>
        /// <exception cref="ArgumentNullException">
        /// stream
        /// or
        /// encoding
        /// </exception>
        public ErrorMessageFormat(Stream stream, Encoding encoding)
        {
            if (stream == null)
            {
                throw new ArgumentNullException(nameof(stream));
            }

            _stream = stream;
            _encoding = encoding ?? Encoding.UTF8;
        }

        /// <summary>
        /// Gets the stream.
        /// </summary>
        /// <value>
        /// The stream.
        /// </value>
        public Stream Stream
        {
            get
            {
                return _stream;
            }
        }

        /// <summary>
        /// Gets the encoding.
        /// </summary>
        /// <value>
        /// The encoding.
        /// </value>
        public Encoding Encoding
        {
            get
            {
                return _encoding;
            }
        }

    }
}
