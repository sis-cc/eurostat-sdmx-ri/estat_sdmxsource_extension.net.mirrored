// -----------------------------------------------------------------------
// <copyright file="MessageObject.cs" company="EUROSTAT">
//   Date Created : 2017-11-16
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net;

namespace Estat.Sdmxsource.Extension.Model.Error
{
    /// <summary>
    /// Represents an SDMX v2.1 Coded Message 
    /// </summary>
    /// <seealso cref="Estat.Sdmxsource.Extension.Model.Error.IMessageObject" />
    public class MessageObject : IMessageObject
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MessageObject"/> class.
        /// </summary>
        /// <param name="text">The text.</param>
        public MessageObject(IList<ITextTypeWrapper> text)
        {
            if (text == null || text.Count == 0)
            {
                text = new ITextTypeWrapper[] { new TextTypeWrapperImpl("en", "Success", null) };
            }

            Text = new ReadOnlyCollection<ITextTypeWrapper>(text.ToArray());
            HttpCode = HttpStatusCode.OK;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MessageObject" /> class.
        /// </summary>
        /// <param name="text">The text.</param>
        /// <param name="code">The HTTP code.</param>
        public MessageObject(IList<ITextTypeWrapper> text, HttpStatusCode code)
        {
            if (text == null || text.Count == 0)
            {
                text = new ITextTypeWrapper[] { new TextTypeWrapperImpl("en", "Success", null) };
            }

            Text = new ReadOnlyCollection<ITextTypeWrapper>(text.ToArray());
            HttpCode = code;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MessageObject"/> class.
        /// </summary>
        /// <param name="text">The text.</param>
        /// <param name="errorCode">The error code.</param>
        public MessageObject(IList<ITextTypeWrapper> text, SdmxErrorCode errorCode)
        {
            if (errorCode == null)
            {
                throw new ArgumentNullException(nameof(errorCode));
            }

            if (text == null || text.Count == 0)
            {
                text = new ITextTypeWrapper[] { new TextTypeWrapperImpl("en", errorCode.ErrorString, null) };
            }

            Text = new ReadOnlyCollection<ITextTypeWrapper>(text.ToArray());
            ErrorCode = errorCode;
            HttpCode = (HttpStatusCode)errorCode.HttpRestErrorCode;
        }

        /// <summary>
        /// Gets the text.
        /// </summary>
        /// <value>
        /// The text.
        /// </value>
        public IList<ITextTypeWrapper> Text
        {
            get;
        }

        /// <summary>
        /// Gets the error code.
        /// </summary>
        /// <value>
        /// The error code.
        /// </value>
        public SdmxErrorCode ErrorCode { get; }

        /// <summary>
        /// Gets the HTTP code.
        /// </summary>
        /// <value>The HTTP code.</value>
        public HttpStatusCode HttpCode { get; }
    }
}
