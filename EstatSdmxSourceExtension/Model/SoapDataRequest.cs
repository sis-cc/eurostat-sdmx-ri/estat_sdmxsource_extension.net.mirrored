﻿// -----------------------------------------------------------------------
// <copyright file="SoapDataRequest.cs" company="EUROSTAT">
//   Date Created : 2016-07-07
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sdmxsource.Extension.Model
{
    using Estat.Sdmxsource.Extension.Constant;

    using Org.Sdmxsource.Sdmx.Api.Constants;

    /// <summary>
    /// The soap data request.
    /// </summary>
    public class SoapDataRequest : ISoapRequest<DataType>
    {
        /// <summary>
        /// The _extensions.
        /// </summary>
        private readonly SdmxExtension _extensions;

        /// <summary>
        /// The _format type.
        /// </summary>
        private readonly DataType _formatType;

        /// <summary>
        /// The _operation.
        /// </summary>
        private readonly string _operation;

        /// <summary>
        /// Initializes a new instance of the <see cref="SoapDataRequest"/> class.
        /// </summary>
        /// <param name="operation">
        /// The operation.
        /// </param>
        /// <param name="formatType">
        /// Type of the format.
        /// </param>
        /// <param name="extensions">
        /// The extensions.
        /// </param>
        public SoapDataRequest(string operation, DataType formatType, SdmxExtension extensions)
        {
            this._operation = operation;
            this._formatType = formatType;
            this._extensions = extensions;
        }

        /// <summary>
        /// Gets the extensions.
        /// </summary>
        /// <value>
        /// The extensions.
        /// </value>
        public SdmxExtension Extensions
        {
            get
            {
                return this._extensions;
            }
        }

        /// <summary>
        /// Gets the type of the format.
        /// </summary>
        /// <value>
        /// The type of the format.
        /// </value>
        public DataType FormatType
        {
            get
            {
                return this._formatType;
            }
        }

        /// <summary>
        /// Gets the operation. It should a valid SDMX v2.0 or v2.1 operation name.
        /// </summary>
        /// <value>
        /// The operation.
        /// </value>
        public string Operation
        {
            get
            {
                return this._operation;
            }
        }
    }
}