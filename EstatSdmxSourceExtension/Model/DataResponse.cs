// -----------------------------------------------------------------------
// <copyright file="DataResponse.cs" company="EUROSTAT">
//   Date Created : 2015-12-09
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sdmxsource.Extension.Model
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Estat.Sdmxsource.Extension.Engine;

    using Org.Sdmxsource.Sdmx.Api.Model.Data.Query;

    /// <summary>
    /// Class DataResponse.
    /// </summary>
    /// <typeparam name="TWriter">The type of the writer.</typeparam>
    /// <typeparam name="TQuery">The type of the query.</typeparam>
    public class DataResponse<TWriter, TQuery> : IDataResponse<TWriter>
        where TQuery : IBaseDataQuery where TWriter : IDisposable
    {
        /// <summary>
        /// The _action
        /// </summary>
        private readonly Func<TQuery, TWriter, Task> _function;

        /// <summary>
        /// The _queries
        /// </summary>
        private readonly IList<TQuery> _queries;

        /// <summary>
        /// Initializes a new instance of the <see cref="DataResponse{TWriter, TQuery}"/> class.
        /// </summary>
        /// <param name="queries">The queries.</param>
        /// <param name="function">The function.</param>
        public DataResponse(IList<TQuery> queries, Func<TQuery, TWriter, Task> function)
        {
            if (queries == null)
            {
                throw new ArgumentNullException("queries");
            }

            if (function == null)
            {
                throw new ArgumentNullException("function");
            }

            this._queries = queries;
            this._function = function;
        }

        /// <summary>
        /// Gets the query.
        /// </summary>
        /// <value>The query.</value>
        public TQuery Query
        {
            get
            {
                return this._queries.First();
            }
        }

        /// <summary>
        /// Accepts the specified visitor.
        /// </summary>
        /// <param name="visitor">The visitor.</param>
        /// <exception cref="ArgumentNullException"><paramref name="visitor"/> is <see langword="null" />.</exception>
        public void Accept(IDataQueryVisitor visitor)
        {
            if (visitor == null)
            {
                throw new ArgumentNullException("visitor");
            }

            visitor.Visit(this.Query);
        }

        /// <summary>
        /// Writes the response.
        /// </summary>
        /// <param name="writer">The writer.</param>
        public void WriteResponse(TWriter writer)
        {
            using (writer)
            {
                this._function(this.Query, writer);
            }
        }


        /// <summary>
        /// Writes the response asynchronous.
        /// </summary>
        /// <param name="writer">The writer.</param>
        public async Task WriteResponseAsync(TWriter writer)
        {
            using (writer)
            {
                await this._function(this.Query, writer);
            }
        }
    }
}