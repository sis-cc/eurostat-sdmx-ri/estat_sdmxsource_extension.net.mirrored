// -----------------------------------------------------------------------
// <copyright file="DataRequest.cs" company="EUROSTAT">
//   Date Created : 2016-07-05
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sdmxsource.Extension.Model
{
    using Estat.Sdmxsource.Extension.Factory;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
    using Org.Sdmxsource.Sdmx.Api.Model.Data.Query;

    /// <summary>
    /// The data request model class.
    /// </summary>
    public class DataRequest
    {
        /// <summary>
        /// The _data query
        /// </summary>
        private readonly IDataQuery _dataQuery;

        /// <summary>
        /// The _retriever factory
        /// </summary>
        private readonly IRetrieverFactory _retrieverFactory;

        /// <summary>
        /// The _SDMX object retrieval
        /// </summary>
        private readonly ISdmxObjectRetrievalManager _sdmxObjectRetrieval;

        /// <summary>
        /// Initializes a new instance of the <see cref="DataRequest"/> class.
        /// </summary>
        /// <param name="dataQuery">The data query.</param>
        /// <param name="retrieverFactory">The retriever factory.</param>
        /// <param name="sdmxObjectRetrieval">The SDMX object retrieval.</param>
        public DataRequest(IDataQuery dataQuery, IRetrieverFactory retrieverFactory, ISdmxObjectRetrievalManager sdmxObjectRetrieval, SdmxStandard sdmxStandard)
        {
            this._dataQuery = dataQuery;
            this._retrieverFactory = retrieverFactory;
            this._sdmxObjectRetrieval = sdmxObjectRetrieval;
            SdmxStandard = sdmxStandard;
        }

        /// <summary>
        /// Gets the data query.
        /// </summary>
        /// <value>
        /// The data query.
        /// </value>
        public IDataQuery DataQuery
        {
            get
            {
                return this._dataQuery;
            }
        }

        /// <summary>
        /// Gets the retriever factory.
        /// </summary>
        /// <value>
        /// The retriever factory.
        /// </value>
        public IRetrieverFactory RetrieverFactory
        {
            get
            {
                return this._retrieverFactory;
            }
        }

        /// <summary>
        /// Gets the SDMX object retrieval.
        /// </summary>
        /// <value>
        /// The SDMX object retrieval.
        /// </value>
        public ISdmxObjectRetrievalManager SdmxObjectRetrieval
        {
            get
            {
                return this._sdmxObjectRetrieval;
            }
        }

        public SdmxStandard SdmxStandard { get; }
    }
}