﻿// -----------------------------------------------------------------------
// <copyright file="MappingValidationFormat.cs" company="EUROSTAT">
//   Date Created : 2018-01-12
//   Copyright (c) 2009, 2018 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sdmxsource.Extension.Model
{
    using System.Text;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Data;

    /// <summary>
    /// MappingValidationFormat
    /// </summary>
    /// <seealso cref="T:Estat.Sdmxsource.Extension.Model.AbstractRestFormat" />
    /// <seealso cref="T:Org.Sdmxsource.Sdmx.Api.Model.Data.IDataFormat" />
    public class MappingValidationFormat : AbstractRestFormat, IDataFormat
    {
        /// <inheritdoc />
        /// <summary>
        /// Initializes a new instance of the <see cref="T:Estat.Sdmxsource.Extension.Model.MappingValidationFormat" /> class.
        /// </summary>
        /// <param name="encoding">The encoding.</param>
        public MappingValidationFormat(Encoding encoding)
            : base(encoding)
        {
        }

        /// <inheritdoc />
        /// <summary>
        /// Gets a string representation of the format, that can be used for auditing and debugging purposes.
        /// <p />
        /// This is expected to return a not null response.
        /// </summary>
        public string FormatAsString => string.Empty;

        /// <inheritdoc />
        /// <summary>
        /// Gets the sdmx data format that this interface is describing.
        /// If this is not describing an SDMX message then this will return null and the implementation class will be expected
        /// to expose additional methods to understand the data
        /// </summary>
        public DataType SdmxDataFormat => DataType.GetFromEnum(DataEnumType.Other);
    }
}