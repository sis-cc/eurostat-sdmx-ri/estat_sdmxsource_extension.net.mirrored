// -----------------------------------------------------------------------
// <copyright file="RestResponse.cs" company="EUROSTAT">
//   Date Created : 2016-07-06
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or � as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sdmxsource.Extension.Model
{
    using System;

    using Org.Sdmxsource.Sdmx.Api.Model.Data;
    using Org.Sdmxsource.Sdmx.Api.Model.Format;

    /// <summary>
    /// The rest data response.
    /// </summary>
    /// <typeparam name="TOutputFormat">The type of the output format.</typeparam>
    /// <seealso cref="IRestResponse{TOutputFormat}" />
    public class RestResponse<TOutputFormat> : IRestResponse<TOutputFormat>
    {
        /// <summary>
        /// The format.
        /// </summary>
        private readonly Lazy<TOutputFormat> _dataFormat;

        /// <summary>
        /// The _response content header
        /// </summary>
        private readonly IResponseContentHeader _responseContentHeader;

        /// <summary>
        /// Initializes a new instance of the <see cref="RestResponse{TOutputFormat}" /> class.
        /// </summary>
        /// <param name="dataFormat">The output format. It could be a <see cref="IDataFormat" /> or a <see cref="IStructureFormat" /></param>
        /// <param name="responseContentHeader">The response content header.</param>
        public RestResponse(Lazy<TOutputFormat> dataFormat, IResponseContentHeader responseContentHeader)
        {
            if (responseContentHeader == null)
            {
                throw new ArgumentNullException("responseContentHeader");
            }

            this._dataFormat = dataFormat;
            this._responseContentHeader = responseContentHeader;
        }

        /// <summary>
        /// Gets the output format. It could be a <see cref="IDataFormat"/> or a <see cref="IStructureFormat"/>
        /// </summary>
        /// <value>
        /// The output format.
        /// </value>
        public TOutputFormat Format
        {
            get
            {
                return this._dataFormat.Value;
            }
        }

        /// <summary>
        /// Gets the response content header.
        /// </summary>
        /// <value>
        /// The response content header.
        /// </value>
        public IResponseContentHeader ResponseContentHeader
        {
            get
            {
                return this._responseContentHeader;
            }
        }
    }
}