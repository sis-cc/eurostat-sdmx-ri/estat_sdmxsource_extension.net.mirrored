// -----------------------------------------------------------------------
// <copyright file="WsInfo.cs" company="EUROSTAT">
//   Date Created : 2017-06-14
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sdmxsource.Extension.Model
{
    /// <summary>
    /// The SOAP service configuration
    /// </summary>
    public class WsInfo
    {
        /// <summary>
        /// Gets or sets the domain.
        /// </summary>
        /// <value>
        /// The domain.
        /// </value>
        public string Domain { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [enable HTTP authentication].
        /// </summary>
        /// <value>
        /// <c>true</c> if [enable HTTP authentication]; otherwise, <c>false</c>.
        /// </value>
        public bool EnableHTTPAuthentication { get; set; }

        /// <summary>
        /// Gets or sets the end point.
        /// </summary>
        /// <value>
        /// The end point.
        /// </value>
        public string EndPoint { get; set; }

        /// <summary>
        /// Gets a value indicating whether the <see cref="EndPoint"/> is REST.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is rest; otherwise, <c>false</c>.
        /// </value>
        public bool IsRest
        {
            get
            {
                return Wsdl == null;
            }
        }

        /// <summary>
        /// Gets or sets the password.
        /// </summary>
        /// <value>
        /// The password.
        /// </value>
        public string Password { get; set; }

        /// <summary>
        /// Gets the prefix.
        /// </summary>
        /// <value>
        /// The prefix.
        /// </value>
        public string Prefix
        {
            get { return "Web"; }
        }

        /// <summary>
        /// Gets or sets the name of the user.
        /// </summary>
        /// <value>
        /// The name of the user.
        /// </value>
        public string UserName { get; set; }

        /// <summary>
        /// Gets or sets the WSDL.
        /// </summary>
        /// <value>
        /// The WSDL.
        /// </value>
        public string Wsdl { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether to enable proxy.
        /// </summary>
        /// <value>
        ///   <c>true</c> if [enable proxy]; otherwise, <c>false</c>.
        /// </value>
        public bool EnableProxy { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether to use system proxy.
        /// </summary>
        /// <value>
        ///   <c>true</c> if [use system proxy]; otherwise, <c>false</c>.
        /// </value>
        public bool UseSystemProxy { get; set; }

        /// <summary>
        /// Gets the proxy server.
        /// </summary>
        /// <value>
        /// The proxy server.
        /// </value>
        public string ProxyServer { get;  set; }

        /// <summary>
        /// Gets the proxy server port.
        /// </summary>
        /// <value>
        /// The proxy server port.
        /// </value>
        public int ProxyServerPort { get;  set; }

        /// <summary>
        /// Gets the name of the proxy user.
        /// </summary>
        /// <value>
        /// The name of the proxy user.
        /// </value>
        public string ProxyUserName { get;  set; }

        /// <summary>
        /// Gets the proxy password.
        /// </summary>
        /// <value>
        /// The proxy password.
        /// </value>
        public string ProxyPassword { get;  set; }
    }
}