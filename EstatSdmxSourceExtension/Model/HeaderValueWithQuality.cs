﻿// -----------------------------------------------------------------------
// <copyright file="HeaderValueWithQuality.cs" company="EUROSTAT">
//   Date Created : 2016-07-22
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sdmxsource.Extension.Model
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;

    /// <summary>
    /// The header value with quality.
    /// </summary>
    /// <typeparam name="TValue">
    /// The type of the value
    /// </typeparam>
    public class HeaderValueWithQuality<TValue>
    {
        /// <summary>
        /// The _parameters
        /// </summary>
        private readonly IDictionary<string, string> _parameters;

        /// <summary>
        /// Initializes a new instance of the <see cref="HeaderValueWithQuality{TValue}"/> class.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="quality">The quality.</param>
        public HeaderValueWithQuality(TValue value, decimal quality)
        {
            this.Value = value;
            this.Quality = quality;
            this._parameters = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="HeaderValueWithQuality{TValue}"/> class.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="quality">The quality.</param>
        /// <param name="parameters">The parameters.</param>
        public HeaderValueWithQuality(TValue value, decimal quality, IDictionary<string, string> parameters) : this(value, quality)
        {
            this._parameters = new Dictionary<string, string>(parameters, StringComparer.OrdinalIgnoreCase);
        }

        /// <summary>
        /// Gets the parameters.
        /// </summary>
        /// <value>
        /// The parameters.
        /// </value>
        public IDictionary<string, string> Parameters
        {
            get
            {
                return new Dictionary<string, string>(this._parameters, StringComparer.OrdinalIgnoreCase);
            }
        }

        /// <summary>
        /// Gets the value.
        /// </summary>
        /// <value>
        /// The value.
        /// </value>
        public TValue Value { get; private set; }

        /// <summary>
        /// Gets the quality.
        /// </summary>
        /// <value>
        /// The quality.
        /// </value>
        public decimal Quality { get; private set; }

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            var otherParam = string.Join(string.Empty, this._parameters.Select(pair => string.Format(CultureInfo.InvariantCulture, ";{0}={1}", pair.Key, pair.Value)));
            return string.Format(CultureInfo.InvariantCulture, "{0};q={1}{2}", this.Value, this.Quality, otherParam);
        }
    }
}