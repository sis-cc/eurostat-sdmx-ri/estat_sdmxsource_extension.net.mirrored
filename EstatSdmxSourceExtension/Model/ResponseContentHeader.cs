﻿// -----------------------------------------------------------------------
// <copyright file="ResponseContentHeader.cs" company="EUROSTAT">
//   Date Created : 2016-07-05
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sdmxsource.Extension.Model
{
    using System;
    using System.Net.Mime;

    /// <summary>
    /// A model class that contains the 
    /// </summary>
    /// <seealso cref="Estat.Sdmxsource.Extension.Model.IResponseContentHeader" />
    public class ResponseContentHeader : IResponseContentHeader
    {
        /// <summary>
        /// The _language
        /// </summary>
        private readonly Lazy<string> _language;

        /// <summary>
        /// The _media type
        /// </summary>
        private readonly ContentType _mediaType;

        /// <summary>
        /// Initializes a new instance of the <see cref="ResponseContentHeader"/> class.
        /// </summary>
        /// <param name="mediaType">
        /// Type of the media.
        /// </param>
        /// <param name="language">
        /// The language.
        /// </param>
        public ResponseContentHeader(ContentType mediaType, Lazy<string> language)
        {
            this._mediaType = mediaType;
            this._language = language;
        }

        /// <summary>
        /// Gets the language. The value is expected to be valid for HTTP header Accept-Language
        /// </summary>
        /// <value>
        /// The language.
        /// </value>
        public string Language
        {
            get
            {
                var language = this._language;
                if (language != null)
                {
                    return language.Value;
                }

                return null;
            }
        }

        /// <summary>
        /// Gets the <see cref="ContentType" />.
        /// </summary>
        /// <value>
        /// The type of the media.
        /// </value>
        public ContentType MediaType
        {
            get
            {
                return this._mediaType;
            }
        }
    }
}