﻿// -----------------------------------------------------------------------
// <copyright file="CsvWriterOptions.cs" company="EUROSTAT">
//   Date Created : 2016-10-20
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sdmxsource.Extension.Model
{
    using System.Collections.Generic;
    
    /// <summary>
    /// Options for the CSV writer
    /// </summary>
    public class CsvWriterOptions
    {
        /// <summary>
        /// Gets or sets the columns.
        /// </summary>
        /// <value>
        /// The columns.
        /// </value>
        public IEnumerable<string> Columns { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [fields can be quoted].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [fields can be quoted]; otherwise, <c>false</c>.
        /// </value>
        public bool FieldsCanBeQuoted { get; set; }

        /// <summary>
        /// Gets or sets the field separator.
        /// </summary>
        /// <value>
        /// The field separator.
        /// </value>
        public string FieldSeparator { get; set; }

        /// <summary>
        /// Gets or sets the file location.
        /// </summary>
        /// <value>
        /// The file location.
        /// </value>
        public string FileLocation { get; set; }
    }
}