// -----------------------------------------------------------------------
// <copyright file="GlobalSuppressions.cs" company="EUROSTAT">
//   Date Created : 2016-03-10
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System.Diagnostics.CodeAnalysis;

[assembly:
    SuppressMessage(
        "Microsoft.Usage", 
        "CA1806:DoNotIgnoreMethodResults", 
        MessageId = "System.Enum.TryParse<Org.Sdmxsource.Sdmx.Api.Constants.Severity>(System.String,Org.Sdmxsource.Sdmx.Api.Constants.Severity@)", 
        Scope = "member", 
        Target = "Estat.Sdmxsource.Extension.Util.SdmxMessageUtilExt.#ParseSdmxFooterMessage(Org.Sdmxsource.Sdmx.Api.Util.IReadableDataLocation)", 
        Justification = "CA1806:DoNotIgnoreMethodResults")]
[assembly:
    SuppressMessage(
        "Microsoft.Globalization", 
        "CA1303:Do not pass literals as localized parameters", 
        MessageId = "Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base.IAnnotationMutableObject.AddText(System.String,System.String)", 
        Scope = "member", 
        Target = "Estat.Sdmxsource.Extension.Constant.CustomAnnotationTypeExtensions.#ToAnnotation`1(Estat.Sdmxsource.Extension.Constant.CustomAnnotationType)", 
        Justification = "CA1303:Do not pass literals as localized parameters")]