﻿// -----------------------------------------------------------------------
// <copyright file="RDFDataWritingContract.cs" company="EUROSTAT">
//   Date Created : 2016-08-12
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace RDFPlugin.Factory
{
    using System;
    using System.Text;
    using System.Xml;

    using Estat.Sdmxsource.Extension.Factory;
    using Estat.Sdmxsource.Extension.Model;
    
    using RDFPlugin.Model;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Data;
    using System.Collections.Generic;
    using System.Net.Mime;
    

    /// <summary>
    /// The sample data writing contract.
    /// </summary>
    public class RDFDataWritingContract : AbstractDataWriterPluginContract
    {
        private const string ApplicationRdfXML = "application/rdf+xml";

        /// <summary>
        /// Initializes a new instance of the <see cref="RDFDataWritingContract"/> class.
        /// </summary>
        public RDFDataWritingContract()

            // TODO CHANGE THIS the supported formats
            : base(
                new[]
                    {
                        ApplicationRdfXML,
                        /* no  xml*/
                        /* add your supported media types e.g. "mytype/subtype+a;version=2.3", "othertype/someothersubtype" */
                    })
        {
        }

        /// <summary>
        /// Gets the data format.
        /// </summary>
        /// <param name="soapRequest">
        /// Type of the data.
        /// </param>
        /// <param name="xmlWriter">
        /// The XML writer.
        /// </param>
        /// <returns>
        /// The <see cref="T:Org.Sdmxsource.Sdmx.Api.Model.Data.IDataFormat"/>.
        /// </returns>
        public override IDataFormat GetDataFormat(ISoapRequest<DataType> soapRequest, XmlWriter xmlWriter)
        {
            return null;
        }

        /// <summary>
        /// Builds the format.
        /// </summary>
        /// <param name="mediaType">Type of the media.</param>
        /// <param name="encoding">The encoding.</param>
        /// <param name="dataRequest">The rest data request.</param>
        /// <returns>
        /// The <see cref="T:Org.Sdmxsource.Sdmx.Api.Model.Data.IDataFormat" />.
        /// </returns>
        protected override IDataFormat BuildFormat(
            string mediaType,
            Encoding encoding,
            IRestDataRequest dataRequest)
        {
            /*
            // In case we need to, we can check the requested media types sorted by quality 
            var acceptHeaderValue = dataRequest.SortedAcceptHeaders.ContentTypes;

            // get requested languages if needed sorted by quality
            var languages = dataRequest.SortedAcceptHeaders.Languages;
            //// -or-
            // override and use the this.GetLanguage method
            var language = this.GetLanguage(dataRequest);

            // get the requested dataflow if needed
            var dataflow = dataRequest.DataQuery.Dataflow;

            // get the requested dsd if needed
            var dsd = dataRequest.DataQuery.DataStructure;

            // get the Structure retriever manager in case we need other structural metadata
            var retrieverManager = dataRequest.RetrievalManager;
            */

            //SdmxOutputFormat.GetFromEnum(StructureOutputFormatEnumType.Csv)
            //response.Format.SdmxOutputFormat.
           // var format = DataType.GetFromEnum(DataEnumType.Rdf);
           var format = DataType.GetFromEnum(DataEnumType.Other);
           
                
            //return format;

            return new RDFDataRestFormat(format, encoding);
        }
    }
}