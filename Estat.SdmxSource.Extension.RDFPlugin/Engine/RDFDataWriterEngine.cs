// -----------------------------------------------------------------------
// <copyright file="SampleDataWriterEngine.cs" company="EUROSTAT">
//   Date Created : 2016-08-12
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// 
// Original implementation by ISTAT
// </copyright>
// -----------------------------------------------------------------------
namespace RDFPlugin.Engine
{
    using System;
    using System.IO;
    using System.Text;
    using System.Xml;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Engine;
    using Org.Sdmxsource.Sdmx.Api.Model;
    using Org.Sdmxsource.Sdmx.Api.Model.Header;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using RDFProvider.Constants;
    using RDFProvider.Writer.Engine;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Collections;
    using Org.Sdmxsource.Sdmx.Api.Model.Data;

    /// <summary>
    /// The sample data writer engine.
    /// </summary>
    public class RDFDataWriterEngine : IDataWriterEngine, IRDFDataWriterEngine
    {


        #region Fields

        private readonly bool _closeWriter;

        private IDataStructureObject _keyFamilyBean;

        private bool _startedDataSet;

        private ArrayList SerieChiavi = new ArrayList();

        public readonly bool _wrapped;

        private readonly XmlWriter _writer;

        private string _defaultObs = RDFConstants.NaN;

        private readonly RDFNamespaces _namespaces;

        private string _dimensionAtObservation;

        private string _dsdName;

        private string _dfChiave;

        private Action<string> _writeObservationMethod;

        private bool _startedSeries;

        private bool _startedSeriesProv;

        private bool _startedProvSeries;

        private bool _startedSubSeries;

        private bool _primaSerie = false;

        private bool _disposed = false;

        private bool _startedObs = false;

        private bool _scritteOsservazioni = false;

        private bool _trovatoStatus = false;

        private readonly List<KeyValuePair<string, string>> _componentVals = new List<KeyValuePair<string, string>>();



        #endregion


        protected string DimensionAtObservation
        {
            get
            {
                return this._dimensionAtObservation;
            }
        }

        protected string MessageElement
        {
            get
            {
                return RDFNameTableCache.GetElementName(RDFElementNameTable.RDF);
            }
        }

        protected RDFNamespaces Namespaces
        {
            get
            {
                return this._namespaces;
            }
        }

        protected string DefaultObs
        {
            get
            {
                return this._defaultObs;
            }
        }
        /// <summary>
        /// Initializes a new instance of the <see cref="RDFDataWriterEngine"/> class.
        /// </summary>
        /// <param name="outputStream">The output stream.</param>
        /// <param name="outputEncoding">The output encoding.</param>
        /// <remarks>This is used for REST.</remarks>
        //public RDFDataWriterEngine(Stream outputStream, Encoding outputEncoding)
              public RDFDataWriterEngine(Encoding outputEncoding, Stream outputStream)
        {
            if (outputStream == null)
            {
                throw new ArgumentNullException("RDF outputStream");
            }

            if (outputEncoding == null)
            {
                throw new ArgumentNullException("RDF outputEncoding");
            }

            _closeWriter = true;
            _wrapped = false;

            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Indent = true;
            settings.IndentChars = ("\t");
            settings.OmitXmlDeclaration = true;
            
            this._writeObservationMethod = this.RDFWriteObservation21;
            this._namespaces = CreateDataNamespaces();
            this._writer = XmlWriter.Create(outputStream, settings);


            //this._writer = XmlWriter.Create("H://popoli13.xml", settings);
        }



        /// <summary>
        /// Initializes a new instance of the <see cref="RDFDataWriterEngine"/> class.
        /// </summary>
        /// <param name="writer">The writer.</param>
        /// <remarks>This is used for SOAP. SOAP is limited to XML</remarks>
        public RDFDataWriterEngine(XmlWriter writer)
        {
            if (writer == null)
            {
                throw new ArgumentNullException("RDF writer");
            }

            _closeWriter = false;

            // return new RDFDataWriterEngine(writer);
            this._writer = writer;
            this._writeObservationMethod = this.RDFWriteObservation21;
            this._namespaces = CreateDataNamespaces();

            //// TODO This is needed only when overriding existing SDMX XML formats to use under SOAP endpoints. If this is not the case remove it.
            _wrapped = writer.WriteState != WriteState.Start;
        }

        private void RDFWriteObservation21(string obsConcept)
        {
            //this.WriteStartElement(this.Namespaces.Property, this.DimensionAtObservation);
            ////  DA QUI 
          //  if (this._primaSerie == false)
        //    {

                this._primaSerie = true;




                this.WriteStartElement(this.Namespaces.RDF, RDFElementNameTable.Description);

                this.WriteAttributeString(this.Namespaces.RDF, RDFAttributeNameTable.resource, RDFConstants.RdfDataset + "/" + this._dsdName + "/" + this._dfChiave + obsConcept);


                this.WriteStartElement(this.Namespaces.RDF, RDFElementNameTable.type);
                this.WriteAttributeString(this.Namespaces.RDF, RDFAttributeNameTable.resource, RDFConstants.Rdftype);
                this.WriteEndElement();
                this.WriteStartElement(this.Namespaces.QB, RDFElementNameTable.dataset);

                this.WriteAttributeString(this.Namespaces.RDF, RDFAttributeNameTable.resource, RDFConstants.RdfDataset + "/" + this._dsdName);
                this.WriteEndElement();

                this._startedSeries = true;
           // }



            //// A QUI




            IAnnotation[] annotations = null;
            this._scritteOsservazioni = true;
            if(this._startedObs == true)
            {
                //this._startedObs = false;
                //this.WriteEndElement();
                this.StartSeries(annotations);
            }
            this._startedObs = true;
            foreach (String[,] row in SerieChiavi) 
            {
                this.WriteConceptValue(row[0,0],row[0,1]);
            }
            
            /*
            for (int j = 0; j < SerieChiavi.Count; j++)
             {
                   //System.Console.Write(array2D[i, j]);
                 var r = SerieChiavi[0][1];
                 this.WriteConceptValue(SerieChiavi[1, j].ToString(), SerieChiavi[0, j].ToString());
            }
            */
            this.WriteStartElement(this.Namespaces.Property, "TIME_PERIOD");
            this.WriteAttributeString(this.Namespaces.RDF, RDFAttributeNameTable.resource, RDFConstants.RdfTimePeriod + obsConcept);
            this.WriteEndElement();
            
        }

        private static RDFNamespaces CreateDataNamespaces()
        {
            var namespaces = new RDFNamespaces();
            namespaces.Xsi = new NamespacePrefixPair(RDFConstants.XmlSchemaNS, RDFConstants.XmlSchemaPrefix);
            namespaces.RDF = new NamespacePrefixPair(RDFConstants.RdfNs21, RDFPrefixConstants.RDF);
            namespaces.Property = new NamespacePrefixPair(RDFConstants.RdfProperty, RDFPrefixConstants.Property);
            namespaces.QB = new NamespacePrefixPair(RDFConstants.Rdfqb, RDFPrefixConstants.QB);
            namespaces.DocTerms = new NamespacePrefixPair(RDFConstants.RdfDcTerms, RDFPrefixConstants.DocTerms);
            namespaces.RDFs = new NamespacePrefixPair(RDFConstants.RdfS, RDFPrefixConstants.RDFs);
            namespaces.Owl = new NamespacePrefixPair(RDFConstants.RdfOwl, RDFPrefixConstants.Owl);
            namespaces.Skos = new NamespacePrefixPair(RDFConstants.RdfSkos, RDFPrefixConstants.Skos);
            namespaces.Xkos = new NamespacePrefixPair(RDFConstants.Rdfxkos, RDFPrefixConstants.Xkos);
            namespaces.XML = new NamespacePrefixPair(RDFConstants.XmlNs, RDFPrefixConstants.XML);
            namespaces.SdmxConcept = new NamespacePrefixPair(RDFConstants.RdfSdmxConcept, RDFPrefixConstants.SdmxConcept);
            namespaces.Prov = new NamespacePrefixPair(RDFConstants.RdfProv, RDFPrefixConstants.Prov);

            return namespaces;
        }
        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
        }

        /// <summary>
        /// (Optional) Writes a header to the message, any missing mandatory attributes are defaulted.  If a header is required
        /// for a message, then this
        /// call should be the first call on a WriterEngine
        /// <p />
        /// If this method is not called, and a message requires a header, a default header will be generated.
        /// </summary>
        /// <param name="header">The header.</param>
        public void WriteHeader(IHeader header)
        {
            if (header == null)
            {
                throw new ArgumentNullException("RDF header");
            }
            //if (!this.Wrapped)
            // {

            // <RDFData  QUI
            this.WriteStartElement(this.Namespaces.RDF, "RDF");

            // xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
            this.WriteNamespaceDecl(this.Namespaces.Xsi);

            // rdf:Property
            this.WriteNamespaceDecl(this.Namespaces.Property);
            this.WriteNamespaceDecl(this.Namespaces.QB);
            this.WriteNamespaceDecl(this.Namespaces.Skos);
            this.WriteNamespaceDecl(this.Namespaces.Xkos);
            this.WriteNamespaceDecl(this.Namespaces.Owl);
            this.WriteNamespaceDecl(this.Namespaces.RDFs);
            this.WriteNamespaceDecl(this.Namespaces.DocTerms);
            this.WriteNamespaceDecl(this.Namespaces.SdmxConcept);
            this.WriteNamespaceDecl(this.Namespaces.Prov);

            string schemaLocation = this.Namespaces.SchemaLocation ?? string.Empty;
            string structureSpecific = string.Empty;

            if (!string.IsNullOrWhiteSpace(schemaLocation))
            {
                this.WriteAttributeString(this.Namespaces.Xsi, RDFConstants.SchemaLocation, schemaLocation);
            }
        }

        /// <summary>
        /// Writes the footer message (if supported by the writer implementation), and then completes the XML document, closes
        /// off all the tags, closes any resources.
        /// <b>NOTE</b> It is very important to close off a completed DataWriterEngine,
        /// as this ensures any output is written to the given location, and any resources are closed.  If this call
        /// is not made, the output document may be incomplete.
        /// <p><b> NOTE: </b> If writing a footer is not supported, then the footer will be silently ignored
        /// </p>
        /// Close also validates that the last series key or group key has been completed.
        /// </summary>
        /// <param name="footer">The footer.</param>
        public void Close(params IFooterMessage[] footer)
        {
            // Handle footers if format supports footers
            // then dispose

            //this.CloseMessageTag();

            this.Dispose();

        }

        /// <summary>
        /// Starts a dataset with the data conforming to the DSD
        /// </summary>
        /// <param name="dataflow">Optional. The dataflow can be provided to give extra information about the dataset.</param>
        /// <param name="dsd">The data structure is used to know the dimensionality of the data</param>
        /// <param name="header">Dataset header containing, amongst others, the dataset action, reporting dates,
        /// dimension at observation if null then the dimension at observation is assumed to be TIME_PERIOD and the dataset
        /// action is assumed to be INFORMATION</param>
        /// <param name="annotations">Any additional annotations that are attached to the dataset, can be null
        /// if no annotations exist</param>
        public void StartDataset(
            IDataflowObject dataflow,
            IDataStructureObject dsd,
            IDatasetHeader header,
            params IAnnotation[] annotations)
        {

            // throw new ArgumentNullException("STARTDATASET RDF dsd");
            if (dsd == null)
            {
                throw new ArgumentNullException("dsd","STARTDATASET RDF dsd");
            }

            if (dataflow == null)
            {
                throw new ArgumentNullException("dataflow");
            }

            this._keyFamilyBean = dsd;
            this._dsdName = dsd.AgencyId + "_" + dsd.Id + "_" + dsd.Version;
            this._dimensionAtObservation = this.GetDimensionAtObservation(header);
           
            this.WriteStartElement(this.Namespaces.RDF, RDFElementNameTable.RDF);
            this.RDFWriteProvenance(dataflow.Name.ToString(), RDFConstants.RdfDataset);

            //this.RDFWriterStrucInfo(dataflow.Name.ToString(), dataflow.DataStructureRef.MaintainableId.ToString());
            this.RDFWriterStrucInfo(dataflow.Id.ToString(), dsd.Id);


            this._startedDataSet = true;


            // all other parameters can be null
            //throw new NotImplementedException();
        }

        protected virtual string GetDimensionAtObservation(IDatasetHeader header)
        {
            string dimensionAtObservation = "TIME_PERIOD";
            if (header != null && (header.DataStructureReference != null
                    && !string.IsNullOrWhiteSpace(header.DataStructureReference.DimensionAtObservation)))
            {
                dimensionAtObservation = header.DataStructureReference.DimensionAtObservation;
            }

            return dimensionAtObservation;
        }


        protected void RDFWriteProvenance(string element, string type)
        {
            if (element == null)
            {
                throw new ArgumentNullException("element");
            }
            if (type == null)
            {
                throw new ArgumentNullException("constant");
            }

            DateTime data = DateTime.Now;
            string provenanceID = data.ToString("yyyyMMddHHmmss");
            string label = "Transformed " + element + " data";

            this.StartSeriesProv(provenanceID, RDFConstants.RdfProvenance);
            this.RDFWriteGeneralTag(Namespaces.RDF, RDFElementNameTable.type, RDFAttributeNameTable.about, RDFConstants.RdfActivity);

            this.RDFWriteDescriptionTag(this.Namespaces.RDFs, RDFElementNameTable.prefLabel, RDFAttributeNameTable.lang, "en", label);
            this.RDFWriteElementWithStringTag(this.Namespaces.Prov, this.Namespaces.RDF, RDFElementNameTable.startedAtTime, RDFAttributeNameTable.datatype, data.ToString("yyyy-MM-ddTHH:mm:ssZ"), RDFConstants.RdfDateTime);
            this.StartProvSeries();
            this.StartSubSeries(type + element);
            this.RDFWriteGeneralTag(Namespaces.RDF, RDFElementNameTable.type, RDFAttributeNameTable.resource, RDFConstants.RdfEntity);
            this.RDFWriteElementWithStringTag(this.Namespaces.Prov, this.Namespaces.RDF, RDFElementNameTable.generatedAtTime, RDFAttributeNameTable.datatype, data.ToString("yyyy-MM-ddTHH:mm:ssZ"), RDFConstants.RdfDateTime);

            //Manca -> <prov:wasDerivedFrom rdf:resource="http://example.org/data/SEP_AMBIENTE_ACQ_ACQDOM.xml"/>

            this.RDFWriteGeneralTag(this.Namespaces.Prov, this.Namespaces.RDF, RDFElementNameTable.wasGeneratedBy, RDFAttributeNameTable.resource, RDFConstants.RdfProvenance + provenanceID);
            this.RDFWriteGeneralTag(this.Namespaces.DocTerms, RDFElementNameTable.issued, RDFAttributeNameTable.datatype, RDFConstants.RdfLicense);
            this.RDFWriteElementWithStringTag(this.Namespaces.DocTerms, this.Namespaces.RDF, RDFElementNameTable.issued, RDFAttributeNameTable.datatype, data.ToString("yyyy-MM-ddTHH:mm:ssZ"), RDFConstants.RdfDateTime);
            this.RDFWriteGeneralTag(this.Namespaces.DocTerms, RDFElementNameTable.creator, RDFAttributeNameTable.resource, RDFConstants.ISTAT);
            this.EndSubSeries();
            this.EndProvSeries();
            this.EndSeriesProv();

            provenanceID = null;
            label = null;
        }

        public void StartSeriesProv(string value, string constant)
        {
            EndSeriesProv();
            this.WriteStartElement(this.Namespaces.RDF, RDFElementNameTable.Description);
            this.WriteAttributeString(this.Namespaces.RDF, RDFAttributeNameTable.about, constant + value);
            this._startedSeriesProv = true;
        }


        private void EndSeriesProv()
        {
            if (this._startedSeriesProv)
            {
                // in which case close it
                this.WriteEndElement();
                this._startedSeriesProv = false;
            }
        }

        public void RDFWriteGeneralTag(NamespacePrefixPair NS, RDFElementNameTable Element, RDFAttributeNameTable Attribute, string Constant)
        {
            this.WriteStartElement(NS, Element);
            this.WriteAttributeString(NS, Attribute, Constant);
            this.WriteEndElement();
        }

        public void RDFWriteGeneralTag(NamespacePrefixPair NS, NamespacePrefixPair NSAtt, RDFElementNameTable Element, RDFAttributeNameTable Attribute, string Constant)
        {
            this.WriteStartElement(NS, Element);
            this.WriteAttributeString(NSAtt, Attribute, Constant);
            this.WriteEndElement();
        }

        public void RDFWriteDescriptionTag(NamespacePrefixPair NS, RDFElementNameTable Element, RDFAttributeNameTable Attribute, string Locale, string Value)
        {
            this.WriteStartElement(NS, Element);
            this.WriteAttributeString(this.Namespaces.XML, Attribute, Locale);
            this.WriteString(Value);
            this.WriteEndElement();
        }

        public void RDFWriteElementWithStringTag(NamespacePrefixPair NS, NamespacePrefixPair NSAtt, RDFElementNameTable Element, RDFAttributeNameTable Attribute, string Text, string Constant)
        {
            this.WriteStartElement(NS, Element);
            this.WriteAttributeString(NSAtt, Attribute, Constant);
            this.WriteString(Text);
            this.WriteEndElement();
        }

        public void StartProvSeries()
        {
            EndProvSeries();
            this.WriteStartElement(this.Namespaces.Prov, RDFElementNameTable.generated);
            this._startedProvSeries = true;
        }

        private void EndProvSeries()
        {
            if (this._startedProvSeries)
            {
                // in which case close it
                this.WriteEndElement();
                this._startedProvSeries = false;
            }
        }

        public void StartSubSeries(string value)
        {
            EndSubSeries();
            this.WriteStartElement(this.Namespaces.RDF, RDFElementNameTable.Description);
            this.WriteAttributeString(this.Namespaces.RDF, RDFAttributeNameTable.about, value);
            this._startedSubSeries = true;
        }

        private void EndSubSeries()
        {
            if (this._startedSubSeries)
            {
                // in which case close it
                this.WriteEndElement();
                this._startedSubSeries = false;
            }
        }
        //************************

        protected void WriteNamespaceDecl(NamespacePrefixPair ns)
        {
            this.WriteAttributeString(RDFConstants.Xmlns, ns.Prefix, ns.NS);
        }

        protected void WriteAttributeString(NamespacePrefixPair namespacePrefixPair, string name, string value)
        {
            this._writer.WriteAttributeString(namespacePrefixPair.Prefix, name, namespacePrefixPair.NS, value);
        }

        protected void WriteAttributeString(NamespacePrefixPair namespacePrefixPair, RDFAttributeNameTable name, string value)
        {
            this.WriteAttributeString(namespacePrefixPair, RDFNameTableCache.GetAttributeName(name), value);
        }

        protected void WriteAttributeString(string prefix, string name, Uri value)
        {
            this._writer.WriteAttributeString(prefix, name, null, value.ToString());
        }

        protected void WriteAttributeString(string prefix, string name, string value)
        {
            this._writer.WriteAttributeString(prefix, name, null, value);
        }

        //QUI
        protected void WriteAttributeString(string id, string value)
        {
            this._writer.WriteAttributeString(id, value);
        }

        protected void WriteStartElement(NamespacePrefixPair ns, RDFElementNameTable element)
        {
            this.WriteStartElement(ns, RDFNameTableCache.GetElementName(element));
        }

        protected void WriteStartElement(NamespacePrefixPair ns, string element)
        {
            this._writer.WriteStartElement(ns.Prefix, element, ns.NS);
        }

        protected void WriteStartElement(string element, string ns)
        {
            this._writer.WriteStartElement(element, ns);
        }

        protected void WriteElement(NamespacePrefixPair namespacePrefix, string element, bool value)
        {
            this._writer.WriteElementString(
                namespacePrefix.Prefix, element, namespacePrefix.NS, XmlConvert.ToString(value));
        }

        protected void WriteString(string ObsValue)
        {
            this._writer.WriteString(ObsValue);
        }

        protected virtual void EndDataSet()
        {
            if (this._startedDataSet)
            {
                // this.WriteEndElement();
                this._startedDataSet = false;
                SerieChiavi.Clear() ;

                this._dfChiave = "";
                this.WriteEndElement();
            }
        }

        protected void WriteEndDocument()
        {
            if (this._wrapped)
            {
                return;
            }

            this._writer.WriteEndElement();
            this._writer.Flush();
        }

        protected void WriteEndElement()
        {
            this._writer.WriteEndElement();
        }
        /// <summary>
        /// Starts a group with the given id, the subsequent calls to <c>writeGroupKeyValue</c> will write the id/values to
        /// this group.  After
        /// the group key is complete <c>writeAttributeValue</c> may be called to add attributes to this group.
        /// <p /><b>Example Usage</b>
        /// A group 'demo' is made up of 3 concepts (Country/Sex/Status), and has an attribute (Collection).
        /// <code>
        /// DataWriterEngine dre = //Create instance
        /// dre.startGroup("demo");
        /// dre.writeGroupKeyValue("Country", "FR");
        /// dre.writeGroupKeyValue("Sex", "M");
        /// dre.writeGroupKeyValue("Status", "U");
        /// dre.writeAttributeValue("Collection", "A");
        /// </code>
        /// Any subsequent calls, for example to start a series, or to start a new group, will close off this exiting group.
        /// </summary>
        /// <param name="groupId">Group Id</param>
        /// <param name="annotations">Annotations any additional annotations that are attached to the group, can be null if no
        /// annotations exist</param>
        public void StartGroup(string groupId, params IAnnotation[] annotations)
        {
            if (groupId == null)
            {
                throw new ArgumentNullException("RDF groupId");
            }

            throw new NotImplementedException();
        }

        /// <summary>
        /// Starts a new series, closes off any existing series keys or attribute/observations.
        /// </summary>
        /// <param name="annotations">Any additional annotations that are attached to the series, can be null if no annotations
        /// exist</param>
        public void StartSeries(params IAnnotation[] annotations)
        {
            // string[] val;
            //val = (values.Split('/'));
            // val = annotations.ToString();
            /*
            if (this._startedSeries)
            {
                // in which case close it
                // this.WriteEndElement();
                this._startedSeries = false;

            }
            */
        //    if (this._primaSerie == true) {
                
               
               // this.WriteEndElement();
                return;

          //  }
           // this._primaSerie = true;

            this.WriteStartElement(this.Namespaces.RDF, RDFElementNameTable.Description);
            
            this.WriteAttributeString(this.Namespaces.RDF, RDFAttributeNameTable.resource, RDFConstants.RdfDataset + "/" + this._dsdName + this._dfChiave);
            

            this.WriteStartElement(this.Namespaces.RDF, RDFElementNameTable.type);
            this.WriteAttributeString(this.Namespaces.RDF, RDFAttributeNameTable.resource, RDFConstants.Rdftype);
            this.WriteEndElement();
            this.WriteStartElement(this.Namespaces.QB, RDFElementNameTable.dataset);
            
            this.WriteAttributeString(this.Namespaces.RDF, RDFAttributeNameTable.resource, RDFConstants.RdfDataset + "/" + this._dsdName);
            this.WriteEndElement();

            this._startedSeries = true;
        }


        private void EndSeries()
        {
            //var c = this._startedSeries;
            //var ca = this._startedDataSet;
            if (this._startedSeries)
            {
                // in which case close it
               // this.WriteEndElement();
                this._startedSeries = false;
                
            }
        }
        /// <summary>
        /// Writes an attribute value for the given id.
        /// <p />
        /// If this method is called immediately after a <c>writeSeriesKeyValue</c> method call then it will write
        /// the attribute at the series level.  If it is called after a <c>writeGroupKeyValue</c> it will write the attribute
        /// against the group.
        /// <p />
        /// If this method is called immediately after a <c>writeObservation</c> method call then it will write the attribute
        /// at the observation level.
        /// </summary>
        /// <param name="id">the id of the given value for example 'OBS_STATUS'</param>
        /// <param name="valueRen">The value.</param>
        public void WriteAttributeValue(string id, string valueRen)
        {
            if (id == null)
            {
                throw new ArgumentNullException("RDF id");
            }

            if (id == "TIME_FORMAT" && this._primaSerie == false)
            {
                this.WriteEndElement();
            }
            //YY COMMENTARE IF
            if (id != "TIME_FORMAT") { 
                this.WriteStartElement(this.Namespaces.Property, id);
                this._writer.WriteAttributeString(id, valueRen);
                this.WriteEndElement();
            }
             
            if (id == "OBS_STATUS")
            {
                this._trovatoStatus = true;
              //XX  this.WriteEndElement();
            }
            //this._writer.WriteAttributeString(id, valueRen);

            //throw new NotImplementedException();  QUI
        }

        /// <summary>
        /// Writes a group key value, for example 'Country' is 'France'.  A group may contain multiple id/value pairs in the
        /// key, so this method may be called consecutively with an id / value for each key item.
        /// <p />
        /// If this method is called consecutively multiple times and a duplicate id is passed in, then an exception will be
        /// thrown, as a group can only declare one value for a given id.
        /// <p />
        /// The <c>startGroup</c> method must be called before calling this method to add the first id/value, as the
        /// WriterEngine needs to know what group to assign the id/values to.
        /// </summary>
        /// <param name="id">the id of the concept or dimension</param>
        /// <param name="valueRen">The value.</param>
        public void WriteGroupKeyValue(string id, string valueRen)
        {
            if (id == null)
            {
                throw new ArgumentNullException("RDF id");
            }

            if (valueRen == null)
            {
                throw new ArgumentNullException("RDF valueRen");
            }

            throw new NotImplementedException();
        }


        public void RDFWriterStrucInfo(string dataset, string struc)
        {
            this.WriteStartElement(this.Namespaces.RDF, RDFElementNameTable.Description);
            this.WriteAttributeString(this.Namespaces.RDF, RDFAttributeNameTable.about, RDFConstants.Rdfqb + dataset);
            this.WriteStartElement(this.Namespaces.RDF, RDFElementNameTable.type);
            this.WriteAttributeString(this.Namespaces.RDF, RDFAttributeNameTable.resource, RDFConstants.RdfqbDataSet);
            this.WriteEndElement();
            this.WriteStartElement(this.Namespaces.QB, RDFElementNameTable.structure.ToString());
            this.WriteAttributeString(this.Namespaces.RDF, RDFAttributeNameTable.resource, RDFConstants.RdfStructure + struc);
            this.WriteEndElement();
            this.WriteStartElement(this.Namespaces.DocTerms, RDFElementNameTable.identifier);
            this.RDFWriteString(dataset);
            this.WriteEndElement();
            this.WriteEndElement();
        }

        public void RDFWriteObservation(string obsConceptValue, string obsValue)
        {
            //string obsConcept = true ? this._dimensionAtObservation : ConceptRefUtil.GetConceptId(this.KeyFamily.TimeDimension.ConceptRef);
            string obsConcept = "TIME_PERIOD";
            this.RDFWriteObservation(obsConcept, obsConceptValue, obsValue);
        }

        public void RDFWriteObservation(string observationConceptId, string obsConceptValue, string primaryMeasureValue)
        {
            if (observationConceptId == null)
            {
                throw new ArgumentNullException("observationConceptId");
            }

            string obsValue = string.IsNullOrEmpty(primaryMeasureValue) ? this.DefaultObs : primaryMeasureValue;

            if (this._primaSerie == true) this.WriteEndElement(); //YY aggiunto

            this._primaSerie = true;




            this.WriteStartElement(this.Namespaces.RDF, RDFElementNameTable.Description);

            this.WriteAttributeString(this.Namespaces.RDF, RDFAttributeNameTable.resource, RDFConstants.RdfDataset + "/" + this._dsdName + "/" + this._dfChiave + obsConceptValue);


            this.WriteStartElement(this.Namespaces.RDF, RDFElementNameTable.type);
            this.WriteAttributeString(this.Namespaces.RDF, RDFAttributeNameTable.resource, RDFConstants.Rdftype);
            this.WriteEndElement();
            this.WriteStartElement(this.Namespaces.QB, RDFElementNameTable.dataset);

            this.WriteAttributeString(this.Namespaces.RDF, RDFAttributeNameTable.resource, RDFConstants.RdfDataset + "/" + this._dsdName);
            this.WriteEndElement();

            this._startedSeries = true;
            // }



            //// A QUI




            IAnnotation[] annotations = null;
            this._scritteOsservazioni = true;
            if (this._startedObs == true)
            {
                //this._startedObs = false;
                //this.WriteEndElement();
                this.StartSeries(annotations);
            }
            this._startedObs = true;
            foreach (String[,] row in SerieChiavi)
            {
                this.WriteConceptValue(row[0, 0], row[0, 1]);
            }

           
            this.WriteStartElement(this.Namespaces.Property, "TIME_PERIOD");
            this.WriteAttributeString(this.Namespaces.RDF, RDFAttributeNameTable.resource, RDFConstants.RdfTimePeriod + obsConceptValue);
            this.WriteEndElement();

            if (!string.IsNullOrEmpty(obsValue))
            {
                this.WriteStartElement(this.Namespaces.Property, RDFElementNameTable.OBS_VALUE);
                this.WriteAttributeString(this.Namespaces.RDF, RDFAttributeNameTable.datatype, RDFConstants.RdfObsValue);
                this.RDFWriteString(obsValue);
                this.WriteEndElement();
                this.WriteStartElement(this.Namespaces.Property, RDFElementNameTable.OBS_STATUS);
                this.WriteAttributeString(this.Namespaces.RDF, RDFAttributeNameTable.resource, RDFConstants.RdfObsStatus);
                this.WriteEndElement();
            }
            
            //this.WriteEndElement();  //YY commentato
        }

        public void RDFWriteString(string obsValue)
        {
            this.WriteString(obsValue);
        }

        public void WriteSeriesKeyValue(string key, string value, string version, string id)
        {
            //this.WriteConceptValue(key, value, version, id);  QUI
            this.WriteConceptValue(value, id);
        }

        //private void WriteConceptValue(string concept, string value, string version, string id) QUI
        private void WriteConceptValue(string value, string id)
        {
            //this.WriteStartElement(this.Namespaces.Property, concept); QUI
            //this.WriteAttributeString(this.Namespaces.RDF, RDFAttributeNameTable.resource, RDFConstants.RdfCode + version + "/" + id + "/" + value); QUI
            this.WriteStartElement(this.Namespaces.Property, id);
            this.WriteAttributeString(this.Namespaces.RDF, RDFAttributeNameTable.resource, RDFConstants.RdfCode + "/" + id + "/" + value);
            this.WriteEndElement();
        }
        /// <summary>
        /// Writes an observation, the observation concept is assumed to be that which has been defined to be at the
        /// observation level (as declared in the start dataset method DatasetHeaderObject).
        /// </summary>
        /// <param name="obsConceptValue">May be the observation time, or the cross section value</param>
        /// <param name="obsValue">The observation value - can be numerical</param>
        /// <param name="annotations">Any additional annotations that are attached to the observation, can be null if no
        /// annotations exist</param>
        public void WriteObservation(string obsConceptValue, string obsValue, params IAnnotation[] annotations)
        {

            this.RDFWriteObservation(obsConceptValue, obsConceptValue, obsValue);
            /*if (!string.IsNullOrEmpty(obsValue))
           {
               this.WriteStartElement(this.Namespaces.Property, RDFElementNameTable.OBS_VALUE);
               this.WriteAttributeString(this.Namespaces.RDF, RDFAttributeNameTable.datatype, RDFConstants.RdfObsValue);
               this.RDFWriteString(obsValue);
               this.WriteEndElement();
               this.WriteStartElement(this.Namespaces.Property, RDFElementNameTable.OBS_STATUS);
               this.WriteAttributeString(this.Namespaces.RDF, RDFAttributeNameTable.resource, RDFConstants.RdfObsStatus);
               this.WriteEndElement();
           }
             * */
        }

        /// <summary>
        /// Writes an Observation value against the current series.
        /// <p />
        /// The current series is determined by the latest writeKeyValue call,
        /// If this is a cross sectional dataset, then the observation Concept is expected to be the cross sectional concept
        /// value - for
        /// example if this is cross sectional on Country the id may be "FR"
        /// If this is a time series dataset then the observation Concept is expected to be the observation time - for example
        /// 2006-12-12
        /// <p />
        /// Validates the following:
        /// <ul><li>The observation Time string format is one of an allowed SDMX time format</li><li>The observation Time does not replicate a previously reported observation Time for the current series</li></ul>
        /// </summary>
        /// <param name="observationConceptId">the concept id for the observation, for example 'COUNTRY'.  If this is a Time
        /// Series, then the id will be DimensionBean.TIME_DIMENSION_FIXED_ID.</param>
        /// <param name="obsConceptValue">may be the observation time, or the cross section value</param>
        /// <param name="obsValue">The observation value - can be numerical</param>
        /// <param name="annotations">Any additional annotations that are attached to the observation, can be null if no
        /// annotations exist</param>
        public void WriteObservation(
            string observationConceptId,
            string obsConceptValue,
            string obsValue,
            params IAnnotation[] annotations)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Writes an Observation value against the current series
        /// <p />
        /// The date is formatted as a string, the format rules are determined by the TIME_FORMAT argument
        /// <p />
        /// Validates the following:
        /// <ul><li>The observation Time does not replicate a previously reported observation Time for the current series</li></ul>
        /// </summary>
        /// <param name="obsTime">the time of the observation</param>
        /// <param name="obsValue">the observation value - can be numerical</param>
        /// <param name="sdmxSwTimeFormat">the time format to format the observation Time in when converting to a string</param>
        /// <param name="annotations">The annotations.</param>
        public void WriteObservation(DateTime obsTime, string obsValue, TimeFormat sdmxSwTimeFormat, params IAnnotation[] annotations)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Writes a series key value.  This will have the effect of closing off any observation, or attribute values if they
        /// are any present
        /// <p />
        /// If this method is called after calling writeAttribute or writeObservation, then the engine will start a new series.
        /// </summary>
        /// <param name="id">the id of the value for example 'Country'</param>
        /// <param name="valueRen">The value.</param>
        public void WriteSeriesKeyValue(string id, string valueRen)
        {
            if (id == null)
            {
                throw new ArgumentNullException("RDF id");
            }
            if (this._scritteOsservazioni == true) {
                this._dfChiave = "";
                SerieChiavi.Clear();
                this._scritteOsservazioni = false;
            }

            SerieChiavi.Add(new string[,] { { valueRen, id  } });

            this._dfChiave += valueRen + "/";

           //OK  this.WriteConceptValue(valueRen, id);
            
            //this.WriteConceptValue(key, valueRen, version, id);
            // throw new NotImplementedException();
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="managed"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        protected virtual void Dispose(bool managed)
        {
            if (managed)
            {

                if (this._disposed == false)
                {
                    this.EndSeries();
                    this.EndDataSet();
                    this.WriteEndDocument();
                    if (_closeWriter)
                    {
                        _writer.Close();
                    }
                }

                this._disposed = true;
                //  this._writer.Close();
                //  this._streamWriter.Dispose();
                // TODO dispose anything that needs to. 
            }
        }

        public void WriteComplexAttributeValue(IKeyValue keyValue)
        {
            throw new NotImplementedException();
        }

        public void WriteComplexMeasureValue(IKeyValue keyValue)
        {
            throw new NotImplementedException();
        }

        public void WriteMeasureValue(string id, string value)
        {
            throw new NotImplementedException();
        }

        public void WriteObservation(string obsConceptValue, params IAnnotation[] annotations)
        {
            throw new NotImplementedException();
        }
    }
}